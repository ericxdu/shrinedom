LibreGaming
===========

Who We Are
----------

LibreGaming is an organization of software freedom and gaming 
enthusiasts who meet on the web to discuss and play computer games that 
are **free/libre** and **open source software**. We focus on multiplayer games 
that can be played over the Internet like 
[*Xonotic*](https://xonotic.org/), [*The Battle for 
Wesnoth*](http://www.wesnoth.org/), 
[*Hedgewars*](https://www.hedgewars.org/), 
[*OpenTTD*](https://www.openttd.org/), and more.

What To Do
----------

To join us for conversation and matchmaking, see our [contact 
information](https://libregaming.org/chat-with-us/).

If you are new to libre gaming or are looking for game to play, visit 
[LibreGameWiki](https://libregamewiki.org/). If you find something you 
like, tell us about it!

We have written a small primer on [why you should be a libre 
gamer](libre-games-primer.html).

We also have a detailed page [introducing our organization](intro.html).
