+++
title = "Play libre games!"
+++
<!-- SPDX-License-Identifier: CC0-1.0 -->

# Play libre games!

# What are they?

Libre games are fun games owned by the community, including everyone who plays the game, develops it, and shares it.

## What does it mean for a game to be owned by the community?

It means everyone should be able to do anything they want with it, like:

- Create and share a modified project (new artwork, remixes etc. from simple modifications to full conversions).
- Freely use the game's content on any platform (like a gameplay video/stream).
- Participate in the game's development by improving it and/or giving feedback.
- Learn how the game is built by looking under the hood.
- Create or improve translations.
- Host their own server to play with their friends or in a LAN.
- Share copies of the game.
- Help port the game to new platforms and ensure it lives forever.

# How is this achieved?

By sharing everything needed to make the game with its players. This includes code, music, art, 3d models, documentation - with full permission to share and remix (and even make a new game from it).

Authors can still get credit for the achievement, get paid by users installing the game via stores like itch.io, Steam or Google Play and require that any changes are made available under the same terms.

# Examples

The games below are libre games meant to exemplify their advantages. For a large list of libre games to play, check out [libregamewiki](https://libregamewiki.org/List_of_games).

## [Battle for Wesnoth](https://wesnoth.org/)

Battle for Wesnoth is a turn based strategy libre game.

![Wesnoth gameplay image](https://wesnoth.org/images/sshots/wesnoth-1.14.0-2.jpg)

### Mods:

Battle for Wesnoth has a wealth of [mods](https://www.wesnoth.org/addons/1.14/). Some of them drastically change the gameplay like:

- Elvish dynasty (Story based resource management game)
- Conquest (Risk like game)
- Bob's RPG (RPG game)
- Legend of the Invincibles
- Cities of the Frontier
- Den of Thieves (stealth based game)

Given that the art assets are widely available, shareable and modifiable they can be reused in completely different projects such as:

- [Arcmage](https://arcmage.org/#) (A card game)
- [Hale](https://sourceforge.net/projects/hale/) (A turn based RPG)

### Community translations

Battle for Wesnoth is translated to multiple [languages](https://wiki.wesnoth.org/WesnothTranslations) including minority languages like Basque that wouldn't get translated in non-libre games.

### Community development

The development of Battle for Wesnoth has been highly influenced by the community, especially in the early stages. There were many artists who contributed to the game, people that contributed new campaigns to it, etc. Much of that development happened on the main forums where people would propose and discuss the additions.

The game has been refined over the years thanks to the countless [contributors](https://wiki.wesnoth.org/Credits) and people who reported issues and gave feedback on the game.

### Platform support

Battle for Wesnoth is available on every main platform (Linux, MacOS, Windows, Android, iOS).
Not only that but it runs on homebrew consoles like the Pyra and Pandora.

### Forever alive

The first development release of Wesnoth 0.1 happened on June 18, 2003. It has been fully playable for a number of years, and runs today on all the platforms above. Like many libre games, it is constantly being updated, with more features, fixes and content being produced.

## [Mindustry](https://mindustrygame.github.io/)

Mindustry is an original game bringing together elements of tower defense and factory building. It can be bought on Steam, or downloaded from GitHub and Itch.io.

![Mndustry gameplay image](https://libregamewiki.org/images/4/49/Mindustry.png)

### Community development

Although the game is libre and the community makes many contributions, its author has a clear and strong vision and keeps control of how the game is developed. At the same time, you can edit the copy of the game code on your computer to make it work however you want, and share those modifications freely.

### Modding

Mindustry supports modding, and many player-made maps are available that were created with the built-in map editor.

## [0ad](https://play0ad.com/)

0 A.D. is a real time strategy game about ancient warfare, where players take charge of different civilizations and lead them into battle.

![0 A.D. gameplay image](https://play0ad.com/wp-content/gallery/screenshots/Kushcitycenter.jpg)

### Community development

The list of contributors to 0 A.D. includes dozens of names, spanning everything from art through audio through community management and balancing.

### Forever alive

0 A.D. was released as open source in 2009, though it started life in 2001 as a concept for a mod of Age of Empires II. It is still receiving updates to this day.

## [Endless Sky](https://endless-sky.github.io/)

Endless Sky is a 2D space exploration, combat and trading sim. It features many types of ships and upgrades, a large and detailed galaxy, and an optional main storyline along with various randomized events.

![Endless Sky gameplay image](https://endless-sky.github.io/images/screenshots/battle.jpg)

### Community development

When the main developer of the game took a break in mid-2018, members of the community forked the game and continued work on it. Following this, the main developer gave access to those members to make modifications to the main code repository, meaning the official version of the game was again active.

### Forever alive

As of May 2020, there have been 4 releases since the game "changed hands", and the main developer hasn't yet returned to a lead role. He says "I'm glad to be able to take a break, work on creative writing and other random projects, and not feel duty-bound to spend all my weekends keeping up with issues..."

This shows the power of a community owning its own game and being able to work on it independently, and its benefit to the creator as well.

## [Freedoom](https://freedoom.github.io/) and other Doom-related games

Since the source code for Doom's engine was released under the GPL in 1999, so many mods and conversions have been made for it that it's hard to count them all. Freedoom is a complete game based on this engine.

![Freedoom gameplay image](https://freedoom.github.io/img/screenshots/phase2-0.12_04.webp)

### Modding

Freedoom is compatible with mods for the original Doom.

In general, the spread of Doom source ports since the engine code was released meant that developers were soon fixing old bugs and adding new features. Those include CTF mode, jumping, an inventory system, friendly monsters and more.

### Platform support

One of the source ports of Doom, Odamex, states: "ODAMEX is designed with portability in mind. From PCs to netbooks, consoles to handhelds, Amiga to Windows - our goal is for any device to run multiplayer Doom." It has been confirmed to run on Intel x86, PowerPC, and SPARC systems.

Other ports of the Doom source code listed in doomwiki.org cover platforms from DOS through BeOS through Atari systems to Nokia cell phones.

### Forever alive

Source ports like Odamex and GZDoom are actively used to play Doom today, and are still receiving updates. Two decades is not bad for a game.

# FAQ

##### I already own the games I buy!

Many games you buy are not exactly owned by you. In fact, some newer games and "game streaming" services work under a license model where they never claim that you own the game.

If you own your game, can you:
 - run your own server
 - stream it without worrying about copyright strikes
 - play it without an (stable) internet connection...
 - ... and without installing DRM or anticheat
 - play an older version if an update breaks something you like
 - release your own derivative fanworks, such as a remix album, comic, or even a fangame
 - translate it to your language without having to reverse engineer it...
 - ... and share that translation without fear of legal threats
 - keep playing after the developer stops supporting it
 
If there is an item on that list you can't do legally, can you really say you own the game?

##### Why 'Libre'?

'Libre', originally from Spanish/French, means having liberty/freedom. It means that everyone has the freedom to access, modify and share the game. The game is free from any ownership: everyone (the community) owns it. It is one of several [similar terms](https://en.wikipedia.org/wiki/Alternative_terms_for_free_software) used to avoid any confusion with 'free of charge'.

##### How can the game have a common vision if everyone can make any changes they wish?
Each game project has a main trusted community that makes the decisions. Anyone is able to change the game locally, and to create a derivative project (usually called a fork), but this doesn't mean those changes will get accepted into the main project.

##### I'm a game developer and I'm concerned that my game will be stolen!
You can't steal a game owned by everyone!

It's important to realize that releasing your game as libre is a trade-off. You give away some of your control, but you gain something bigger: a game that you started, that will live forever, through everyone who will ever play or develop it. A piece of it will also live in every derivative.

Also, when you release your code or assets as libre, you keep on being the copyright owner, and no one can take that away - your name will always be on your art. This is true even when someone forks your game.

##### I downloaded a game for free, is it libre?
There are many free of charge (gratis) games, but many of them are not libre. (In fact, you can buy some libre games.) To know if it's libre, ask yourself: does it have all the benefits described above? If it doesn't, it's not libre.

These are some commonly used, overlapping definitions you can refer to if you want to know more detail about determining if a game's license is libre: [Debian Free Software Guidelines](https://www.debian.org/social_contract#guidelines), [Free Software Definition](https://www.gnu.org/philosophy/free-sw.html), [Open Source Definition](https://opensource.org/osd-annotated), [Free Cultural Works](https://freedomdefined.org/Definition) (see also [Creative Commons](https://creativecommons.org/share-your-work/public-domain/freeworks)), [SPDX short identifiers](https://spdx.org/licenses/)

##### If anyone can change a game, people will cheat!
Cheating is a problem in all multiplayer games, and the best antidote isn't preventing the community from owning the game - it's to let the community police its own game. Hackers will always find ways around clever protections, and then distribute their hacks. So the key is to play with those you trust, and compete with those you respect.
