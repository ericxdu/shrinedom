+++
title = "Libre Gaming Community"
+++
<!-- SPDX-License-Identifier: CC0-1.0 -->

# LibreGaming community - Introduction

We are a community dedicated to playing and developing [libre games], that is videogames and board games with free/libre software and algorithms, and free/libre artistic assets (models, musics). We believe [software] and [culture] should be free as part of a global struggle for freedom and equality online and offline. We aim to become a meta-community that can bring closer smaller communities. We don't want to become a big central organization, but rather a network of organic initiatives scattered throughout cyberspace.

For example, we'd like to bring players alongside game developers and designers, as well as contributors to [libre games launchers] like [Athenaeum] and [GameHub]. We believe this approach can enrich and empower the entire libre gaming ecosystem.

We are just getting started. Feel free to contribute new ideas and feedback by [contacting us]. Check out [the preliminary notes] to our first meeting, as well as the [first], [second] and [third] meeting's minutes.

[libre games]: @/play-libre-games.md
[software]: https://www.gnu.org/philosophy/shouldbefree.html
[culture]: https://artlibre.org/faq_eng/
[libre games launchers]: @/game-launcher-concept.md
[Athenaeum]: https://gitlab.com/librebob/athenaeum
[GameHub]: https://github.com/tkashkin/GameHub
[contacting us]: @/chat-with-us.md
[the preliminary notes]: https://hribhrib.at:5443/upload/5035aee454f8553c88bd1f7623218485341099ed/dZSQFC3lP1lMmTQFyfLKQFBBDIC4tOrQ41Js4nzn/firstthoughts.txt
[first]: @/2021-08-07.md
[second]: @/2022-01-30.md
[third]: @/2022-04-24.md

## We are

- a loose collective rather than a formal organization
  - feel free to invite new people and share permissions with people you know
  - don’t ask permission to improve things, just go ahead and see where it goes, then let it be known so that others can review what you did
- supportive of [accessibility]: everyone should be able to play games, no matter their physical or hardware/network capabilities
- encouraging existing libre games communities to join us and help improve the overall ecosystem

## We are not

- morally-superior purists: we want to develop a libre ecosystem, but will not judge or insult people who take part in other ecosystems
- a community where nazis and harassers are  welcome
- a space to discuss non-free games and ecosystem, see Gaming Space or Linux Gaming (**TODO:** links) instead

[accessibility]: https://en.wikipedia.org/wiki/Accessibility

# Communication

- **Domain name:** following a [public poll], we went with `libregaming.org`, see also [previous name poll]
- **Privacy:** While we defend privacy online, our chatrooms are public spaces and may be logged accordingly
- **Moderation:** We defend free speech, which means we believe no government can tell you how to think/feel, but [doesn't mean you can engage in abusive behavior]; nazis, harassers, and other abusers are not welcome in our community
- **Color scheme:** We use <span style="color: #FF8F23;">Orange</span> (#FF8F23) and <span style="color: #4FBAD5">Blue</span> (#4FBAD5)

If you would like to take part in the community, please see [contacting us].

[public poll]: https://webapp.oulu.fi/framadate/adminstuds.php?poll=Vmv6hF1oJ9ain1SIusvq7qk8
[previous name poll]: https://poll.disroot.org/LZwvudXCHyBvDY2d
[doesn't mean you can engage in abusive behavior]: https://xkcd.com/1357/

# Services

For now, we do not provide any services as LibreGaming collectives. Below you will find a list of services we would be interested to maintain.

## Internal use

- A URL shortener
- an audio/video conferencing server? (video requires a lot of resources)

## Public use

- A website with information about libre games (maybe cooperate with [LibreGameWiki]), and links to various related communities, as well as tutorials to selfhost your own libre game servers
- A matchmaking service/bot to find people to play with
- A chat bridging service (eg. [matterbridge]) for existing communities, to open oneself to new protocols/ecosystems
- Tooling/scripts to make it easier to selfhost libre game servers
- Subdomains on libregaming.org for new projects to get started; only for libregaming projects, because we don't want external projects to technically depend on libregaming.org?

[LibreGameWiki]: https://libregamewiki.org/Main_Page
[matterbridge]: https://github.com/42wim/matterbridge

# Resources

- [Our logos] (SVG)
- [Our first attempt at a website] (ZIP), based on [play-libre-games.md]
- [LibreGaming space screenshot on matrix] (PNG)

[Our logos]: https://freedombox.emorrp1.name/_matrix/media/r0/download/freedombox.emorrp1.name/IwuxGwOfjbdjcaSLFgjqXGqa
[Our first attempt at a website]: https://matrix-client.matrix.org/_matrix/media/r0/download/matrix.org/hmdmKGDlmwrCKuWnJLxGPRgJ
[play-libre-games.md]: @/play-libre-games.md
[LibreGaming space screenshot on matrix]: https://freedombox.emorrp1.name/_matrix/media/r0/download/matrix.org/TqNyUvWqIgJqxIfCzaYbQGyq

# External resources

- Organizing a meeting
  - [planning a timezone-aware event] (alternative to framadate, **TODO:** can we selfhost it?)
  - [sharing a specific time across timezones] (**TODO:** can we selfhost it?)
  - [taking notes together] (use "Freely" permissions to avoid everyone having to register)
    - [People involved with LibreGaming.Org](https://md.roflcopter.fr/oFeu6XXoRNqGeEZvYhhjOQ?both)
    - [Overview of topics/“directions” being worked on](https://md.roflcopter.fr/O1KGHXZ3SPC20fMqzBT3XQ?both)
  - audio conferencing: [Mumble] server on hribhrib.at (**TODO:** can we setup [mumble-web] client and [mumble-web-proxy] WebRTC server?)
- Other related collectives
  - [FreeGameDev.net] has a modern IRC server (with a public Jabber/XMPP gateway and a matchmaking bot) and a forum for libre gamedev
  - [LibreGameWiki] a wiki with detailed information about libre games

[planning a timezone-aware event]: https://www.when2meet.com/
[sharing a specific time across timezones]: https://time.is/compare
[taking notes together]: https://md.roflcopter.fr/
[Mumble]: https://mumble.org/
[mumble-web]: https://github.com/Johni0702/mumble-web
[mumble-web-proxy]: https://github.com/johni0702/mumble-web-proxy
[FreeGameDev.net]: https://freegamedev.net

**TODO:** below are leftovers from the previous pad, what are they for? do they work?

- https://remixicon.com/ finding Apache-2.0 svg avatars (94% scale for rooms)
- https://storm.debian.net/ Sandstorm ad-hoc hosting

-----

# Below hasn't been updated yet

**TODO:** maybe move to the meeting minutes this last part?

* Bridging: XMPP, irc (limited rooms?), discord?
* pro-actively bridge to IRC but be clear that it's a degraded user experience and try to limit the sheer number of rooms to match user expectations
* The bifrost matrix bridge to XMPP is not great, perhaps can host a public XMPP gateway to matrix instead?
  * matrix-bifrost is the only xmpp-matrix bridge in both ways
  * it's also not good on xmpp->matrix side, at least when using the official matrix.org gateway
* [Discussion (long) about Discord]
* object to Discord on principle of non-libre, centralised, compromising ideals, so shouldn't pro-actively bridge channels from here to there
* aware that some libre games have their official communities on Discord, and sadly they may not see anything wrong with that, e.g. strong network effects
* popularity isn't everything, it's ok to be a small cohesive community, but advertising libre stuff on non-libre platforms is ok for outreach
* not everyone agrees with the statements above :p

[Discussion (long) about Discord]: https://matrix.to/#/!qLhNfILESSCaasbRWB:freedombox.emorrp1.name/$pES_yrnhm4kjonxfnrPmqV2dv3CmAyWBH2eadKgZ4wM?via=freedombox.emorrp1.name&via=matrix.org&via=nordgedanken.dev
