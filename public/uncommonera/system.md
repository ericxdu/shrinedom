Example System
==============

Uncommon Era is meant as the basis for storytelling and sytem building.
A system is a collection of mechanics for a video game or role-playing
game in the world of Uncommon Era. Here is described a sample system
for a role-playing game using an OpenD6 customized ruleset.

- Game World Name: Uncommon Era
- Game Designer: Optimus Games

Character Information
---------------------

- Species: humanoid or xenoid
- Age Requirements:
- Groups:
- Starting Attribute Dice: 15D
- Starting Skill Dice: 7D
- Starting Money: 3D

### Damage System

Wounds: 5

### Advantages and Disadvantages

Attributes and Skills
---------------------

### Strength

Physical power when moving heavy objects or inflicting damage.

### Endurance

Bodily resistance to injury, poison, disease, and magical sickness.

### Reflexes

Gross motor coordination when dodging an attack, fighting, and balance.

- **ACROBATICS**: performing feats of gymnastics, balance, and dance.
- **MELEE WEAPON**: wielding a hand-to-hand weapon
- **MISSILE WEAPON**: aiming and firing a ranged weapon
- **SNEAK**: moving silently, avoiding detection, and hiding oneself
- **SWIMMING**: moving and surviving in a liquid medium
- **VESSEL PILOTING**: operating a vehicle or vessel

### Knowledge

Memory recall and advanced learning.

- **HEALING**: first aid, medical procedures, and diagnosis
- **NAVIGATION**: recognizing landmarks and using navigational tools
- **SPEAK LANGUAGE**: spoken and written communication in a language
- **TECHNICAL**: using complex equipment and techniques

### Perception

- **HAGGLING**: negotiations, business transactions, and bribery
- **INFORMATION GATHERING**: researching and analyzing information
- **TRACKING**: following the trail of a person or creature
- **PSIONIC POWER**: wielding a metaphysical power (see below)
