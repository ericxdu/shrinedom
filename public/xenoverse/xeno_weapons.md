The projects needs custom weapons for several reasons. First of all
to match the theme of piloting a powered armor suit, weapon ready and
firing sprites must not show the bare arm of a human. Second of all
to match the sci-fi aesthetic, weapons like shotgun and chainsaw must
be replaced by something like lasers and piledrivers.

Unless necessary, the actual behavior of weapons (frames) will not be
modified.

To expedite the prototyping process, the project will make use of
placeholders. The main placeholder will be a modified version of
SHTG from Freedoom with different muzzle flashes and an icon
indicating which weapon is currently ready.

0. Impact Driver [1]

1. Point Gun [2]

2. Spread Gun [3]

3. Machine Gun [4]

4. Bazooka [5]

5. Ion Cannon [6]

6. Photon Cannon [7]

7. Angle Grinder [1]

8. Scatter Gun [3]

The list of named assets that must be replaced is below.
