Xenoverse Enemy Roster
======================

This is an outline for the enemy roster of Xeno Quest. It is meant to include all 20 of Boom's map-placeable "monster" Things plus the Player.


Spacenoids
----------

![MT_POSSESSED](files/monster-zombie.png "Drone Grunt")

![MT_SHOTGUY](files/monster-shotgun-zombie.png "Drone Commando")

![MT_CHAINGUY](files/monster-chaingun-zombie.png "Drone Gunner")

![MT_WOLFSS](files/sswva1.png "Drone Sentry")

Hired by warlords or working for themselves to plunder items of value, these are soldiers for hire divided into strategic ranks of Grunt, Commando, Gunner, and Sentry. Being on no particular side, they'll often oppose each other on the battlefield and mistrust each other as often as they work together. Spacers have a physiology that evolved to protect vital organs from vacuum and other harsh environments.


Espemorphs
----------

![MT_TROOP](files/monster-serpentipede.png "Serpentipede")

![MT_SHADOWS](files/monster-flesh-worm.png "Vore Worm")

![MT_HEAD](files/monster-trilobite.png "Trilobite")

![MT_SKULL](files/monster-hatchling.png "Hatchling")

![MT_PAIN](files/monster-matribite.png "Matribite")

Genetically modified creatures designed to serve in an army or wait in ambush.


Thanaperium
-----------

![MT_PAIN](files/monster-summoner.png "Summoner")

![MT_SKULL](files/monster-deadflare.png "Deadflare")

![MT_VILE](files/monster-necromancer.png "Necromancer")

![MT_UNDEAD](files/monster-dark-soldier.png "Wizard")

![MT_SHADOWS](files/monster-stealth-worm.png "Shadows")

![MT_VILE](files/monster-flamebringer.png "Flamebringer")

![MT_SPAWNSHOT](files/bosfa1.png)

Former members and servants of the now-defunct Nek'raal empire, who seeded the galaxy with their minions centuries ago.

The Summoner manufactures Deadflare swarms. It deploys a single Deadflare at a time, intending to overwhelming its enemies over time. Destroying it will release any Deadflare inside that were being manufactured, but it will not be making any more after that. A large swarm of Deadflare near a source of Thanatos may be able to create a new Summoner, thereby ensuring their continued existence.

Once a member of the ancient Nek'raal empire, the Necromancer is revered by all species on the battlefield. Even war constructs will not knowingly harm these creatures. A Necromancer will often fortify a strategic location and raise an army, supporting their gathered warriors by reviving them if they are felled. However, this proud magician will not hesitate to destroy any creature that attacks it by casting a devastating fire bomb.

Brutallion
----------

![MT_BRUISER](file/monster-pain-lord.png "Pain Lord")

![MT_KNIGHT](file/monster-painbringer.png "Painbringer")

Incredibly resilient warriors of the Bru'al species, the Painbringer is biotechnically enhanced with armor, weapons, and dense musculature. They often fight in pairs, and can even work together in large groups or under the command of the stronger Pain Lords. They can fight in close quarters using a scratch attack, but they favor their long-range comet launchers. Pain Bringers and Pain Lords will not knowingly attack one another, and they are immune to each other's projectiles.

Cyberborg
---------

Simple programming causes the Walker and the Tripod to attack anything identified as a threat, and advanced armor makes them immune to indirect damage from explosives. The Walker is armed with a vulcan cannon, while the Tripod attacks with a rocket launcher.

A smaller version of the large Walker is the Miniwalker, equipped with an ion cannon and deployed in greater numbers.

The Flamethrower is large construct equipped with dual fireball launchers.

Esperpods are creatures kept in suspended animation and are used to form a security network using ESP. Destroying all of them will unlock the way forward.

A Battle Brain is a super-intelligent war coordinator. Destroying it is a key objective in any battle where it is present.

Unafilliated
------------

These units have no particular faction affiliation and can be found alone or among the ranks of an army.

![MT_PLAYER]

![MT_KEEN]

![MT_SPIDER](files/monster-large-walker.png "Walker")

![MT_CYBORG](files/monster-assault-tripod.png "Assault Tripod")

![MT_BABY](files/monster-technospider.png "Miniwalker")

![MT_FATSO](files/monster-combat-slug.png "Flamethrower")

![MT_BOSSBRAIN](files/battle-brain.png "Battle Brain")

A Space Ranger travels the galaxy in search of contract work. Rather than serving as a military contractor like Spacer Mercenaries, the Space Ranger usually works alone, often taking on hordes of enemies with skill and grit.

Not much is known about the Dark Ranger, who often waits in ambush either alone or in groups. They roam the galaxy after their own enigmatic purposes, and are capable combatants. They can fire guided and unguided lava rockets, but will often switch tactics and close to melee range striking with their fists, only to counterattack with a devastating close-range rocket.
Links

    Alien Lore source files
    Freedoom


