<table>
<thead>
</thead>
<tbody>
	<tr>
		<td colspan="1">Physical Ranged DPS</td>
		<td colspan="1">Magical Ranged DPS</td>
		<td colspan="1">Disciples of the Hand</td>
		<td colspan="1">Tank</td>
		<td colspan="1">Disciples of the Land</td>
		<td colspan="1">Healer</td>
		<td colspan="3">Melee DPS</td>
	</tr>
	<tr>
		<th>Umbrella</th>
		<th>Classes</th>
		<th>Jobs</th>
	</tr>
	<tr>
		<td>Aiming</td>
		<td>Archer</td>
		<td>Bard, machinist, dancer</td>
	</tr>
	<tr>
		<td>Casting</td>
		<td>Thaumaturge, arcanist</td>
		<td>Black mage, summoner, red mage, blue mage</td>
	</tr>
	<tr>
		<td>Crafting</td>
		<td>Carpenter, blacksmith, armorer, goldsmith, leatherworker, weaver, alchemist, culinarian</td>
	</tr>
	<tr>
		<td>Fending</td>
		<td>Gladiator, marauder</td>
		<td>Paladin, warrior, dark knight, gunbreaker</td>
	</tr>
	<tr>
		<td>Gathering</td>
		<td>Miner, botanist, fisher</td>
	</tr>
	<tr>
		<td>Healing</td>
		<td>Conjurer</td>
		<td>White mage, scholar, astrologian, sage</td>
	</tr>
	<tr>
		<td>Maiming</td>
		<td>Lancer</td>
		<td>Dragoon, reaper</td>
	</tr>
	<tr>
		<td>Striking</td>
		<td>Pugilist</td>
		<td>Monk, samurai</td>
	</tr>
	<tr>
		<td>Scouting</td>
		<td>Rogue</td>
		<td>Ninja</td>
	</tr>
</tbody>
<tfoot>
</tfoot>
</table>

<table>
<thead>
</thead>
<tbody>
	<tr>
		<th>Hand/Land</th>
		<th>War/Magic</th>
	</tr>
	<tr>
		<td>Farmer</td>
		<td>Reaper</td>
	</tr>
	<tr>
		<td>Fisher</td>
		<td>Lancer</td>
	</tr>
	<tr>
		<td>Lumberer</td>
		<td>Marauder</td>
	</tr>
	<tr>
		<td>Hunter</td>
		<td>Archer</td>
	</tr>
	<tr>
		<td>Wanderer</td>
		<td>Conjurer</td>
	</tr>
</tbody>
<tfoot>
</tfoot>
</table>
