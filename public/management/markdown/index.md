Window Management
=================

This documentation is for setting up a lightweight alternative to fully-featured desktop environments like [GNOME](https://www.gnome.org/) and [KDE](https://kde.org/). It is meant for low-powered hardware like system-on-a-chip or reduced instruction set architecture with limited random access memory. Furthermore, a retro design using historical tools is a fun and educational experience. Learn how to set up and use classic window managers like Openbox and TWM for a snappy, resource-lite desktop.

- [Openbox](openbox.html)
- [Tab Window Manager](twm.html)

X versus Wayland
----------------

Drawbacks and Pitfalls
----------------------

Links
-----

- [Blackbox](https://sourceforge.net/projects/blackboxwm/)
- [Extended Window Manager Hints](https://web.archive.org/web/20071001170417/http://blackboxwm.sourceforge.net/NetWM)
- [Fluxbox](http://fluxbox.org/)
- [freedesktop.org](https://www.freedesktop.org/wiki/)
- [ROX Desktop](https://rox.sourceforge.net/desktop/)
- [Suckless Tools](https://tools.suckless.org/)
- [Window Maker](https://www.windowmaker.org/)
- [X.Org Foundation](https://www.x.org/wiki/)

