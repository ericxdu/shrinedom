Theoretical Pantheon
--------------------

- Bona Dea or Fauna (Good, Healing, and Protection)
- Feronia (Plants, Animals, and Healing)
- Hercules (Protection, Strength, and Travel)
- Juno (Protection, Law, and War)
- Jove or Jupiter (Air, Law, and Protection)
- Justitia or Themis (Law, Knowledge, and Good)
- Mars (War, Destruction, and Plants)
- Mercury (Travel, Luck, and Trickery)
- Minerva (Knowledge, Law, and War)
- Neptune or Fontus (Water, Animals, and Sun)
- Pluto or Hades (Death, Plants, and Earth)
- Sol (Sun, Healing, and Animals)
- Terra or Ceres (Earth, Plants, and Animals)
- Vulcan (Fire, Destruction, and Protection)
