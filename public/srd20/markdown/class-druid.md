Druid
=====

<small>This material is Open Game Content, and is licensed for public 
use under the terms of the Open Game License v1.0a.</small>

### Game Rule Information

A druid has the following game statistics.

__Alignment:__ Neutral good, lawful neutral, neutral, chaotic neutral, 
or neutral evil.

__Hit Die:__ d8.

__Base Attack Bonus:__ Equal to 3/4 total Hit Dice (as cleric).

__Good Saving Throws:__ Fortitude and Will.

#### Class Skills

The druid’s class skills (and the key ability for each skill) are 
Concentration (Con), Craft (Int), Diplomacy (Cha), Handle Animal (Cha), 
Heal (Wis), Knowledge (nature) (Int), Listen (Wis), Profession (Wis), 
Ride (Dex), Spellcraft (Int), Spot (Wis), Survival (Wis), and Swim 
(Str).

__Skill Points at Each Level:__ 4 + Int modifier.

#### Class Features

All of the following are class features of the druid.

__Weapon and Armor Proficiency:__ Druids are proficient with the 
following weapons: club, dagger, dart, quarterstaff, scimitar, sickle, 
shortspear, sling, and spear. They are also proficient with all natural 
attacks (claw, bite, and so forth) of any form they assume with wild 
shape (see below).

Druids are proficient with light and medium armor but are prohibited 
from wearing metal armor; thus, they may wear only padded, leather, or 
hide armor. (A druid may also wear wooden armor that has been altered by 
the _ironwood_ spell so that it functions as though it were steel. See 
the _ironwood_ spell description) Druids are proficient with shields 
(except tower shields) but must use only wooden ones.

A druid who wears prohibited armor or carries a prohibited shield is 
unable to cast druid spells or use any of her supernatural or spell-like 
class abilities while doing so and for 24 hours thereafter.

Spells: A druid casts divine spells, which are drawn from the druid spell list. Her alignment may restrict her from casting certain spells opposed to her moral or ethical beliefs; see Chaotic, Evil, Good, and Lawful Spells, below. A druid must choose and prepare her spells in advance (see below).
To prepare or cast a spell, the druid must have a Wisdom score equal to at least 10 + the spell level. The Difficulty Class for a saving throw against a druid’s spell is 10 + the spell level + the druid’s Wisdom modifier.
Like other spellcasters, a druid can cast only a certain number of spells of each spell level per day. Her base daily spell allotment is given on Table: The Druid. In addition, she receives bonus spells per day if she has a high Wisdom score. She does not have access to any domain spells or granted powers, as a cleric does.
A druid prepares and casts spells the way a cleric does, though she cannot lose a prepared spell to cast a cure spell in its place (but see Spontaneous Casting, below). A druid may prepare and cast any spell on the druid spell list, provided that she can cast spells of that level, but she must choose which spells to prepare during her daily meditation.

Spontaneous Casting: A druid can channel stored spell energy into summoning spells that she hasn’t prepared ahead of time. She can “lose” a prepared spell in order to cast any summon nature’s ally spell of the same level or lower. Chaotic, Evil, Good, and Lawful Spells: A druid can’t cast spells of an alignment opposed to her own or her deity’s (if she has one). Spells associated with particular alignments are indicated by the chaos, evil, good, and law descriptors in their spell descriptions.

Bonus Languages: A druid’s bonus language options include Sylvan, the language of woodland creatures. This choice is in addition to the bonus languages available to the character because of her race.
A druid also knows Druidic, a secret language known only to druids, which she learns upon becoming a 1st-level druid. Druidic is a free language for a druid; that is, she knows it in addition to her regular allotment of languages and it doesn’t take up a language slot. Druids are forbidden to teach this language to nondruids.
Druidic has its own alphabet.

Animal Companion (Ex): A druid may begin play with an animal companion selected from the following list: badger, camel, dire rat, dog, riding dog, eagle, hawk, horse (light or heavy), owl, pony, snake (Small or Medium viper), or wolf. If the campaign takes place wholly or partly in an aquatic environment, the following creatures are also available: crocodile, porpoise, Medium shark, and squid. This animal is a loyal companion that accompanies the druid on her adventures as appropriate for its kind.
A 1st-level druid’s companion is completely typical for its kind except as noted below. As a druid advances in level, the animal’s power increases as shown on the table. If a druid releases her companion from service, she may gain a new one by performing a ceremony requiring 24 uninterrupted hours of prayer. This ceremony can also replace an animal companion that has perished.
A druid of 4th level or higher may select from alternative lists of animals (see below). Should she select an animal companion from one of these alternative lists, the creature gains abilities as if the character’s druid level were lower than it actually is. Subtract the value indicated in the appropriate list header from the character’s druid level and compare the result with the druid level entry on the table to determine the animal companion’s powers. (If this adjustment would reduce the druid’s effective level to 0 or lower, she can’t have that animal as a companion.)

Nature Sense (Ex): A druid gains a +2 bonus on Knowledge (nature) and Survival checks.

Wild Empathy (Ex): A druid can improve the attitude of an animal. This ability functions just like a Diplomacy check made to improve the attitude of a person. The druid rolls 1d20 and adds her druid level and her Charisma modifier to determine the wild empathy check result.
The typical domestic animal has a starting attitude of indifferent, while wild animals are usually unfriendly.
To use wild empathy, the druid and the animal must be able to study each other, which means that they must be within 30 feet of one another under normal conditions. Generally, influencing an animal in this way takes 1 minute but, as with influencing people, it might take more or less time.
A druid can also use this ability to influence a magical beast with an Intelligence score of 1 or 2, but she takes a –4 penalty on the check.

Woodland Stride (Ex): Starting at 2nd level, a druid may move through any sort of undergrowth (such as natural thorns, briars, overgrown areas, and similar terrain) at her normal speed and without taking damage or suffering any other impairment. However, thorns, briars, and overgrown areas that have been magically manipulated to impede motion still affect her.

Trackless Step (Ex): Starting at 3rd level, a druid leaves no trail in natural surroundings and cannot be tracked. She may choose to leave a trail if so desired.

Resist Nature’s Lure (Ex): Starting at 4th level, a druid gains a +4 bonus on saving throws against the spell-like abilities of fey.

Wild Shape (Su): At 5th level, a druid gains the ability to turn herself into any Small or Medium animal and back again once per day. Her options for new forms include all creatures with the animal type. This ability functions like the polymorph spell, except as noted here. The effect lasts for 1 hour per druid level, or until she changes back. Changing form (to animal or back) is a standard action and doesn’t provoke an attack of opportunity.
The form chosen must be that of an animal the druid is familiar with.
A druid loses her ability to speak while in animal form because she is limited to the sounds that a normal, untrained animal can make, but she can communicate normally with other animals of the same general grouping as her new form. (The normal sound a wild parrot makes is a squawk, so changing to this form does not permit speech.)
A druid can use this ability more times per day at 6th, 7th, 10th, 14th, and 18th level, as noted on Table: The Druid. In addition, she gains the ability to take the shape of a Large animal at 8th level, a Tiny animal at 11th level, and a Huge animal at 15th level.
The new form’s Hit Dice can’t exceed the character’s druid level.
At 12th level, a druid becomes able to use wild shape to change into a plant creature with the same size restrictions as for animal forms. (A druid can’t use this ability to take the form of a plant that isn’t a creature.)
At 16th level, a druid becomes able to use wild shape to change into a Small, Medium, or Large elemental (air, earth, fire, or water) once per day. These elemental forms are in addition to her normal wild shape usage. In addition to the normal effects of wild shape, the druid gains all the elemental’s extraordinary, supernatural, and spell-like abilities. She also gains the elemental’s feats for as long as she maintains the wild shape, but she retains her own creature type.
At 18th level, a druid becomes able to assume elemental form twice per day, and at 20th level she can do so three times per day. At 20th level, a druid may use this wild shape ability to change into a Huge elemental.

Venom Immunity (Ex): At 9th level, a druid gains immunity to all poisons.

A Thousand Faces (Su): At 13th level, a druid gains the ability to change her appearance at will, as if using the alter self spell, but only while in her normal form.

Timeless Body (Ex): After attaining 15th level, a druid no longer takes ability score penalties for aging and cannot be magically aged. Any penalties she may have already incurred, however, remain in place.
Bonuses still accrue, and the druid still dies of old age when her time is up.
 
#### Ex-Druids

A druid who ceases to revere nature, changes to a prohibited alignment, or teaches the Druidic language to a nondruid loses all spells and druid abilities (including her animal companion, but not including weapon, armor, and shield proficiencies). She cannot thereafter gain levels as a druid until she atones (see the atonement spell description).
 
THE DRUID’S ANIMAL COMPANION
A druid’s animal companion is different from a normal animal of its kind in many ways. A druid’s animal companion is superior to a normal animal of its kind and has special powers, as described below.

Table: The Druid's Animal Companion

Animal Companion Basics: Use the base statistics for a creature of the companion’s kind, but make the following changes.
Class Level: The character’s druid level. The druid’s class levels stack with levels of any other classes that are entitled to an animal companion for the purpose of determining the companion’s abilities and the alternative lists available to the character.
Bonus HD: Extra eight-sided (d8) Hit Dice, each of which gains a Constitution modifier, as normal. Remember that extra Hit Dice improve the animal companion’s base attack and base save bonuses. An animal companion’s base attack bonus is the same as that of a druid of a level equal to the animal’s HD. An animal companion has good Fortitude and Reflex saves (treat it as a character whose level equals the animal’s HD). An animal companion gains additional skill points and feats for bonus HD as normal for advancing a monster’s Hit Dice.
Natural Armor Adj.: The number noted here is an improvement to the animal companion’s existing natural armor bonus.
Str/Dex Adj.: Add this value to the animal companion’s Strength and Dexterity scores.
Bonus Tricks: The value given in this column is the total number of “bonus” tricks that the animal knows in addition to any that the druid might choose to teach it (see the Handle Animal skill). These bonus tricks don’t require any training time or Handle Animal checks, and they don’t count against the normal limit of tricks known by the animal. The druid selects these bonus tricks, and once selected, they can’t be changed.
Link (Ex): A druid can handle her animal companion as a free action, or push it as a move action, even if she doesn’t have any ranks in the Handle Animal skill. The druid gains a +4 circumstance bonus on all wild empathy checks and Handle Animal checks made regarding an animal companion.
Share Spells (Ex): At the druid’s option, she may have any spell (but not any spell-like ability) she casts upon herself also affect her animal companion. The animal companion must be within 5 feet of her at the time of casting to receive the benefit. If the spell or effect has a duration other than instantaneous, it stops affecting the animal companion if the companion moves farther than 5 feet away and will not affect the animal again, even if it returns to the druid before the duration expires.
Additionally, the druid may cast a spell with a target of “You” on her animal companion (as a touch range spell) instead of on herself. A druid and her animal companion can share spells even if the spells normally do not affect creatures of the companion’s type (animal).
Evasion (Ex): If an animal companion is subjected to an attack that normally allows a Reflex saving throw for half damage, it takes no damage if it makes a successful saving throw.
Devotion (Ex): An animal companion gains a +4 morale bonus on Will saves against enchantment spells and effects.
Multiattack: An animal companion gains Multiattack as a bonus feat if it has three or more natural attacks and does not already have that feat. If it does not have the requisite three or more natural attacks, the animal companion instead gains a second attack with its primary natural weapon, albeit at a –5 penalty.
Improved Evasion (Ex): When subjected to an attack that normally allows a Reflex saving throw for half damage, an animal companion takes no damage if it makes a successful saving throw and only half damage if the saving throw fails.

ALTERNATIVE ANIMAL COMPANIONS
A druid of sufficiently high level can select her animal companion from one of the following lists, applying the indicated adjustment to the druid’s level (in parentheses) for purposes of determining the companion’s characteristics and special abilities.
 
4th Level or Higher (Level –3)
Ape (animal)
Bear, black (animal)
Bison (animal)
Boar (animal)
Cheetah (animal)
Crocodile (animal)1
Dire badger
Dire bat
Dire weasel
Leopard (animal)
Lizard, monitor (animal)
Shark, Large1 (animal)
Snake, constrictor (animal)
Snake, Large viper (animal)
Wolverine (animal)
 
7th Level or Higher (Level –6)
Bear, brown (animal)
Dire wolverine
Crocodile, giant (animal)
Deinonychus (dinosaur)
Dire ape
Dire boar
Dire wolf
Elasmosaurus1 (dinosaur)
Lion (animal)
Rhinoceros (animal)
Snake, Huge viper (animal)
Tiger (animal)
 
10th Level or Higher (Level –9)
Bear, polar (animal)
Dire lion
Megaraptor (dinosaur)
Shark, Huge1 (animal)
Snake, giant constrictor (animal)
Whale, orca1 (animal)
 
13th Level or Higher (Level –12)
Dire bear
Elephant (animal)
Octopus, giant1 (animal)
 
16th Level or Higher (Level –15)
Dire shark1
Dire tiger
Squid, giant1 (animal)
Triceratops (dinosaur)
Tyrannosaurus (dinosaur)
 
1 Available only in an aquatic environment.


Legal Information
=================

OPEN GAME LICENSE Version 1.0a
 
The following text is the property of Wizards of the Coast, Inc. and is Copyright 2000 Wizards of the Coast, Inc ("Wizards"). All Rights Reserved.
 
1. Definitions: (a)"Contributors" means the copyright and/or trademark owners who have contributed Open Game Content; (b)"Derivative Material" means copyrighted material including derivative works and translations (including into other computer languages), potation, modification, correction, addition, extension, upgrade, improvement, compilation, abridgment or other form in which an existing work may be recast, transformed or adapted; (c) "Distribute" means to reproduce, license, rent, lease, sell, broadcast, publicly display, transmit or otherwise distribute; (d)"Open Game Content" means the game mechanic and includes the methods, procedures, processes and routines to the extent such content does not embody the Product Identity and is an enhancement over the prior art and any additional content clearly identified as Open Game Content by the Contributor, and means any work covered by this License, including translations and derivative works under copyright law, but specifically excludes Product Identity. (e) "Product Identity" means product and product line names, logos and identifying marks including trade dress; artifacts; creatures characters; stories, storylines, plots, thematic elements, dialogue, incidents, language, artwork, symbols, designs, depictions, likenesses, formats, poses, concepts, themes and graphic, photographic and other visual or audio representations; names and descriptions of characters, spells, enchantments, personalities, teams, personas, likenesses and special abilities; places, locations, environments, creatures, equipment, magical or supernatural abilities or effects, logos, symbols, or graphic designs; and any other trademark or registered trademark clearly identified as Product identity by the owner of the Product Identity, and which specifically excludes the Open Game Content; (f) "Trademark" means the logos, names, mark, sign, motto, designs that are used by a Contributor to identify itself or its products or the associated products contributed to the Open Game License by the Contributor (g) "Use", "Used" or "Using" means to use, Distribute, copy, edit, format, modify, translate and otherwise create Derivative Material of Open Game Content. (h) "You" or "Your" means the licensee in terms of this agreement.
 
2. The License: This License applies to any Open Game Content that contains a notice indicating that the Open Game Content may only be Used under and in terms of this License. You must affix such a notice to any Open Game Content that you Use. No terms may be added to or subtracted from this License except as described by the License itself. No other terms or conditions may be applied to any Open Game Content distributed using this License.
 
3. Offer and Acceptance: By Using the Open Game Content You indicate Your acceptance of the terms of this License.
 
4. Grant and Consideration: In consideration for agreeing to use this License, the Contributors grant You a perpetual, worldwide, royalty-free, non-exclusive license with the exact terms of this License to Use, the Open Game Content.
 
5. Representation of Authority to Contribute: If You are contributing original material as Open Game Content, You represent that Your Contributions are Your original creation and/or You have sufficient rights to grant the rights conveyed by this License.
 
6. Notice of License Copyright: You must update the COPYRIGHT NOTICE portion of this License to include the exact text of the COPYRIGHT NOTICE of any Open Game Content You are copying, modifying or distributing, and You must add the title, the copyright date, and the copyright holder's name to the COPYRIGHT NOTICE of any original Open Game Content you Distribute.
 
7. Use of Product Identity: You agree not to Use any Product Identity, including as an indication as to compatibility, except as expressly licensed in another, independent Agreement with the owner of each element of that Product Identity. You agree not to indicate compatibility or co-adaptability with any Trademark or Registered Trademark in conjunction with a work containing Open Game Content except as expressly licensed in another, independent Agreement with the owner of such Trademark or Registered Trademark. The use of any Product Identity in Open Game Content does not constitute a challenge to the ownership of that Product Identity. The owner of any Product Identity used in Open Game Content shall retain all rights, title and interest in and to that Product Identity.
 
8. Identification: If you distribute Open Game Content You must clearly indicate which portions of the work that you are distributing are Open Game Content.
 
9. Updating the License: Wizards or its designated Agents may publish updated versions of this License. You may use any authorized version of this License to copy, modify and distribute any Open Game Content originally distributed under any version of this License.
 
10. Copy of this License: You MUST include a copy of this License with every copy of the Open Game Content You Distribute.
 
11. Use of Contributor Credits: You may not market or advertise the Open Game Content using the name of any Contributor unless You have written permission from the Contributor to do so.
 
12. Inability to Comply: If it is impossible for You to comply with any of the terms of this License with respect to some or all of the Open Game Content due to statute, judicial order, or governmental regulation then You may not Use any Open Game Material so affected.
 
13. Termination: This License will terminate automatically if You fail to comply with all terms herein and fail to cure such breach within 30 days of becoming aware of the breach. All sublicenses shall survive the termination of this License.
 
14. Reformation: If any provision of this License is held to be unenforceable, such provision shall be reformed only to the extent necessary to make it enforceable.
 
15. COPYRIGHT NOTICE
Open Game License v 1.0a Copyright 2000, Wizards of the Coast, Inc.
 
__System Reference Document__ Copyright 2000-2003, Wizards of the Coast, Inc.; Authors Jonathan Tweet, Monte Cook, Skip Williams, Rich Baker, Andy Collins, David Noonan, Rich Redman, Bruce R. Cordell, John D. Rateliff, Thomas Reid, James Wyatt, based on original material by E. Gary Gygax and Dave Arneson.

__Pathfinder Roleplaying Game Conversion Guide__. Copyright 2009, Paizo Publishing, LLC; Author: Jason Bulmahn.

__Pathfinder Roleplaying Game Core Rulebook__. Copyright 2009, Paizo Publishing, LLC; Author: Jason Bulmahn, based on material by Jonathan Tweet, Monte Cook, and Skip Williams.
 
END OF LICENSE
