## 1D2

Roll any even-sided die. If the result is an even number, read it as
2. If the result is an odd number, read it as 1.

## 1D3

Roll a D6. If the die shows &gt; 3, simply check the face down side for 
your result or use the following conversion: 4 is 3, 5 is 2, 6 is 1.

## 1D12

Roll a D6. If the result is an even number, read it as 6. Otherwise read 
it as 0. Roll another D6 and add this result to the first result.
    
## 1D100

Roll a D10 and multiply the result by 10. Reroll the die and add the 
second result to the first result. Any result of 10 on the D10 should be 
read as 0.
