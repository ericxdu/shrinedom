Extended Character Backgrounds
==============================

At GM discretion, these character backgrounds are available in addition to the ten standard character backgrounds.

<table>
<caption>Table: Background Statistics</caption>
<tbody>
  <tr>
    <th>Name</th>
    <th>Size and Type</th>
    <th>Ability Adjustments</th>
    <th>Favored Class</th>
  </tr>
  <tr>
    <td><a href="#aasimar">Aasimar</a></td>
    <td>Medium outsider (native)</td>
    <td>+2 Wisdom, +2 Charisma</td>
    <td>Paladin</td>
  </tr>
  <tr>
    <td><a href="#blues">Blue</a></td>
    <td>Small humanoid</td>
    <td>&minus;2 Strength, +2 Dexterity, &minus;2 Charisma</td>
    <td>Rogue</td>
  </tr>
  <tr>
    <td><a href="#dromites">Dromite</a></td>
    <td>Small monstrous humanoid</td>
    <td>&minus;2 Strength, &minus;2 Wisdom, +2 Charisma</td>
    <td>Sorcerer</td>
  </tr>
  <tr>
    <td><a href="#duergar">Duergar</a></td>
    <td>Medium humanoid</td>
    <td>&minus;2 Dexterity, +2 Constitution</td>
    <td>Fighter</td>
  </tr>
  <tr>
    <td><a href="#elans">Elan</a></td>
    <td>Medium aberration</td>
    <td>&minus;2 Charisma</td>
    <td>Wizard</td>
  </tr>
  <tr>
    <td><a href="#half-giants">Half-giant</a></td>
    <td>Medium giant</td>
    <td>+2 Strength, &minus;2 Dexterity</td>
    <td>Cleric</td>
  </tr>
  <tr>
    <td><a href="#maenads">Maenad</a></td>
    <td>Medium humanoid</td>
    <td>&mdash;</td>
    <td>Sorcerer</td>
  </tr>
  <tr>
    <td><a href="#tieflings">Tiefling</a></td>
    <td>Medium outsider (native)</td>
    <td>+2 Dexterity, +2 Intellegence, &minus;2 Charisma</td>
    <td>Rogue</td>
  </tr>
  <tr>
    <td><a href="#xeph">Xeph</a></td>
    <td>Medium humanoid</td>
    <td>&minus;2 Strength, +2 Dexterity</td>
    <td>Monk</td>
  </tr>
</tbody>
</table>


Aasimar <a id="aasimar"></a>
-------

* &bull; +2 Wisdom, +2 Charisma.
* &bull; Aasimar are outsiders with the native subtype.
* &bull; Medium: As Medium creatures, aasimar have no special bonuses or 
  penalties due to their size.
* &bull; Aasimar base land speed is 30 feet.
* &bull; Darkvision: Aasimars can see in the dark up to 60 feet.
* &bull; +2 racial bonus on Spot and Listen checks.
* &bull; Racial Feats: An aasimar gains feats according to its class 
  levels.
* &bull; Daylight: An aasimar can use daylight once per day as a 
  1st-level caster or a caster of his class levels, whichever is higher.
* &bull; Special Qualities: Resistance to acid 5, cold 5, and 
  electricity 5.
* &bull; Automatic Languages: Common, Celestial. Bonus Languages: 
  Draconic, Dwarven, Elven, Gnome, Halfling, Sylvan.
* &bull; Favored Class: Paladin.


Blues <a id="blues"></a>
-----

* &bull; –2 Strength, +2 Intelligence, –2 Charisma.
* &bull; Small size: +1 bonus to Armor Class, +1 bonus on attack rolls, 
  +4 bonus on Stealth checks, –4 penalty on grapple checks, lifting and 
  carrying limits 3/4 of those of Medium characters.
* &bull; Blue base land speed is 30 feet.
* &bull; Darkvision: Blues can see in the dark up to 60 feet. Darkvision 
  is black and white only, but it is otherwise like normal sight, and 
  tiefling can function just fine with no light at all.
* &bull; Channeler: Blue spellcasters gain bonus spells as if the 
  governing ability score were 1 point higher. This benefit does not 
  grant them the ability to cast spells unless they gain that ability 
  through levels in a spellcaster class. See [Basics](srd/Basics.html) 
  for Table: Ability Modifiers and Bonus Spells.
* &bull; Racial Skills: A blue character has a +4 racial bonus on 
  Stealth checks and Ride checks.
* &bull; Automatic Languages: Common, Goblin. Bonus Languages: Draconic, 
  Elven, Giant, Gnoll, Orc.
* &bull; Favored Class: Wizard.
* &bull; Level Adjustment: +1.


Dromites <a id="dromites"></a>
--------


Duergar <a id="duergar"></a>
-------

<small>This material is Open Game Content, and is licensed for public 
use under the terms of the Open Game License v1.0a.</small>

Elans <a id="elans"></a>
-----


Half-giants <a id="half-giants"></a>
-----------

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

### Half-giant Traits

* &bull; +2 Strength, +2 Constitution, –2 Dexterity: Half-giants are 
  tough and strong, but not too nimble.
* &bull; Giant: Half-giants are not subject to spells or effects that 
  affect humanoids only, such as charm person or dominate person.
* &bull; Medium: As Medium creatures, half-giants have no special 
  bonuses or penalties due to their size.
* &bull; Half-giant base land speed is 30 feet.
* &bull; Low-Light Vision: A half-giant can see twice as far as a 
  normal in starlight, moonlight, torchlight, and similar conditions of 
  poor illumination. He retains the ability to distinguish color and 
  detail under these conditions.
* &bull; Powerful Build: The physical stature of half-giants lets them 
  function in many ways as if they were one size category larger.
Whenever a half-giant is subject to a size modifier or special size 
  modifier for an opposed check (such as during grapple checks, bull 
  rush attempts, and trip attempts), the half-giant is treated as one 
  size larger if doing so is advantageous to him.
A half-giant is also considered to be one size larger when determining 
  whether a creature’s special attacks based on size (such as improved 
  grab or swallow whole) can affect him. A half-giant can use weapons 
  designed for a creature one size larger without penalty. However, his 
  space and reach remain those of a creature of his actual size. The 
  benefits of this racial trait stack with the effects of powers, 
  abilities, and spells that change the subject’s size category.
* &bull; +2 racial bonus on Diplomacy and Sense Motive checks.
* &bull; +2 racial bonus on Craft (any) checks.
* &bull; Automatic Language: Common. Bonus Languages: Draconic, Giant, 
  Gnoll, Ignan.
* &bull; Favored Class: Cleric.


Maenads <a id="maenads"></a>
-------

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>


Tieflings <a id="tieflings"></a>
---------

* &bull; +2 Dexterity, +2 Intelligence, &minus;2 Charisma.

* &bull; Tieflings are outsiders with the native subtype.

* &bull; Medium: As Medium creatures, tieflings have no special bonuses 
  or penalties due to their size.

* &bull; Tiefling base land speed is 30 feet.

* &bull; Darkvision: Tieflings can see in the dark up to 60 feet. 
  Darkvision is black and white only, but it is otherwise like normal 
  sight, and tiefling can function just fine with no light at all.

* &bull; +2 racial bonus on Bluff and Stealth checks.

* &bull; Darkness: A tiefling can use darkness once per day (caster 
  level equal to class levels).

* &bull; Special Qualities: Resistance to cold 5, electricity 5, and 
  fire 5.

* &bull; Automatic Languages: Common, Infernal. Bonus Languages: 
  Draconic, Dwarven, Elven, Gnome, Goblin, Halfling, Orc.

* &bull; Favored Class: Rogue.


Xeph <a id="xeph"></a>
----


Appendix: Notes for background descriptions
===========================================

### Xeph

- [Edo Period Fashion](https://en.wikipedia.org/wiki/Edo_period#Fashion)


<table id="exoticbackgrounds">
<caption>Source Table: Exotic Backgrounds Extended</caption>
<tbody>
  <tr>
    <th>Name</th>
    <th>Size and Type</th>
    <th>Level Adjustment</th>
    <th>Ability Adjustments</th>
    <th>Favored Class</th>
    <th>Channeling</th>
  </tr>
  <tr>
    <td>Blue</td>
    <td>Small humanoid</td>
    <td>+1</td>
    <td>&minus;2 Strength, +2 Intelligence, &minus;2 Charisma</td>
    <td>Wizard</td>
    <td>1</td>
  </tr>
  <tr>
    <td>Dromite</td>
    <td>Small monstrous humanoid</td>
    <td>+1</td>
    <td>+2 Charisma, &minus;2 Strength, &minus;2 Wisdom</td>
    <td>Sorcerer</td>
    <td>1</td>
  </tr>
  <tr>
    <td>Duergar, psionic</td>
    <td>Medium humanoid</td>
    <td>+1</td>
    <td>+2 Constitution, &minus;4 Charisma</td>
    <td>Bard</td>
    <td>3</td>
  </tr>
  <tr>
    <td>Elan</td>
    <td>Medium aberration</td>
    <td>+0</td>
    <td>&minus;2 Charisma</td>
    <td>Wizard</td>
    <td>2</td>
  </tr>
  <tr>
    <td>Half-giant, psionic</td>
    <td>Medium giant</td>
    <td>+1</td>
    <td>+2 Strength, +2 Constitution, &minus;2 Dexterity</td>
    <td>Bard</td>
    <td>2</td>
  </tr>
  <tr>
    <td>Maenad</td>
    <td>Medium humanoid</td>
    <td>+0</td>
    <td>&mdash;</td>
    <td>Sorcerer</td>
    <td>2</td>
  </tr>
  <tr>
    <td>Xeph</td>
    <td>Medium humanoid</td>
    <td>+0</td>
    <td>+2 Dexterity, &minus;2 Strength</td>
    <td>Monk</td>
    <td>1</td>
  </tr>
</tbody>
</table>

Legal Information
=================

