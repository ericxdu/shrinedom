Planetary Romance Character Backgrounds
=======================================

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

This setting provides ten main playable character backgrounds adapted 
from the SRD. 

* <a href="#blues">Blue</a>               -2 Str, +2 Dex, +2 Int
* <a href="#dromites">Dromite</a>         -2 Str, +2 Dex, +2 Cha
* <a href="#duergar">Duergar</a>          +2 Con, +2 Wis, -2 Cha
* <a href="#elans">Elan</a>               +2 Any
* <a href="#gnolls">Gnoll</a>             +2 Str, +2 Con, -2 Int
* <a href="#half-giants">Half-giant</a>   +2 Str, -2 Dex, +2 Wis
* <a href="#maenads">Maenad</a>           +2 Any
* <a href="#rakshasa">Rakshasa</a>        +2 Con, -2 Wis, +2 Cha
* <a href="#saurians">Saurian</a>         +2 Dex, +2 Wis, -2 Cha
* <a href="#xeph">Xeph</a>                -2 Str, +2 Dex, +2 Cha


Blues <a id="blues"></a>
-----

Blues are descended from a goblin-like species who evolved in rainforests and jungles.

Blue characters possess the following racial traits.

&mdash; -2 Strength, +2 Dexterity, –2 Charisma.

&mdash; Small: As a Small creature, a blue gains a +1 size bonus to Armor Class, a +1 size bonus on attack rolls, and a +4 size bonus on Hide checks, but it uses smaller weapons than normal, and its lifting and carrying limits are three-quarters of those of a Medium character.

&mdash; Blue base land speed is 30 feet.

&mdash; A jungle goblin has a climb speed of 20 feet. Jungle goblins are natural climbers, able to scramble up trees with ease. A jungle goblin has a +8 racial bonus on all Climb checks. It must make a Climb check to climb any wall or slope with a DC of more than 0, but it always can choose to take 10, even if rushed or threatened while climbing. If it chooses an accelerated climb, it moves at double its climb speed and makes a single Climb check at a -5 penalty. It cannot run while climbing. It retains its Dexterity bonus to Armor Class (if any) while climbing, and opponents get no special bonus on their attacks against a climbing jungle goblin. Also, jungle goblins add their Dexterity modifier to Climb checks instead of their Strength modifier.

&mdash; Low-Light Vision: A blue can see twice as far as normal in starlight, moonlight, torchlight, and similar conditions of poor illumination. She retains the ability to distinguish color and detail under these conditions.

&mdash; -1 penalty to Diplomacy checks when interacting with non-goblinoids, +1 bonus to Diplomacy checks when interacting with goblins.

&mdash; +2 racial bonus on Jump checks.

&cross; +2 racial bonus on Stealth and Ride checks.


Dromites <a id="dromites"></a>
--------

Dromites are humanoids with bug-like features and chitinous 
exoskeletons. They make their homes under the surface of barren, rocky 
satellites. They stand about 3 feet tall and usually weigh slightly more 
than 30 pounds. They have iridescent compound eyes. Dromites wear heavy 
boots and light clothing, and are sometimes content with just a sturdy 
harness.

### Dromite Traits

&bull; -2 Strength, +2 Dexterity, +2 Charisma.

&bull; Monstrous Humanoid: Dromites are not subject to spells or 
effects that affect humanoids only, such as charm person or dominate 
person.

&bull; Small: As a Small creature, a dromite gains a +1 size bonus to 
Armor Class, a +1 size bonus on attack rolls, and a +4 size bonus on 
Hide checks, but it uses smaller weapons than normal, and its lifting 
and carrying limits are three-quarters of those of a Medium character.

&bull; Dromite base land speed is 20 feet.

&bull; Chitin: A dromite's skin is hardened, almost like an 
exoskeleton, and grants the character a +1 natural armor bonus to AC.

&bull; Scent: Its antennae give a dromite the scent ability. A dromite 
can detect opponents by scent within 30 feet. If the opponent is upwind, 
the range increases to 60 feet; if downwind, it drops to 15 feet. Strong 
scents, such as smoke or rotting garbage, can be detected at twice the 
ranges noted above. Overpowering scents, such as skunk musk or 
troglodyte stench, can be detected at triple normal range. When a 
dromite detects a scent, the exact location of the source is not 
revealed&mdash;only its presence somewhere within range. The dromite can 
take a move action to note the direction of the scent. Whenever the 
dromite comes within 5 feet of the source, the dromite pinpoints the 
source's location.

&bull; Compound Eyes: This feature of its anatomy gives a dromite a +2 
racial bonus on Perception checks.


Duergar <a id="duergar"></a>
-------

Most duergar are bald (even the females), and they dress in drab clothing that is designed to blend into stone. In their lairs they may wear jewelry, but it is always kept dull.

### Duergar Traits

&bull; +2 Constitution, &minus;2 Dexterity.

&bull; Medium-size: As Medium creatures, duergar have no special bonuses or penalties due to their size.

&bull; Duergar base land speed is 20 feet. However, duergar can move at this speed even when wearing medium or heavy armor or when carrying a medium or heavy load (unlike other creatures, whose speed is reduced in such situations).

&bull; Heat Endurance: Duergar gain a +4 racial bonus on Fortitude saves made to resist the effects of hot weather.

&bull; Darkvision: Duergar can see in the dark up to 60 feet. Darkvision is black and white only, but it is otherwise like normal sight, and duergar can function just fine with no light at all.

&bull; +2 racial bonus on saves against poison, spells and spell-like effects.

&bull; Stability: Duergar are exceptionally stable on their feet. A duergar receives a +4 bonus on ability checks made to resist being bull rushed or tripped when standing on the ground (but not when climbing, flying, riding or otherwise not standing firmly on the ground).


Elans <a id="elans"></a>
-----

Elans are formerly human creatures sustained by and infused with supernatural energies. The typical elan stands just under 6 feet tall and weighs in the neighborhood of 180 pounds, with men sometimes taller and heavier than women, but not always.

### Elan Traits

• +2 Wisdom, −2 Charisma.

• Medium: As Medium creatures, elans have no special bonuses or 
penalties due to their size.

• Elan base land speed is 30 feet.

• __Supernatural Abilities:__ Elans can channel energy to manifest 
supernatural powers. They can use these abilities a number of times per 
day equal to 1&frasl;2 character level &times; Charisma modifier (mimimum 
2). Furthermore, they can &ldquo;lose&rdquo; any prepared arcane spell in 
order to use one of these abilities instead.

_Resistance (Su):_ Elans can channel energy to increase their resistance 
to various forms of attack. As an immediate action, an elan can expend one 
nergy channeling use to gain a +4 racial bonus on saving throws until the 
beginning of her next action.

_Resilience (Su):_ When an elan takes damage, she can channel energy to 
reduce its severity. As an immediate action, she can reduce the damage she 
is about to take by 2 hit points for every energy channeling use she 
expends.

_Repletion (Su):_ An elan can sustain her body without need of food or 
water. If she expends a single energy channeling use, an elan does not 
need to eat or drink for 24 hours.


Half-giants <a id="half-giants"></a>
-----------

Half-giants typically stand from 7 feet to nearly 8 feet tall and weigh from 250 to 400 pounds, with men noticeably taller and heavier than women.

### Half-giant Traits

• +2 Strength, −2 Dexterity, <s>+2 Wisdom</s>.

• Giant: Half-giants are not subject to spells or effects that affect humanoids only, such as _charm person_ or _dominate person_.

• Medium: As Medium creatures, half-giants have no special bonuses or penalties due to their size.

• Half-giant base land speed is 30 feet.

• +2 racial bonus on Diplomacy and Sense Motive checks.

• +2 racial bonus on Craft (any) checks.

• The physical stature of half-giants lets them function in many 
ways as if they were one size category larger.

Whenever a half-giant is subject to a size modifier or special size 
modifier for an opposed check (such as during grapple checks, bull rush 
attempts, and trip attempts), the half-giant is treated as one size 
larger if doing so is advantageous to him.

A half-giant is also considered to be one size larger when determining 
whether a creature’s special attacks based on size (such as 
improved grab or swallow whole) can affect him. A half-giant can use 
weapons designed for a creature one size larger without penalty. 
However, his space and reach remain those of a creature of his actual 
size. The benefits of this racial trait stack with the effects of 
powers, abilities, and spells that change the subject’s size 
category.


Maenads <a id="maenads"></a>
----------------------------

Maenads are a human offshoot cursed with a raging supernatural energy. They typically stand more than 6 feet tall and weigh about 200 pounds; males are the same height as and only marginally heavier than maenad females. Maenads have no facial or body hair, and they prefer heavier clothing and armor if possible.

### Maenad Traits

* &bull; Medium: As Medium creatures, maenads have no special bonuses or
  penalties due to their size.
* &bull; Maenad base land speed is 30 feet.

* &bull; Sonic Ray (Sp): Once per day as a standard action, a maenad can
  generate a tremendous scream of rage that strikes a target within 25 feet
  &plus; 5 feet per 4 character levels, dealing 1d6&minus;1 points of sonic
  damage if he succeeds on a ranged touch attack with the ray. This damage
  ignores hardness.
* &bull; Outburst (Ex): Once per day, for up to 4 rounds, a maenad can
  subjugate her mentality to gain a boost of raw physical power. When she does
  so, she takes a &minus;2 penalty to Intelligence and Wisdom but gains a +2
  bonus to Strength.


<a if="rakshasas"></a>
Rakshasas
---------

### Rakshasa Traits

Rakshasa characters possess the following racial traits.

• +2 Dexterity, −2 Wisdom, <s>+2 Charisma</s>.

• Medium: As Medium creatures, rakshasas have no special bonuses or penalties due to their size.

• Rakshasa base land speed is 30 feet.

• Low-Light Vision: A rakshasa can see twice as far as normal in starlight, moonlight, torchlight, and similar conditions of poor illumination. They retain the ability to distinguish color and detail under these conditions.

• +2 racial bonus on Perception, Stealth, and Survival checks. Rakshasas are natural hunters.


<a id="saurians"></a>
Saurians
--------

### Saurian Traits

Saurian characters possess the following racial traits.

&bull; +2 Dexterity, +2 Wisdom, -2 Charisma.

&bull; Medium: As Medium creatures, saurians have no special bonuses or 
penalties due to their size.

&bull; Saurian base land speed is 40 feet.


Xeph <a id="xeph"></a>
----

Xeph description is under construction. See [note/link](#xephnotes).

### Xeph Traits

* &bull; +2 Dexterity, &minus;2 Strength.
* &bull; Medium: As Medium creatures, xeph have no special bonuses or penalties
  due  to their size.
* &bull; Xeph base land speed is 30 feet.
* &bull; Darkvision: Xeph can see in the dark up to 60 feet. Darkvision is
  black and white only, but it is otherwise like normal sight, and xeph can
  function just fine with no light at all.
* &bull; +1 racial bonus on saving throws against spells and spell-like
  effects. Xeph have an innate resistance to magic.
* &bull; Channeling: Xeph spellcasters gain bonus spells as if the governing
  ability score were 1 point higher. This benefit does not grant them the
  ability to cast spells unless they gain that ability through levels in a
  spellcaster class. See [Basics](srd/Basics.html) for Table: Ability
  Modifiers and Bonus Spells.
* &bull; Burst (Su): Three times per day, a xeph can increase his or her 
  speed by 10 feet, plus 10 feet per four character levels beyond 1st, 
  to a maximum increase of 30 feet at 9th character level and higher. 
  These bursts of speed are considered a competence bonus to the xeph's 
  base speed. A burst of speed lasts 3 rounds.
* &bull; Favored Class: Monk.
* &bull; Level Adjustment: +0.


Appendix: Psionic Abilities
===========================

These were created to replace psi-like abilities.

&mdash; Stomp (Sp): Once per day as a standard action, an risi can 
precipitate a shock wave that travels along the ground, toppling 
creatures and loose objects. The shock wave affects only creatures 
standing on the ground within a 20-foot cone. Creatures that fail a 
Reflex save with a DC equal to 11 &plus; the risi's Charisma modifier 
are thrown to the ground, become prone, and take 1d4 points of 
nonlethal damage.

&mdash; Channeling: Half-giant spellcasters gain bonus spells as if the 
governing ability score were 2 points higher. This benefit does not 
grant them the ability to cast spells unless they gain that ability 
through levels in a spellcaster class. See [Basics](srd/Basics.html) for 
Table: Ability Modifiers and Bonus Spells.


Appendix: Notes for background descriptions
===========================================

### Xeph

- [Edo Period Fashion](https://en.wikipedia.org/wiki/Edo_period#Fashion)


Appendix: Complete Background Tables
====================================

<table id="standardbackgrounds">
<caption>Source Table: Standard Backgrounds Extended</caption>
<tbody>
  <tr>
    <th>Background</th>
    <th>Size and Type</th>
    <th>Level Adjustment</th>
    <th>Ability Adjustments</th>
    <th>Favored Class</th>
  </tr>
  <tr>
    <td>Dwarf</td>
    <td>Medium humanoid</td>
    <td>+0</td>
    <td>+2 Constitution, &minus;2 Charisma</td>
    <td>Fighter</td>
  </tr>
  <tr>
    <td>Elf</td>
    <td>Medium humanoid</td>
    <td>+0</td>
    <td>+2 Dexterity, &minus;2 Constitution</td>
    <td>Wizard</td>
  </tr>
  <tr>
    <td>Gnome</td>
    <td>Small humanoid</td>
    <td>+0</td>
    <td>+2 Constitution, &minus;2 Strength</td>
    <td>Bard</td>
  </tr>
  <tr>
    <td>Goblin</td>
    <td>Small humanoid</td>
    <td>+0</td>
    <td>-2 Strength, +2 Dexterity, -2 Charisma</td>
    <td>Rogue</td>
  </tr>
  <tr>
    <td>Half-elf</td>
    <td>Medium humanoid</td>
    <td>+0</td>
    <td>&mdash;</td>
    <td>Any</td>
  </tr>
  <tr>
    <td>Half-orc</td>
    <td>Medium humanoid</td>
    <td>+0</td>
    <td>+2 Strength, &minus;2 Intelligence, &minus;2 Charisma</td>
    <td>Barbarian</td>
  </tr>
  <tr>
    <td>Halfling</td>
    <td>Small humanoid</td>
    <td>+0</td>
    <td>+2 Dexterity, &minus;2 Strength</td>
    <td>Rogue</td>
  </tr>
  <tr>
    <td>Kobold</td>
    <td>Small humanoid</td>
    <td>+0</td>
    <td>&minus;4 Strength, +2 Dexterity, &minus;2 Constitution</td>
    <td>Sorcerer</td>
  </tr>
  <tr>
    <td>Orc</td>
    <td>Medium humanoid</td>
    <td>+0</td>
    <td>+4 Strength, &minus;2 Intelligence, &minus;2 Wisdom, &minus;2 Charisma</td>
    <td>Barbarian</td>
  </tr>
</tbody>
<tfoot>
  <tr>
    <td colspan="5">
      See the SRD for standard background traits:
      <a href="srd/MonstersIntro-A.html">A</a>
      <a href="srd/MonstersB-C.html">B-C</a>
      <a href="srd/MonstersD-De.html">D-De</a>
      <a href="srd/MonstersDi-Do.html">Di-Do</a>
      <a href="srd/MonstersDr-Dw.html">Dr-Dw</a>
      <a href="srd/MonstersE-F.html">E-F</a>
      <a href="srd/MonstersG.html">G</a>
      <a href="srd/MonstersH-I.html">H-I</a>
      <a href="srd/MonstersK-L.html">K-L</a>
      <a href="srd/MonstersM-N.html">M-N</a>
      <a href="srd/MonstersO-R.html">O-R</a>
      <a href="srd/MonstersS.html">S</a>
      <a href="srd/MonstersT-Z.html">T-Z</a>
      <a href="srd/Races.html">Races</a>
    </td>
  </tr>
</tfoot>
</table>

<table id="exoticbackgrounds">
<caption>Source Table: Exotic Backgrounds Extended</caption>
<tbody>
  <tr>
    <th>Name</th>
    <th>Size and Type</th>
    <th>Level Adjustment</th>
    <th>Ability Adjustments</th>
    <th>Favored Class</th>
    <th>Channeling</th>
  </tr>
  <tr>
    <td>Blue</td>
    <td>Small humanoid</td>
    <td>+1</td>
    <td>&minus;2 Strength, +2 Intelligence, &minus;2 Charisma</td>
    <td>Wizard</td>
    <td>1</td>
  </tr>
  <tr>
    <td>Dromite</td>
    <td>Small monstrous humanoid</td>
    <td>+1</td>
    <td>+2 Charisma, &minus;2 Strength, &minus;2 Wisdom</td>
    <td>Sorcerer</td>
    <td>1</td>
  </tr>
  <tr>
    <td>Duergar, psionic</td>
    <td>Medium humanoid</td>
    <td>+1</td>
    <td>+2 Constitution, &minus;4 Charisma</td>
    <td>Bard</td>
    <td>3</td>
  </tr>
  <tr>
    <td>Elan</td>
    <td>Medium aberration</td>
    <td>+0</td>
    <td>&minus;2 Charisma</td>
    <td>Wizard</td>
    <td>2</td>
  </tr>
  <tr>
    <td>Half-giant, psionic</td>
    <td>Medium giant</td>
    <td>+1</td>
    <td>+2 Strength, +2 Constitution, &minus;2 Dexterity</td>
    <td>Bard</td>
    <td>2</td>
  </tr>
  <tr>
    <td>Maenad</td>
    <td>Medium humanoid</td>
    <td>+0</td>
    <td>&mdash;</td>
    <td>Sorcerer</td>
    <td>2</td>
  </tr>
  <tr>
    <td>Xeph</td>
    <td>Medium humanoid</td>
    <td>+0</td>
    <td>+2 Dexterity, &minus;2 Strength</td>
    <td>Monk</td>
    <td>1</td>
  </tr>
</tbody>
</table>
