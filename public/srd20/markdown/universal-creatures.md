Universal Creatures
===================

- [The Monster](#fleshgolem)
- [The Wolf Man](#werewolf)
- [The Priest](#mummylord)
- [The Creature](#sahuagin)
- [The Immortal](#vampire)


Golem
-----

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

Golems are magically created automatons of great power. Constructing one involves the employment of mighty magic and elemental forces.

The animating force for a golem is a spirit from the Elemental Plane of Earth. The process of creating the golem binds the unwilling spirit to the artificial body and subjects it to the will of the golem’s creator.

### Combat

Golems are tenacious in combat and prodigiously strong as well. Being mindless, they do nothing without orders from their creators. They follow instructions explicitly and are incapable of any strategy or tactics. They are emotionless in combat and cannot be provoked.

A golem’s creator can command it if the golem is within 60 feet and can see and hear its creator. If uncommanded, a golem usually follows its last instruction to the best of its ability, though if attacked it returns the attack. The creator can give the golem a simple command to govern its actions in his or her absence. The golem’s creator can order the golem to obey the commands of another person (who might in turn place the golem under someone else’s control, and so on), but the golem’s creator can always resume control over his creation by commanding the golem to obey him alone.

__Immunity to Magic (Ex):__ Golems have immunity to most magical and supernatural effects, except when otherwise noted.

### Construction

The cost to create given for each golem includes the cost of the physical body and all the materials and spell components that are consumed or become a permanent part of the golem. Creating a golem is essentially similar to creating any sort of magic item. However, a golem’s body includes costly material components that may require some extra preparation. The golem’s creator can assemble the body or hire someone else to do the job. The builder must have the appropriate skill, which varies with the golem variety.

Completing the golem’s creation drains the appropriate XP from the creator and requires casting any spells on the final day.

The creator must cast the spells personally, but they can come from outside sources, such as scrolls.

The characteristics of a golem that come from its nature as a magic item (caster level, prerequisite feats and spells, market price, cost to create) are given in summary form at the end of each golem’s description.

*Note:* The market price of an advanced golem (a golem with more Hit Dice than the typical golem described in each entry) is increased by 5,000 gp for each additional Hit Die, and increased by an additional 50,000 gp if the golem’s size increases. The XP cost for creating an advanced golem is equal to 1/25 the advanced golem’s market price minus the cost of the special materials required.

<h3 id="fleshgolem">Flesh Golem</h3>

- **Large Construct**
- **Hit Dice:** 9d10+30 (79 hp)
- **Initiative:** –1
- **Speed:** 30 ft. (6 squares)
- **Armor Class:** 18 (–1 size, –1 Dex, +10 natural), touch 8, flat-footed 18
- **Base Attack/Grapple:** +6/+15
- **Attack:** Slam +10 melee (2d8+5)
- **Full Attack:** 2 slams +10 melee (2d8+5)
- **Space/Reach:** 10 ft./10 ft.
- **Special Attacks:** Berserk
- **Special Qualities:** Construct traits, damage reduction 5/adamantine, darkvision 60 ft., immunity to magic, low-light vision
- **Saves:** Fort +3, Ref +2, Will +3
- **Abilities:** Str 21, Dex 9, Con —, Int —, Wis 11, Cha 1
- **Skills:** —
- **Feats:** —
- **Environment:** Any
- **Organization:** Solitary or gang (2–4)
- **Challenge Rating:** 7
- **Treasure:** None
- **Alignment:** Always neutral
- **Advancement:** 10–18 HD (Large); 19–27 HD (Huge)
- **Level Adjustment:** —

A flesh golem is a ghoulish collection of stolen humanoid body parts, stitched together into a single composite form. No natural animal willingly tracks a flesh golem. The golem wears whatever clothing its creator desires, usually just a ragged pair of trousers. It has no possessions and no weapons. It stands 8 feet tall and weighs almost 500 pounds.

A flesh golem golem cannot speak, although it can emit a hoarse roar of sorts. It walks and moves with a stiff-jointed gait, as if not in complete control of its body.

#### Combat

__Berserk (Ex):__ When a flesh golem enters combat, there is a cumulative 1% chance each round that its elemental spirit breaks free and the golem goes berserk. The uncontrolled golem goes on a rampage, attacking the nearest living creature or smashing some object smaller than itself if no creature is within reach, then moving on to spread more destruction. The golem’s creator, if within 60 feet, can try to regain control by speaking firmly and persuasively to the golem, which requires a DC 19 Charisma check. It takes 1 minute of inactivity by the golem to reset the golem’s berserk chance to 0%.

__Immunity to Magic (Ex):__ A flesh golem is immune to any spell or spell-like ability that allows spell resistance. In addition, certain spells and effects function differently against the creature, as noted below.

A magical attack that deals cold or fire damage slows a flesh golem (as the *slow* spell) for 2d6 rounds, with no saving throw.

A magical attack that deals electricity damage breaks any *slow* effect on the golem and heals 1 point of damage for every 3 points of damage the attack would otherwise deal. If the amount of healing would cause the golem to exceed its full normal hit points, it gains any excess as temporary hit points. For example, a flesh golem hit by a *lightning bolt* heals 3 points of damage if the attack would have dealt 11 points of damage. A flesh golem golem gets no saving throw against attacks that deal electricity damage.

#### Construction

The pieces of a flesh golem must come from normal human corpses that have not decayed significantly. Assembly requires a minimum of six different bodies—one for each limb, the torso (including head), and the brain. In some cases, more bodies may be necessary. Special unguents and bindings worth 500 gp are also required. Note that creating a flesh golem requires casting a spell with the evil descriptor.

Assembling the body requires a DC 13 Craft (leatherworking) check or a DC 13 Heal check.

CL 8th; Craft Construct, *animate dead*, *bull’s strength*, *geas/quest*, *limited wish*, caster must be at least 8th level; Price 20,000 gp; Cost 10,500 gp + 780 XP.


Lycanthrope
-----------

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

Lycanthropes are humanoids or giants who can transform themselves into animals. In its natural form, a lycanthrope looks like any other members of its kind, though natural lycanthropes and those who have been afflicted for a long time tend to have or acquire features reminiscent of their animal forms. In animal form, a lycanthrope resembles a powerful version of the normal animal, but on close inspection, its eyes (which often glow red in the dark) show a faint spark of unnatural intelligence.

Lycanthropy can be spread like a disease. Sometimes a lycanthrope begins life as a normal humanoid or giant who subsequently contracts lycanthropy after being wounded by a lycanthrope. Such a creature is called an afflicted lycanthrope. Other lycanthropes are born as lycanthropes, and are known as natural lycanthropes.

#### Combat

A lycanthrope in its humanoid (or giant) form uses whatever tactics and weapons are favored by others of its kind, though it tends to be slightly more aggressive. A lycanthrope possesses the senses of its animal form, including scent and low-light vision, and it has a deep empathy for (and ability to communicate with) animals of its animal form. An afflicted lycanthrope damaged in combat may be overwhelmed by rage, causing it to change to its animal form involuntarily.

A lycanthrope in animal form fights like the animal it resembles, although its bite carries the disease of lycanthropy. It is preternaturally cunning and strong, and possesses damage reduction that is overcome only by silvered weapons.

Finally, a natural lycanthrope (or an afflicted lycanthrope that has become aware of its affliction) can assume a hybrid form that is a mix of its humanoid and animal forms. A hybrid has hands and can use weapons, but it can also attack with its teeth and claws. A hybrid can spread lycanthropy with its bite, and it has the same damage reduction that its animal form possesses.

<h3 id="werewolf">Werewolf</h3>

<table>
  <thead>
    <tr>
      <th></th>
      <th>Werewolf, Human Form</th>
      <th>Werewolf, Wolf Form</th>
      <th>Werewolf, Hybrid Form</th>
    </tr>
	  <tr>
	    <th></th>
      <th>Medium Humanoid (Human, Shapechanger)</th>
      <th>Medium Humanoid (Human, Shapechanger)</th>
      <th>Medium Humanoid (Human, Shapechanger)</th>
    </tr>
  </thead>
  <tr>
    <th>Hit Dice:</th>
    <td>1d8+1 plus 2d8+6 (20 hp)</td>
    <td>1d8+1 plus 2d8+6 (20 hp)</td>
    <td>1d8+1 plus 2d8+6 (20 hp)</td>
  </tr>
  <tr>
    <th>Initiative:</th>
    <td>+4</td>
    <td>+6</td>
    <td>+6</td>
  </tr>
  <tr>
    <th>Speed:</th>
    <td>30 ft. (6 squares)</td>
    <td>50 ft. (10 squares)</td>
    <td>30 ft. (6 squares)</td>
  </tr>
  <tr>
    <th>Armor Class:</th>
    <td>17 (+2 natural, +4 chain shirt, +1 light shield) touch 10, flat-footed 17</td>
    <td>16 (+2 Dex, +4 natural), touch 12, flat-footed 14</td>
    <td>16 (+2 Dex, +4 natural), touch 12, flat-footed 14</td>
  </tr>
  <tr>
    <th>Base Attack/Grapple:</th>
    <td>+2/+3</td>
    <td>+2/+4</td>
    <td>+2/+4</td>
  </tr>
  <tr>
    <th>Attack:</th>
    <td>Longsword +3 melee (1d8+1/19–20) or light crossbow +2 ranged (1d8/19–20)</td>
    <td>Bite +5 melee (1d6+3)</td>
    <td>Claw +4 melee (1d4+2)</td>
  </tr>
  <tr>
    <th>Full Attack:</th>
    <td>Longsword +3 melee (1d8+1/19–20) or light crossbow +2 ranged (1d8/19–20)</td>
    <td>Bite +5 melee (1d6+3)</td>
    <td>2 claws +4 melee (1d4+2) and bite +0 melee (1d6+1)</td>
  </tr>
  <tr>
    <th>Space/Reach:</th>
    <td>5 ft./5 ft.</td>
    <td>5 ft./5 ft.</td>
    <td>5 ft./5 ft.</td>
  </tr>
  <tr>
    <th>Special Attacks:</th>
    <td>—</td>
    <td>Curse of lycanthropy, trip</td>
    <td>Curse of lycanthropy</td>
  </tr>
  <tr>
    <th>Special Qualities:</th>
    <td>Alternate form, wolf empathy, low-light vision, scent</td>
    <td>Alternate form, wolf empathy, damage reduction 10/silver, low-light vision, scent</td>
    <td>Alternate form, wolf empathy, damage reduction 10/silver, low-light vision, scent</td>
  </tr>
  <tr>
    <th>Saves:</th>
    <td>Fort +6, Ref +3, Will +2</td>
    <td>Fort +8, Ref +5, Will +2</td>
    <td>Fort +8 Ref +5, Will +2</td>
  </tr>
  <tr>
    <th>Abilities:</th>
    <td>Str 13, Dex 11, Con 12, Int 10, Wis 11, Cha 8</td>
    <td>Str 15, Dex 15, Con 16, Int 10, Wis 11, Cha 8</td>
    <td>Str 15, Dex 15, Con 16, Int 10, Wis 11, Cha 8</td>
  </tr>
  <tr>
    <th>Skills:</th>
    <td>Handle Animal +1, Hide +1, Listen +1, Move Silently +2, Spot +1, Survival +2</td>
    <td>Handle Animal +1, Hide +6, Listen +1, Move Silently +6, Spot +1, Survival +2*</td>
    <td>Handle Animal +1, Hide +6, Listen +1, Move Silently +6, Spot +1, Survival +2*</td>
  </tr>
  <tr>
    <th>Feats:</th>
    <td>Improved Initiative, Iron WillB, Stealthy, TrackB, Weapon Focus (bite)</td>
    <td>(same as human form)</td>
    <td>(same as human form)</td>
  </tr>
  <tr>
    <th>Environment:</th>
    <td>Temperate forests</td>
    <td>Temperate forests</td>
    <td>Temperate forests</td>
  </tr>
  <tr>
    <th>Organization:</th>
    <td>Solitary, pair, pack (6–10), or troupe (2–5 plus 5–8 wolves)</td>
    <td>(same as human form)</td>
    <td>(same as human form)</td>
  </tr>
  <tr>
    <th>Challenge Rating:</th>
    <td>3</td>
    <td>3</td>
    <td>3</td>
  </tr>
  <tr>
    <th>Treasure:</th>
    <td>Standard</td>
    <td>Standard</td>
    <td>Standard</td>
  </tr>
  <tr>
    <th>Alignment:</th>
    <td>Always chaotic evil</td>
    <td>Always chaotic evil</td>
    <td>Always chaotic evil</td>
  </tr>
  <tr>
    <th>Advancement:</th>
    <td>By character class</td>
    <td>By character class</td>
    <td>By character class</td>
  </tr>
  <tr>
    <th>Level Adjustment:</th>
    <td>+3</td>
    <td>+3</td>
    <td>+3</td>
  </tr>
</table>

Werewolves in humanoid form have no distinguishing traits.

#### Combat

In wolf form, a werewolf can trip just as a normal wolf does. A werewolf in hybrid form usually dispenses with weapon attacks, though it can wield a weapon and use its bite as a secondary natural attack.

__Alternate Form (Su):__ A werewolf can assume a bipedal hybrid form or the form of a wolf.

__Curse of Lycanthropy (Su):__ Any humanoid or giant hit by a werewolf’s bite attack in animal or hybrid form must succeed on a DC 15 Fortitude save or contract lycanthropy.

__Trip (Ex):__ A werewolf in animal form that hits with a bite attack can attempt to trip the opponent (+2 check modifier) as a free action without making a touch attack or provoking an attack of opportunity.

If the attempt fails, the opponent cannot react to trip the werewolf.

__Wolf Empathy (Ex):__ Communicate with wolves and dire wolves, and +4 racial bonus on Charisma-based checks against wolves and dire wolves.

__Skills:__ &ast;A werewolf in hybrid or wolf form gains a +4 racial bonus on Survival checks when tracking by scent.

The werewolf presented here is based on a 1st-level human warrior and natural lycanthrope, using the following base ability scores: Str 13, Dex 11, Con 12, Int 10, Wis 9, Cha 8.


### Lycanthrope, Werewolf Lord

<table>
  <thead>
    <tr>
      <th></th>
      <th>Werewolf Lord, Human Form</th>
      <th>Werewolf Lord, Dire Wolf Form</th>
      <th>Werewolf Lord, Hybrid Form</th>
    </tr>
	  <tr>
	    <th></th>
      <th>Medium Humanoid</th>
      <th>Large Humanoid</th>
      <th>Large Humanoid</th>
    </tr>
	  <tr>
	    <th></th>
      <th>(Human, Shapechanger)</th>
      <th>(Human, Shapechanger)</th>
      <th>(Human, Shapechanger)</th>
    </tr>
  </thead>
  <tr>
    <th>Hit Dice:</th>
    <td>10d10+20 plus 6d8+30 (132 hp)</td>
    <td>10d10+20 plus 6d8+30 (132 hp)</td>
    <td>10d10+20 plus 6d8+30 (132 hp)</td>
  </tr>
  <tr>
    <th>Initiative:</th>
    <td>+2</td>
    <td>+4</td>
    <td>+4</td>
  </tr>
  <tr>
    <th>Speed :</th>
    <td>30 ft. (6 squares)</td>
    <td>40 ft. (8 squares)</td>
    <td>30 ft. (6 squares)</td>
  </tr>
  <tr>
    <th>Armor Class:</th>
    <td>26 (+2 Dex, +3 natural, +6 +2 mithral chain shirt, +5 +3 heavy shield) touch 12, flat-footed 24</td>
    <td>19 (–1 size, +4 Dex, +6 natural), touch 13, flat-footed 15</td>
    <td>19 (–1 size, +4 Dex, +6 natural), touch 13, flat-footed 15</td>
  </tr>
  <tr>
    <th>Base Attack/Grapple:</th>
    <td>+14/+18</td>
    <td>+14/+29</td>
    <td>+14/+27</td>
  </tr>
  <tr>
    <th>Attack:</th>
    <td>+2 bastard sword +21 melee (1d10+8/17–20) or masterwork composite longbow (+4 Str bonus) (2d8+15/17–20) +17 ranged (1d8+4/×3)</td>
    <td>Bite +25 melee (2d6+16/19–20)</td>
    <td>Claw +24 melee (1d6+11) or +2 bastard sword +27 melee</td>
  </tr>
  <tr>
    <th>Full Attack:</th>
    <td>+2 bastard sword +21/+16/+11 melee (1d10+8/17–20) or masterwork composite longbow (+4 Str bonus) +17/+12/+7ranged (1d8+4/×3)</td>
    <td>Bite +25 melee (2d6+16/19–20)</td>
    <td>2 claws +24 melee (1d6+11) and bite +20 melee (2d6+5/19–20); or +2 bastard sword +27/+22/+17 melee (2d8+15/17–20) and bite +20 melee (2d6+5/19–20)</td>
  </tr>
  <tr>
    <th>Space/Reach:</th>
    <td>5 ft./5 ft.</td>
    <td>10 ft./5 ft.</td>
    <td>10 ft./10 ft.</td>
  </tr>
  <tr>
    <th>Special Attacks:</th>
    <td>—</td>
    <td>Curse of lycanthropy, trip</td>
    <td>Curse of lycanthropy</td>
  </tr>
  <tr>
    <th>Special Qualities:</th>
    <td>Alternate form, wolf empathy, low-light vision, scent</td>
    <td>Alternate form, wolf empathy, damage reduction 10/silver, low-light vision, scent</td>
    <td>Alternate form, wolf empathy, damage reduction 10/silver, low-light vision, scent</td>
  </tr>
  <tr>
    <th>Saves:</th>
    <td>Fort +16, Ref +12, Will +13</td>
    <td>Fort +17, Ref +12, Will +11</td>
    <td>Fort +17, Ref +12, Will +11</td>
  </tr>
  <tr>
    <th>Abilities:</th>
    <td>Str 18, Dex 14, Con 14 Int 10, Wis 12, Cha 12</td>
    <td>Str 32, Dex 18, Con 20 Int 10, Wis 12, Cha 12</td>
    <td>Str 32, Dex 18, Con 20 Int 10, Wis 12, Cha 12</td>
  </tr>
  <tr>
    <th>Skills:</th>
    <td>Handle Animal +4, Hide +6, Listen +9, Move Silently +8, Spot +13, Survival +5</td>
    <td>Handle Animal +4, Hide +6, Listen +9, Move Silently +12, Spot +13, Survival +5*</td>
    <td>Handle Animal +4, Hide +6, Listen +9, Move Silently +12, Spot +13, Survival +5*</td>
  </tr>
  <tr>
    <th>Feats:</th>
    <td>Alertness, Cleave, Combat Reflexes, Exotic Weapon Proficiency (bastard Sword), Improved Critical (bastard sword), Improved Critical (bite), Improved Natural Armor, Improved Natural Attack (bite), Iron WillB, Power Attack, Run, Stealthy, TrackB, Weapon Focus (bastard sword), Weapon Focus (bite), Weapon Specialization (bastard sword)</td>
    <td>(same as human form)</td>
    <td>(same as human form)</td>
  </tr>
  <tr>
    <th>Environment:</th>
    <td>Temperate forests</td>
    <td>Temperate forests</td>
    <td>Temperate forests</td>
  </tr>
  <tr>
    <th>Organization:</th>
    <td>Solitary, pair, or pack (1–2 werewolf lords plus 2–4 werewolves plus 5–8 wolves)</td>
    <td>(same as human form)</td>
    <td>(same as human form)</td>
  </tr>
  <tr>
    <th>Challenge Rating:</th>
    <td>14</td>
    <td>14</td>
    <td>14</td>
  </tr>
  <tr>
    <th>Treasure:</th>
    <td>Standard</td>
    <td>Standard</td>
    <td>Standard</td>
  </tr>
  <tr>
    <th>Alignment:</th>
    <td>Always chaotic evil</td>
    <td>Always chaotic evil</td>
    <td>Always chaotic evil</td>
  </tr>
  <tr>
    <th>Advancement:</th>
    <td>By character class</td>
    <td>By character class</td>
    <td>By character class</td>
  </tr>
  <tr>
    <th>Level Adjustment:</th>
    <td>+3</td>
    <td>+3</td>
    <td>+3</td>
  </tr>
</table>


Stronger, hardier, and more deadly than its lesser fellows, the werewolf lord is a murderous beast that delights in wreaking havoc.

The werewolf lord presented here is a 10th-level human fighter and natural lycanthrope, using the following base ability scores: Str 16, Dex 14, Con 14, Int 10, Wis 10, Cha 12.

#### Combat

Werewolf lords can assume a hybrid form as well as an animal form. In dire wolf form, they can trip just as normal wolves do. In hybrid form, they can wield weapons or fight with their claws.

__Alternate Form (Su):__ A werewolf lord can assume a bipedal hybrid form or the form of a dire wolf.

__Curse of Lycanthropy (Su):__ Any humanoid or giant hit by a werewolf lord’s bite attack in wolf or hybrid form must succeed on a DC 15 Fortitude save or contract lycanthropy.

__Trip (Ex):__ A werewolf lord in dire wolf form that hits with a bite attack can attempt to trip the opponent (+15 check modifier) as a free action without making a touch attack or provoking an attack of opportunity. If the attempt fails, the opponent cannot react to trip the werewolf lord.

__Wolf Empathy (Ex):__ Communicate with wolves and dire wolves, and +4 racial bonus on Charisma-based checks against wolves and dire wolves.

__Skills:__ &ast;A werewolf lord in hybrid or wolf form gains a +4 racial bonus on Survival checks when tracking by scent.

*Possessions:* *+2 mithral chain shirt*, *+3 heavy shield*, *+2 bastard sword*, *gauntlets of ogre power*, *cloak of resistance +2*, masterwork composite longbow (+4 Str bonus).

*Curse of Lycanthropy (Su):* Any humanoid or giant hit by a natural lycanthrope’s bite attack in animal or hybrid form must succeed on a DC 15 Fortitude save or contract lycanthropy. If the victim’s size is not within one size category of the lycanthrope the victim cannot contract lycanthropy from that lycanthrope. Afflicted lycanthropes cannot pass on the curse of lycanthropy.

__Special Qualities:__ A lycanthrope retains all the special qualities of the base creature and the base animal, and also gains those described below.

*Alternate Form (Su):* A lycanthrope can shift into animal form as though using the *polymorph* spell on itself, though its gear is not affected, it does not regain hit points for changing form, and only the specific animal form indicated for the lycanthrope can be assumed. It does not assume the ability scores of the animal, but instead adds the animal’s physical ability score modifiers to its own ability scores. A lycanthrope also can assume a bipedal hybrid form with prehensile hands and animalistic features.

Changing to or from animal or hybrid form is a standard action.

A slain lycanthrope reverts to its humanoid form, although it remains dead. Separated body parts retain their animal form, however.

Afflicted lycanthropes find this ability difficult to control (see Lycanthropy as an Affliction, below), but natural lycanthropes have full control over this power.

*Damage Reduction (Ex):* An afflicted lycanthrope in animal or hybrid form has damage reduction 5/silver. A natural lycanthrope in animal or hybrid form has damage reduction 10/silver.

*Lycanthropic Empathy (Ex):* In any form, lycanthropes can communicate and empathize with normal or dire animals of their animal form. This gives them a +4 racial bonus on checks when influencing the animal’s attitude and allows the communication of simple concepts and (if the animal is friendly) commands, such as “friend,” “foe,” “flee,” and “attack.”

*Low-Light Vision (Ex):* A lycanthrope has low-light vision in any form.

*Scent (Ex):* A lycanthrope has the scent ability in any form.

### Lycanthropy as an Affliction

When a character contracts lycanthropy through a lycanthrope’s bite (see above), no symptoms appear until the first night of the next full moon. On that night, the afflicted character involuntarily assumes animal form and forgets his or her own identity, temporarily becoming an NPC. The character remains in animal form, assuming the appropriate alignment, until the next dawn.

The character’s actions during this first episode are dictated by the alignment of its animal form. The character remembers nothing about the entire episode (or subsequent episodes) unless he succeeds on a DC 15 Wisdom check, in which case he becomes aware of his lycanthropic condition.

Thereafter, the character is subject to involuntary transformation under the full moon and whenever damaged in combat. He or she feels an overwhelming rage building up and must succeed on a Control Shape check (see below) to resist changing into animal form. Any player character not yet aware of his or her lycanthropic condition temporarily becomes an NPC during an involuntary change, and acts according to the alignment of his or her animal form.

A character with awareness of his condition retains his identity and does not lose control of his actions if he changes. However, each time he changes to his animal form, he must make a Will save (DC 15 + number of times he has been in animal form) or permanently assume the alignment of his animal form in all shapes.

Once a character becomes aware of his affliction, he can now voluntarily attempt to change to animal or hybrid form, using the appropriate Control Shape check DC. An attempt is a standard action and can be made each round. Any voluntary change to animal or hybrid form immediately and permanently changes the character’s alignment to that of the appropriate lycanthrope.

#### Changing Form

Changing form is a standard action. If the change is involuntary, the character performs the change on his next turn following the triggering event. Changing to animal or hybrid form ruins the character’s armor and clothing (including any items worn) if the new form is larger than the character’s natural form; carried items are simply dropped. Characters can hastily doff clothing while changing, but not armor. Magic armor survives the change if it succeeds on a DC 15 Fortitude save. An afflicted character who is not aware of his condition remains in animal form until the next dawn. An afflicted character who is aware of his or her condition (see above) can try to resume humanoid form following a change (voluntary or involuntary) with a Control Shape check, but if he fails his check, he remains in animal (or hybrid) form until the following dawn.

#### Curing Lycanthropy

An afflicted character who eats a sprig of belladonna (also called wolfsbane) within 1 hour of a lycanthrope’s attack can attempt a DC 20 Fortitude save to shake off the affliction. If a healer administers the herb, use the character’s save bonus or the healer’s Heal modifier, whichever is higher. The character gets only one chance, no matter how much belladonna is consumed. The belladonna must be reasonably fresh (picked within the last week).

However, fresh or not, belladonna is toxic. The character must succeed on a DC 13 Fortitude save or take 1d6 points of Strength damage. One minute later, the character must succeed on a second DC 13 save or take an additional 2d6 points of Strength damage.

A *remove disease* or *heal* spell cast by a cleric of 12th level or higher also cures the affliction, provided the character receives the spell within three days of the lycanthrope’s attack.

The only other way to remove the affliction is to cast *remove curse* or *break enchantment* on the character during one of the three days of the full moon. After receiving the spell, the character must succeed on a DC 20 Will save to break the curse (the caster knows if the spell works). If the save fails, the process must be repeated.

Characters undergoing this cure are often kept bound or confined in cages until the cure takes effect.

Only afflicted lycanthropes can be cured of lycanthropy.

### Lycanthropes as Characters

Becoming a lycanthrope does not change a character’s favored class but usually changes alignment (see above). This alignment change may cause characters of certain classes to lose some of their class features.

Lycanthrope characters possess the following racial traits.

— +2 Wisdom. Physical abilities are increased by the animal form’s ability modifiers when a lycanthrope changes to its hybrid or animal forms.

—Size same as the base creature or the base animal form.

—Low-light vision in any form.

—Scent in any form.

—Racial Hit Dice: A lycanthrope adds the Hit Dice of its animal form to its base Hit Dice for race, level, and class. These additional Hit Dice modify the lycanthrope’s base attack bonus and base saving throw bonuses accordingly.

—Racial Skills: A lycanthrope adds skill points for its animal Hit Dice much as if it had multiclassed into the animal type. It gains skill points equal to (2 + Int modifier, minimum 1) per Hit Die of the animal form. Any skills that appear in the animal’s description are treated as class skills for the lycanthrope’s animal levels. The lycanthrope’s maximum skill ranks are equal to its animal form Hit Dice + its racial Hit Dice (if any) + its class levels + 3. Any racial skill adjustments of the lycanthrope’s base race and its animal form (but not conditional adjustments) are added to its skill modifiers in any form.

—Racial Feats: Add the animal’s Hit Dice to the base character’s own Hit Dice to determine how many feats the character has. All lycanthropes gain Iron Will as a bonus feat.

— +2 natural armor bonus in any form.

—Special Qualities (see above): Alternate form, lycanthropic empathy, curse of lycanthropy (in animal or hybrid form only).

—Afflicted lycanthrope: damage reduction 5/silver (in animal or hybrid form only).

—Natural lycanthrope: damage reduction 10/silver (in animal or hybrid form only).

—Automatic Languages: As base creature.

—Favored Class: Same as the base creature.

—Level adjustment: Same as the base creature +2 (afflicted) or +3 (natural).

### Control Shape (Wis)

Any character who has contracted lycanthropy and is aware of his condition can learn Control Shape as a class skill. (An afflicted lycanthrope not yet aware of his condition can attempt Control Shape checks untrained.) This skill determines whether an afflicted lycanthrope can control his shape. A natural lycanthrope does not need this skill, since it has full control over its shape.

__Check (Involuntary Change):__ An afflicted character must make a check at moonrise each night of the full moon to resist involuntarily assuming animal form. An injured character must also check for an involuntary change after accumulating enough damage to reduce his hit points by one-quarter and again after each additional one-quarter lost.

<table>
  <tr>
    <th>Involuntary Change</th>
	  <th>Control Shape DC</th>
  </tr>
  <tr>
    <td>Resist involuntary change</td>
    <td>25</td>
  </tr>
</table>

On a failed check, the character must remain in animal form until the next dawn, when he automatically returns to his base form. A character aware of his condition may make one attempt to return to humanoid form (see below), but if he fails, he remains in animal form until the next dawn.

__Retry (Involuntary Change):__ Check to resist an involuntary change once each time a triggering event occurs.

__Check (Voluntary Change):__ In addition, an afflicted lycanthrope aware of his condition may attempt to use this skill voluntarily in order to change to animal form, assume hybrid form, or return to humanoid form, regardless of the state of the moon or whether he has been injured.

<table>
  <tr>
    <th>Involuntary Change</th>
    <th>Control Shape DC</th>
  </tr>
  <tr>
    <td>Return to humanoid form (full moon*)</td>
    <td>25</td>
  </tr>
  <tr>
    <td>Return to humanoid form (not full moon)</td>
    <td>20</td>
  </tr>
  <tr>
    <td>Assume hybrid form</td>
    <td>15</td>
  </tr>
  <tr>
    <td>Voluntary change to animal form (full moon)</td>
    <td>15</td>
  </tr>
  <tr>
    <td>Voluntary change to animal form (not full moon)</td>
    <td>20</td>
  </tr>
  <tfoot>
    <tr>
      <td colspan="2">* For game purposes, the full moon lasts three days every month.</td>
    </tr>
  </tfoot>
</table>

__Retry (Voluntary Change):__ A character can retry voluntary changes to animal form or hybrid form as often as he likes. Each attempt is a standard action. However, on a failed check to return to humanoid form, the character must remain in animal or hybrid form until the next dawn, when he automatically returns to humanoid form.

__Special:__ An afflicted lycanthrope cannot attempt a voluntary change until it becomes aware of its condition (see Lycanthropy as an Affliction).

See [Monsters K-L](srd/MonstersK-L.html)


Mummy
-----

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

<table>
  <tr>
    <th></th>
    <th>Mummy</th>
    <th>Mummy Lord, 10th-Level Cleric</th>
  </tr>
  <tr>
    <th></th>
    <th>Medium Undead</th>
    <th>Medium Undead</th>
  </tr>
  <tr>
    <th>Hit Dice:</th>
    <td>8d12+3 (55 hp)</td>
    <td>8d12 plus 10d8 (97 hp)</td>
  </tr>
  <tr>
    <th>Initiative:</th>
    <td>+0</td>
    <td>+5</td>
  </tr>
  <tr>
    <th>Speed:</th>
    <td>20 ft. (4 squares)</td>
    <td>15 ft. in half-plate armor (3 squares); base speed 20 ft.</td>
  </tr>
  <tr>
    <th>Armor Class:</th>
    <td>20 (+10 natural), touch 10, flat-footed 20</td>
    <td>30 (+1 Dex, +10 natural, +9 +2 half-plate armor), touch 11, flat-footed 29</td>
  </tr>
  <tr>
    <th>Base Attack/Grapple:</th>
    <td>+4/+11</td>
    <td>+11/+19</td>
  </tr>
  <tr>
    <th>Attack:</th>
    <td>Slam +11 melee (1d6+10 plus mummy rot)</td>
    <td>Slam +20 melee (1d6+12/19–20 plus mummy rot)</td>
  </tr>
  <tr>
    <th>Full Attack:</th>
    <td>Slam +11 melee (1d6+10 plus mummy rot)</td>
    <td>Slam +20 melee (1d6+12/19–20 plus mummy rot)</td>
  </tr>
  <tr>
    <th>Space/Reach:</th>
    <td>5 ft./5 ft.</td>
    <td>5 ft./5 ft.</td>
  </tr>
  <tr>
    <th>Special Attacks:</th>
    <td>Despair, mummy rot</td>
    <td>Despair, mummy rot, rebuke undead, spells</td>
  </tr>
  <tr>
    <th>Special Qualities:</th>
    <td>Damage reduction 5/–, darkvision 60 ft., undead traits, vulnerability to fire</td>
    <td>Damage reduction 5/–, darkvision 60 ft., resistance to fire 10, undead traits, vulnerability to fire</td>
  </tr>
  <tr>
    <th>Saves:</th>
    <td>Fort +4, Ref +2, Will +8</td>
    <td>Fort +13, Ref +8, Will +20</td>
  </tr>
  <tr>
    <th>Abilities:</th>
    <td>Str 24, Dex 10, Con —, Int 6, Wis 14, Cha 15</td>
    <td>Str 26, Dex 12, Con —, Int 8, Wis 20, Cha 17</td>
  </tr>
  <tr>
    <th>Skills:</th>
    <td>Hide +7, Listen +8, Move Silently +7, Spot +8</td>
    <td>Concentration +8, Knowledge (religion) +4, Listen +18, Move Silently +5, Spot +18</td>
  </tr>
  <tr>
    <th>Feats:</th>
    <td>Alertness, Great Fortitude, Toughness</td>
    <td>Alertness, Combat Casting, Great Fortitude, Improved Critical (slam), Improved Initiative, Weapon Focus (slam)</td>
  </tr>
  <tr>
    <th>Environment:</th>
    <td>Any</td>
    <td>Any</td>
  </tr>
  <tr>
    <th>Organization:</th>
    <td>Solitary, warden squad (2–4), or guardian detail (6–10)</td>
    <td>Solitary or tomb guard (1 mummy lord and 6–10 mummies)</td>
  </tr>
  <tr>
    <th>Challenge Rating:</th>
    <td>5</td>
    <td>15</td>
  </tr>
  <tr>
    <th>Treasure:</th>
    <td>Standard</td>
    <td>Standard plus possessions noted below</td>
  </tr>
  <tr>
    <th>Alignment:</th>
    <td>Usually lawful evil</td>
    <td>Usually lawful evil</td>
  </tr>
  <tr>
    <th>Advancement:</th>
    <td>9–16 HD (Medium); 17–24 HD (Large)</td>
    <td>By character class</td>
  </tr>
  <tr>
    <th>Level Adjustment:</th>
    <td>—</td>
    <td>—</td>
  </tr>
</table>

Mummies are preserved corpses animated through the auspices of dark desert gods best forgotten.

Most mummies are 5 to 6 feet tall and weigh about 120 pounds.

Mummies can speak Common, but seldom bother to do so.

### Combat

__Despair (Su):__ At the mere sight of a mummy, the viewer must succeed on a DC 16 Will save or be paralyzed with fear for 1d4 rounds. Whether or not the save is successful, that creature cannot be affected again by the same mummy’s despair ability for 24 hours. The save DC is Charisma-based.

__Mummy Rot (Su):__ Supernatural disease—slam, Fortitude DC 16, incubation period 1 minute; damage 1d6 Con and 1d6 Cha. The save DC is Charisma-based.

Unlike normal diseases, mummy rot continues until the victim reaches Constitution 0 (and dies) or is cured as described below.

Mummy rot is a powerful curse, not a natural disease. A character attempting to cast any conjuration (healing) spell on a creature afflicted with mummy rot must succeed on a DC 20 caster level check, or the spell has no effect on the afflicted character.

To eliminate mummy rot, the curse must first be broken with *break enchantment* or *remove curse* (requiring a DC 20 caster level check for either spell), after which a caster level check is no longer necessary to cast healing spells on the victim, and the mummy rot can be magically cured as any normal disease.

An afflicted creature who dies of mummy rot shrivels away into sand and dust that blow away into nothing at the first wind.

<h3 id="mummylord">Mummy Lord</h3>

Unusually powerful or evil individuals preserved as mummies sometimes rise as greater mummies after death. A mummy lord resembles its lesser fellows, but often wears or carries equipment it used in life.

Mummy lords are often potent spellcasters. They are found as guardians of the tombs of high lords, priests, and mages. Most are sworn to defend for eternity the resting place of those whom they served in life, but in some cases a mummy lord’s unliving state is the result of a terrible curse or rite designed to punish treason, infidelity, or crimes of an even more abhorrent nature. A mummy lord of this sort is usually imprisoned in a tomb that is never meant to be opened again.

__Despair (Su)__: The save DC against this mummy lord’s despair is 17.

__Mummy Rot (Su)__: The save DC against this mummy lord’s mummy rot is 17.

*Typical Cleric Spells Prepared* (6/7/6/5/5/4; save DC 15 + spell level): 0—*detect magic* (2), *guidance*, *read magic*, *resistance*, *virtue*; 1st—*bane*, *command*, *deathwatch*, *divine favor*, *doom*, *sanctuary*&ast;, *shield of faith*; 2nd—*bull’s strength*, *death knell*&ast;, *hold person*, *resist energy*, *silence*, *spiritual weapon*; 3rd—*animate dead*&ast;, *deeper darkness*, *dispel magic*, *invisibility purge*, *searing light*; 4th—*air walk*, *dismissal*, *divine power*, *giant vermin*, *spell immunity*&ast;; 5th—*insect plague*, *slay living*&ast;, *spell resistance*, *symbol of pain*.

&ast;Domain Spell. Domains: Death and Protection.

*Possessions*: *+2 half-plate armor*, *cloak of resistance +2*, *ring of minor elemental resistance (fire)*, *brooch of shielding*. (Different mummy lords may have different possessions.)

See [Monsters M-N](MonstersM-N.html)


<h2 id="sahuagin">Sahuagin</h2>
 
<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Medium Monstrous Humanoid (Aquatic)**
- **Hit Dice:** 2d8+2 (11 hp)
- **Initiative:** +1
- **Speed:** 30 ft. (6 squares), swim 60 ft.
- **Armor Class:** 16 (+1 Dex, +5 natural), touch 11, flat-footed 15
- **Base Attack/Grapple:** +2/+4
- **Attack:** Talon +4 melee (1d4+2) or trident +4 melee (1d8+3) or heavy crossbow +3 ranged (1d10/19–20)
- **Full Attack:** Trident +4 melee (1d8+3) and bite +2 melee (1d4+1); or 2 talons +4 melee (1d4+2) and bite +2 melee (1d4+1); or heavy crossbow +3 ranged (1d10/19–20)
- **Space/Reach:** 5 ft./5 ft.
- **Special Attacks:** Blood frenzy, rake 1d4+1
- **Special Qualities:** Blindsense 30 ft., darkvision 60 ft., freshwater sensitivity, light blindness, speak with sharks, water dependent
- **Saves:** Fort +3, Ref +4, Will +4
- **Abilities:** Str 14, Dex 13, Con 12, Int 14, Wis 13, Cha 9
- **Skills:** Handle Animal +4&ast;, Hide +6&ast;, Listen +6&ast;, Profession (hunter) +1&ast;, Ride +3, Spot +6&ast;, Survival +1&ast;
- **Feats:** Great Fortitude, Multiattack<sup>B</sup>
- **Environment:** Warm aquatic
- **Organization:** Solitary, pair, team (5–8), patrol (11–20 plus 1 3rd-level lieutenant and 1–2 sharks), band (20–80 plus 100% noncombatants plus 1 3rd-level lieutenant and 1 4th-level chieftain per 20 adults plus 1–2 sharks), or tribe (70–160 plus 100% noncombatants plus 1 3rd-level lieutenant per 20 adults, 1 4th-level chieftain per 40 adults, 9 4th-level guards, 1–4 underpriestesses of 3rd–6th level, 1 7th-level priestess, and 1 baron of 6th–8th level plus 5–8 sharks)
- **Challenge Rating:** 2
- **Treasure:** Standard
- **Alignment:** Usually lawful evil
- **Advancement:** 3–5 HD (Medium), 6–10 HD (Large), or by character class
- **Level Adjustment:** +2 (+3 if four-armed)

Most sahuagin feature green coloration, darker along the back and lighter on the belly. Many have dark stripes, bands, or spots, but these tend to fade with age. An adult male sahuagin stands roughly 6 feet tall and weighs about 200 pounds.

Sahuagin are the natural enemy of aquatic elves. The two cannot coexist peacefully: Wars between them are prolonged, bloody affairs that sometimes interfere with shipping and maritime trade. Sahuagin have an only slightly less vehement hatred for tritons.

Sahuagin speak their own language, Sahuagin. Thanks to their high Intelligence scores, most sahuagin also speak two bonus languages, usually Common and Aquan.

### Combat

Sahuagin are savage fighters, asking for and giving no quarter. When swimming, a sahuagin tears with its feet as it strikes with its talons or a weapon. About half of any group of sahuagin are also armed with nets.

__Blindsense (Ex):__ A sahuagin can locate creatures underwater within a 30-foot radius. This ability works only when the sahuagin is underwater.

__Blood Frenzy:__ Once per day a sahuagin that takes damage in combat can fly into a frenzy in the following round, clawing and biting madly until either it or its opponent is dead. It gains +2 Constitution and +2 Strength, and takes a –2 penalty to Armor Class. A sahuagin cannot end its frenzy voluntarily.

__Rake (Ex):__ Attack bonus +2 melee, damage 1d4+1. A sahuagin also gains two rake attacks when it attacks while swimming.

__Freshwater Sensitivity (Ex):__ A sahuagin fully immersed in fresh water must succeed on a DC 15 Fortitude save or become fatigued. Even on a success, it must repeat the save attempt every 10 minutes it remains immersed.

__Light Blindness (Ex):__ Abrupt exposure to bright light (such as sunlight or a *daylight* spell) blinds sahuagin for round. On subsequent rounds, they are dazzled while operating in bright light.

__Speak with Sharks (Ex):__ Sahuagin can communicate telepathically with sharks up to 150 feet away. The communication is limited to fairly simple concepts such as “food,” “danger,” and “enemy.” Sahuagin can use the Handle Animal skill to befriend and train sharks.

__Water Dependent (Ex):__ Sahuagin can survive out of the water for 1 hour per 2 points of Constitution (after that, refer to the drowning rules in [Wilderness and Environment](srd/WildernessandEnvironment.html)).

__Skills:__ A sahuagin has a +8 racial bonus on any Swim check to perform some special action or avoid a hazard. It can always choose to take 10 on a Swim check, even if distracted or endangered. It can use the run action while swimming, provided it swims in a straight line.

&ast;Underwater, a sahuagin has a +4 racial bonus on Hide, Listen, and Spot checks.

&ast;A sahuagin has a +4 racial bonus on Survival and Profession (hunter) checks within 50 miles of its home.

&ast;A sahuagin has a +4 racial bonus on Handle Animal checks when working with sharks.

### Sahuagin Mutants

About one in two hundred sahuagin has four arms. Such creatures can make four claw attacks or use extra weapons, in addition to the claw and bite attacks.

If a community of aquatic elves is located within 100 miles of a sahuagin community, about one in one hundred sahuagin looks just like an aquatic elf. These creatures, called malenti, have a swim speed of 40 feet, can remain out of water for 1 hour per point of Constitution, and have freshwater sensitivity and light sensitivity (dazzled in bright light). Malenti have no natural attacks. They are otherwise identical with sahuagin.


<h2 id="vampire">Vampire</h2>

<table>
  <tr>
    <th></th>
    <th>Vampire, 5th-Level Human Fighter</th>
    <th>Elite Vampire, 13th-Level Half-Elf Monk/Shadowdancer</th>
  </tr>
  <tr>
    <th></th>
    <th>Medium Undead (Augmented Humanoid)</th>
    <th>Medium Undead (Augmented Humanoid)</th>
  </tr>
  <tr>
    <th>Hit Dice:</th>
    <td>5d12 (32 hp)</td>
    <td>13d12 (90 hp)</td>
  </tr>
  <tr>
    <th>Initiative:</th>
    <td>+7</td>
    <td>+8</td>
  </tr>
  <tr>
    <th>Speed:</th>
    <td>30 ft. (6 squares)</td>
    <td>60 ft. (12 squares)</td>
  </tr>
  <tr>
    <th>Armor Class:</th>
    <td>23 (+3 Dex, +6 natural, +4 masterwork chain shirt), touch 13, flat-footed 20</td>
    <td>32 (+4 Dex, +6 natural, +6 Wis, +1 intrinsic, <em>bracers of armor +3</em>, <em>ring of protection +2</em>), touch 23, flat-footed 32</td>
  </tr>
  <tr>
    <th>Base Attack/Grapple:</th>
    <td>+5/+11</td>
    <td>+9/+14</td>
  </tr>
  <tr>
    <th>Attack:</th>
    <td>Slam +11 melee (1d6+9 plus energy drain) or <em>+1 spiked chain</em> +13 melee (2d4+12) or masterwork shortbow +9 ranged (1d6/&times;3)</td>
    <td>Unarmed strike +14 melee (1d10+5 plus energy drain) or <em>+2 keen kama</em> +16 melee (1d6+7) or <em>+1 frost sling</em> +14 ranged (1d4+1 plus 1d6 cold)</td>
  </tr>
  <tr>
    <th>Full Attack:</th>
    <td>Slam +11 melee (1d6+9 plus energy drain) or <em>+1 spiked chain</em> +13 melee (2d4+12) or masterwork shortbow +9 ranged (1d6/&times;3)</td>
    <td>Unarmed strike +14/+14/9 melee (1d10+5 plus energy drain) or <em>+2 keen kama</em> +16/+16/+11 melee (1d6+7) or <em>+1 frost sling</em> +14 ranged (1d4+1 plus 1d6 cold)</td>
  </tr>
  <tr>
    <th>Space/Reach:</th>
    <td>5 ft./5 ft. (10 ft. with spiked chain)</td>
    <td>5 ft./5 ft.</td>
  </tr>
  <tr>
    <th>Special Attacks:</th>
    <td>Blood drain, children of the night, create spawn, dominate, energy drain</td>
    <td>Blood drain, children of the night, create spawn, dominate, energy drain, furry of blows, <em>ki</em> strike (magic), <em>shadow illusion</em>, <em>summon shadow</em></td>
  </tr>
  <tr>
    <th>Special Qualities:</th>
    <td>Alternate form, damage reduction 10/silver and magic, darkvision 60 ft., fast healing 5, gaseous form, resistance to cold 10 and electricity 10, spider climb, undead traits, vampire weaknesses</td>
    <td>Alternate form, damage reduction 10/silver and magic, darkvision 60 ft., fast healing 5, gaseous form, half-elf traits, hide in plain sight, improved evasion, purity of body, resistance to cold 10 and electricity 10, <em>shadow jump</em> 20 ft., slow fall 40 ft., spider climb, still mind, uncanny dodge (Dex bonus to AC), undead traits, vampire weaknesses, wholeness of body</td>
  </tr>
  <tr>
    <th>Saves:</th>
    <td>Fort +4, Ref +6, Will +4</td>
    <td>Fort +7, Ref +16, Will +213</td>
  </tr>
  <tr>
    <th>Abilities:</th>
    <td>Str 22, Dex 17, Con —, Int 12, Wis 16, Cha 12</td>
    <td>Str 20, Dex 19, Con —, Int 12, Wis 22, Cha 12</td>
  </tr>
  <tr>
    <th>Skills:</th>
    <td>Bluff +9, Climb +10, Hide +10, Listen +17, Move Silently +10, Ride +11, Search +9, Sense Motive +11, Spot +17</td>
    <td>Balance +22, Bluff +9, Climb +9, Hide +28, Jump +23, Listen +16, Move Silently +28, Perform (wind instruments) +7, Search +9, Sense Motive +14, Spot +16, Tumble +22</td>
  </tr>
  <tr>
    <th>Feats:</th>
    <td>Alertness<sup>B</sup>, Blind-Fight, Combat Reflexes<sup>B</sup>, Dodge<sup>B</sup>, Exotic Weapon Proficiency (spiked chain), Improved Initiative<sup>B</sup>, Lightning Reflexes<sup>B</sup>, Mobility, Power Attack, Weapon Focus (spiked chain), Weapon Specialization (spiked chain)</td>
    <td>Alertness, Blind-Fight, Combat Reflexes, Deflect Arrows<sup>B</sup>, Dodge, Improved Initiative, Improved Disarm<sup>B</sup>, Lightning Reflexes, Mobility, Spring Attack, Stunning Fist<sup>B</sup></td>
  </tr>
  <tr>
    <th>Environment:</th>
    <td>Temperate plains</td>
    <td>Temperate forests</td>
  </tr>
  <tr>
    <th>Organization:</th>
    <td>Solitary</td>
    <td>Solitary</td>
  </tr>
  <tr>
    <th>Challenge Rating:</th>
    <td>7</td>
    <td>15</td>
  </tr>
  <tr>
    <th>Treasure:</th>
    <td>Double Standard</td>
    <td>Double Standard</td>
  </tr>
  <tr>
    <th>Alignment:</th>
    <td>Always evil (any)</td>
    <td>Always evil (any)</td>
  </tr>
  <tr>
    <th>Advancement:</th>
    <td>By character class</td>
    <td>By character class</td>
  </tr>
  <tr>
    <th>Level Adjustment:</th>
    <td>+5</td>
    <td>+8</td>
  </tr>
</table>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

Vampires appear just as they did in life, although their features are often hardened and feral, with the predatory look of wolves.

Like liches, they often embrace finery and decadence and may assume the guise of nobility. Despite their human appearance, vampires can be easily recognized, for they cast no shadows and throw no reflections in mirrors.

Vampires speak any languages they knew in life.

__Special Attacks:__ A vampire retains all the special attacks of the base creature and gains those described below. Saves have a DC of 10 + 1/2 vampire’s HD + vampire’s Cha modifier unless noted otherwise.

__*Blood Drain (Ex):*__ A vampire can suck blood from a living victim with its fangs by making a successful grapple check. If it pins the foe, it drains blood, dealing 1d4 points of Constitution drain each round the pin is maintained. On each such successful attack, the vampire gains 5 temporary hit points.

__*Children of the Night (Su):*__ Vampires command the lesser creatures of the world and once per day can call forth 1d6+1 rat swarms, 1d4+1 bat swarms, or a pack of 3d6 wolves as a standard action. (If the base creature is not terrestrial, this power might summon other creatures of similar power.) These creatures arrive in 2d6 rounds and serve the vampire for up to 1 hour.

__*Dominate (Su):*__ A vampire can crush an opponent’s will just by looking onto his or her eyes. This is similar to a gaze attack, except that the vampire must use a standard action, and those merely looking at it are not affected. Anyone the vampire targets must succeed on a Will save or fall instantly under the vampire’s influence as though by a dominate person spell (caster level 12th). The ability has a range of 30 feet.

__*Create Spawn (Su):*__ A humanoid or monstrous humanoid slain by a vampire’s energy drain rises as a vampire spawn (see the Vampire Spawn entry) 1d4 days after burial.

If the vampire instead drains the victim’s Constitution to 0 or lower, the victim returns as a spawn if it had 4 or less HD and as a vampire if it had 5 or more HD. In either case, the new vampire or spawn is under the command of the vampire that created it and remains enslaved until its master’s destruction. At any given time a vampire may have enslaved spawn totaling no more than twice its own Hit Dice; any spawn it creates that would exceed this limit are created as free-willed vampires or vampire spawn. A vampire that is enslaved may create and enslave spawn of its own, so a master vampire can control a number of lesser vampires in this fashion. A vampire may voluntarily free an enslaved spawn in order to enslave a new spawn, but once freed, a vampire or vampire spawn cannot be enslaved again.

__*Energy Drain (Su):*__ Living creatures hit by a vampire’s slam attack (or any other natural weapon the vampire might possess) gain two negative levels. For each negative level bestowed, the vampire gains 5 temporary hit points. A vampire can use its energy drain ability once per round.

__Special Qualities:__ A vampire retains all the special qualities of the base creature and gains those described below.

*Alternate Form (Su):* A vampire can assume the shape of a bat, dire bat, wolf, or dire wolf as a standard action. This ability is similar to a *polymorph* spell cast by a 12th-level character, except that the vampire does not regain hit points for changing form and must choose from among the forms mentioned here. While in its alternate form, the vampire loses its natural slam attack and dominate ability, but it gains the natural weapons and extraordinary special attacks of its new form. It can remain in that form until it assumes another or until the next sunrise. (If the base creature is not terrestrial, this power might allow other forms.)

*Damage Reduction (Su):* A vampire has damage reduction 10/silver and magic. A vampire’s natural weapons are treated as magic weapons for the purpose of overcoming damage reduction.

*Fast Healing (Ex):* A vampire heals 5 points of damage each round so long as it has at least 1 hit point. If reduced to 0 hit points in combat, it automatically assumes gaseous form and attempts to escape. It must reach its coffin home within 2 hours or be utterly destroyed. (It can travel up to nine miles in 2 hours.) Any additional damage dealt to a vampire forced into gaseous form has no effect. Once at rest in its coffin, a vampire is helpless. It regains 1 hit point after 1 hour, then is no longer helpless and resumes healing at the rate of 5 hit points per round.

*Gaseous Form (Su):* As a standard action, a vampire can assume gaseous form at will as the spell (caster level 5th), but it can remain gaseous indefinitely and has a fly speed of 20 feet with perfect maneuverability.

*Resistances (Ex):* A vampire has resistance to cold 10 and electricity 10.

*Spider Climb (Ex):* A vampire can climb sheer surfaces as though with a *spider climb* spell.

*Turn Resistance (Ex):* A vampire has +4 turn resistance.

&ast; Vampires have a +8 racial bonus on Bluff, Hide, Listen, Move Silently, Search, Sense Motive, and Spot checks. Otherwise same as the base creature.

B: Vampires gain Alertness, Combat Reflexes, Dodge, Improved Initiative, and Lightning Reflexes, assuming the base creature meets the prerequisites and doesn’t already have these feats.

#### Vampire Weaknesses

For all their power, vampires have a number of weaknesses.

__Repelling a Vampire:__ Vampires cannot tolerate the strong odor of garlic and will not enter an area laced with it. Similarly, they recoil from a mirror or a strongly presented holy symbol. These things don’t harm the vampire—they merely keep it at bay. A recoiling vampire must stay at least 5 feet away from a creature holding the mirror or holy symbol and cannot touch or make melee attacks against the creature holding the item for the rest of the encounter. Holding a vampire at bay takes a standard action.

Vampires are also unable to cross running water, although they can be carried over it while resting in their coffins or aboard a ship.

They are utterly unable to enter a home or other building unless invited in by someone with the authority to do so. They may freely enter public places, since these are by definition open to all.

__Slaying a Vampire:__ Reducing a vampire’s hit points to 0 or lower incapacitates it but doesn’t always destroy it (see the note on fast healing). However, certain attacks can slay vampires. Exposing any vampire to direct sunlight disorients it: It can take only a single move action or attack action and is destroyed utterly in the next round if it cannot escape. Similarly, immersing a vampire in running water robs it of one-third of its hit points each round until it is destroyed at the end of the third round of immersion. Driving a wooden stake through a vampire’s heart instantly slays the monster. However, it returns to life if the stake is removed, unless the body is destroyed. A popular tactic is to cut off the creature’s head and fill its mouth with holy wafers (or their equivalent).

#### Vampire Characters

Vampires are always evil, which causes characters of certain classes to lose some class abilities. In addition, certain classes take additional penalties.

__Clerics:__ Vampire clerics lose their ability to turn undead but gain the ability to rebuke undead. This ability does not affect the vampire’s controller or any other vampires that a master controls. A vampire cleric has access to two of the following domains: Chaos, Destruction, Evil, or Trickery.

__Sorcerers and Wizards:__ Vampire sorcerers and wizards retain their class abilities, but if a character has a familiar other than a rat or bat, the link between them is broken, and the familiar shuns its former companion. The character can summon another familiar, but it must be a rat or bat.

See [Monsters T-Z](srd/MonstersT-Z.html)


Vampire Spawn
-------------
	
- **Medium Undead**
- **Hit Dice:** 4d12+3 (29 hp)
- **Initiative:** +6
- **Speed:** 30 ft. (6 squares)
- **Armor Class:** 15 (+2 Dex, +3 natural), touch 12, flat-footed 13
- **Base Attack/Grapple:** +2/+5
- **Attack:** Slam +5 melee (1d6+4 plus energy drain)
- **Full Attack:** Slam +5 melee (1d6+4 plus energy drain)
- **Space/Reach:** 5 ft./5 ft.
- **Special Attacks:** Blood drain, domination, energy drain
- **Special Qualities:** +2 turn resistance, damage reduction 5/silver, darkvision 60 ft., fast healing 2, gaseous form, resistance to cold 10 and electricity 10, spider climb, undead traits
- **Saves:** Fort +1, Ref +5, Will +5
- **Abilities:** Str 16, Dex 14, Con &mdash;, Int 13, Wis 13, Cha 14
- **Skills:** Bluff +6, Climb +8, Craft or Profession (any one) +4, Diplomacy +4, Hide +10, Jump +8, Listen +11, Move Silently +10, Search +8, Sense Motive +11, Spot +11
- **Feats:** Alertness<sup>B</sup>, Improved Initiative<sup>B</sup>, Lightning Reflexes<sup>B</sup>, Skill Focus (selected Craft or Profession skill), Toughness
- **Environment:** Any
- **Organization:** Solitary or pack (2–5)
- **Challenge Rating:** 4
- **Treasure:** Standard
- **Alignment:** Always evil (any)
- **Advancement:** &mdash;
- **Level Adjustment:** &mdash;

Vampire spawn are undead creatures that come into being when vampires slay mortals. Like their creators, spawn remain bound to their coffins and to the soil of their graves. Vampire spawn appear much as they did in life, although their features are often hardened, with a predatory look.

Vampire spawn speak Common.

### Combat

Vampire spawn use their inhuman strength when engaging mortals, hammering their foes with powerful blows and dashing them against rocks or walls. They also use their gaseous form and flight abilities to strike where opponents are most vulnerable.

__Blood Drain (Ex):__ A vampire spawn can suck blood from a living victim with its fangs by making a successful grapple check. If it pins the foe, it drains blood, dealing 1d4 points of Constitution drain each round. On each such successful drain attack, the vampire spawn gains 5 temporary hit pints.

__Domination (Su):__ A vampire spawn can crush an opponent’s will just by looking onto his or her eyes. This is similar to a gaze attack, except that the vampire must take a standard action, and those merely looking at it are not affected. Anyone the vampire targets must succeed on a DC 14 Will save or fall instantly under the vampire’s influence as though by a dominate person spell from a 5thlevel caster. The ability has a range of 30 feet. The save DC is Charisma-based.

__Energy Drain (Su):__ Living creatures hit by a vampire spawn’s slam attack gain one negative level. The DC is 14 for the Fortitude save to remove a negative level. The save DC is Charisma-based. For each such negative level bestowed, the vampire spawn gains 5 temporary hit points.

__Fast Healing (Ex):__ A vampire spawn heals 2 points of damage each round so long as it has at least 1 hit point. If reduced to 0 hit points in combat, it automatically assumes gaseous form and attempts to escape. It must reach its coffin home within 2 hours or be utterly destroyed. (It can travel up to nine miles in 2 hours.) Once at rest in its coffin, it is helpless. It regains 1 hit point after 1 hour, then is no longer helpless and resumes healing at the rate of 2 hit points per round.

__Gaseous Form (Su):__ As a standard action, a vampire spawn can assume *gaseous form* at will as the spell (caster level 6th), but it can remain gaseous indefinitely and has a fly speed of 20 feet with perfect maneuverability.

__Spider Climb (Ex):__ A vampire spawn can climb sheer surfaces as though with a *spider climb* spell.

__Skills:__ Vampire spawn have a +4 racial bonus on Bluff, Hide, Listen, Move Silently, Search, Sense Motive, and Spot checks.

### Vampire Spawn Weaknesses

Vampire spawn are vulnerable to all attacks and effects that repel or slay vampires. For details, see the Vampire entry.
