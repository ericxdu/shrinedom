Fundamental Character Classes
=============================

<table style="float:right;">
  <caption>Table: Core Class Statistics</caption>
  <tr>
    <th>Class</th>
    <th>Hit Die</th>
    <th>Attack Bonus</th>
    <th>Good Saving Throws</th>
    <th>Skill Points</th>
  </tr>
  <tr>
    <td>Fighter</td>
    <td>d10</td>
    <td>Level &times;1</td>
    <td>Fort</td>
    <td>2 + Int mod</td>
  </tr>
  <tr>
    <td>Cleric</td>
    <td>d8</td>
    <td>Level &times;3&frasl;4</td>
    <td>Fort, Will</td>
    <td>2 + Int mod</td>
  </tr>
  <tr>
    <td>Rogue</td>
    <td>d6</td>
    <td>Level &times;3&frasl;4</td>
    <td>Ref</td>
    <td>8 + Int mod</td>
  </tr>
</table>

<table style="clear:both;float:right;">
  <caption>Table: Modified Class Statistics</caption>
  <tr>
    <th>Class</th>
    <th>Hit Die</th>
    <th>Attack Bonus</th>
    <th>Good Saving Throws</th>
    <th>Skill Points</th>
  </tr>
  <tr>
    <td>Fighter</td>
    <td>d10</td>
    <td>Level &times;1</td>
    <td>Fort, Ref</td>
    <td>4 + Int mod</td>
  </tr>
  <tr>
    <td>Healer</td>
    <td>d8</td>
    <td>Level &times;3&frasl;4</td>
    <td>Fort, Will</td>
    <td>6 + Int mod</td>
  </tr>
  <tr>
    <td>Expert</td>
    <td>d6</td>
    <td>Level &times;3&frasl;4</td>
    <td>Ref, Will</td>
    <td>8 + Int mod</td>
  </tr>
</table>

<table style="clear:both;float:right;">
  <caption>Table: Special Abilities</caption>
  <tr>
    <th>Class</th>
    <th>Special Abilities</th>
  </tr>
  <tr>
    <td>Fighter</td>
    <td>Strength, Dexterity, Constitution</td>
  </tr>
  <tr>
    <td>Healer</td>
    <td>Constitution, Wisdom, Charisma</td>
  </tr>
  <tr>
    <td>Expert</td>
    <td>Dexterity, Intelligence, Wisdom</td>
  </tr>
</table>

The fighter class, the cleric class, and the rogue class are considered 
<q>fundamental to any culture or race</q> (<cite>CRIIv.3.0</cite>, p. 
21). I take this to mean the character concepts exemplified by these 
classes are integral to the skill and combat system, so here we will 
experiment with generalizing them to work within other genres like 
adventure, science-fiction, etc..

## What Needed to Change

The fighter is general enough, simply receiving bonus feats. The rogue 
is generalized to learn a myriad of skills, but leans a little hard on 
combat and trapfinding abilities. The cleric is specifically a magic 
user and priest, which doesn't fit into many genres.

### Saving Throws and Skill Points

Since a lack of specific class features makes them rather underpowered; 
the fighter, cleric, and rogue foundations are given a boost to their 
saving throws and skill points.

### Bonus Feats

Each class gains bonus feats in a similar manner to the core class 
fighter. This opens up many customization options.

### Special Abilities

Each class gains special abilities from a list according its three most 
important ability scores.

### Weapon and Armor Proficiency

Not all genres include martial weapons and armor as widely-used 
equipment. By default the foundational classes are proficient with all 
simple weapons, but not with any type of armor or shields. The DM may 
decide to modify these base proficiencies depending on the setting.


Fighter
-------

<table style="float:right;">
<caption>Table: The Fighter</caption>
<tr><th>Level</th><th>Base Attack Bonus</th><th>Fort Save</th><th>Ref Save</th><th>Will Save</th><th>Special</th></tr>
<tr><td>1st </td><td>+1            </td><td>+2 </td><td>+2 </td><td>+0</td><td>Bonus feat, special ability</td></tr>
<tr><td>2nd </td><td>+2            </td><td>+3 </td><td>+3 </td><td>+0</td><td>Bonus feat                 </td></tr>
<tr><td>3rd </td><td>+3            </td><td>+3 </td><td>+3 </td><td>+1</td><td>Special ability            </td></tr>
<tr><td>4th </td><td>+4            </td><td>+4 </td><td>+4 </td><td>+1</td><td>Bonus feat                 </td></tr>
<tr><td>5th </td><td>+5            </td><td>+4 </td><td>+4 </td><td>+1</td><td>Special ability            </td></tr>
<tr><td>6th </td><td>+6/+1         </td><td>+5 </td><td>+5 </td><td>+2</td><td>Bonus feat                 </td></tr>
<tr><td>7th </td><td>+7/+2         </td><td>+5 </td><td>+5 </td><td>+2</td><td>Special ability            </td></tr>
<tr><td>8th </td><td>+8/+3         </td><td>+6 </td><td>+6 </td><td>+2</td><td>Bonus feat                 </td></tr>
<tr><td>9th </td><td>+9/+4         </td><td>+6 </td><td>+6 </td><td>+3</td><td>Special ability            </td></tr>
<tr><td>10th</td><td>+10/+5        </td><td>+7 </td><td>+7 </td><td>+3</td><td>Bonus feat                 </td></tr>
<tr><td>11th</td><td>+11/+6/+1     </td><td>+7 </td><td>+7 </td><td>+3</td><td>Special ability            </td></tr>
<tr><td>12th</td><td>+12/+7/+2     </td><td>+8 </td><td>+8 </td><td>+4</td><td>Bonus feat                 </td></tr>
<tr><td>13th</td><td>+13/+8/+3     </td><td>+8 </td><td>+8 </td><td>+4</td><td>Special ability            </td></tr>
<tr><td>14th</td><td>+14/+9/+4     </td><td>+9 </td><td>+9 </td><td>+4</td><td>Bonus feat                 </td></tr>
<tr><td>15th</td><td>+15/+10/+5    </td><td>+9 </td><td>+9 </td><td>+5</td><td>Special ability            </td></tr>
<tr><td>16th</td><td>+16/+11/+6/+1 </td><td>+10</td><td>+10</td><td>+5</td><td>Bonus feat                 </td></tr>
<tr><td>17th</td><td>+17/+12/+7/+2 </td><td>+10</td><td>+10</td><td>+5</td><td>Special ability            </td></tr>
<tr><td>18th</td><td>+18/+13/+8/+3 </td><td>+11</td><td>+11</td><td>+6</td><td>Bonus feat                 </td></tr>
<tr><td>19th</td><td>+19/+14/+9/+4 </td><td>+11</td><td>+11</td><td>+6</td><td>Special ability            </td></tr>
<tr><td>20th</td><td>+20/+15/+10/+5</td><td>+12</td><td>+12</td><td>+6</td><td>Bonus feat                 </td></tr>
</table>

The fighter is the brawn of the party, designed to be brave and stalwart 
in battle. This character is strong, fast, and tough.

### Game Rule Information

The fighter has the following game statistics.

**Abilities**: Strength, Dexterity, and Constitution.

**Alignment**: Any.

**Hit Die**: d10.

**Base Attack Bonus**: Equal to total Hit Dice (as fighter).

**Good Saving Throws**: Fortitude and Reflex.

#### Class Skills

The fighter's class skills (and the key ability for each skill) are: Balance (Dex), Climb (Str), Concentration (Con), Craft (Int), Diplomacy (Cha), Escape Artist (Dex), Handle Animal (Cha), Heal (Wis), Hide (Dex), Intimidate (Cha), Jump (Str), Knowledge (arcana) (Int), Knowledge (nobility and royalty) (Int), Knowledge (religion) (Int), Listen (Wis), Move Silently (Dex), Perform (Cha), Profession (Wis), Ride (Dex), Sense Motive (Wis), Spot (Wis), Survival (Wis), Swim (Str), and Tumble (Dex). See [Skills I](srd/SkillsI.html) and [Skills II](srd/SkillsII.html) for skill descriptions.

**Skill Points at 1st Level**: (4 + Int modifier) x 4.

**Skill Points at Each Additional Level**: 4 + Int modifier.

#### Class Features

All of the following are class features of the fighter.

**Weapon and Armor Proficiency**: Characters in this class are 
proficient with all simple weapons. They are not proficient with any 
type of armor or shield.

**Bonus Feats**: At 1st level, this class gains a bonus feat in addition 
to the feat that any 1st-level character gets and the bonus feat granted 
to a human character. The class gains an additional bonus feat at 2nd 
level and every two class levels thereafter (4th, 6th, 8th, 10th, 12th, 
14th, 16th, 18th, and 20th). These bonus feats may be drawn from [SRD 
Feats](srd/Feats.html) or any other list approved by the DM.

These bonus feats are in addition to the feat that a character of any 
class gets from advancing levels.

**Special abilities**: At 1st level, and every two levels thereafter, 
this class gains a special ability. This may be chosen from among any 
Strength, Constitution, or Dexterity ability from the [Special 
Abilities](ability-specials.html) list.


Healer
------

<table style="float:right;">
<caption>Table: The Healer</caption>
<tr><th>Level</th><th>Base Attack Bonus</th><th>Fort Save</th><th>Ref Save</th><th>Will Save</th><th>Special</th></tr>
<tr><td>1st </td><td>+0        </td><td>+2 </td><td>+0</td><td>+2 </td><td>Bonus feat, special ability</td></tr>
<tr><td>2nd </td><td>+1        </td><td>+3 </td><td>+0</td><td>+3 </td><td>Bonus feat                 </td></tr>
<tr><td>3rd </td><td>+2        </td><td>+3 </td><td>+1</td><td>+3 </td><td>Special ability            </td></tr>
<tr><td>4th </td><td>+3        </td><td>+4 </td><td>+1</td><td>+4 </td><td>Bonus feat                 </td></tr>
<tr><td>5th </td><td>+3        </td><td>+4 </td><td>+1</td><td>+4 </td><td>Special ability            </td></tr>
<tr><td>6th </td><td>+4        </td><td>+5 </td><td>+2</td><td>+5 </td><td>Bonus feat                 </td></tr>
<tr><td>7th </td><td>+5        </td><td>+5 </td><td>+2</td><td>+5 </td><td>Special ability            </td></tr>
<tr><td>8th </td><td>+6/+1     </td><td>+6 </td><td>+2</td><td>+6 </td><td>Bonus feat                 </td></tr>
<tr><td>9th </td><td>+6/+1     </td><td>+6 </td><td>+3</td><td>+6 </td><td>Special ability            </td></tr>
<tr><td>10th</td><td>+7/+2     </td><td>+7 </td><td>+3</td><td>+7 </td><td>Bonus feat                 </td></tr>
<tr><td>11th</td><td>+8/+3     </td><td>+7 </td><td>+3</td><td>+7 </td><td>Special ability            </td></tr>
<tr><td>12th</td><td>+9/+4     </td><td>+8 </td><td>+4</td><td>+8 </td><td>Bonus feat                 </td></tr>
<tr><td>13th</td><td>+9/+4     </td><td>+8 </td><td>+4</td><td>+8 </td><td>Special ability            </td></tr>
<tr><td>14th</td><td>+10/+5    </td><td>+9 </td><td>+4</td><td>+9 </td><td>Bonus feat                 </td></tr>
<tr><td>15th</td><td>+11/+6/+1 </td><td>+9 </td><td>+5</td><td>+9 </td><td>Special ability            </td></tr>
<tr><td>16th</td><td>+12/+7/+2 </td><td>+10</td><td>+5</td><td>+10</td><td>Bonus feat                 </td></tr>
<tr><td>17th</td><td>+12/+7/+2 </td><td>+10</td><td>+5</td><td>+10</td><td>Special ability            </td></tr>
<tr><td>18th</td><td>+13/+8/+3 </td><td>+11</td><td>+6</td><td>+11</td><td>Bonus feat                 </td></tr>
<tr><td>19th</td><td>+14/+9/+4 </td><td>+11</td><td>+6</td><td>+11</td><td>Special ability            </td></tr>
<tr><td>20th</td><td>+15/+10/+5</td><td>+12</td><td>+6</td><td>+12</td><td>Bonus feat                 </td></tr>
</table>

The healer is the heart of the party, designed to support the other 
characters and stay alive in combat. This character is tough, dedicated,
and charismatic.

### Game Rule Information

The healer has the following game statistics.

**Abilities**: Constitution, Wisdom, and Charisma.

**Alignment**: Any.

**Hit Die**: d8.

**Base Attack Bonus**: Equal to 3/4 total Hit Dice (as cleric).

**Good Saving Throws**: Fortitude and Will.

#### Class Skills

The healer's class skills (and the key ability for each skill) are: Bluff (Cha), Concentration (Con), Craft (Int), Decipher Script (Int), Diplomacy (Cha), Handle Animal (Cha), Heal (Wis), Knowledge (all skills, taken individually) (Int), Listen (Wis), Profession (Wis), Ride (Dex), Spellcraft (Int), Spot (Wis), Survival (Wis), and Swim (Str). See [Skills I](srd/SkillsI.html) and [Skills II](srd/SkillsII.html) for skill descriptions.

**Skill Points at 1st Level**: (6 + Int modifier) x 4.

**Skill Points at Each Additional Level**: 6 + Int modifier.

#### Class Features

All of the following are class features of the healer.

**Weapon and Armor Proficiency**: Characters in this class are 
proficient with all simple weapons. They are not proficient with any 
type of armor or shield.

**Bonus Feats**: At 1st level, this class gains a bonus feat in addition 
to the feat that any 1st-level character gets and the bonus feat granted 
to a human character. The class gains an additional bonus feat at 2nd 
level and every two class levels thereafter (4th, 6th, 8th, 10th, 12th, 
14th, 16th, 18th, and 20th). These bonus feats may be drawn from [SRD 
Feats](srd/Feats.html) or any other list approved by the DM.

These bonus feats are in addition to the feat that a character of any 
class gets from advancing levels.

**Special abilities**: At 1st level, and every two levels thereafter, 
this class gains a special ability. This may be chosen from among any 
Constitution, Wisdom, or Charisma ability from the [Special 
Abilities](ability-specials.html) list.


Expert
------

<table style="float:right;">
<caption>Table: The Expert</caption>
<tr><th>Level</th><th>Base Attack Bonus</th><th>Fort Save</th><th>Ref Save</th><th>Will Save</th><th>Special</th></tr>
<tr><td>1st </td><td>+0        </td><td>+0</td><td>+2 </td><td>+2 </td><td>Bonus feat, special ability</td></tr>
<tr><td>2nd </td><td>+1        </td><td>+0</td><td>+3 </td><td>+3 </td><td>Bonus feat                 </td></tr>
<tr><td>3rd </td><td>+2        </td><td>+1</td><td>+3 </td><td>+3 </td><td>Special ability            </td></tr>
<tr><td>4th </td><td>+3        </td><td>+1</td><td>+4 </td><td>+4 </td><td>Bonus feat                 </td></tr>
<tr><td>5th </td><td>+3        </td><td>+1</td><td>+4 </td><td>+4 </td><td>Special ability            </td></tr>
<tr><td>6th </td><td>+4        </td><td>+2</td><td>+5 </td><td>+5 </td><td>Bonus feat                 </td></tr>
<tr><td>7th </td><td>+5        </td><td>+2</td><td>+5 </td><td>+5 </td><td>Special ability            </td></tr>
<tr><td>8th </td><td>+6/+1     </td><td>+2</td><td>+6 </td><td>+6 </td><td>Bonus feat                 </td></tr>
<tr><td>9th </td><td>+6/+1     </td><td>+3</td><td>+6 </td><td>+6 </td><td>Special ability            </td></tr>
<tr><td>10th</td><td>+7/+2     </td><td>+3</td><td>+7 </td><td>+7 </td><td>Bonus feat                 </td></tr>
<tr><td>11th</td><td>+8/+3     </td><td>+3</td><td>+7 </td><td>+7 </td><td>Special ability            </td></tr>
<tr><td>12th</td><td>+9/+4     </td><td>+4</td><td>+8 </td><td>+8 </td><td>Bonus feat                 </td></tr>
<tr><td>13th</td><td>+9/+4     </td><td>+4</td><td>+8 </td><td>+8 </td><td>Special ability            </td></tr>
<tr><td>14th</td><td>+10/+5    </td><td>+4</td><td>+9 </td><td>+9 </td><td>Bonus feat                 </td></tr>
<tr><td>15th</td><td>+11/+6/+1 </td><td>+5</td><td>+9 </td><td>+9 </td><td>Special ability            </td></tr>
<tr><td>16th</td><td>+12/+7/+2 </td><td>+5</td><td>+10</td><td>+10</td><td>Bonus feat                 </td></tr>
<tr><td>17th</td><td>+12/+7/+2 </td><td>+5</td><td>+10</td><td>+10</td><td>Special ability            </td></tr>
<tr><td>18th</td><td>+13/+8/+3 </td><td>+6</td><td>+11</td><td>+11</td><td>Bonus feat                 </td></tr>
<tr><td>19th</td><td>+14/+9/+4 </td><td>+6</td><td>+11</td><td>+11</td><td>Special ability            </td></tr>
<tr><td>20th</td><td>+15/+10/+5</td><td>+6</td><td>+12</td><td>+12</td><td>Bonus feat                 </td></tr>
</table>

The expert is the brains of the party, specializing in skill and 
technique. This character is fast, smart, and dedicated.

### Game Rule Information

The expert has the following game statistics.

**Abilities**: Dexterity, Intelligence, and Wisdom.

**Alignment**: Any.

**Hit Die**: d6.

**Base Attack Bonus**: Equal to 3/4 total Hit Dice (as cleric).

**Good Saving Throws**: Reflex and Will.

#### Class Skills

The expert's class skills (and the key ability for each skill) are: Appraise (Int), Balance (Dex), Bluff (Cha), Climb (Str), Concentration (Con), Craft (Int), Decipher Script (Int), Diplomacy (Cha), Disable Device (Int), Disguise (Cha), Escape Artist (Dex), Forgery (Int), Gather Information (Cha), Handle Animal (Cha), Heal (Wis), Hide (Dex), Intimidate (Cha), Jump (Str), Knowledge (all skills, taken individually) (Int), Listen (Wis), Move Silently (Dex), Open Lock (Dex), Perform (Cha), Profession (Wis), Ride (Dex), Search (Int), Sense Motive (Wis), Sleight of Hand (Dex), Speak Language (n/a), Spellcraft (Int), Spot (Wis), Survival (Wis), Swim (Str), Tumble (Dex), Use Magic Device (Cha), and Use Rope (Dex).
 See [Skills I](srd/SkillsI.html) and [Skills II](srd/SkillsII.html) for skill descriptions.

**Skill Points at 1st Level**: (8 + Int modifier) x 4.

**Skill Points at Each Additional Level**: 8 + Int modifier.

#### Class Features

All of the following are class features of the expert.

**Weapon and Armor Proficiency**: Characters in this class are 
proficient with all simple weapons. They are not proficient with any 
type of armor or shield.

**Bonus Feats**: At 1st level, this class gains a bonus feat in addition 
to the feat that any 1st-level character gets and the bonus feat granted 
to a human character. The class gains an additional bonus feat at 2nd 
level and every two class levels thereafter (4th, 6th, 8th, 10th, 12th, 
14th, 16th, 18th, and 20th). These bonus feats may be drawn from [SRD 
Feats](srd/Feats.html) or any other list approved by the DM.

These bonus feats are in addition to the feat that a character of any 
class gets from advancing levels.

**Special abilities**: At 1st level, and every two levels thereafter, 
this class gains a special ability. This may be chosen from among any 
Dexterity, Intelligence, or Wisdom ability from the [Special 
Abilities](ability-specials.html) list.
