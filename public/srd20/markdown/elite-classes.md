# Elite Class System

This experimental system includeds new character classes, class 
features, feats, and prestige classes. Its intended to rival the 
diversity of classing options in D20 SRD.


## Progression Tracks

Some abilities improve in effectiveness according to an indefinite 
pattern.

  - Epic Track: 1st level and every two class levels thereafter
    + martial combat bonus +1
    + sneak attack extra damage +1d6
  - Good Track: 2nd level and every two class levels thereafter
    + basic attack bonus +1
    + martial bonus feats
  - Mean Track: 3rd level and every four class levels thereafter
    + simple combat bonus +1
  - Able Track: 4th level and every four class levels thereafter
    + barbaric rage +1/day
  - Poor Track: 3rd level and every three class levels thereafter
    + poor save bonus +1


## Elite Attack and Save Bonus

Your base attack bonus starts at +0 and increases by +1 every 2nd 
character level. Some classes have a feature that further improves your 
base attack bonus at certain levels.

Your base save bonus starts at +0 and increases by +1 every 3rd 
character level. Each class has features that improve your base save 
bonus for one or more of the save categories.


## Classes

These are meant to cover--generally--the diversity of the 11 base 
classes (see [Classes I](d20srd/ClassesI.html) and [Classes 
II](d20srd/ClassesII.html)).

### Elite Warrior

The elite warrior's hit die, Fortitude, and martial combat feature 
serve as a solid base from which to train a regular fighter or a 
paladin. Options will make possible some wilderness abilities akin 
to the barbarian or the ranger.

  - Hit Die: d10
  - Class Features: Fighter's Fortitude, Martial Combat Training

### Elite Adept

  - Hit Die: d8
  - Class Features: Fighter's Fortitude, Martial Combat Training

### Elite Expert

The elite expert has the reflexes and skill focus necessary to 
train a formidable rogue, but with a little magic can equally well 
become a kind of bard or sorceror, or dabble in the ways of a 
cleric. Some specialized training

  - Hit Die: d6
  - Class Features: Rogue's Reflexes, Simple Combat Training

### Elite Mage

  - Hit Die: d4
  - Class Features: Wizard's Will


## Features

### FIGHTER'S FORTITUDE

**Benefits**: you get a +2 bonus on all Fortitude saving throws. 
Furthermore, your base save bonus for Fortitude is +1 for every 2 levels 
you gain in a class that has Fighter's Fortitude if that bonus is higher 
than your normal Fortitude save bonus.

### ROGUE'S REFLEXES

**Benefits**: you get a +2 bonus on all Reflex saving throws. 
Furthermore, your base save bonus for Reflex is +1 for every 2 levels 
you gain in a class that has Rogue's Reflexes if that bonus is higher 
than your normal Reflex save bonus.

### WIZARD'S WILL

**Benefits**: you get a +2 bonus on all Will saving throws. Furthermore, 
your base save bonus for Will is +1 for every 2 levels you gain in a 
class that has Wizard's Will if that bonus is higher than your normal 
Will save bonus.

### MARTIAL COMBAT TRAINING


  - **Benefits**: you are proficient with all simple and martial 
  weapons, and you receive a +1 bonus to attacks at 1st level and every 
  two levels thereafter.

### SIMPLE COMBAT TRAINING

  - **Benefits**: you receive a +1 bonus to attacks at 3rd level 
  and every four levels thereafter.

### DIVINE DEVOTION

  - **Prerequisites**: Wis 13, lawful good alignment
  - **Benefits**: turn undead a number of times per day equal to 3 + 
your Charisma modifier.

### DIVINE NATURE

**Prerequisites**: Wild Empathy
**Benefits**: gain Animal Friendship

### ARCANE TALENT

**Prerequisites**:
**Benefits**:

### SMITE EVIL

## Epic Save and Attack Bonuses

This is a table taken from [Epic Level Basics](d20srd/EpicLevelBasics/html).

<table style="float:right;">
<caption>Table: Epic Save and Attack Bonuses</caption>
<tr><th>Character Level</th><th>Epic Save Bonus</th><th>Epic Attack Bonus</th></tr>
<tr><td>1st</td><td>+0</td><td>+1</td></tr>
<tr><td>2nd</td><td>+1</td><td>+1</td></tr>
<tr><td>3rd</td><td>+1</td><td>+2</td></tr>
<tr><td>4th</td><td>+2</td><td>+2</td></tr>
<tr><td>5th</td><td>+2</td><td>+3</td></tr>
<tr><td>6th</td><td>+3</td><td>+3</td></tr>
<tr><td>7th</td><td>+3</td><td>+4</td></tr>
<tr><td>8th</td><td>+4</td><td>+4</td></tr>
<tr><td>9th</td><td>+4</td><td>+5</td></tr>
<tr><td>10th</td><td>+5</td><td>+5</td></tr>
<tr><td>11th</td><td>+5</td><td>+6</td></tr>
<tr><td>12th</td><td>+6</td><td>+6</td></tr>
<tr><td>13th</td><td>+6</td><td>+7</td></tr>
<tr><td>14th</td><td>+7</td><td>+7</td></tr>
<tr><td>15th</td><td>+7</td><td>+8</td></tr>
<tr><td>16th</td><td>+8</td><td>+8</td></tr>
<tr><td>17th</td><td>+8</td><td>+9</td></tr>
<tr><td>18th</td><td>+9</td><td>+9</td></tr>
<tr><td>19th</td><td>+9</td><td>+10</td></tr>
<tr><td>20th</td><td>+10</td><td>+10</td></tr>
</table>
