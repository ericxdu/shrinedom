Feat Descriptions
-----------------

<h3 id="aircraftoperation">Aircraft Operation</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

Select a class of aircraft (heavy aircraft, helicopters, jet fighters, or spacecraft). The character is proficient at
operating that class of aircraft.

The heavy aircraft class includes jumbo passenger airplanes, large cargo planes, heavy bombers, and any other aircraft
with three or more engines. Helicopters include transport and combat helicopters of all types. Jet fighters include
military fighter and ground attack jets. Spacecraft are vehicles such as the space shuttle and the lunar lander.

**Prerequisite**: Pilot 4 ranks.

**Benefit**: The character takes no penalty on Pilot checks or attack rolls made when operating an aircraft of the
selected class.

**Normal**: Characters without this feat take a –4 penalty on Pilot checks made to operate an aircraft that falls in
any of these classes, and on attacks made with aircraft weapons. There is no penalty when the character operates a
general-purpose aircraft.

**Special**: The character can gain this feat multiple times. Each time the character takes the feat, the character
selects a different class of aircraft.

<h3 id="attentive">Attentive</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

**Benefit**: The character gets a +2 bonus on all Investigate checks and Sense Motive checks.

**Special**: Remember that the Investigate skill can’t be used untrained.

<h3 id="builder">Builder</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

**Benefit**: Pick two of the following skills: Craft (chemical), Craft (electronic), Craft (mechanical), and Craft
(structural). The character gets a +2 bonus on all checks with those skills.

**Special**: The character can select this feat twice. The second time, the character applies it to the two skills he
or she didn’t pick originally. Remember that Craft (chemical), Craft (electronic), and Craft (mechanical) cannot be
used untrained.

<h3 id="cautious">Cautious</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

**Benefit**: The character gets a +2 bonus on all Demolitions checks and Disable Device checks.

**Special**: Remember that the Demolitions skill and the Disable Device skill can’t be used untrained.

<h3 id="confident">Confident</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

**Benefit**: The character gets a +2 bonus on all Gamble checks and Intimidate checks, and on level checks to resist intimidation.

<h3 id="gearhead">Gearhead</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

**Benefit**: The character gets a +2 bonus on all Computer Use checks and Repair checks.

**Special**: Remember that the Computer Use skill and the Repair skill can only be used untrained in certain
situations.

<h3 id="forcestop">Force Stop</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

**Prerequisites**: Drive 4 ranks, Vehicle Expert.

**Benefit**: When the character at­tempts a sideswipe stunt with a surface vehicle, the character can force the other
vehicle to a stop by nudging it into a controlled side­ways skid. In addition to the nor­mal requirements for
attempting a sideswipe stunt, the character must have sufficient movement re­maining to move a number of squares equal
to the character’s turn number.

After succeeding on the check to attempt the sideswipe, the character makes a Drive check opposed by the other driver.
If the character succeeds, turn the other vehicle 90 degrees across the front of the character’s, so that they form a
tee. Move them forward a distance equal to the character’s turn number. The vehicles end their movement at that
location, at stationary speed, and take their normal sideswipe damage.

If the character fails the check, resolve the sideswipe normally.

<h3 id="guide">Guide</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

**Benefit**: The character gets a +2 bonus on all Navigate checks and Survival checks.

<h3 id="medicalexpert">Medical Expert</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

**Benefit**: The character gets a +2 bonus on all Craft (pharmaceutical) checks and Treat Injury checks.

**Special**: Remember that the Craft (pharmaceutical) skill can’t be used untrained.

<h3 id="studious">Studious</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

**Benefit**: The character gets a +2 bonus on all Decipher Script checks and Research checks. 

<h3 id="surfacevehicleoperation">Surface Vehicle Operation</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

Select a class of surface vehicle (heavy wheeled, powerboat, sailboat, ship, or tracked). The character is proficient
at operating that class of vehicle.

The heavy wheeled class includes all kinds of semi-trucks and tractor-trailers, as well as wheeled construction
ve­hicles (such as earth movers) and wheeled armored vehicles (such as some armored personnel carriers). Powerboats are
engine-powered water vessels designed for operation by a single person and usually no more than 100 feet in length.
Sailboats are wind-powered water vessels. Ships are large, multicrewed water vessels. Tracked vehicles include
bulldozers and tanks and other military vehicles.

**Prerequisite**: Drive 4 ranks.

**Benefit**: The character takes no penalty on Drive checks or attack rolls made when operating a surface vehicle of
the selected class.

**Normal**: Characters without this feat take a –4 penalty on Drive checks made to operate a surface vehicle that falls
under any of these classes, and to attacks made with vehicle weapons. There is no penalty when you operate a
general-purpose surface vehicle.

**Special**: A character can gain this feat as many as five times. Each time the character takes the feat, he or she
selects a different class of surface vehicle.

<h3 id="surgery">Surgery</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

**Prerequisite**: Treat Injury 4 ranks.

**Benefit**: The character can use the Treat Injury skill to perform surgery without penalty.  See the Treat Injury
skill description.

**Normal**: Characters without this feat take a –4 penalty on Treat Injury checks made to perform surgery.

<h3 id="vehicledodge">Vehicle Dodge</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

**Prerequisites**: Dexterity 13, Drive 6 ranks or Pilot 6 ranks, Vehicle Expert.

**Benefit**: When driving a vehicle, during the character’s action the character designates an opposing vehicle or a
single opponent. The character’s vehicle and everyone aboard it receive a +1 dodge bonus to Defense against attacks
from that vehicle or opponent. The character can select a new vehicle or opponent on any action.

<h3 id="vehicleexpert">Vehicle Expert</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

**Benefit**: The character gets a +2 bonus on all Drive checks and Pilot checks.


Legal Information
-----------------

[MSRD Open Gaming License](srd/msrdlegal.html)
