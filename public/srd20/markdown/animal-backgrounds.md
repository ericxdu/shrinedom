<table>
  <caption>Primary Backgrounds</caption>
  <tr>
    <th>Background</th>
    <th>Ability Adjustments</th>
    <th>Favored Class</th>
    <th>Level Adjustment</th>
  </tr>
  <tr>
    <td><a href="#badger">Badger</a></td>
    <td>+2 Constitution, –2 Charisma</td>
    <td>Barbarian</td>
    <td>+1</td>
  </tr>
  <tr>
    <td><a href="#monitor">Monitor</a></td>
    <td>+2 Strength, +2 Constitution, –2 Intelligence, –4 Charisma</td>
    <td>Barbarian</td>
    <td>+1</td>
  </tr>
  <tr>
    <td><a href="#mouse">Mouse</a></td>
    <td>–4 Strength, +2 Dexteriy, +2 Wisdom, –2 Charisma</td>
    <td>Rogue</td>
    <td>+0</td>
  </tr>
<tfoot>
  <tr>
    <td colspan="5">1 A character’s starting Intelligence score is always
    at least 3. If this adjustment would lower the character’s score to 1 or
    2, the score remains 3.</td>
  </tr>
</tfoot>
</table>


Badger
------

The badger is a furry animal with a squat, powerful body. Its strong forelimbs are armed with long claws for digging.

A badger is a medium humanoid with the anthropomorphic subtype.

Badger characters possess the following racial traits.

&mdash; +2 Constitution, –2 Charisma.

&mdash;Medium: No special bonuses or penalties due to size. 

&mdash;A badger’s base land speed is 30 feet.

&mdash;Darkvision out to 60 feet and low-light vision.

&mdash;Natural Weapons: 2 claws (1d3).

&mdash;Scent: A badger can detect approaching enemies, sniff out hidden foes, and track by sense of smell. Creatures with the scent ability can identify familiar odors just as humans do familiar sights.

&mdash;Racial Skills: A badger has a +4 racial bonus on Escape Artist checks.

&mdash;Racial Feats: A badger receives Track as a bonus feat.

&mdash;Favored Class: Barbarian.

&mdash;Level adjustment +1.


Monitor
-------
	
Monitors are fairly large, carnivorous lizards.

A monitor is a humanoid with the reptilian and anthropomorphic subtypes.

Monitor characters possess the following racial traits.

&mdash; +2 Strength, –2 Intelligence, –2 Charisma.

&mdash;Medium: No special bonuses or penalties due to size. 

&mdash;A monitor’s base land speed is 30 feet.

&mdash;Darkvision out to 60 feet and low-light vision.
	
&mdash; +3 natural armor bonus.

&mdash;Racial Skills: Monitors have a +4 racial bonus on Hide and Move Silently checks. In forested or overgrown areas, the Hide bonus improves to +8.

&mdash;Favored Class: Barbarian.

&mdash;Level adjustment +1.


Mouse
-----

These omnivorous rodents thrive almost anywhere.

A mouse is a humanoid with the anthropomorphic subtype.

Mouse characters possess the following racial traits.

&mdash; –4 Strength, +2 Dexteriy, +2 Wisdom.

&mdash;Small size: +1 bonus to Armor Class, +1 bonus on attack rolls, +4 bonus on Hide checks, –4 penalty on grapple checks, lifting and carrying limits 3/4 those of Medium characters.

&mdash;A mouse’s base land speed is 15 feet, climb 15 feet.

&mdash;Darkvision out to 60 feet and low-light vision.

&mdash;Scent: A mouse can detect approaching enemies, sniff out hidden foes, and track by sense of smell. Creatures with the scent ability can identify familiar odors just as humans do familiar sights.

&mdash;Skills: Mice have a +4 racial bonus on Hide and Move Silently checks, and a +8 racial bonus on Balance and Climb checks. A mouse can always choose to take 10 on Climb checks, even if rushed or threatened. A mouse uses its Dexterity modifier instead of its Strength modifier for Climb checks.

&mdash;Favored Class: Rogue.
