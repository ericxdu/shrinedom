<h3 id="extrausesperday">Extra Spell-like Uses</h3>

You can use your spell-like abilities more often than usual.

__Prerequisite__: Spell-like abilities.

__Benefit__: Each time you take this feat, you can use your spell-like abilities two more times per day than normal.

__Normal__: Without this feat, the number of times per day a character can use spell-like abilities typically does not increase in any way.

__Special__: You can gain Extra Uses Per Day multiple times. Its effects stack. Each time you take this feat, you can use each of your spell-like abilities two additional times per day.
