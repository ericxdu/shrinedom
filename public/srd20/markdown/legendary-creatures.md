Legendary Creatures
===================

They live in a far-off, legendary magical forest.


Balor
-----

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Large Outsider (Chaotic, Extraplanar, Evil)**
- **Hit Dice:** 20d8+200 (290 hp)
- **Initiative:** +11
- **Speed:** 40 ft. (8 squares), fly 90 ft. (good)
- **Armor Class:** 35 (–1 size, +7 Dex, +19 natural), touch 16, flat-footed 28
- **Base Attack/Grapple:** +20/+36
- **Attack:** +1 vorpal longsword +33 melee (2d6+8/19–20)
- **Full Attack:** +1 vorpal longsword +31/+26/+21/+16 melee (2d6+8/19–20) and +1 flaming whip +30/+25 melee (1d4+4 plus 1d6 fire plus entangle); or 2 slams +31 melee (1d10+7)
- **Space/Reach:** 10 ft./10 ft. (20 ft. with +1 flaming whip)
- **Special Attacks:** Death throes, entangle, spell-like abilities, summon demon, vorpal sword
- **Special Qualities:** Damage reduction 15/cold iron and good, darkvision 60 ft., flaming body, immunity to electricity, fire, and poison, resistance to acid 10 and cold 10, spell resistance 28, telepathy 100 ft., true seeing
- **Saves:** Fort +22, Ref +19, Will +19
- **Abilities:** Str 35, Dex 25, Con 31, Int 24, Wis 24, Cha 26
- **Skills:** Bluff +31, Concentration +33, Diplomacy +35, Disguise +8 (+10 acting), Hide +26, Intimidate +33, Knowledge (any two) +30, Listen +38, Move Silently +30, Search +30, Sense Motive +30, Spellcraft +30 (+32 scrolls), Spot +38, Survival +7 (+9 following tracks), Use Magic Device +31 (+33 scrolls)
- **Feats:** Cleave, Improved Initiative, Improved Two-Weapon Fighting, Power Attack, Quicken Spell-Like Ability (telekinesis), Two-Weapon Fighting, Weapon Focus (longsword)
- **Environment:** A chaotic evil-aligned plane
- **Organization:** Solitary or troupe (1 balor, 1 marilith, and 2–5 hezrous)
- **Challenge Rating:** 20
- **Treasure:** Standard coins; double goods; standard items, plus +1 vorpal greatsword and +1 flaming whip
- **Alignment:** Always chaotic evil
- **Advancement:** 21–30 HD (Large); 31–60 HD (Huge)
- **Level Adjustment:** &mdash;

Balors are a race of creatures native to chaotic evil-aligned planes. They are ferocity personified and will attack any creature just for the sheer fun of it&mdash;even other balors.

A balor stands about 12 feet tall. Its skin is usually dark red. It weighs about 4,500 pounds.

Balor Traits: A balor possesses the following traits.

&mdash;Immunity to electricity and poison.

&mdash;Resistance to acid 10, cold 10, and fire 10.

&mdash;Telepathy.

Balors speak Abyssal, Celestial, and Draconic.

### Combat

Balors love to join battle armed with their swords and whips. If they face stiff resistance, they may teleport away to loose a few spell-like effects at the foe.

A balor’s *+1 flaming whip* is a long, flexible weapon with many tails tipped with hooks, spikes, and balls. The weapon deals bludgeoning and slashing damage, in addition to fire damage.

A balor’s natural weapons, as well as any weapons it wields, are treated as chaotic-aligned and evil-aligned for the purpose of overcoming damage reduction.

__Death Throes (Ex):__ When killed, a balor explodes in a blinding flash of light that deals 100 points of damage to anything within 100 feet (Reflex DC 30 half ). This explosion automatically destroys any weapons the balor is holding. The save DC is Constitution-based.

__Entangle (Ex):__ A balor’s *+1 flaming whip* entangles foes much like an attack with a net. The whip has 20 hit points. The whip needs no folding. If it hits, the target and the balor immediately make opposed Strength checks; if the balor wins, it drags the target against its flaming body (see below). The target remains anchored against the balor’s body until it escapes the whip.

__Spell-Like Abilities:__ At will&mdash; *blasphemy* (DC 25), *dominate monster* (DC 27), *greater dispel magic*, *greater teleport* (self plus 50 pounds of objects only), *insanity* (DC 25), *power word stun*, *telekinesis* (DC 23), *unholy aura* (DC 26); 1/day&mdash;*fire storm* (DC 26), *implosion* (DC 27). Caster level 20th. The save DCs are Charisma-based.

__Vorpal Sword (Su):__ Every balor carries a *+1 vorpal longsword* that looks like a flame or a bolt of lightning.

__*Summon Demon* (Sp):__ Once per day a balor can automatically summon 4d10 dretches, 1d4 hezrous, or one nalfeshnee, glabrezu, marilith, or balor. This ability is the equivalent of a 9th-level spell.

__Flaming Body (Su):__ The body of a balor is wreathed in flame.

Anyone grappling a balor takes 6d6 points of fire damage each round.

__True Seeing (Su):__ Balors have a continuous true seeing ability, as the spell (caster level 20th).

__Skills:__ Balors have a +8 racial bonus on Listen and Spot checks.

### Source

See [Monsters D-De](srd/MonstersD-De.html)


Goblin
------

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Goblin, 1st-Level Warrior**
- **Small Humanoid (Goblinoid)**
- **Hit Dice:** 1d8+1 (5 hp)
- **Initiative:** +1
- **Speed:** 30 ft. (6 squares)
- **Armor Class:** 15 (+1 size, +1 Dex, +2 leather armor, +1 light shield), touch 12, flat-footed 14
- **Base Attack/Grapple:** +1/–3
- **Attack:** Morningstar +2 melee (1d6) or javelin +3 ranged (1d4)
- **Full Attack:** Morningstar +2 melee (1d6) or javelin +3 ranged (1d4)
- **Space/Reach:** 5 ft./5 ft.
- **Special Attacks:** &mdash;
- **Special Qualities:** Darkvision 60 ft.
- **Saves:** Fort +3, Ref +1, Will –1
- **Abilities:** Str 11, Dex 13, Con 12, Int 10, Wis 9, Cha 6
- **Skills:** Hide +5, Listen +2, Move Silently +5, Ride +4, Spot +2
- **Feats:** Alertness
- **Environment:** Temperate plains
- **Organization:** Gang (4–9), band (10–100 plus 100% noncombatants plus 1 3rd-level sergeant per 20 adults and 1 leader of 4th–6th level), warband (10–24 with worg mounts), or tribe (40–400 plus 100% noncombatants plus 1 3rd-level sergeant per 20 adults, 1 or 2 lieutenants of 4th or 5th level, 1 leader of 6th–8th level, 10–24 worgs, and 2–4 dire wolves)
- **Challenge Rating:** 1/3
- **Treasure:** Standard
- **Alignment:** Usually neutral evil
- **Advancement:** By character class
- **Level Adjustment:** +0

A goblin stands 3 to 3-1/2 feet tall and weigh 40 to 45 pounds. Its eyes are usually dull and glazed, varying in color from red to yellow. A goblin’s skin color ranges from yellow through any shade of orange to a deep red; usually all members of a single tribe are about the same color. Goblins wear clothing of dark leather, tending toward drab, soiled-looking colors. Goblins speak Goblin; those with Intelligence scores of 12 or higher also speak Common.

Most goblins encountered outside their homes are warriors; the information in the statistics block is for one of 1st level.

### Combat

Being bullied by bigger, stronger creatures has taught goblins to exploit what few advantages they have: sheer numbers and malicious ingenuity. The concept of a fair fight is meaningless in their society. They favor ambushes, overwhelming odds, dirty tricks, and any other edge they can devise.

Goblins have a poor grasp of strategy and are cowardly by nature, tending to flee the field if a battle turns against them. With proper supervision, though, they can implement reasonably complex plans, and in such circumstances their numbers can be a deadly advantage.

Skills: Goblins have a +4 racial bonus on Move Silently and Ride checks. Goblin cavalry (mounted on worgs) usually select the Mounted Combat feat in place of the Alertness feat, which reduces their Spot and Listen check modifiers from +3 to +1.

Challenge Rating: Goblins with levels in NPC classes have a CR equal to their character level &minus;2.

### Goblins as Characters

Goblin characters possess the following racial traits.

&mdash; &minus;2 Strength, +2 Dexterity, &minus;2 Charisma.

&mdash;Small size: +1 bonus to Armor Class, +1 bonus on attack rolls, +4 bonus on Hide checks, &minus;4 penalty on grapple checks, lifting and carrying limits 3&frasl;4 those of Medium characters.

&mdash;A goblin’s base land speed is 30 feet.

&mdash;Darkvision out to 60 feet.

&mdash; +4 racial bonus on Move Silently and Ride checks.

&mdash;Automatic Languages: Common, Goblin. Bonus Languages: Draconic, Elven, Giant, Gnoll, Orc.

&mdash;Favored Class: Rogue.

The goblin warrior presented here had the following ability scores before racial adjustments: Str 13, Dex 11, Con 12, Int 10, Wis 9, Cha 8.


Green Hag
---------

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Medium Monstrous Humanoid**
- **Hit Dice:** 9d8+9 (49 hp)
- **Initiative:** +1
- **Speed:** 30 ft. (6 squares), swim 30 ft.
- **Armor Class:** 22 (+1 Dex, +11 natural), touch 11, flat-footed 21
- **Base Attack/Grapple:** +9/+13
- **Attack:** Claw +13 melee (1d4+4)
- **Full Attack:** 2 claws +13 melee (1d4+4)
- **Space/Reach:** 5 ft./5 ft.
- **Special Attacks:** Spell-like abilities, weakness, mimicry
- **Special Qualities:** Darkvision 90 ft., spell resistance 18
- **Saves:** Fort +6, Ref +7, Will +7
- **Abilities:** Str 19, Dex 12, Con 12, Int 13, Wis 13, Cha 14
- **Skills:** Concentration +7, Craft or Knowledge (any one) +7, Hide +9, Listen +11, Spot +11 Swim +12
- **Feats:** Alertness, Blind-Fight, Combat Casting, Great Fortitude
- **Environment:** Temperate marshes
- **Organization:** Solitary or covey (3 hags of any kind plus 1–8 ogres and 1–4 evil giants)
- **Challenge Rating:** 5
- **Treasure:** Standard
- **Alignment:** Usually chaotic evil
- **Advancement:** By character class
- **Level Adjustment:** &mdash;

Green hags take the form of crones whose bent shapes belie their fierce power and swiftness. They are found in desolate swamps and dark forests.

A green hag is about the same height and weight as a female human.

Hags speak Giant and Common.

### Combat

Hags are tremendously strong. They are naturally resistant to spells and can cast magic of their own. Hags often gather to form coveys. A covey, usually containing one hag of each type, can use powers beyond those of the individual members.

Green hags prefer to attack from hiding, usually after distracting foes. They often use darkvision to their advantage by attacking during moonless nights.

__Spell-Like Abilities:__ At will&mdash;*dancing lights*, *disguise self*, *ghost sound* (DC 12), *invisibility*, *pass without trace*, *tongues*, *water breathing*. Caster level 9th. The save DC is Charisma-based.

__Weakness (Su):__ A green hag can weaken a foe by making a special touch attack. The opponent must succeed on a DC 16 Fortitude save or take 2d4 points of Strength damage. The save DC is Charisma-based.

__Mimicry (Ex):__ A green hag can imitate the sounds of almost any animal found near its lair.

__Skills:__ A green hag has a +8 racial bonus on any Swim check to perform some special action or avoid a hazard. It can always choose to take 10 on a Swim check, even if distracted or endangered. It can use the run action while swimming, provided it swims in a straight line.

### Source

See [Monsters H-I](srd/MonstersH-I.html)


Merfolk
-------

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Merfolk, 1st-Level Warrior**
- **Medium Humanoid (Aquatic)**
- **Hit Dice:** 1d8+2 (6 hp)
- **Initiative:** +1
- **Speed:** 5 ft. (1 square), swim 50 ft.
- **Armor Class:** 13 (+1 Dex, +2 leather), touch 11, flat-footed 12
- **Base Attack/Grapple:** +1/+2
- **Attack:** Trident +2 melee (1d8+1) or heavy crossbow +2 ranged (1d10/19–20)
- **Full Attack:** Trident +2 melee (1d8+1) or heavy crossbow +2 ranged (1d10/19–20)
- **Space/Reach:** 5 ft./5 ft.
- **Special Attacks:** &mdash;
- **Special Qualities:** Amphibious, low-light vision
- **Saves:** Fort +4, Ref +1, Will –1
- **Abilities:** Str 13, Dex 13, Con 14, Int 10, Wis 9, Cha 10
- **Skills:** Listen +3, Spot +3, Swim +9
- **Feats:** Alertness
- **Environment:** Temperate aquatic
- **Organization:** Company (2–4), patrol (11–20 plus 2 3rd-level lieutenants and 1 leader of 3rd–6th level), or band (30–60 plus 1 3rd-level sergeant per 20 adults, 5 5th-level lieutenants, 3 7th-level captains, and 10 porpoises)
- **Challenge Rating:** 1/2
- **Treasure:** Standard
- **Alignment:** Usually neutral
- **Advancement:** By character class
- **Level Adjustment:** +1

A merfolk is about 8 feet long from the top of the head to the end of the tail, and weighs about 400 pounds.

Merfolk speak Common and Aquan.

Most merfolk encountered outside their home are warriors; the information in the statistics block is for one of 1st level.

### Combat

Merfolk favor heavy crossbows of shell and coral that fire bolts fashioned from blowfish spines, with an underwater range increment of 30 feet. Merfolk often barrage their enemies before closing, when they resort to tridents.

__Amphibious (Ex):__ Merfolk can breathe both air and water, although they rarely travel more than a few feet from the water’s edge.

__Skills:__ A merfolk has a +8 racial bonus on any Swim check to perform some special action or avoid a hazard. It can always choose to take 10 on a Swim check, even if distracted or endangered. It can use the run action while swimming, provided it swims in a straight line.

The merfolk warrior presented here had the following ability scores before racial adjustments: Str 13, Dex 11, Con 12, Int 10, Wis 9, Cha 8.

### Merfolk Characters

A merfolk’s favored class is bard.


Pixie
-----

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Small Fey**
- **Hit Dice:** 1d6 (3 hp)
- **Initiative:** +4
- **Speed:** 20 ft. (4 squares), fly 60 ft. (good)
- **Armor Class:** 16 (+1 size, +4 Dex, +1 natural), touch 15, flat-footed 12
- **Base Attack/Grapple:** +0/–6
- **Attack:** Short sword +5 melee (1d4–2/19–20) or longbow +5 ranged (1d6–2/&times;3)
- **Full Attack:** Short sword +5 melee (1d4–2/19–20) or longbow +5 ranged (1d6–2)/x3
- **Space/Reach:** 5 ft./5 ft.
- **Special Attacks:** Spell-like abilities, special arrows
- **Special Qualities:** Damage reduction 10/cold iron, greater invisibility, low-light vision, spell resistance 15
- **Saves:** Fort +0, Ref +6, Will +4
- **Abilities:** Str 7, Dex 18, Con 11, Int 16, Wis 15, Cha 16
- **Skills:** Bluff +7, Concentration +4, Escape Artist +8, Hide +8, Listen +8, Move Silently +8, Ride +8, Search +9, Sense Motive +6, Spot +8
- **Feats:** Dodge<sup>B</sup>, Weapon Finesse
- **Environment:** Temperate forests
- **Organization:** Gang (2–4), band (6–11), or tribe (20–80)
- **Challenge Rating:** 4 (5 with irresistible dance)
- **Treasure:** No coins; 50% goods; 50% items
- **Alignment:** Always neutral good
- **Advancement:** 2–3 HD (Small)
- **Level Adjustment:** +4 (+6 with irresistible dance)

Pixies are reclusive fey. They go out of their way to fight evil and ugliness and to protect their homelands. Pixies wear bright clothing, often including a cap and shoes with curled and pointed toes.

A pixie stands about 2-1/2 feet tall and weighs about 30 pounds.

Pixies speak Sylvan and Common, and may know other languages as well.

#### Combat

Pixies fight their opponents with spell-like abilities and pintsized weaponry. They prefer ambushes and other trickery over direct confrontation.

The normally carefree pixies ferociously attack evil creatures and unwanted intruders. They take full advantage of their invisibility and other abilities to harass and drive away opponents.

__Greater Invisibility (Su):__ A pixie remains invisible even when it attacks. This ability is constant, but the pixie can suppress or resume it as a free action.

__Spell-Like Abilities:__ 1/day&mdash;*lesser confusion* (DC 14), *dancing lights*, *detect chaos*, *detect good*, *detect evil*, *detect law*, *detect thoughts* (DC 15), *dispel magic*, *entangle* (DC 14), *permanent image* (DC 19; visual and auditory elements only), *polymorph* (self only). Caster level 8th. The save DCs are Charisma-based.

One pixie in ten can use *irresistible dance* (caster level 8th) once per day.

__Special Arrows (Ex):__ Pixies sometimes employ arrows that deal no damage but can erase memory or put a creature to sleep.

*Memory Loss:* An opponent struck by this arrow must succeed on a DC 15 Will save or lose all memory. The save DC is Charisma-based and includes a +2 racial bonus. The subject retains skills, languages, and class abilities but forgets everything else until he or she receives a *heal* spell or memory restoration with *limited wish*, *wish*, or *miracle*.

*Sleep:* Any opponent struck by this arrow, regardless of Hit Dice, must succeed on a DC 15 Fortitude save or be affected as though by a sleep spell. The save DC is Charisma-based and includes a +2 racial bonus.

__Skills:__ All sprites have a +2 racial bonus on Search, Spot, and Listen checks.

#### Pixies as Characters

A pixie character exchanges its 1 HD of fey for its first class level.

Pixie characters possess the following racial traits.

&mdash; –4 Strength, +8 Dexterity, +6 Intelligence, +4 Wisdom, +6 Charisma.

&mdash;Small size. +1 bonus to Armor Class, +1 bonus on attack rolls, +4 bonus on Hide checks, –4 penalty on grapple checks, lifting and carrying limits 3/4 those of Medium characters.

&mdash;A pixie’s base land speed is 20 feet. It also has a fly speed of 60 feet (good).

&mdash;Low-light vision.

&mdash;Skills: Pixies have a +2 racial bonus on Listen, Search, and Spot checks.

&mdash;Racial Feats: A pixie receives Dodge as a bonus feat.

&mdash; +1 natural armor bonus.

&mdash;Special Attacks (see above): Spell-like abilities.

&mdash;Special Qualities (see above): Damage reduction 10/cold iron, greater invisibility, spell resistance equal to 15 + class levels.

&mdash;Automatic Languages: Common, Sylvan. Bonus Languages: Elven, Gnome, Halfling.

&mdash;Favored Class: Sorcerer.

&mdash;Level adjustment +4 (+6 if the pixie can use *irresistible dance*).


Unicorn
-------

<table>
  <thead>
    <tr>
      <th></th>
      <th>Unicorn</th>
      <th>Celestial Charger, 7th-Level Cleric</th>
    </tr>
    <tr>
      <th></th>
      <th>Large Magical Beast</th>
      <th>Large Magical Beast</th>
    </tr>
  </thead>
  <tr>
    <th>Hit Dice:</th>
    <td>4d10+20 (42 hp)</td>
    <td>8d10+7d8+75 (155 hp)</td>
  </tr>
  <tr>
    <th>Initiative:</th>
    <td>+3</td>
    <td>+4</td>
  </tr>
  <tr>
    <th>Speed:</th>
    <td>60 ft. (12 squares)</td>
    <td>60 ft. (12 squares)</td>
  </tr>
  <tr>
    <th>Armor Class:</th>
    <td>18 (–1 size, +3 Dex, +6 natural), touch 12, flat-footed 15</td>
    <td>24 (–1 size, +4 Dex, +6 natural, +5 bracers of armor +5), touch 13, flat-footed 20</td>
  </tr>
  <tr>
    <th>Base Attack/Grapple:</th>
    <td>+4/+13</td>
    <td>+13/+24</td>
  </tr>
  <tr>
    <th>Attack:</th>
    <td>Horn +11 melee (1d8+8)</td>
    <td>Horn +22 melee (1d8+10)</td>
  </tr>
  <tr>
    <th>Full Attack:</th>
    <td>Horn +11 melee (1d8+8) and 2 hooves +3 melee (1d4+2)</td>
    <td>Horn +22 melee (1d8+10) and 2 hooves +14 melee (1d4+3)</td>
  </tr>
  <tr>
    <th>Space/Reach:</th>
    <td>10 ft./5 ft.</td>
    <td>10 ft./5 ft.</td>
  </tr>
  <tr>
    <th>Special Attacks:</th>
    <td>&mdash;</td>
    <td>Turn undead 13/day, smite evil, spells</td>
  </tr>
  <tr>
    <th>Special Qualities:</th>
    <td>Darkvision 60 ft., magic circle against evil, spell-like abilities, immunity to poison, charm, and compulsion, low-light vision, scent, wild empathy</td>
    <td>Damage reduction 10/magic, darkvision 60 ft., immunity to poison, charm, and compulsion, low-light vision, magic circle against evil, resistance to acid 10, cold 10, and electricity 10, scent, spell-like abilities, spell resistance 20, wild empathy</td>
  </tr>
  <tr>
    <th>Saves:</th>
    <td>Fort +9, Ref +7, Will +6</td>
    <td>Fort +16, Ref +12, Will +15</td>
  </tr>
  <tr>
    <th>Abilities:</th>
    <td>Str 20, Dex 17, Con 21, Int 10, Wis 21, Cha 24</td>
    <td>Str 24, Dex 18, Con 20, Int 13, Wis 27, Cha 22</td>
  </tr>
  <tr>
    <th>Skills:</th>
    <td>Jump +21, Listen +11, Move Silently +9, Spot +11, Survival +8&ast;</td>
    <td>Concentration +11, Knowledge (nature) +9, Knowledge (religion) +8, Listen +15, Move Silently +12, Spellcraft +5, Spot +15, Survival +15 (+17 aboveground)&ast;</td>
  </tr>
  <tr>
    <th>Feats:</th>
    <td>Alertness, Skill Focus (Survival)</td>
    <td>Alertness, Combat Casting, Extra Turning, Improved Turning, Run, Skill Focus (Survival)</td>
  </tr>
  <tr>
    <th>Environment:</th>
    <td>Temperate forests</td>
    <td>A chaotic good plane</td>
  </tr>
  <tr>
    <th>Organization:</th>
    <td>Solitary, pair, or grace (3–6)</td>
    <td>Solitary</td>
  </tr>
  <tr>
    <th>Challenge Rating:</th>
    <td>3</td>
    <td>13</td>
  </tr>
  <tr>
    <th>Treasure:</th>
    <td>None</td>
    <td>None</td>
  </tr>
  <tr>
    <th>Alignment:</th>
    <td>Always chaotic good</td>
    <td>Always chaotic good</td>
  </tr>
  <tr>
    <th>Advancement:</th>
    <td>5–8 HD (Large)</td>
    <td>By character class</td>
  </tr>
  <tr>
    <th>Level Adjustment:</th>
    <td>+4 (cohort)</td>
    <td>+8 (cohort)</td>
  </tr>
</table>

A unicorn has deep sea-blue, violet, brown, or fiery gold eyes. Males sport a white beard.

A typical adult unicorn grows to 8 feet in length, stands 5 feet high at the shoulder, and weighs 1,200 pounds. Females are slightly smaller and slimmer than males.

Unicorns speak Sylvan and Common.

### Combat

Unicorns normally attack only when defending themselves or their forests. They either charge, impaling foes with their horns like lances, or strike with their hooves. The horn is a +3 magic weapon, though its power fades if removed from the unicorn.

__Magic Circle against Evil (Su):__ This ability continuously duplicates the effect of the spell. A unicorn cannot suppress this ability.

__Spell-Like Abilities:__ Unicorns can use *detect evil* at will as a free action.

Once per day a unicorn can use *greater teleport* to move anywhere within its home. It cannot teleport beyond the forest boundaries nor back from outside.

A unicorn can use *cure light wounds* three times per day and *cure moderate wounds* once per day (caster level 5th) by touching a wounded creature with its horn. Once per day it can use *neutralize poison* (DC 21, caster level 8th) with a touch of its horn. The save DC is Charisma-based.

__Wild Empathy (Ex):__ This power works like the druid’s wild empathy class feature, except that a unicorn has a +6 racial bonus on the check.

__Skills:__ Unicorns have a +4 racial bonus on Move Silently checks. &ast;Unicorns have a +3 competence bonus on Survival checks within the boundaries of their forest.

### Celestial Charger

The celestial charger described here is an 8 HD celestial unicorn with seven levels of cleric.

#### Combat

The save DC for this celestial charger’s *neutralize poison* ability (DC 20) is adjusted for its greater Hit Dice and altered Charisma score.

A celestial charger’s natural weapons are treated as magic weapons for the purpose of overcoming damage reduction.

__Smite Evil (Su):__ Once per day a celestial charger can make a normal melee attack to deal 15 points of extra damage against an evil foe.

*Cleric Spells Prepared* (6/7/6/5/4; save DC 18 + spell level): 0&mdash; *detect magic*, *detect poison* (2), *light*, *virtue* (2); 1st&mdash;*bless* (2), *calm animals*&ast;, *obscuring mist*, *remove fear*, *sanctuary*, *shield of faith*; 2nd&mdash;*aid*&ast; (2), *animal messenger*, *lesser restoration*, *remove paralysis*, *shield other*; 3rd&mdash;*prayer*, *protection from energy*, *remove curse*, *searing light* (2); 4th&mdash;*air walk*, *divine power*, *holy smite*&ast;, *restoration*.

&ast;Domain spell. Domains: Animal and Good.
