Barbarian
=========

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

### Game Rule Information

A barbarian has the following game statistics.

__Alignment:__ Any nonlawful.

__Hit Die:__ d12.

__Base Attack Bonus:__ Equal to total Hit Dice (as fighter).

__Good Saving Throw:__ Fortitude.

 
#### Class Skills

The barbarian’s class skills (and the key ability for each skill) are 
Climb (Str), Craft (Int), Handle Animal (Cha), Intimidate (Cha), Jump 
(Str), Listen (Wis), Ride (Dex), Survival (Wis), and Swim (Str).

__Skill Points at 1st Level:__ (4 + Int modifier) x 4.

__Skill Points at Each Additional Level:__ 4 + Int modifier.
 
#### Class Features

All of the following are class features of the barbarian.

__Weapon and Armor Proficiency:__ A barbarian is proficient with all 
simple and martial weapons, light armor, medium armor, and shields 
(except tower shields).

__Fast Movement (Ex):__ A barbarian’s land speed is faster than the norm 
for his race by +10 feet. This benefit applies only when he is wearing 
no armor, light armor, or medium armor and not carrying a heavy load. 
Apply this bonus before modifying the barbarian’s speed because of any 
load carried or armor worn.

__Literacy:__ Barbarians enjoy books.

__Rage (Ex):__ Rage is now tracked as a number of rounds per day. Modify 
and transcribe that information here.

A barbarian can fly into a rage for a number of rounds per day equal to 
4 &plus; their Constitution modifier. This number increases by 2 rounds 
for every level beyond 1st. Entering a rage takes no time itself, but a 
barbarian can do it only during his action, not in response to someone 
else’s action.

__Rage Powers:__ Starting at 2nd level, barbarians gain a rage power. These are special abilities that the barbarian can use while raging. Barbarians gain one power at 2nd level and one additional power for every two levels beyond 2nd. Rage powers may be selected from the following list.

List of rage powers.

__Uncanny Dodge (Ex):__ At 2nd level, a barbarian retains his Dexterity 
bonus to AC (if any) even if he is caught flat-footed or struck by an 
invisible attacker. However, he still loses his Dexterity bonus to AC if 
immobilized. If a barbarian already has uncanny dodge from a different 
class, he automatically gains improved uncanny dodge (see below) 
instead.

__Trap Sense (Ex):__ Starting at 3rd level, a barbarian gains a +1 bonus 
on Reflex saves made to avoid traps and a +1 dodge bonus to AC against 
attacks made by traps. These bonuses rise by +1 every three barbarian 
levels thereafter (6th, 9th, 12th, 15th, and 18th level). Trap sense 
bonuses gained from multiple classes stack.

__Improved Uncanny Dodge (Ex):__ At 5th level and higher, a barbarian 
can no longer be flanked. This defense denies a rogue the ability to 
sneak attack the barbarian by flanking him, unless the attacker has at 
least four more rogue levels than the target has barbarian levels. If a 
character already has uncanny dodge (see above) from a second class, the 
character automatically gains improved uncanny dodge instead, and the 
levels from the classes that grant uncanny dodge stack to determine the 
minimum level a rogue must be to flank the character.

__Damage Reduction (Ex):__ At 7th level, a barbarian gains Damage 
Reduction. Subtract 1 from the damage the barbarian takes each time he 
is dealt damage from a weapon or a natural attack. At 10th level, and 
every three barbarian levels thereafter (13th, 16th, and 19th level), 
this damage reduction rises by 1 point. Damage reduction can reduce 
damage to 0 but not below 0.

__Greater Rage (Ex):__ At 11th level, a barbarian’s bonuses to Strength 
and Constitution during his rage each increase to +6, and his morale 
bonus on Will saves increases to +3. The penalty to AC remains at –2.

__Indomitable Will (Ex):__ While in a rage, a barbarian of 14th level or 
higher gains a +4 bonus on Will saves to resist enchantment spells. This 
bonus stacks with all other modifiers, including the morale bonus on 
Will saves he also receives during his rage.

__Tireless Rage (Ex):__ At 17th level and higher, a barbarian no longer 
becomes fatigued at the end of his rage. Mighty Rage (Ex): At 20th 
level, a barbarian’s bonuses to Strength and Constitution during his 
rage each increase to +8, and his morale bonus on Will saves increases 
to +4. The penalty to AC remains at –2.
 
#### Ex-Barbarians

A barbarian who becomes lawful loses the ability to rage and cannot gain 
more levels as a barbarian. He retains all the other benefits of the 
class (damage reduction, fast movement, trap sense, and uncanny dodge).


Legal Information
=================

OPEN GAME LICENSE Version 1.0a
 
The following text is the property of Wizards of the Coast, Inc. and is Copyright 2000 Wizards of the Coast, Inc ("Wizards"). All Rights Reserved.
 
1. Definitions: (a)"Contributors" means the copyright and/or trademark owners who have contributed Open Game Content; (b)"Derivative Material" means copyrighted material including derivative works and translations (including into other computer languages), potation, modification, correction, addition, extension, upgrade, improvement, compilation, abridgment or other form in which an existing work may be recast, transformed or adapted; (c) "Distribute" means to reproduce, license, rent, lease, sell, broadcast, publicly display, transmit or otherwise distribute; (d)"Open Game Content" means the game mechanic and includes the methods, procedures, processes and routines to the extent such content does not embody the Product Identity and is an enhancement over the prior art and any additional content clearly identified as Open Game Content by the Contributor, and means any work covered by this License, including translations and derivative works under copyright law, but specifically excludes Product Identity. (e) "Product Identity" means product and product line names, logos and identifying marks including trade dress; artifacts; creatures characters; stories, storylines, plots, thematic elements, dialogue, incidents, language, artwork, symbols, designs, depictions, likenesses, formats, poses, concepts, themes and graphic, photographic and other visual or audio representations; names and descriptions of characters, spells, enchantments, personalities, teams, personas, likenesses and special abilities; places, locations, environments, creatures, equipment, magical or supernatural abilities or effects, logos, symbols, or graphic designs; and any other trademark or registered trademark clearly identified as Product identity by the owner of the Product Identity, and which specifically excludes the Open Game Content; (f) "Trademark" means the logos, names, mark, sign, motto, designs that are used by a Contributor to identify itself or its products or the associated products contributed to the Open Game License by the Contributor (g) "Use", "Used" or "Using" means to use, Distribute, copy, edit, format, modify, translate and otherwise create Derivative Material of Open Game Content. (h) "You" or "Your" means the licensee in terms of this agreement.
 
2. The License: This License applies to any Open Game Content that contains a notice indicating that the Open Game Content may only be Used under and in terms of this License. You must affix such a notice to any Open Game Content that you Use. No terms may be added to or subtracted from this License except as described by the License itself. No other terms or conditions may be applied to any Open Game Content distributed using this License.
 
3. Offer and Acceptance: By Using the Open Game Content You indicate Your acceptance of the terms of this License.
 
4. Grant and Consideration: In consideration for agreeing to use this License, the Contributors grant You a perpetual, worldwide, royalty-free, non-exclusive license with the exact terms of this License to Use, the Open Game Content.
 
5. Representation of Authority to Contribute: If You are contributing original material as Open Game Content, You represent that Your Contributions are Your original creation and/or You have sufficient rights to grant the rights conveyed by this License.
 
6. Notice of License Copyright: You must update the COPYRIGHT NOTICE portion of this License to include the exact text of the COPYRIGHT NOTICE of any Open Game Content You are copying, modifying or distributing, and You must add the title, the copyright date, and the copyright holder's name to the COPYRIGHT NOTICE of any original Open Game Content you Distribute.
 
7. Use of Product Identity: You agree not to Use any Product Identity, including as an indication as to compatibility, except as expressly licensed in another, independent Agreement with the owner of each element of that Product Identity. You agree not to indicate compatibility or co-adaptability with any Trademark or Registered Trademark in conjunction with a work containing Open Game Content except as expressly licensed in another, independent Agreement with the owner of such Trademark or Registered Trademark. The use of any Product Identity in Open Game Content does not constitute a challenge to the ownership of that Product Identity. The owner of any Product Identity used in Open Game Content shall retain all rights, title and interest in and to that Product Identity.
 
8. Identification: If you distribute Open Game Content You must clearly indicate which portions of the work that you are distributing are Open Game Content.
 
9. Updating the License: Wizards or its designated Agents may publish updated versions of this License. You may use any authorized version of this License to copy, modify and distribute any Open Game Content originally distributed under any version of this License.
 
10. Copy of this License: You MUST include a copy of this License with every copy of the Open Game Content You Distribute.
 
11. Use of Contributor Credits: You may not market or advertise the Open Game Content using the name of any Contributor unless You have written permission from the Contributor to do so.
 
12. Inability to Comply: If it is impossible for You to comply with any of the terms of this License with respect to some or all of the Open Game Content due to statute, judicial order, or governmental regulation then You may not Use any Open Game Material so affected.
 
13. Termination: This License will terminate automatically if You fail to comply with all terms herein and fail to cure such breach within 30 days of becoming aware of the breach. All sublicenses shall survive the termination of this License.
 
14. Reformation: If any provision of this License is held to be unenforceable, such provision shall be reformed only to the extent necessary to make it enforceable.
 
15. COPYRIGHT NOTICE
Open Game License v 1.0a Copyright 2000, Wizards of the Coast, Inc.
 
__System Reference Document__ Copyright 2000-2003, Wizards of the Coast, Inc.; Authors Jonathan Tweet, Monte Cook, Skip Williams, Rich Baker, Andy Collins, David Noonan, Rich Redman, Bruce R. Cordell, John D. Rateliff, Thomas Reid, James Wyatt, based on original material by E. Gary Gygax and Dave Arneson.

__Pathfinder Roleplaying Game Conversion Guide__. Copyright 2009, Paizo Publishing, LLC; Author: Jason Bulmahn.

__Pathfinder Roleplaying Game Core Rulebook__. Copyright 2009, Paizo Publishing, LLC; Author: Jason Bulmahn, based on material by Jonathan Tweet, Monte Cook, and Skip Williams.
 
END OF LICENSE
