Chimera
-------
	

Large Magical Beast

Hit Dice:
	

9d10+27 (76 hp)

Initiative:
	

+1

Speed:
	

30 ft. (6 squares), fly 50 ft. (poor)

Armor Class:
	

19 (–1 size, +1 Dex, +9 natural), touch 10, flat-footed 18

Base Attack/Grapple:
	

+9/+17

Attack:
	

Bite +12 melee (2d6+4)

Full Attack:
	

Bite +12 melee (2d6+4) and bite +12 melee (1d8+4) and gore +12 melee (1d8+4) and 2 claws +10 melee (1d6+2)

Space/Reach:
	

10 ft./5 ft.

Special Attacks:
	

Breath weapon

Special Qualities:
	

Darkvision 60 ft., low-light vision, scent

Saves:
	

Fort +9, Ref +7, Will +6

Abilities:
	

Str 19, Dex 13, Con 17, Int 4, Wis 13, Cha 10

Skills:
	

Hide +1*, Listen +9, Spot +9

Feats:
	

Alertness, Hover, Iron Will, Multiattack

Environment:
	

Temperate hills

Organization:
	

Solitary, pride (3–5), or flight (6–13)

Challenge Rating:
	

7

Treasure:
	

Standard

Alignment:
	

Usually chaotic evil

Advancement:
	

10–13 HD (Large); 14–27 HD (Huge)

Level Adjustment:
	

+2 (cohort)

A chimera is about 5 feet tall at the shoulder, nearly 10 feet long, and weighs about 4,000 pounds. A chimera’s dragon head might be black, blue, green, red, or white.

Chimeras can speak Draconic but seldom bother to do so, except when toadying to more powerful creatures.

COMBAT

The chimera prefers to surprise prey. It often swoops down from the sky or lies concealed until it charges. The dragon head can loose a breath weapon instead of biting. Several chimeras attack in concert.

Breath Weapon (Su): A chimera’s breath weapon depends on the color of its dragon head, as summarized on the table below. Regardless of its type, a chimera’s breath weapon is usable once every 1d4 rounds, deals 3d8 points of damage, and allows a DC 17 Reflex save for half damage. The save DC is Constitution-based.

To determine a chimera’s head color and breath weapon randomly, roll 1d10 and consult the table below.

1d10
	

Head Color
	

Breath Weapon

1–2
	

Black
	

40-foot line of acid

3–4
	

Blue
	

40-foot line of lightning

5–6
	

Green
	

20-foot cone of gas (acid)

7–8
	

Red
	

20-foot cone of fire

9–10
	

White
	

20-foot cone of cold

Skills: A chimera’s three heads give it a +2 racial bonus on Spot and Listen checks.

*In areas of scrubland or brush, a chimera gains a +4 racial bonus on Hide checks.

Carrying Capacity: A light load for a chimera is up to 348 pounds; a medium load, 349–699 pounds, and a heavy load, 700–1,050 pounds.


Kraken
------
	

Gargantuan Magical Beast (Aquatic)

Hit Dice:
	

20d10+180 (290 hp)

Initiative:
	

+4

Speed:
	

Swim 20 ft. (4 squares)

Armor Class:
	

20 (–4 size, +14 natural), touch 6, flat-footed 20

Base Attack/Grapple:
	

+20/+44

Attack:
	

Tentacle +28 melee (2d8+12/19–20)

Full Attack:
	

2 tentacles +28 melee (2d8+12/19–20) and 6 arms +23 melee (1d6+6) and bite +23 melee (4d6+6)

Space/Reach:
	

20 ft./15 ft. (60 ft. with tentacle, 30 ft. with arm)

Special Attacks:
	

Improved grab, constrict 2d8+12 or 1d6+6

Special Qualities:
	

Darkvision 60 ft., ink cloud, jet, low-light vision, spell-like abilities

Saves:
	

Fort +21, Ref +12, Will +13

Abilities:
	

Str 34, Dex 10, Con 29, Int 21, Wis 20, Cha 20

Skills:
	

Concentration +21, Diplomacy +7, Hide +0, Intimidate +16, Knowledge (geography) +17, Knowledge (nature) +16, Listen +30, Search +28, Sense Motive +17, Spot +30, Survival +5 (+7 following tracks), Swim +20, Use Magic Device +16

Feats:
	

Alertness, Blind-Fight, Combat Expertise, Improved Critical (tentacle), Improved Initiative, Improved Trip, Iron Will

Environment:
	

Temperate aquatic

Organization:
	

Solitary

Challenge Rating:
	

12

Treasure:
	

Triple standard

Alignment:
	

Usually neutral evil

Advancement:
	

21–32 HD (Gargantuan); 33–60 HD (Colossal)

Level Adjustment:
	

—

Six of the beast’s tentacles are shorter arms about 30 feet long; the remaining two are nearly 60 feet long and covered with barbs. Its beaklike mouth is located where the tentacles meet the lower portion of its body.

Krakens speak Common and Aquan.

COMBAT

Krakens strike their opponents with their barbed tentacles, then grab and crush with their arms or drag victims into their huge jaws. An opponent can make sunder attempts against a kraken’s tentacles or arms as if they were weapons. A kraken’s tentacles have 20 hit points, and its arms have 10 hit points. If a kraken is currently grappling a target with one tentacle or arm, it usually uses another limb to make its attack of opportunity against the sunder attempt. Severing a kraken’s tentacle or arm deals damage to the kraken equal to half the limb’s full normal hit points. A kraken usually withdraws from combat if it loses both tentacles or three of its arms. A kraken regrows severed limbs in 1d10+10 days.

Improved Grab (Ex): To use this ability, the kraken must hit with an arm or tentacle attack. It can then attempt to start a grapple as a free action without provoking an attack of opportunity. If it wins the grapple check, it establishes a hold and can constrict.

Constrict (Ex): A kraken deals automatic arm or tentacle damage with a successful grapple check.

Jet (Ex): A kraken can jet backward once per round as a full-round action, at a speed of 280 feet. It must move in a straight line, but does not provoke attacks of opportunity while jetting.

Ink Cloud (Ex): A kraken can emit a cloud of jet-black ink in an 80-foot spread once per minute as a free action. The cloud provides total concealment, which the kraken normally uses to escape a fight that is going badly. Creatures within the cloud are considered to be in darkness.

Spell-Like Abilities: 1/day—control weather, control winds, dominate animal (DC 18), resist energy. Caster level 9th. The save DC is Charisma-based.

Skills: A kraken has a +8 racial bonus on any Swim check to perform some special action or avoid a hazard. It can always choose to take 10 on a Swim check, even if distracted or endangered. It can use the run action while swimming, provided it swims in a straight line.


Manticore
---------
	

Large Magical Beast

Hit Dice:
	

6d10+24 (57 hp)

Initiative:
	

+2

Speed:
	

30 ft. (6 squares), fly 50 ft. (clumsy)

Armor Class:
	

17 (–1 size, +2 Dex, +6 natural), touch 11, flat-footed 15

Base Attack/Grapple:
	

+6/+15

Attack:
	

Claw +10 melee (2d4+5) or 6 spikes +8 ranged (1d8+2/19–20)

Full Attack:
	

2 claws +10 melee (2d4+5) and bite +8 melee (1d8+2); or 6 spikes +8 ranged (1d8+2/19–20)

Space/Reach:
	

10 ft./5 ft.

Special Attacks:
	

Spikes

Special Qualities:
	

Darkvision 60 ft., low-light vision, scent

Saves:
	

Fort +9, Ref +7, Will +3

Abilities:
	

Str 20, Dex 15, Con 19, Int 7, Wis 12, Cha 9

Skills:
	

Listen +5, Spot +9, Survival +1

Feats:
	

Flyby Attack, Multiattack, TrackB, Weapon Focus (spikes)

Environment:
	

Warm marshes

Organization:
	

Solitary, pair, or pride (3–6)

Challenge Rating:
	

5

Treasure:
	

Standard

Alignment:
	

Usually lawful evil

Advancement:
	

7–16 HD (Large); 17–18 HD (Huge)

Level Adjustment:
	

+3 (cohort)

A typical manticore is about 10 feet long and weighs about 1,000 pounds. Manticores speak Common.

COMBAT

A manticore begins most attacks with a volley of spikes, then closes. In the outdoors, it often uses its powerful wings to stay aloft during battle.

Spikes (Ex): With a snap of its tail, a manticore can loose a volley of six spikes as a standard action (make an attack roll for each spike). This attack has a range of 180 feet with no range increment. All targets must be within 30 feet of each other. The creature can launch only twenty-four spikes in any 24-hour period.

Skills: *Manticores have a +4 racial bonus on Spot checks.


Pegasus
-------
	

Large Magical Beast

Hit Dice:
	

4d10+12 (34 hp)

Initiative:
	

+2

Speed:
	

60 ft. (12 squares), fly 120 ft. (average)

Armor Class:
	

14 (–1 size, +2 Dex, +3 natural), touch 11, flat-footed 12

Base Attack/Grapple:
	

+4/+12

Attack:
	

Hoof +7 melee (1d6+4)

Full Attack:
	

2 hooves +7 melee (1d6+4) and bite +2 melee (1d3+2)

Space/Reach:
	

10 ft./5 ft.

Special Attacks:
	

—

Special Qualities:
	

Darkvision 60 ft., low-light vision, scent, spell-like abilities

Saves:
	

Fort +7, Ref +6, Will +4

Abilities:
	

Str 18, Dex 15, Con 16, Int 10, Wis 13, Cha 13

Skills:
	

Diplomacy +3, Listen +8, Sense Motive +9, Spot +8

Feats:
	

Flyby Attack, Iron Will

Environment:
	

Temperate forests

Organization:
	

Solitary, pair, or herd (6–10)

Challenge Rating:
	

3

Treasure:
	

None

Alignment:
	

Usually chaotic good

Advancement:
	

5–8 HD (Large)

Level Adjustment:
	

+2 (cohort)

The pegasus is a magnificent winged horse that sometimes serves the cause of good. Though highly prized as aerial steeds, pegasi are wild and shy creatures not easily tamed.

A typical pegasus stands 6 feet high at the shoulder, weighs 1,500 pounds, and has a wingspan of 20 feet. Pegasi cannot speak, but they understand Common.

COMBAT

Spell-Like Abilities: At will—detect good and detect evil within a 60-foot radius. Caster level 5th.

Skills: Pegasi have a +4 racial bonus on Listen and Spot checks.

TRAINING A PEGASUS

Although intelligent, a pegasus requires training before it can bear a rider in combat. To be trained, a pegasus must have a friendly attitude toward the trainer (this can be achieved through a successful Diplomacy check). Training a friendly pegasus requires six weeks of work and a DC 25 Handle Animal check. Riding a pegasus requires an exotic saddle. A pegasus can fight while carrying a rider, but the rider cannot also attack unless he or she succeeds on a Ride check.

Pegasus eggs are worth 2,000 gp each on the open market, while young are worth 3,000 gp per head. Pegasi mature at the same rate as horses. Professional trainers charge 1,000 gp to rear or train a pegasus, which serves a good or neutral master with absolute faithfulness for life.

Carrying Capacity: A light load for a pegasus is up to 300 pounds; a medium load, 301–600 pounds; and a heavy load, 601–900 pounds.


TRITON

	

Medium Outsider (Native, Water)

Hit Dice:
	

3d8+3 (16 hp)

Initiative:
	

+0

Speed:
	

5 ft. (1 square), swim 40 ft.

Armor Class:
	

16 (+6 natural), touch 10, flat-footed 16

Base Attack/Grapple:
	

+3/+4

Attack:
	

Trident +4 melee (1d8+1) or heavy crossbow +3 ranged (1d10/19–20)

Full Attack:
	

Trident +4 melee (1d8+1) or heavy crossbow +3 ranged (1d10/19–20)

Space/Reach:
	

5 ft./5 ft.

Special Attacks:
	

Spell-like abilities

Special Qualities:
	

Darkvision 60 ft.

Saves:
	

Fort +4, Ref +3, Will +4

Abilities:
	

Str 12, Dex 10, Con 12, Int 13, Wis 13, Cha 11

Skills:
	

Craft (any one) +7, Diplomacy +2, Hide +6, Listen +7, Move Silently +6, Ride +6, Search +7, Sense Motive +7, Spot +7, Survival +7 (+9 following tracks), Swim +9

Feats:
	

Mounted Combat, Ride-By Attack

Environment:
	

Temperate aquatic

Organization:
	

Company (2–5), squad (6–11), or band (20–80)

Challenge Rating:
	

2

Treasure:
	

Standard

Alignment:
	

Usually neutral good

Advancement:
	

4–9 HD (Medium)

Level Adjustment:
	

+2

A triton has silvery skin that fades into silver-blue scales on the lower half of its body. A triton’s hair is deep blue or blue-green.

A triton is about the same size and weight as a human. Tritons speak Common and Aquan.

COMBAT

The reclusive tritons prefer to avoid combat, but they fiercely defend their homes. They attack with either melee or ranged weapons as the circumstances warrant. When encountered outside their lair, they are 90% likely to be mounted on friendly sea creatures such as porpoises.

Spell-Like Abilities: 1/day—summon nature’s ally IV. Caster level 7th. Tritons often choose water elementals for their companions.

Skills: A triton has a +8 racial bonus on any Swim check to perform some special action or avoid a hazard. It can always choose to take 10 on a Swim check, even if distracted or endangered. It can use the run action while swimming, provided it swims in a straight line.
