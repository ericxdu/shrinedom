<table>
  <caption>Table: Creature Attributes</caption>
  <tr>
    <th>Creature</th>
  </tr>
  <tr>
    <td>Dog</td>
  </tr>
  <tr>
    <td>Lizard</td>
  </tr>
  <tr>
    <td>Manta Ray</td>
  </tr>
  <tr>
    <td>Monkey</td>
  </tr>
  <tr>
    <td>Porpoise</td>
  </tr>
  <tr>
    <td>Rat</td>
  </tr>
  <tr>
    <td>Raven</td>
  </tr>
  <tr>
    <td>Shark</td>
  </tr>
  <tr>
    <td>Squid</td>
  </tr>
  <tr>
    <td>Giant Squid</td>
  </tr>
  <tr>
    <td>Baleen Whale</td>
  </tr>
  <tr>
    <td>Cachalot Whale</td>
  </tr>
  <tr>
    <td>Orca</td>
  </tr>
</table>

### Dog

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

+ **Small Animal**
+ **Hit Dice**: 1d8+2 (6 hp)
+ **Initiative**: +3
+ **Speed**: 40 ft. (8 squares)
+ **Armor Class**: 15 (+1 size, +3 Dex, +1 natural), touch 14, flat-footed 12
+ **Base Attack/Grapple**: +0/–3
+ **Attack**: Bite +2 melee (1d4+1)
+ **Full Attack**: Bite +2 melee (1d4+1)
+ **Space/Reach**: 5 ft./5 ft.
+ **Special Attacks**: &mdash;
+ **Special Qualities**: Low-light vision, scent
+ **Saves**: Fort +4, Ref +5, Will +1
+ **Abilities**: Str 13, Dex 17, Con 15, Int 2, Wis 12, Cha 6
+ **Skills**: Jump +7, Listen +5, Spot +5, Survival +1*
+ **Feats**: Alertness, TrackB
+ **Environment**: Temperate plains
+ **Organization**: Solitary or pack (5–12)
+ **Challenge Rating**: 1/3
+ **Advancement**: &mdash;
+ **Level Adjustment**: &mdash;

The statistics presented here describe a fairly small dog of about 20 to 50 pounds in weight. They also can be used for small wild canines such as coyotes, jackals, and African wild dogs.

#### Combat

Dogs generally hunt in packs, chasing and exhausting prey until they can drag it down.

**Skills**: Dogs have a +4 racial bonus on Jump checks. &ast;Dogs have a +4 racial bonus on Survival checks when tracking by scent.

### Lizard

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Tiny Animal**
- **Hit Dice**: 1/2 d8 (2 hp)
- **Initiative**: +2
- **Speed**: 20 ft. (4 squares), climb 20 ft.
- **Armor Class**: 14 (+2 size, +2 Dex), touch 14, flat-footed 12
- **Base Attack/Grapple**: +0/–12
- **Attack**: Bite +4 melee (1d4–4)
- **Full Attack**: Bite +4 melee (1d4–4)
- **Space/Reach**: 2-1/2 ft./0 ft.
- **Special Attacks**: &mdash;
- **Special Qualities**: Low-light vision
- **Saves**: Fort +2, Ref +4, Will +1
- **Abilities**: Str 3, Dex 15, Con 10, Int 1, Wis 12, Cha 2
- **Skills**: Balance +10, Climb +12, Hide +10, Listen +3, Spot +3
- **Feats**: Weapon Finesse
- **Environment**: Warm forests
- **Organization**: Solitary
- **Challenge Rating**: 1/6
- **Advancement**: &mdash;
- **Level Adjustment**: &mdash;

The statistics presented here describe small, nonvenomous lizards of perhaps a foot or two in length, such as an iguana.

#### Combat

Lizards prefer flight to combat, but they can bite painfully if there is no other option.

**Skills**: Lizards have a +8 racial bonus on Balance checks. They also have a +8 racial bonus on Climb checks and can always choose to take 10 on Climb checks, even if rushed or threatened. Lizards use their Dexterity modifier instead of their Strength modifier for Climb checks.

### Manta Ray

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

<table>
<tr><th></th><th>Large Animal (Aquatic)</th></tr>
<tr><th>Hit Dice:</th><td>4d8 (18 hp)</td></tr>
<tr><th>Initiative:</th><td>+0</td></tr>
<tr><th>Speed:</th><td>Swim 30 ft. (6 squares)</td></tr>
<tr><th>Armor Class:</th><td>12 (&ndash;1 size, +3 natural), touch 9, flat-footed 12</td></tr>
<tr><th>Base Attack/Grapple:</th><td>+3/+9</td></tr>
<tr><th>Attack:</th><td>Ram &ndash;1 melee* (1d6+1)</td></tr>
<tr><th>Full Attack:</th><td>Ram &ndash;1 melee* (1d6+1)</td></tr>
<tr><th>Space/Reach:</th><td>10 ft./5 ft.</td></tr>
<tr><th>Special Attacks:</th><td>&mdash;</td></tr>
<tr><th>Special Qualities:</th><td>Low-light vision</td></tr>
<tr><th>Saves:</th><td>Fort +4, Ref +4, Will +2</td></tr>
<tr><th>Abilities:</th><td>Str 15, Dex 11, Con 10, Int 1, Wis 12, Cha 2</td></tr>
<tr><th>Skills:</th><td>Listen +7, Spot +6, Swim +10</td></tr>
<tr><th>Feats:</th><td>Alertness, Endurance</td></tr>
<tr><th>Environment:</th><td>Warm aquatic</td></tr>
<tr><th>Organization:</th><td>Solitary or school (2&ndash;5)</td></tr>
<tr><th>Challenge Rating:</th><td>1</td></tr>
<tr><th>Advancement:</th><td>5&ndash;6 HD (Medium)</td></tr>
<tr><th>Level Adjustment:</th><td>&mdash;</td></tr>
</table>

These fish are nonaggressive and generally avoid contact with other creatures. They filter plankton and similar small organisms from the water through their gaping, toothless maws.

#### Combat

&ast;If threatened, a manta ray uses its size and weight to ram opponents. This is treated as a secondary attack.

**Skills**: A manta ray has a +8 racial bonus on any Swim check to perform some special action or avoid a hazard. It can always choose to take 10 on a Swim check, even if distracted or endangered. It can use the run action while swimming, provided it swims in a straight line.

### Monkey

- **Tiny Animal**
- **Hit Dice:** 1d8 (4 hp)
- **Initiative:** +2
- **Speed:** 30 ft. (6 squares), climb 30 ft.
- **Armor Class:** 14 (+2 size, +2 Dex), touch 14, flat-footed 12
- **Base Attack/Grapple:** +0/–12
- **Attack:** Bite +4 melee (1d3–4)
- **Full Attack:** Bite +4 melee (1d3–4)
- **Space/Reach:** 2-1/2 ft./0 ft.
- **Special Attacks:** —
- **Special Qualities:** Low-light vision
- **Saves:** Fort +2, Ref +4, Will +1
- **Abilities:** Str 3, Dex 15, Con 10, Int 2, Wis 12, Cha 5
- **Skills:** Balance + 10, Climb +10, Hide +10, Listen +3, Spot +3
- **Feats:** Weapon Finesse
- **Environment:** Warm forests
- **Organization:** Troop (10–40)
- **Challenge Rating:** 1/6
- **Advancement:** 2–3 HD (Small)
- **Level Adjustment:** —

The statistics presented here can describe any arboreal monkey that is no bigger than a housecat, such as a colobus or capuchin.

#### Combat

Monkeys generally flee into the safety of the trees, but if cornered can fight ferociously.

__Skills:__ Monkeys have a +8 racial bonus on Balance and Climb checks. They can always choose to take 10 on Climb checks, even if rushed or threatened. They use their Dexterity modifier instead of their Strength modifier for Climb checks.

### Porpoise

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

<table>
<tr><th></th><th>Medium Animal</th></tr>
<tr><th>Hit Dice:</th><td>2d8+2 (11 hp)</td></tr>
<tr><th>Initiative:</th><td>+3</td></tr>
<tr><th>Speed:</th><td>Swim 80 ft. (16 squares)</td></tr>
<tr><th>Armor Class:</th><td>15 (+3 Dex, +2 natural), touch 13, flat-footed 12</td></tr>
<tr><th>Base Attack/Grapple:</th><td>+1/+1</td></tr>
<tr><th>Attack:</th><td>Slam +4 melee (2d4)</td></tr>
<tr><th>Full Attack:</th><td>Slam +4 melee (2d4)</td></tr>
<tr><th>Space/Reach:</th><td>5 ft./5 ft.</td></tr>
<tr><th>Special Attacks:</th><td>&mdash;</td></tr>
<tr><th>Special Qualities:</th><td>Blindsight 120 ft., hold breath, low-light vision</td></tr>
<tr><th>Saves:</th><td>Fort +4, Ref +6, Will +1</td></tr>
<tr><th>Abilities:</th><td>Str 11, Dex 17, Con 13, Int 2, Wis 12, Cha 6</th></tr>
<tr><th>Skills:</th><td>Listen +8*, Spot +7*, Swim +8</td></tr>
<tr><th>Feats:</th><td>Weapon Finesse</th></tr>
<tr><th>Environment:</th><td>Temperate aquatic</td></tr>
<tr><th>Organization:</th><td>Solitary, pair, or school (3&ndash;20)</td></tr>
<tr><th>Challenge Rating:</th><td>1/2</th></tr>
<tr><th>Advancement:</th><td>3&ndash;4 HD (Medium); 5&ndash;6 HD (Large)</td></tr>
<tr><th>Level Adjustment:</th><td>&mdash;</td></tr>
</table>

Porpoises are mammals that tend to be playful, friendly, and helpful. A typical porpoise is 4 to 6 feet long and weighs 110 to 160 pounds. The statistics presented here can describe any small whale of similar size.

#### Combat

<p></p>

**Blindsight (Ex)**: Porpoises can &ldquo;see&rdquo; by emitting high-frequency sounds, inaudible to most other creatures, that allow them to locate objects and creatures within 120 feet. A silence spell negates this and forces the porpoise to rely on its vision, which is approximately as good as a human&rsquo;s.

**Hold Breath (Ex)**: A porpoise can hold its breath for a number of rounds equal to 6 &times its Constitution score before it risks drowning.

**Skills**: A porpoise has a +8 racial bonus on any Swim check to perform some special action or avoid a hazard. It can always choose to take 10 on a Swim check, even if distracted or endangered. It can use the run action while swimming, provided it swims in a straight line. *A porpoise has a +4 racial bonus on Spot and Listen checks. These bonuses are lost if its blindsight is negated.

### Rat

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Tiny Animal**
- **Hit Dice**: 1/4 d8 (1 hp)
- **Initiative**: +2
- **Speed**: 15 ft. (3 squares), climb 15 ft., swim 15 ft.
- **Armor Class**: 14 (+2 size, +2 Dex), touch 14, flat-footed 12
- **Base Attack/Grapple**: +0/–12
- **Attack**: Bite +4 melee (1d3–4)
- **Full Attack**: Bite +4 melee (1d3–4)
- **Space/Reach**: 2-1/2 ft./0 ft.
- **Special Attacks**: &mdash;
- **Special Qualities**: Low-light vision, scent
- **Saves**: Fort +2, Ref +4, Will +1
- **Abilities**: Str 2, Dex 15, Con 10, Int 2, Wis 12, Cha 2
- **Skills**: Balance +10, Climb +12, Hide +14, Move Silently +10, Swim +10
- **Feats**: Weapon Finesse
- **Environment**: Any
- **Organization**: Plague (10–100)
- **Challenge Rating**: 1/8
- **Advancement**: &mdash;
- **Level Adjustment**: &mdash;

These omnivorous rodents thrive almost anywhere.

#### Combat

Rats usually run away. They bite only as a last resort.

**Skills**: Rats have a +4 racial bonus on Hide and Move Silently checks, and a +8 racial bonus on Balance, Climb, and Swim checks. A rat can always choose to take 10 on Climb checks, even if rushed or threatened. A rat uses its Dexterity modifier instead of its Strength modifier for Climb and Swim checks. A rat has a +8 racial bonus on any Swim check to perform some special action or avoid a hazard. It can always choose to take 10 on a Swim check, even if distracted or endangered. It can use the run action while swimming, provided it swims in a straight line.

### Raven

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Tiny Animal**
- **Hit Dice**: 1/4 d8 (1 hp)
- **Initiative**: +2
- **Speed**: 10 ft. (2 squares), fly 40 ft. (average)
- **Armor Class**: 14 (+2 size, +2 Dex), touch 14, flat-footed 12
- **Base Attack/Grapple**: +0/–13
- **Attack**: Claws +4 melee (1d2–5)
- **Full Attack**: Claws +4 melee (1d2–5)
- **Space/Reach**: 2-1/2 ft./0 ft.
- **Special Attacks**: &mdash;
- **Special Qualities**: Low-light vision
- **Saves**: Fort +2, Ref +4, Will +2
- **Abilities**: Str 1, Dex 15, Con 10, Int 2, Wis 14, Cha 6
- **Skills**: Listen +3, Spot +5
- **Feats**: Weapon Finesse
- **Environment**: Temperate forests
- **Organization**: Solitary
- **Challenge Rating**: 1/6
- **Advancement**: &mdash;
- **Level Adjustment**: &mdash;

These glossy black birds are about 2 feet long and have wingspans of about 4 feet. They combine both claws into a single attack. The statistics presented here can describe most nonpredatory birds of similar size.

### Shark

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

<table style="margin:0;width:100%;">
<tr><th></th><th>Shark, Medium</th><th>Shark, Large</th><th>Shark, Huge</th></tr>
<tr><th></th><th>Medium Animal (Aquatic)</th><th>Large Animal (Aquatic)</th><th>Huge Animal (Aquatic)</th></tr>
<tr><th>Hit Dice:</th><td>3d8+3 (16 hp)</td><td>7d8+7 (38 hp)</td><td>10d8+20 (65 hp)</td></tr>
<tr><th>Initiative:</th><td>+2</td><td>+6</td><td>+6</td></tr>
<tr><th>Speed:</th><td>Swim 60 ft. (12 squares)</td><td>Swim 60 ft. (12 squares)</td><td>Swim 60 ft. (12 squares)</td></tr>
<tr><th>Armor Class:</th><td>15 (+2 Dex, +3 natural), touch 12, flat-footed 13</td><td>15 (&ndash;1 size, +2 Dex, +4 natural), touch 11, flat-footed 13</td><td>15 (&ndash;2 size, +2 Dex, +5 natural), touch 10, flat-footed 13</td></tr>
<tr><th>Base Attack/Grapple:</th><td>+2/+3</td><td>+5/+12</td><td>+7/+20</td></tr>
<tr><th>Attack:</th><td>Bite +4 melee (1d6+1)</td><td>Bite +7 melee (1d8+4)</td><td>Bite +10 melee (2d6+7)</td></tr>
<tr><th>Full Attack:</th><td>Bite +4 melee (1d6+1)</td><td>Bite +7 melee (1d8+4)</td><td>Bite +10 melee (2d6+7)</td></tr>
<tr><th>Space/Reach:</th><td>5 ft./5 ft.</td><td>10 ft./5 ft.</td><td>15 ft./10 ft.</td></tr>
<tr><th>Special Attacks:</th><td>&mdash;</td><td>&mdash;</td><td>&mdash;</td></tr>
<tr><th>Special Qualities:</th><td>Blindsense, keen scent</td><td>Blindsense, keen scent</td><td>Blindsense, keen scent</td></tr>
<tr><th>Saves:</th><td>Fort +4, Ref +5, Will +2</td><td>Fort +8, Ref +7, Will +3</td><td>Fort +11, Ref +9, Will +4</td></tr>
<tr><th>Abilities:</th><td>Str 13, Dex 15, Con 13, Int 1, Wis 12, Cha 2</td><td>Str 17, Dex 15, Con 13, Int 1, Wis 12, Cha 2</td><td>Str 21, Dex 15, Con 15, Int 1, Wis 12, Cha 2</td></tr>
<tr><th>Skills:</th><td>Listen +6, Spot +6, Swim +9</td><td>Listen +8, Spot +7, Swim +11</td><td>Listen +10, Spot +10, Swim +13</td></tr>
<tr><th>Feats:</th><td>Alertness, Weapon Finesse</td><td>Alertness, Great Fortitude, Improved Initiative</td><td>Alertness, Great Fortitude, Improved Initiative, Iron Will</td></tr>
<tr><th>Environment:</th><td>Cold aquatic</td><td>Cold aquatic</td><td>Cold aquatic</td></tr>
<tr><th>Organization:</th><td>Solitary, school (2&ndash;5), or pack (6&ndash;11)</td><td>Solitary, school (2&ndash;5), or pack (6&ndash;11)</td><td>Solitary, school (2&ndash;5), or pack (6&ndash;11)</td></tr>
<tr><th>Challenge Rating:</th><td>1</td><td>2</td><td>4</td></tr>
<tr><th>Advancement:</th><td>4&ndash;6 HD (Medium)</td><td>8&ndash;9 HD (Large)</td><td>11&ndash;17 HD (Huge)</td></tr>
<tr><th>Level Adjustment:</th><td>&mdash;</td><td>&mdash;</td><td>&mdash;</td></tr>
</table>

These carnivorous fish are aggressive and liable to make unprovoked attacks against anything that approaches them. Smaller sharks are from 5 to 8 feet long and not usually dangerous to creatures other than their prey. Large sharks can reach around 15 feet in length and are a serious threat. Huge sharks are true monsters, like great whites, that can exceed 20 feet in length.
	
#### Combat

Sharks circle and observe potential prey, then dart in and bite with their powerful jaws.

**Blindsense (Ex)**: A shark can locate creatures underwater within a 30-foot radius. This ability works only when the shark is underwater.

**Keen Scent (Ex)**: A shark can notice creatures by scent in a 180-foot radius and detect blood in the water at ranges of up to a mile.

**Skills**: A shark has a +8 racial bonus on any Swim check to perform some special action or avoid a hazard. It can always choose to take 10 on a Swim check, even if distracted or endangered. It can use the run action while swimming, provided it swims in a straight line.

### Squid

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Medium Animal (Aquatic)**
- **Hit Dice**: 3d8 (13 hp)
- **Initiative**: +3
- **Speed**: Swim 60 ft. (12 squares)
- **Armor Class**: 16 (+3 Dex, +3 natural), touch 13, flat-footed 13
- **Base Attack/Grapple**: +2/+8*
- **Attack**: Arms +4 melee (0)
- **Full Attack**: Arms +4 melee (0) and bite –1 melee (1d6+1)
- **Space/Reach**: 5 ft./5 ft.
- **Special Attacks**: Improved grab
- **Special Qualities**: Ink cloud, jet, low-light vision
- **Saves**: Fort +3, Ref +6, Will +2
- **Abilities**: Str 14, Dex 17, Con 11, Int 1, Wis 12, Cha 2
- **Skills**: Listen +7, Spot +7, Swim +10
- **Feats**: Alertness, Endurance
- **Environment**: Temperate aquatic
- **Organization**: Solitary or school (6–11)
- **Challenge Rating**: 1
- **Advancement**: 4–6 HD (Medium); 7–11 HD (Large)
- **Level Adjustment**: &mdash;

These free-swimming mollusks are fairly aggressive. They are more feared than sharks in some locales.

#### Combat

**Improved Grab (Ex)**: To use this ability, a squid must hit an opponent of any size with its arms attack. It can then attempt to start a grapple as a free action without provoking an attack of opportunity. If it wins the grapple check, it establishes a hold and automatically deals bite damage. &ast;A squid has a +4 racial bonus on grapple checks.

**Ink Cloud (Ex)**: A squid can emit a cloud of jet-black ink 10 feet high by 10 feet wide by 10 feet long once per minute as a free action. The cloud provides total concealment, which the squid normally uses to escape a losing fight. All vision within the cloud is obscured.

**Jet (Ex)**: A squid can jet backward once per round as a full-round action, at a speed of 240 feet. It must move in a straight line, but does not provoke attacks of opportunity while jetting.

**Skills**: A squid has a +8 racial bonus on any Swim check to perform some special action or avoid a hazard. It can always choose to take 10 on a Swim check, even if distracted or endangered. It can use the run action while swimming, provided it swims in a straight line

### Squid, Giant

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Huge Animal (Aquatic)**
- **Hit Dice**: 12d8+18 (72 hp)
- **Initiative**: +3
- **Speed**: Swim 80 ft. (16 squares)
- **Armor Class**: 17 (–2 size, +3 Dex, +6 natural), touch 11, flat-footed 14
- **Base Attack/Grapple**: +9/+29
- **Attack**: Tentacle +15 melee (1d6+8)
- **Full Attack**: 10 tentacles +15 melee (1d6+8) and bite +10 melee (2d8+4)
- **Space/Reach**: 15 ft./15 ft. (30 ft. with tentacle)
- **Special Attacks**: Constrict 1d6+8, improved grab
- **Special Qualities**: Ink cloud, jet, low-light vision
- **Saves**: Fort +9, Ref +11, Will +5
- **Abilities**: Str 26, Dex 17, Con 13, Int 1, Wis 12, Cha 2
- **Skills**: Listen +10, Spot +11, Swim +16
- **Feats**: Alertness, Diehard, Endurance, Toughness (2)
- **Environment**: Temperate aquatic
- **Organization**: Solitary
- **Challenge Rating**: 9
- **Advancement**: 13–18 HD (Huge); 19–36 HD (Gargantuan)
- **Level Adjustment**: &mdash;

These voracious creatures can have bodies more than 20 feet long and attack almost anything they meet.

#### Combat

An opponent can attack a giant squid&rsquo;s tentacles with a sunder attempt as if they were weapons. A giant squid’s tentacles have 10 hit points each. If a giant squid is currently grappling a target with the tentacle that is being attacked, it usually uses another limb to make its attack of opportunity against the opponent making the sunder attempt. Severing one of a giant squid’s tentacles deals 5 points of damage to the creature. A giant squid usually withdraws from combat if it loses five tentacles. The creature regrows severed limbs in 1d10+10 days.

**Constrict (Ex)**: A giant squid deals 1d6+8 points of damage with a successful grapple check.

**Improved Grab (Ex)**: To use this ability, a giant squid must hit an opponent of any size with a tentacle attack. It can then attempt to start a grapple as a free action without provoking an attack of opportunity. If it wins the grapple check, it establishes a hold and can constrict. *A giant squid has a +4 racial bonus on grapple checks.

**Ink Cloud (Ex)**: A giant squid can emit a cloud of jet-black ink 20 feet high by 20 feet wide by 20 feet long once per minute as a free action. The cloud provides total concealment, which the squid normally uses to escape a losing fight. All vision within the cloud is obscured.

**Jet (Ex)**: A giant squid can jet backward once per round as a full-round action, at a speed of 320 feet. It must move in a straight line, but does not provoke attacks of opportunity while jetting.

**Skills**: A giant squid has a +8 racial bonus on any Swim check to perform some special action or avoid a hazard. It can always choose to take 10 on a Swim check, even if distracted or endangered. It can use the run action while swimming, provided it swims in a straight line.

### Whale

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

Some varieties of these seagoing mammals are among the largest animals known. Relatively small whales (such as the orca presented here) can be vicious predators, attacking virtually anything they detect.

**Blindsight (Ex)**: Whales can &ldquo;see&rdquo; by emitting high-frequency sounds, inaudible to most other creatures, that allow them to locate objects and creatures within 120 feet. A silence spell negates this and forces the whale to rely on its vision, which is approximately as good as a human’s.

**Hold Breath (Ex)**: A whale can hold its breath for a number of rounds equal to 8 &times; its Constitution score before it risks drowning.

**Skills**: A whale has a +8 racial bonus on any Swim check to perform some special action or avoid a hazard. It can always choose to take 10 on a Swim check, even if distracted or endangered. It can use the run action while swimming, provided it swims in a straight line. &ast;A whale has a +4 racial bonus on Spot and Listen checks. These bonuses are lost if its blindsight is negated.

- **Baleen Whale**
- **Gargantuan Animal**
- **Hit Dice**: 12d8+78 (132 hp)
- **Initiative**: +1
- **Speed**: Swim 40 ft. (8 squares)
- **Armor Class**: 16 (–4 size, +1 Dex, +9 natural), touch 7, flat-footed 15
- **Base Attack/Grapple**: +9/+33
- **Attack**: Tail slap +17 melee (1d8+18)
- **Full Attack**: Tail slap +17 melee (1d8+18)
- **Space/Reach**: 20 ft./15 ft.
- **Special Attacks**: &mdash;
- **Special Qualities**: Blindsight 120 ft., hold breath, low-light vision
- **Saves**: Fort +14, Ref +9, Will +5
- **Abilities**: Str 35, Dex 13, Con 22, Int 2, Wis 12, Cha 6
- **Skills**: Listen +15*, Spot +14*, Swim +20
- **Feats**: Alertness, Diehard, Endurance, Toughness (2)
- **Environment**: Warm aquatic
- **Organization**: Solitary
- **Challenge Rating**: 6
- **Advancement**: 13–18 HD (Gargantuan); 19–36 HD (Colossal)
- **Level Adjustment**: &mdash;

The statistics here describe a plankton-feeding whale between 30 and 60 feet long, such as gray, humpback, and right whales. These massive creatures are surprisingly gentle. If harassed or provoked, they are as likely to flee as they are to retaliate.

- **Cachalot Whale**
- **Gargantuan Animal**
- **Hit Dice**: 12d8+87 (141 hp)
- **Initiative**: +1
- **Speed**: Swim 40 ft. (8 squares)
- **Armor Class**: 16 (–4 size, +1 Dex, +9 natural), touch 7, flat-footed 15
- **Base Attack/Grapple**: +9/+33
- **Attack**: Bite +17 melee (4d6+12)
- **Full Attack**: Bite +17 melee (4d6+12) and tail slap +12 melee (1d8+6)
- **Space/Reach**: 20 ft./15 ft.
- **Special Attacks**: &mdash;
- **Special Qualities**: Blindsight 120 ft., hold breath, low-light vision
- **Saves**: Fort +15, Ref +9, Will +6
- **Abilities**: Str 35, Dex 13, Con 24, Int 2, Wis 14, Cha 6
- **Skills**: Listen +15*, Spot +14*, Swim +20
- **Feats**: Alertness, Diehard, Endurance, Improved Natural Attack (bite), Toughness
- **Environment**: Temperate aquatic
- **Organization**: Solitary or pod (6–11)
- **Challenge Rating**: 7
- **Advancement**: 13–18 HD (Gargantuan); 19–36 HD (Colossal)
- **Level Adjustment**: &mdash;

Also known as sperm whales, these creatures can be up to 60 feet long. They prey on giant squids.

- **Orca**
- **Huge Animal**
- **Hit Dice**: 9d8+48 (88 hp)
- **Initiative**: +2
- **Speed**: Swim 50 ft. (10 squares)
- **Armor Class**: 16 (–2 size, +2 Dex, +6 natural), touch 10, flat-footed 14
- **Base Attack/Grapple**: +6/+22
- **Attack**: Bite +12 melee (2d6+12)
- **Full Attack**: Bite +12 melee (2d6+12)
- **Space/Reach**: 15 ft./10 ft.
- **Special Attacks**: &mdash;
- **Special Qualities**: Blindsight 120 ft., hold breath, low-light vision
- **Saves**: Fort +11, Ref +8, Will +5
- **Abilities**: Str 27, Dex 15, Con 21, Int 2, Wis 14, Cha 6
- **Skills**: Listen +14*, Spot +14*, Swim +16
- **Feats**: Alertness, Endurance, Run, Toughness
- **Environment**: Cold aquatic
- **Organization**: Solitary or pod (6–11)
- **Challenge Rating**: 5
- **Advancement**: 10–13 HD (Huge); 14–27 HD (Gargantuan)
- **Level Adjustment**: &mdash;

These ferocious creatures are about 30 feet long. They eat fish, squid, seals, and other whales.
