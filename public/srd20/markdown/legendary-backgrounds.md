Forest of Titan Character Backgrounds
=====================================

The legendary Forest of Titan is inhabited by many playable creatures. All of
the creatures here have had their racial hit dice, racial skills, and racial
feats removed in order to be replaced by a character class. Racial traits
have been reduced to a level appropriate for a character with 1 hit die. In
effect, every creature here has become a playable race.

See [Monsters as Races](srd/MonstersasRaces.html).

<table>
  <caption>Primary Backgrounds</caption>
  <tr>
    <th>Background</th>
    <th>Size and Type</th>
    <th>Level Adjustment</th>
    <th>Ability Adjustments</th>
    <th>Favored Class</th>
  </tr>
  <tr>
    <td><a href="#jotun">Jotun</a></td>
    <td>Medium giant</td>
    <td>+0</td>
    <td>+2 Str, –2 Dex, +2 Con</td>
    <td>Fighter</td>
  </tr>
  <tr>
    <td><a href="#wildelf">Wild elf</a></td>
    <td>Medium humanoid</td>
    <td>+0</td>
    <td>+2 Dex, –2 Int<sup>1</sup></td>
    <td>Sorcerer</td>
  </tr>
  <tr>
    <td><a href="#faun">Faun</a></td>
    <td>Medium fey</td>
    <td>+1</td>
    <td>+2 Dex, +2 Con</td>
    <td>Bard</td>
  </tr>
  <tr>
    <td><a href="#half-elf">Half-elf</a></td>
    <td>Medium humanoid</td>
    <td>+0</td>
    <td>&mdash;</td>
    <td>Any</td>
  </tr>
  <tr>
    <td><a href="#half-orc">Half-orc</a></td>
    <td>Medium humanoid</td>
    <td>+0</td>
    <td>+2 Str, –2 Int, –2 Cha</td>
    <td>Barbarian</td>
  </tr>
  <tr>
    <td><a href="#tallfellow">Tallfellow</a></td>
    <td>Small humanoid</td>
    <td>+0</td>
    <td>+2 Dex, –2 Str</td>
    <td>Rogue</td>
  </tr>
<tfoot>
  <tr>
    <td colspan="5">1 A character’s starting Int score is always
    at least 3. If this adjustment would lower the character’s score to 1 or
    2, the score remains 3.</td>
  </tr>
</tfoot>
</table>

<table>
  <caption>Secondary Backgrounds</caption>
  <tr>
    <th>Background</th>
    <th>Size and Type</th>
    <th>Level Adjustment</th>
    <th>Ability Adjustments</th>
    <th>Favored Class</th>
  </tr>
  <tr>
    <td><a href="#aasimar">Aasimar</a></td>
    <td>Medium outsider (native)</td>
    <td>+1</td>
    <td>+2 Wis, +2 Cha</td>
    <td>Paladin</td>
  </tr>
  <tr>
    <td><a href="#hobgoblin">Hobgoblin</a></td>
    <td>Medium humanoid (goblinoid)</td>
    <td>+1</td>
    <td>+2 Dex, +2 Con</td>
    <td>Fighter</td>
  </tr>
  <tr>
    <td><a href="#kobold">Kobold</a></td>
    <td>Small humanoid (reptilian)</td>
    <td>+0</td>
    <td>–4 Str, +2 Dex, –2 Con</td>
    <td>Sorceror</td>
  </tr>
  <tr>
    <td><a href="#gnoll">Wood gnoll</a></td>
    <td>Medium humanoid (gnoll)</td>
    <td>+1</td>
    <td>+4 Str, –2 Int<sup>1</sup>, –2 Cha</td>
    <td>Ranger</td>
  </tr>
  <tr>
    <td><a href="#tiefling">Tiefling</a></td>
    <td>Medium outsider (native)</td>
    <td>+1</td>
    <td>+2 Dex, +2 Int, –2 Cha</td>
    <td>Rogue</td>
  </tr>
<tfoot>
  <tr>
    <td colspan="5">1 A character’s starting Int score is always
    at least 3. If this adjustment would lower the character’s score to 1 or
    2, the score remains 3.</td>
  </tr>
</tfoot>
</table>

<table>
  <caption>Other Backgrounds</caption>
  <tr>
    <th>Background</th>
    <th>Size and Type</th>
    <th>Level Adjustment</th>
    <th>Ability Adjustments</th>
    <th>Favored Class</th>
  </tr>
  <tr>
    <td><a href="#doppelganger">Changeling</a></td>
    <td>Medium humanoid</td>
    <td>+1</td>
    <td>+2 Wis</td>
    <td>Rogue</td>
  </tr>
  <tr>
    <td><a href="#pixie">Fairy</a></td>
    <td>Small fey</td>
    <td>+0</td>
    <td>–4 Str, +4 Dex, +2 Int, +2 Cha</td>
    <td>Sorcerer</td>
  </tr>
  <tr>
    <td><a href="#goblin">Goblin</a></td>
    <td>Small humanoid</td>
    <td>+0</td>
    <td>-2 Str, +2 Dex, –2 Cha</td>
    <td>Rogue</td>
  </tr>
  <tr>
    <td><a href="#maenad">Maenad</a></td>
    <td>Medium humanoid</td>
    <td>+0</td>
    <td>&mdash;</td>
    <td>Barbarian</td>
  </tr>
  <tr>
    <td><a href="#merfolk">Selkie</a></td>
    <td>Medium humanoid</td>
    <td>+0</td>
    <td>&mdash;</td>
    <td>Bard</td>
  </tr>
  <tr>
    <td><a href="#minotaur">Taurus</a></td>
    <td>Medium humanoid</td>
    <td>+2</td>
    <td>+2 Str, +2 Dex, –4 Int<sup>1</sup>, –2 Cha</td>
    <td>Barbarian</td>
  </tr>
<tfoot>
  <tr>
    <td colspan="5">1 A character’s starting Int score is always
    at least 3. If this adjustment would lower the character’s score to 1 or
    2, the score remains 3.</td>
  </tr>
</tfoot>
</table>


Backgrounds A-Z
===============

All of the backgrounds listed here are equivalent to creatures with 1 or fewer HD. Such creatures <q>replace their monster levels with their character levels"</q> as outlined in [Monsters as Races](srd/MonstersasRaces.html), so they are represented here as additional to SRD standard [races](srd/Races.html).


Aasimar <a id="aasimar"></a>
----------------------------

An aasimar is an outsider with the native subtype.

Aasimar characters possess the following racial traits.

&mdash; +2 Wis, +2 Cha.

&mdash;Outsider (native subtype).

&mdash;Medium: As Medium creatures, aasimar have no special bonuses or penalties due to their size.

&mdash;An aasimar’s base land speed is 30 feet.

&mdash;Darkvision: Aasimars can see in the dark up to 60 feet.

&mdash;Racial Skills: Aasimars have a +2 racial bonus on Spot and Listen checks.

&mdash;Daylight (Sp): An aasimar can use *daylight* once per day as a 1st-level caster or a caster of his class levels, whichever is higher.

&mdash;Special Qualities: Resistance to acid 5, cold 5, and electricity 5.&cross;

&mdash;Automatic Languages: Common, Celestial. Bonus Languages: Draconic, Dwarven, Elven, Gnome, Halfling, Sylvan.

&mdash;Favored Class: Paladin.

&mdash;Level adjustment +1.

### Sources

This background is based on the SRD aasimar (planetouched).

&mdash;[Monsters O-R](srd/MonstersO-R.html).


Changeling <a id="doppelganger"></a>
------------------------------------

Changeling characters possess the following racial traits.

&mdash; +2 Wis.

&mdash;Medium: As Medium creatures, changelings have no special bonuses or penalties due to their size.

&mdash;A changeling’s base land speed is 30 feet.

&mdash;Darkvision: Changelings can see in the dark up to 60 feet.

&mdash;Skills: A changeling has a +4 racial bonus on Bluff and Disguise checks.

&mdash;Spell-like ability: A changeling can use *detect thoughts* once per day (caster level equal to class levels).

&mdash;Immunity to *sleep* and charm effects.

&mdash;Automatic Languages: Common. Bonus Languages: Auran, Dwarven, Elven, Gnome, Halfling, Giant, Terran.

&mdash;Favored Class: Rogue.

&mdash;Level adjustment +1.

### Sources

This background is based on the SRD doppelganger.

&mdash;[Monsters Di-Do](srd/MonstersDi-Do.html).


Drakken <a id="drakken"></a>
--------------------------

Drakken are often more physically imposing than others of their kind that do not have dragon blood. Some show a hint of their heritage such as scaly skin or reptilian eyes.

Drakken characters possess the following racial traits.

&mdash; +2 Strength, +2 Constitution, +2 Charisma.

&mdash;Medium: As Medium creatures, drakken have no special bonuses or penalties due to their size.

&mdash;Darkvision out to 60 feet.

&mdash;Low-light vision.

&mdash; +1 natural armor bonus.

&mdash;Natural Weapons: 2 claws (1d4).

&mdash; +4 racial bonus on saves against magic sleep effects and paralysis.

&mdash;Favored Class: Fighter.

&mdash;Level adjustment +1.

### Sources

This background is based on the SRD half-dragon.

&mdash;[Monsters H-I](srd/MonstersH-I.html)


Fairy <a id="pixie"></a>
------------------------

Fairy characters possess the following racial traits.

&mdash; –4 Str, +4 Dex, +2 Int, +2 Cha.

&mdash;Small size. +1 bonus to Armor Class, +1 bonus on attack rolls, +4 bonus on Hide checks, –4 penalty on grapple checks, lifting and carrying limits 3/4 those of Medium characters.

&mdash;A fairy’s base land speed is 20 feet.

&mdash;Low-light vision.

&mdash;Weapon Proficiency: Fairies are automatically proficient with the short sword and the composite longbow.

&mdash;Skills: Fairies have a +2 racial bonus on Listen, Search, and Spot checks.

&mdash;Spell-like abilities: 1/day&mdash;*dancing lights*, *detect magic*, *ghost sound*. Caster level 1st; save DC 10 + Cha modifier + spell level.

&mdash;Automatic Languages: Common, Sylvan, and a simple language that enables them to communicate on a very basic level with forest animals. Bonus Languages: Elven, Gnome, Halfling.

&mdash;Favored Class: Sorcerer.

### Sources

This background is based on the SRD pixie (sprite).

&mdash;[Monsters S](srd/MonstersS.html).


Faun <a id="faun"></a>
-----------------------

Faun characters possess the following racial traits.

&mdash; +2 Dex, +2 Cha.

&mdash;Medium: As Medium creatures, fauns have no special bonuses or penalties due to their size.

&mdash;A faun’s base land speed is 40 feet.

&mdash;Low-light vision.

&mdash;Fauns have a +2 racial bonus on Hide and Move Silently checks.

&mdash;Automatic Languages: Sylvan. Bonus Languages: Common, Elven, Gnome.

&mdash;Favored Class: Bard.

&mdash;Level Adjustment: +1.

### Sources

This background is based on the SRD satyr.

&mdash;[Monsters S](srd/MonstersS.html).


Goblin <a id="goblin"></a>
--------------------------

Goblin characters possess the following racial traits.

&mdash; –2 Str, +2 Dex, –2 Cha.

&mdash;Small size: +1 bonus to Armor Class, +1 bonus on attack rolls, +4 bonus on Hide checks, –4 penalty on grapple checks, lifting and carrying limits 3/4 those of Medium characters.

&mdash;A goblin’s base land speed is 30 feet.

&mdash;Darkvision out to 60 feet.

&mdash; +4 racial bonus on Move Silently and Ride checks.

&mdash;Automatic Languages: Common, Goblin. Bonus Languages: Draconic, Elven, Giant, Gnoll, Orc.

&mdash;Favored Class: Rogue.

### Sources

This background is based on the SRD goblin.

&mdash;[Monsters G](srd/MonstersG.html).


Goblin, Blue <a id="bluegoblin"></a>
------------------------------------

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

Blues are a subrace of goblins with an innate knack for spell energy. A blue is often smaller than an average goblin, standing just about 3 feet tall and weighing about 40 pounds. Blues have noticeably blue-tinged skin, and their eyes are less dull than those of a common goblin. Otherwise, they resemble their kin. They generally dress in short leather robes, dyed black.

Blue goblin characters possess the following racial traits.

&mdash; –2 Str, +2 Int, –2 Cha.

&mdash;Small size: +1 bonus to Armor Class, +1 bonus on attack rolls, +4 bonus on Hide checks, –4 penalty on grapple checks, lifting and carrying limits 3/4 of those of Medium characters.

&mdash;Blue goblin base land speed is 30 feet.

&mdash;Darkvision out to 60 feet.

&mdash;Channeler: Blue goblin spellcasters gain bonus spells as if the governing ability score were 1 point higher. This benefit does it grant them the ability to cast spells unless they gain that ability through levels in a spellcaster class. See Ability Modifiers and Bonus Spells. [Basics](srd/Basics.html).

&mdash;Racial Skills: A blue goblin has a +4 racial bonus on Move Silently checks and Ride checks.

&mdash;Automatic Languages: Common, Goblin. Bonus Languages: Draconic, Elven, Giant, Gnoll, Orc.

&mdash;Favored Class: Wizard.

&mdash;Level Adjustment: +1.


Half-elf <a id="half-elf"></a>
------------------------------

Half-elves possess the following racial traits.

&mdash;Medium: As Medium creatures, half-elves have no special bonuses or penalties due to their size.

&mdash;A half-elf’s base land speed is 30 feet.

&mdash;Immunity to sleep spells and similar magical effects, and a +2 racial bonus on saving throws against enchantment spells or effects.

&mdash;Low-light vision.

&mdash; +1 racial bonus on Listen, Search, and Spot checks. A half-elf does not have the elf ’s ability to notice secret doors simply by passing near them.

&mdash; +2 racial bonus on Diplomacy and Gather Information checks.

&mdash;Elven Blood: For all effects related to race, a half-elf is considered an elf. Half-elves, for example, are just as vulnerable to special effects that affect elves as their elf ancestors are, and they can use magic items that are only usable by elves.

&mdash;Automatic Languages: Common, Elven. Bonus Languages: Any (other than secret languages, such as Druidic).

&mdash;Favored Class: Any.

### Sources

This background is based on the SRD half-elf.

&mdash;[Monsters E-F](srd/MonstersE-F.html).


Half-giant <a id="half-giant"></a>
----------------------------------

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

Half-giants are a hybrid species born from experiments deep in the deserts of Ares.

Half-giant characters possess the following racial traits.

&mdash; +2 Str, +2 Con, –2 Dex: Half-giants are tough and strong, but not too nimble.

&mdash;Giant: Half-giants are not subject to spells or effects that affect humanoids only, such as charm person or dominate person.

&mdash;Medium: As Medium creatures, half-giants have no special bonuses or penalties due to their size.

&mdash;Half-giant base land speed is 30 feet.

&mdash;Low-Light Vision: A half-giant can see twice as far as a human in starlight, moonlight, torchlight, and similar conditions of poor illumination. He retains the ability to distinguish color and detail under these conditions.

&mdash;Fire Acclimated: Half-giants have a +2 racial bonus on saving throws against all fire spells and effects. Half-giants are accustomed to enduring high temperatures.

&mdash;Powerful Build: The physical stature of half-giants lets them function in many ways as if they were one size category larger.

Whenever a half-giant is subject to a size modifier or special size modifier for an opposed check (such as during grapple checks, bull rush attempts, and trip attempts), the half-giant is treated as one size larger if doing so is advantageous to him.

A half-giant is also considered to be one size larger when determining whether a creature’s special attacks based on size (such as improved grab or swallow whole) can affect him. A half-giant can use weapons designed for a creature one size larger without penalty. However, his space and reach remain those of a creature of his actual size. The benefits of this racial trait stack with the effects of powers, abilities, and spells that change the subject’s size category.

&mdash;Channeler: Half-giant spellcasters gain bonus spells as if the governing ability score were 2 points higher. This benefit does it grant them the ability to cast spells unless they gain that ability through levels in a spellcaster class. See Ability Modifiers and Bonus Spells. [Basics](srd/Basics.html).

&mdash;Stomp (Sp): Once per day as a standard action, a half-giant precipitate a shock wave that travels along the ground, toppling creatures and loose objects. The shock wave affects only creatures standing on the ground within a 20-foot cone. Creatures that fail a Reflex save with a DC equal to 11 &plus; the half-giant's Cha modifier are thrown to the ground, become prone, and take 1d4 points of nonlethal damage.

&mdash;Automatic Language: Common. Bonus Languages: Draconic, Giant, Gnoll, Ignan.

&mdash;Favored Class: Fighter.

&mdash;Level Adjustment: +1.


Half-orc <a id="half-orc"></a>
------------------------------

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

These orc–human crossbreeds can be found in either orc or human society (where their status varies according to local sentiments), or in communities of their own. Half-orcs usually inherit a good blend of the physical characteristics of their parents. They are as tall as humans and a little heavier, thanks to their muscle. They have greenish pigmentation, sloping foreheads, jutting jaws, prominent teeth, and coarse body hair. Half-orcs who have lived among or near orcs have scars, in keeping with orcish tradition.

Half-orcs possess the following racial traits.

&mdash; +2 Str, –2 Int, –2 Cha.

&mdash;Medium: As Medium creatures, half-orcs have no special bonuses or penalties due to their size.

&mdash;A half-orc’s base land speed is 30 feet.

&mdash;Darkvision: Half-orcs can see in the dark up to 60 feet.

&mdash;Orc Blood: For all effects related to race, a half-orc is considered an orc.

&mdash;Automatic Languages: Common, Orc. Bonus Languages: Draconic, Giant, Gnoll, Goblin, Abyssal.

&mdash;Favored Class: Barbarian.

### Sources

This background is based on the SRD half-orc.

&mdash;[Monsters O-R](srd/MonstersO-R.html).


Hobgoblin <a id="hobgoblin"></a>
--------------------------------

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

Hobgoblin characters possess the following racial traits.

&mdash; +2 Dex, +2 Con.

&mdash;Medium: As Medium creatures, hobgoblins have no special bonuses or penalties due to their size.

&mdash;A hobgoblin’s base land speed is 30 feet.

&mdash;Darkvision out to 60 feet.

&mdash; +4 racial bonus on Move Silently checks.

&mdash;Automatic Languages: Common, Goblin. Bonus Languages: Draconic, Dwarven, Infernal, Giant, Orc.

&mdash;Favored Class: Fighter.

&mdash;Level adjustment +1.

### Sources

This background is based on the SRD hobgoblin.

&mdash;[Monsters H-I](srd/MonstersH-I.html).


Jotun <a id="jotun"></a>
------------------------

Jotnar (singular jotun) are medium humanoids with a distant frost giant lineage. They often live in temperate forests, but retain their affinity for cold.

Jotun characters possess the following racial traits.

&mdash; +4 Str, –2 Dex, +2 Con: Jotun are tough and strong, but not too nimble.

&mdash;Giant: Jotnar are not subject to spells or effects that affect humanoids only, such as charm person or dominate person.

&mdash;Medium: As Medium creatures, Jotun have no special bonuses or penalties due to their size.

&mdash;Jotun base land speed is 30 feet.

&mdash;Low-Light Vision: A Jotun can see twice as far as a human in starlight, moonlight, torchlight, and similar conditions of poor illumination. He retains the ability to distinguish color and detail under these conditions.

&mdash;Powerful Build (Ex): The physical stature of half-giants lets them function in many ways as if they were one size category larger. Whenever a half-giant is subject to a size modifier or special size modifier for an opposed check (such as during grapple checks, bull rush attempts, and trip attempts), the half-giant is treated as one size larger if doing so is advantageous to him. A half-giant is also considered to be one size larger when determining whether a creature’s special attacks based on size (such as improved grab or swallow whole) can affect him. A half-giant can use weapons designed for a creature one size larger without penalty. However, his space and reach remain those of a creature of his actual size. The benefits of this racial trait stack with the effects of powers, abilities, and spells that change the subject’s size category.

&mdash;Special Quality: Resistance to cold 5.

&mdash;Automatic Language: Common. Bonus Languages: Draconic, Giant, Gnoll, Ignan.

&mdash;Favored Class: Barbarian.

### Sources

This background is based on the SRD half-giant.

&mdash;[Psionic Races](srd/PsionicRaces.html).


Kobold <a id="kobold"></a>
--------------------------

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

A kobold’s scaly skin ranges from dark rusty brown to a rusty black color. It has glowing red eyes. Its tail is nonprehensile. Kobolds wear ragged clothing, favoring red and orange. A kobold is 2 to 2-1/2 feet tall and weighs 35 to 45 pounds. Kobolds speak Draconic with a voice that sounds like that of a yapping dog.

Kobold characters possess the following racial traits.

&mdash; –4 Str, +2 Dex, –2 Con.

&mdash;Small size: +1 bonus to Armor Class, +1 bonus on attack rolls, +4 bonus on Hide checks, –4 penalty on grapple checks, lifting and carrying limits 3/4 those of Medium characters.

&mdash;A kobold’s base land speed is 30 feet.

&mdash;Darkvision out to 60 feet.

&mdash;Racial Skills: A kobold character has a +2 racial bonus on Craft (trapmaking), Profession (miner), and Search checks.

&mdash; +1 natural armor bonus.

&mdash;Light Sensitivity: Kobolds are dazzled in bright sunlight or within the radius of a *daylight* spell.

&mdash;Automatic Languages: Draconic. Bonus Languages: Common, Undercommon.

&mdash;Favored Class: Sorcerer.


Maenad
------

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

Maenad characters possess the following racial traits.

&mdash;Medium: As Medium creatures, maenads have no special bonuses or penalties due to their size.

&mdash;Maenad base land speed is 30 feet.

&mdash;Spell-like Ability: 1/day&mdash;*energy ray*. This spell-like ability works as *ray of frost* except it deals 1d6 of sonic damage. It is accompanied by a tremendous scream of rage. Caster level is equal to 1/2 Hit Dice (minimum 1st). The save DC is Cha-based.

&mdash;Outburst (Ex): Once per day, for up to 4 rounds, a maenad can subjugate her mentality to gain a boost of raw physical power. When she does so, she takes a –2 penalty to Int and Wis but gains a +2 bonus to Str.

&mdash;Automatic Languages: Common, Maenad. Bonus Languages: Aquan, Draconic, Dwarven, Elven, Goblin.

&mdash;Favored Class: Barbarian.

### Sources

This background is based on the SRD maenad.

&mdash;[Psionic Races](srd/PsionicRaces.html)


Orc <a id="orc"></a>
--------------------

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

An orc’s hair usually is black. It has lupine ears and reddish eyes. Orcs prefer wearing vivid colors that many humans would consider unpleasant, such as blood red, mustard yellow, yellow-green, and deep purple. Their equipment is dirty and unkempt. An adult male orc is a little over 6 feet tall and weighs about 210 pounds. Females are slightly smaller.

The language an orc speaks varies slightly from tribe to tribe, but any orc is understandable by someone else who speaks Orc. Some orcs know Goblin or Giant as well.

Orcs possess the following racial traits.

&mdash; +4 Str, –2 Int, –2 Wis, –2 Cha.

&mdash;An orc’s base land speed is 30 feet.

&mdash;Darkvision out to 60 feet.

&mdash;Light Sensitivity: Orcs are dazzled in bright sunlight or within the radius of a *daylight* spell.

&mdash;Automatic Languages: Common, Orc. Bonus Languages: Dwarven, Giant, Gnoll, Goblin, Undercommon.

&mdash;Favored Class: Barbarian.

### Sources

This background is based on the SRD orc.

&mdash;[Monsters O-R](srd/MonstersO-R.html).


Selkie <a id="merfolk"></a>
---------------------------

A selkie is a semiaquatic medium humanoid with the shapechanger subtype. Its natural form resembles that of a seal with a human upper-body.

Selkie characters possess the following racial traits.

&mdash;Medium: No special bonuses or penalties due to their size.

&mdash;Shapechanger Subtype: A selkie has the ability to transform into a
human with a land speed of 30 feet.

&mdash;A selkie’s base land speed is 5 feet. Swim speed 50 feet.

&mdash;Skills: A selkie has a +8 racial bonus on any Swim check to perform some special action or avoid a hazard. It can always choose to take 10 on a Swim check, even if distracted or endangered. It can use the run action while swimming, provided it swims in a straight line.

&mdash;Hold Breath: A selkie can hold its breath for a number of rounds equal to 6 &times; its Con score before it risks drowning.

&mdash;Change Shape: A selkie can assume its human form, or revert to its natural form, as a standard action. In human form, a selkie has no swim speed and loses its requisite benefits to Swim checks, but gains a land speed of 30 feet. A selkie remains in one form until it chooses to assume a new one. A change in form cannot be dispelled, but the selkie reverts to its natural form when killed. A *true seeing* spell reveals its natural form.

&mdash;Automatic Languages: Common, Aquan. Bonus Languages: Draconic, Elven, Gnoll, Goblin, Orc, Sahuagin, Sylvan.

&mdash;Favored Class: Bard.

### Sources

This background is based on the SRD merfolk.

&mdash;[Monsters M-N](srd/MonstersM-N.html).


Tallfellow <a id="tallfellow"></a>
----------------------------------

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

Tallfellows possess the following racial traits.

&mdash; +2 Dex, –2 Str.

&mdash;Small size. +1 bonus to Armor Class, +1 bonus on attack rolls, +4 bonus on Hide checks, –4 penalty on grapple checks, lifting and carrying limits 3/4 those of Medium characters.

&mdash;A tallfellow’s base land speed is 20 feet.

&mdash; +1 racial bonus on all saving throws.

&mdash; +2 morale bonus on saving throws against fear. This bonus stacks with the tallfellow’s +1 bonus on saving throws in general.

&mdash; +1 racial bonus on attack rolls with thrown weapons and slings.

&mdash; +2 racial bonus on Listen, Search, and Spot checks. An elf who merely passes within 5 feet of a secret or concealed door is entitled to a Search check to notice it as if she were actively looking for it.

&mdash;Automatic Languages: Common, Halfling. Bonus Languages: Dwarven, Elven, Gnome, Goblin, Orc.

&mdash;Favored Class: Rogue.

### Sources

This background is based on the SRD tallfellow (halfling).

&mdash;[Monsters H-I](srd/MonstersH-I.html).


Taurus <a id="minotaur"></a>
----------------------------

Titan is home to a smaller species of minotaur. While the taurus' ability score adjustments are greatly reduced compared to its stronger cousin, it still has natural traits which give it a significant level adjustment.

Taurus characters possess the following racial traits.

&mdash; +2 Str, +2 Con, –4 Int (minimum 3), –2 Cha.

&mdash;Medium: As Medium creatures, taurus have no special bonuses or penalties due to their size.

&mdash;A taurus’ base land speed is 30 feet.

&mdash;Darkvision out to 60 feet.

&mdash;Racial Skills: Taurus have a +2 racial bonus on Search, Spot, and Listen checks.

&mdash;Weapon Proficiency: Taurus are automatically proficient with the greataxe.

&mdash; +2 natural armor bonus.

&mdash;Natural Weapons: Gore (1d4).

&mdash;Automatic Languages: Common, Giant. Bonus Languages: Orc, Goblin, Terran.

&mdash;Favored Class: Barbarian.

&mdash;Level adjustment +1.

### Sources

This background is based on the SRD minotaur.

&mdash;[Monsters M-N](srd/MonstersM-N.html).


Tiefling <a id="tiefling"></a>
------------------------------

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

A tiefling is an outsider with the native subtype.

Tiefling characters possess the following racial traits.

&mdash; +2 Dex, +2 Int, –2 Cha.

&mdash;Medium: As Medium creatures, tieflings have no special bonuses or penalties due to their size.

&mdash;A tiefling’s base land speed is 30 feet.

&mdash;Darkvision out to 60 feet.

&mdash;Racial Skills: Tieflings have a +2 racial bonus on Bluff and Hide checks.

&mdash;Darkness (Sp): A tiefling can use *darkness* once per day (caster level equal to class levels).

&mdash;Special Qualities: Resistance to acid 5, cold 5, and electricity 5.&cross;

&mdash;Automatic Languages: Common, Infernal. Bonus Languages: Draconic, Dwarven, Elven, Gnome, Goblin, Halfling, Orc.

&mdash;Favored Class: Rogue.

&mdash;Level adjustment +1.

### Sources

This background is based on the SRD tiefling (planetouched).

&mdash;[Monsters O-R](srd/MonstersO-R.html).


Wild Elf <a id="wildelf"></a>
-----------------------------

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

Wild elves possess the following racial traits.

&mdash; +2 Str, –2 Int.

&mdash;Medium: As Medium creatures, wild elves have no special bonuses or penalties due to their size.

&mdash;A wild elf’s base land speed is 30 feet.

&mdash;Immunity to _sleep_ spells and effects, and a +2 racial saving throw bonus against enchantment spells or effects.

&mdash;Low-light vision.

&mdash;Weapon Proficiency: Wild elves are automatically proficient with the longsword, rapier, longbow, composite longbow, shortbow, and composite shortbow.

&mdash; +2 racial bonus on Listen, Search, and Spot checks. An elf who merely passes within 5 feet of a secret or concealed door is entitled to a Search check to notice it as if she were actively looking for it.

&mdash;Automatic Languages: Common, Elven. Bonus Languages: Draconic, Gnoll, Gnome, Goblin, Orc, Sylvan.

&mdash;Favored Class: Sorcerer.

### Sources

This background is based on the SRD wild elf (elf).

&mdash;[Monsters E-F](srd/MonstersE-F.html).


Wood Gnoll <a id="gnoll"></a>
-----------------------------

A forest-dwelling offshoot of the plains-dwelling gnoll.

This character must accept a +1 level adjustment to account for natural armor and increased Str.

Wood gnoll characters possess the following racial traits.

&mdash;Str +4, Int –2, Cha –2.

&mdash;Medium: As Medium creatures, wood gnolls have no special bonuses or penalties due to their size.

&mdash;A wood gnoll’s base land speed is 30 feet.

&mdash;Darkvision out to 60 feet.

&mdash; +1 natural armor bonus.

&mdash;Automatic Languages: Gnoll. Bonus Languages: Common, Draconic, Elven, Goblin, Orc.

&mdash;Favored Class: Ranger.

&mdash;Level adjustment +1.

### Sources

This background is based on the SRD gnoll.

&mdash;[Monsters G](srd/MonstersG.html).


Appendix A: All As-Character Monsters
=====================================

There are 45 creatures in the SRD which have a section detailing their traits as a character. These creatures range in effective character level from 1 to 16, and many of them are "usually" or "often" evil. 

<table>
  <caption>Table: Standard Backgrounds</caption>
  <tr>
    <th>Background</th>
    <th>Alignment</th>
    <th>Level Adjustment</th>
    <th>Ability Adjustments</th>
    <th>Favored Class</th>
    <th>Environment</th>
  </tr>
  <tr>
    <td>Human</td>
    <td>Any</td>
    <td>+0</td>
    <td>&mdash;</td>
    <td>Any</td>
    <td>Any</td>
  </tr>
  <tr>
    <td>Dwarf</td>
    <td>Often lawful good</td>
    <td>+0</td>
    <td>+2 Con, –2 Cha</td>
    <td>Fighter</td>
    <td>Temperate mountain</td>
  </tr>
  <tr>
    <td>Elf</td>
    <td>Usually chaotic good</td>
    <td>+0</td>
    <td>+2 Dex, –2 Con</td>
    <td>Wizard</td>
    <td>Temperate forest</td>
  </tr>
  <tr>
    <td>Gnome</td>
    <td>Usually neutral good</td>
    <td>+0</td>
    <td>+2 Con, –2 Str</td>
    <td>Bard</td>
    <td>Temperate hill</td>
  </tr>
  <tr>
    <td><a href="#goblin">Goblin</a></td>
    <td>Usually neutral evil</td>
    <td>+0</td>
    <td>-2 Str, +2 Dex, -2 Cha</td>
    <td>Rogue</td>
    <td>Temperate plain</td>
  </tr>
  <tr>
    <td><a href="#half-elf">Half-elf</a></td>
    <td>Usually chaotic (any)</td>
    <td>+0</td>
    <td>&mdash;</td>
    <td>Any</td>
    <td>Temperate forest</td>
  </tr>
  <tr>
    <td><a href="#half-orc">Half-orc</a></td>
    <td>Usually chaotic (any)</td>
    <td>+0</td>
    <td>+2 Str, –2 Int, –2 Cha</td>
    <td>Barbarian</td>
    <td>Temperate hill</td>
  </tr>
  <tr>
    <td>Halfling</td>
    <td>Usually neutral</td>
    <td>+0</td>
    <td>+2 Dex, –2 Str</td>
    <td>Rogue</td>
    <td>Warm plains</td>
  </tr>
  <tr>
    <td><a href="#kobold">Kobold</a><sup>1</sup></td>
    <td>Usually lawful evil</td>
    <td>+0</td>
    <td>–4 Str, +2 Dex, –2 Con</td>
    <td>Sorcerer</td>
    <td>Temperate forest</td>
  </tr>
  <tr>
    <td><a href="#orc">Orc</a></td>
    <td>Often chaotic evil</td>
    <td>+0</td>
    <td>+4 Str, –2 Int, –2 Wis, –2 Cha</td>
    <td>Barbarian</td>
    <td>Temperate hill</td>
  </tr>
<tfoot>
  <tr>
    <td colspan="7">
      1 This creature has an entry in <cite>Races of the Dragon</cite>.
    </td>
  </tr>
</tfoot>
</table>

<table>
  <caption>Table: Improved Backgrounds</caption>
  <tr>
    <th>Background</th>
    <th>Ability Adjustments</th>
    <th>Environment</th>
    <th>Alignment</th>
    <th>Favored Class</th>
    <th>Level Adjustment</th>
  </tr>
  <tr>
    <td>Aasimar</td>
    <td>+2 Wis, +2 Cha</td>
    <td>Temperate plain</td>
    <td>Usually good (any)</td>
    <td>Paladin</td>
    <td>+1</td>
  </tr>
  <tr>
    <td>Centaur<sup>1</sup></td>
    <td>+2 Dex, –2 Int</td>
    <td>Temperate forest</td>
    <td>Usually neutral good</td>
    <td>Ranger</td>
    <td>Ignored</td>
  </tr>
  <tr>
    <td>Doppelganger<sup>1</sup></td>
    <td>+2 Con</td>
    <td>Any</td>
    <td>Usually neutral</td>
    <td>Rogue</td>
    <td>Ignored</td>
  </tr>
  <tr>
    <td>Gnoll<sup>1</sup></td>
    <td>Str +2, Int –2, Cha –2</td>
    <td>Warm plain</td>
    <td>Usually chaotic evil</td>
    <td>Ranger</td>
    <td>Ignored</td>
  </tr>
  <tr>
    <td>Minotaur<sup>1</sup></td>
    <td>+2 Str, +2 Con, –4 Int (minimum 3), –2 Cha.</td>
    <td>Underground</td>
    <td>Usually chaotic evil</td>
    <td>Barbarian</td>
    <td>Ignored</td>
  </tr>
  <tr>
    <td>Tiefling</td>
    <td>+2 Dex, +2 Int, –2 Cha</td>
    <td>Temperate plain</td>
    <td>Usually evil (any)</td>
    <td>Rogue</td>
    <td>+1</td>
  </tr>
  <tr>
    <td>Troll<sup>1</sup></td>
    <td>+4 Str, +2 Con, –4 Int (minimum 3), –2 Wis, –4 Cha</td>
    <td>Cold mountain</td>
    <td>Usually chaotic evil</td>
    <td>Fighter</td>
    <td>Ignored</td>
  </tr>
<tfoot>
  <tr>
    <td colspan="6">1 This creature must take levels in a "monster class" before multiclassing into a character class.</td>
  </tr>
</tfoot>
</table>

<table>
  <caption>Table: Advanced Backgrounds</caption>
  <tr>
    <th>Background</th>
    <th>Alignment</th>
    <th>Level Adjustment</th>
    <th>Ability Adjustments</th>
    <th>Hit Dice</th>
    <th>Favored Class</th>
    <th>Environment</th>
  </tr>
  <tr>
    <td>Aasimar<sup>1</sup></td>
    <td>Usually good (any)</td>
    <td>+1</td>
    <td>+2 Wis, +2 Cha</td>
    <td>1</td>
    <td>Paladin</td>
    <td>Temperate plain</td>
  </tr>
  <tr>
    <td>Bugbear</td>
    <td>Usually chaotic evil</td>
    <td>+1</td>
    <td>+4 Str, +2 Dex, +2 Con, –2 Cha</td>
    <td>3</td>
    <td>Rogue</td>
    <td>Temperate mountain</td>
  </tr>
  <tr>
    <td>Centaur<sup>2</sup></td>
    <td>Usually neutral good</td>
    <td>+2</td>
    <td>+8 Str, +4 Dex, +4 Con, –2 Int, +2 Wis</td>
    <td>4</td>
    <td>Ranger</td>
    <td>Temperate forest</td>
  </tr>
  <tr>
    <td>Drow</td>
    <td>Usually neutral evil</td>
    <td>+2</td>
    <td>+2 Int, +2 Cha</td>
    <td>1</td>
    <td>Wizard/Cleric</td>
    <td>Underground</td>
  </tr>
  <tr>
    <td>Duergar</td>
    <td>Often lawful evil</td>
    <td>+1</td>
    <td>+2 Con, –4 Cha</td>
    <td>1</td>
    <td>Fighter</td>
    <td>Underground</td>
  </tr>
  <tr>
    <td>Gnoll<sup>2</sup></td>
    <td>Usually chaotic evil</td>
    <td>+1</td>
    <td>Str +4, Con +2, Int –2, Cha –2</td>
    <td>2</td>
    <td>Ranger</td>
    <td>Warm plain</td>
  </tr>
  <tr>
    <td>Grimlock</td>
    <td>Often neutral evil</td>
    <td>+2</td>
    <td>+4 Str, +2 Dex, +2 Con, –2 Wis, –4 Cha</td>
    <td>2</td>
    <td>Barbarian</td>
    <td>Underground</td>
  </tr>
  <tr>
    <td>Hobgoblin</td>
    <td>Usually lawful evil</td>
    <td>+1</td>
    <td>+2 Dex, +2 Con</td>
    <td>1</td>
    <td>Fighter</td>
    <td>Warm hill</td>
  </tr>
  <tr>
    <td>Lizardfolk</td>
    <td>Usually neutral</td>
    <td>+1</td>
    <td>+2 Str, +2 Con, –2 Int</td>
    <td>2</td>
    <td>Druid</td>
    <td>Temperate marsh</td>
  </tr>
  <tr>
    <td>Locathah</td>
    <td>Usually neutral</td>
    <td>+1</td>
    <td>&mdash;</td>
    <td>2</td>
    <td>Barbarian</td>
    <td>Warm aquatic</td>
  </tr>
  <tr>
    <td>Merfolk</td>
    <td>Usually neutral</td>
    <td>+1</td>
    <td>&mdash;</td>
    <td>1</td>
    <td>Bard</td>
    <td>Temperate aquatic</td>
  </tr>
  <tr>
    <td>Minotaur</td>
    <td>Usually chaotic evil</td>
    <td>+2</td>
    <td>+8 Str, +4 Con, –4 Int (minimum 3), –2 Cha.</td>
    <td>6</td>
    <td>Barbarian</td>
    <td>Underground</td>
  </tr>
  <tr>
    <td>Ogre</td>
    <td>Usually chaotic evil</td>
    <td>+2</td>
    <td>+10 Str, –2 Dex, +4 Con, –4 Int, –4 Cha.</td>
    <td>4</td>
    <td>Barbarian</td>
    <td>Temperate hill</td>
  </tr>
  <tr>
    <td>Svirfneblin</td>
    <td>Usually neutral</td>
    <td>+3</td>
    <td>–2 Str, +2 Dex, +2 Wis, –4 Cha</td>
    <td>1</td>
    <td>Rogue</td>
    <td>Underground</td>
  </tr>
  <tr>
    <td>Tiefling<sup>1</sup></td>
    <td>Usually evil (any)</td>
    <td>+1</td>
    <td>+2 Dex, +2 Int, –2 Cha</td>
    <td>1</td>
    <td>Rogue</td>
    <td>Temperate plain</td>
  </tr>
  <tr>
    <td>Troglodyte</td>
    <td>Usually chaotic evil</td>
    <td>+2</td>
    <td>–2 Dex, +4 Con, –2 Int</td>
    <td>2</td>
    <td>Cleric</td>
    <td>Underground</td>
  </tr>
<tfoot>
  <tr>
    <td colspan="7">
      1 This creature has an entry in <cite>Races of Destiny</cite>
    </td>
  </tr>
  <tr>
    <td colspan="7">
      2 This creature has an entry in <cite>Races of the Wild</cite>
    </td>
  </tr>
</tfoot>
</table>

<table>
  <caption>Table: Greater Backgrounds</caption>
  <tr>
    <th>Background</th>
    <th>Alignment</th>
    <th>Level Adjustment</th>
    <th>Ability Adjustments</th>
    <th>Hit Dice</th>
    <th>Favored Class</th>
    <th>Environment</th>
  </tr>
  <tr>
    <td>Azer</td>
    <td>Always lawful neutral</td>
    <td>+4</td>
    <td>+2 Str, +2 Dex, +2 Con, +2 Int, +2 Wis, +2 Cha</td>
    <td>2</td>
    <td>Fighter</td>
    <td>Elemental Plane of Fire</td>
  </tr>
  <tr>
    <td>Doppelganger</a><sup>1</sup></td>
    <td>Usually neutral</td>
    <td>+4</td>
    <td>+2 Str, +2 Dex, +2 Con, +2 Int, +4 Wis, +2 Cha</td>
    <td>4</td>
    <td>Rogue</td>
    <td>Any</td>
  </tr>
  <tr>
    <td>Gargoyle</td>
    <td>Usually chaotic evil</td>
    <td>+5</td>
    <td>+4 Str, +4 Dex, +8 Con, –4 Int, –4 Cha</td>
    <td>4</td>
    <td>Fighter</td>
    <td>Any</td>
  </tr>
  <tr>
    <td>Hill giant</td>
    <td>Often chaotic evil</td>
    <td>+4</td>
    <td>+14 Str, –2 Dex, +8 Con, –4 Int, –4 Cha</td>
    <td>12</td>
    <td>Barbarian</td>
    <td>Temperate hill</td>
  </tr>
  <tr>
    <td>Hound Archon</td>
    <td>Always lawful good</td>
    <td>+5</td>
    <td>+2 Str, +2 Dex, +2 Con, +2 Int, +2 Wis, +2 Cha</td>
    <td>6</td>
    <td>Ranger</td>
    <td>Any lawful good-aligned plane</td>
  </tr>
  <tr>
    <td>Janni</td>
    <td>Usually neutral</td>
    <td>+5</td>
    <td>+6 Str, +4 Dex, +2 Con, +4 Int, +4 Wis, +2 Cha</td>
    <td>6</td>
    <td>Rogue</td>
    <td>Warm desert</td>
  </tr>
  <tr>
    <td>Ogre Mage</td>
    <td>Usually lawful evil</td>
    <td>+7</td>
    <td>+10 Str, +6 Con, +4 Int, +4 Wis, +6 Cha</td>
    <td>5</td>
    <td>Sorceror</td>
    <td>Cold hill</td>
  </tr>
  <tr>
    <td>Pixie</td>
    <td>Always neutral good</td>
    <td>+4</td>
    <td>–4 Str, +8 Dex, +6 Int, +4 Wis, +6 Cha</td>
    <td>1</td>
    <td>Sorceror</td>
    <td>Temperate forest</td>
  </tr>
  <tr>
    <td>Rakshasa</td>
    <td>Always lawful evil</td>
    <td>+7</td>
    <td>+2 Str, +4 Dex, +6 Con, +2 Int, +2 Wis, +6 Cha</td>
    <td>7</td>
    <td>Sorceror</td>
    <td>Warm marsh</td>
  </tr>
  <tr>
    <td>Satyr</td>
    <td>Usually chaotic neutral</td>
    <td>+2</td>
    <td>+2 Dex, +2 Con, +2 Int, +2 Wis, +2 Cha</td>
    <td>5</td>
    <td>Bard</td>
    <td>Temperate forest</td>
  </tr>
  <tr>
    <td>Stone Giant</td>
    <td>Usually neutral</td>
    <td>+4</td>
    <td>+16 Dex, +4 Dex, +8 Con, +2 Wis</td>
    <td>14</td>
    <td>Barbarian</td>
    <td>Temperate mountain</td>
  </tr>
  <tr>
    <td>Troll</td>
    <td>Usually chaotic evil</td>
    <td>+5</td>
    <td>+12 Str, +4 Dex, +12 Con, –4 Int (minimum 3), –2 Wis, –4 Cha</td>
    <td>6</td>
    <td>Fighter</td>
    <td>Cold mountain</td>
  </tr>
<tfoot>
  <tr>
    <td colspan="6">
      1 This creature has an entry in <cite>Races of Destiny</cite>
    </td>
  </tr>
</tfoot>
</table>

<table>
  <caption>Table: Five Main Humanoid Races (AD&D)</caption>
  <tr>
    <th>Background</th>
    <th>Alignment</th>
    <th>Level Adjustment</th>
    <th>Ability Adjustments</th>
    <th>Favored Class</th>
    <th>Environment</th>
  </tr>
  <tr>
    <td>Gnoll</td>
    <td>Usually chaotic evil</td>
    <td>+1</td>
    <td>Str +4, Con +2, Int –2, Cha –2</td>
    <td>Ranger</td>
    <td>Warm plain</td>
  </tr>
  <tr>
    <td>Goblin</td>
    <td>Usually neutral evil</td>
    <td>+0</td>
    <td>–2 Str, +2 Dex, –2 Cha</td>
    <td>Rogue</td>
    <td>Temperate plain</td>
  </tr>
  <tr>
    <td>Kobold</td>
    <td>Usually lawful evil</td>
    <td>+0</td>
    <td>–4 Str, +2 Dex, –2 Con</td>
    <td>Sorcerer</td>
    <td>Temperate forest</td>
  </tr>
  <tr>
    <td>Orc</td>
    <td>Often chaotic evil</td>
    <td>+0</td>
    <td>+4 Str, –2 Int, –2 Wis, –2 Cha</td>
    <td>Barbarian</td>
    <td>Temperate hill</td>
  </tr>
  <tr>
    <td>Troll</td>
    <td>Usually chaotic evil</td>
    <td>+5</td>
    <td>+12 Str, +4 Dex, +12 Con, –4 Int (minimum 3), –2 Wis, –4 Cha.</td>
    <td>Fighter</td>
    <td>Cold mountain</td>
  </tr>
<tfoot>
  <tr>
    <td colspan="6">1 The normal creature was reduced to a starting effective character level of 1.</td>
  </tr>
</tfoot>
</table>

<table>
  <caption>Table: Dwarfs and Gnomes</caption>
  <tr>
    <th>Background</th>
    <th>Alignment</th>
    <th>Level Adjustment</th>
    <th>Ability Adjustments</th>
    <th>Favored Class</th>
    <th>Environment</th>
  </tr>
  <tr>
    <td>Hill dwarf</td>
    <td>Often lawful good</td>
    <td>+0</td>
    <td>+2 Con, –2 Cha</td>
    <td>Fighter</td>
    <td>Temperate mountain</td>
  </tr>
  <tr>
    <td>Deep dwarf</td>
    <td>Usually lawful neutral or neutral</td>
    <td>+0</td>
    <td>+2 Con, –2 Cha</td>
    <td>Fighter</td>
    <td>Underground</td>
  </tr>
  <tr>
    <td>Duergar</td>
    <td>Often lawful evil</td>
    <td>+1</td>
    <td>+2 Con, –4 Cha</td>
    <td>Fighter</td>
    <td>Underground</td>
  </tr>
  <tr>
    <td>Mountain dwarf</td>
    <td>Often lawful good</td>
    <td>+0</td>
    <td>+2 Con, –2 Cha</td>
    <td>Fighter</td>
    <td>Temperate mountain</td>
  </tr>
  <tr>
    <td>Rock gnome</td>
    <td>Usually neutral good</td>
    <td>+0</td>
    <td>+2 Con, –2 Str</td>
    <td>Bard</td>
    <td>Temperate hill</td>
  </tr>
  <tr>
    <td>Svirfneblin</td>
    <td>Usually neutral</td>
    <td>+3</td>
    <td>–2 Str, +2 Dex, +2 Wis, –4 Cha</td>
    <td>Rogue</td>
    <td>Underground</td>
  </tr>
  <tr>
    <td>Forest gnome</td>
    <td>Usually neutral good</td>
    <td>+0</td>
    <td>+2 Con, –2 Str</td>
    <td>Bard</td>
    <td>Temperate forest</td>
  </tr>
</table>

<table>
  <caption>Table: Humans and Half-humans</caption>
  <tr>
    <th>Background</th>
    <th>Alignment</th>
    <th>Level Adjustment</th>
    <th>Ability Adjustments</th>
    <th>Favored Class</th>
    <th>Environment</th>
  </tr>
  <tr>
    <td>Human</td>
    <td>Any</td>
    <td>+0</td>
    <td>&mdash;</td>
    <td>Any</td>
    <td>Any</td>
  </tr>
  <tr>
    <td><a href="#half-elf">Half-elf</a></td>
    <td>Usually chaotic (any)</td>
    <td>+0</td>
    <td>&mdash;</td>
    <td>Any</td>
    <td>Temperate forest</td>
  </tr>
  <tr>
    <td><a href="#half-orc">Half-orc</a></td>
    <td>Usually chaotic (any)</td>
    <td>+0</td>
    <td>+2 Str, –2 Int, –2 Cha</td>
    <td>Barbarian</td>
    <td>Temperate hill</td>
  </tr>
  <tr>
    <td>Orc</td>
    <td>Usually chaotic evil</td>
    <td>+0</td>
    <td>+4 Str, –2 Int, –2 Wis, –2 Cha</td>
    <td>Barbarian</td>
    <td>Temperate hill</td>
  </tr>
  <tr>
    <td>Aasimar</td>
    <td>Usually good (any)</td>
    <td>+1</td>
    <td>+2 Wis, +2 Cha</td>
    <td>Paladin</td>
    <td>Temperate plain</td>
  </tr>
  <tr>
    <td>Doppelganger<sup>1</sup></td>
    <td>Usually neutral</td>
    <td>Ignored</td>
    <td>+2 Con</td>
    <td>Rogue</td>
    <td>Any</td>
  </tr>
  <tr>
    <td>Tiefling</td>
    <td>Usually evil (any)</td>
    <td>+1</td>
    <td>+2 Dex, +2 Int, –2 Cha</td>
    <td>Rogue</td>
    <td>Temperate plain</td>
  </tr>
</table>

<table>
  <caption>Table: Elfs, Halflings, and Wildlings</caption>
  <tr>
    <th>Background</th>
    <th>Alignment</th>
    <th>Level Adjustment</th>
    <th>Ability Adjustments</th>
    <th>Favored Class</th>
    <th>Environment</th>
  </tr>
  <tr>
    <td>High elf</td>
    <td>Usually chaotic good</td>
    <td>+0</td>
    <td>+2 Dex, –2 Con</td>
    <td>Wizard</td>
    <td>Temperate forest</td>
  </tr>
  <tr>
    <td>Half-elf</td>
    <td>Usually chaotic (any)</td>
    <td>+0</td>
    <td>None</td>
    <td>Any</td>
    <td>Temperate forest</td>
  </tr>
  <tr>
    <td>Aquatic elf</td>
    <td>Usually chaotic good</td>
    <td>+0</td>
    <td>+2 Dex, –2 Int</td>
    <td>Fighter</td>
    <td>Temperate aquatic</td>
  </tr>
  <tr>
    <td>Drow</td>
    <td>Usually neutral evil</td>
    <td>+2</td>
    <td>+2 Int, +2 Cha</td>
    <td>Wizard (male), cleric (female)</td>
    <td>Underground</td>
  </tr>
  <tr>
    <td>Gray elf</td>
    <td>Usually chaotic good</td>
    <td>+0</td>
    <td>+2 Int, –2 Str</td>
    <td>Wizard</td>
    <td>Temperate mountains</td>
  </tr>
  <tr>
    <td>Wild elf</td>
    <td>Usually chaotic good</td>
    <td>+0</td>
    <td>+2 Dex, –2 Int</td>
    <td>Sorcerer</td>
    <td>Warm forest</td>
  </tr>
  <tr>
    <td>Wood elf</td>
    <td>Usually neutral</td>
    <td>+0</td>
    <td>+2 Str, –2 Int</td>
    <td>Ranger</td>
    <td>Temperate forest</td>
  </tr>
  <tr>
    <td>Lightfoot halfling</td>
    <td>Usually neutral</td>
    <td>+0</td>
    <td>+2 Dex, –2 Str</td>
    <td>Rogue</td>
    <td>Warm plains</td>
  </tr>
  <tr>
    <td>Tallfellow halfling</td>
    <td>Usually neutral</td>
    <td>+0</td>
    <td>+2 Dex, –2 Str</td>
    <td>Rogue</td>
    <td>Temperate forest</td>
  </tr>
  <tr>
    <td>Deep halfling</td>
    <td>Usually neutral</td>
    <td>+0</td>
    <td>+2 Dex, –2 Str</td>
    <td>Rogue</td>
    <td>Warm hill</td>
  </tr>
  <tr>
    <td>Centaur<sup>1</sup></td>
    <td>Usually neutral good</td>
    <td>Ignored</td>
    <td>+2 Dex, –2 Int</td>
    <td>Ranger</td>
    <td>Temperate forest</td>
  </tr>
  <tr>
    <td>Gnoll<sup>1</sup></td>
    <td>Usually chaotic evil</td>
    <td>Ignored</td>
    <td>Str +2, Int –2, Cha –2</td>
    <td>Ranger</td>
    <td>Warm plain</td>
  </tr>
<tfoot>
  <tr>
    <td colspan="6">1 This creature must take levels in a "monster class" before multiclassing into a character class.</td>
  </tr>
</tfoot>
</table>
