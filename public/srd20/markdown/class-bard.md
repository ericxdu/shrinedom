Bard
====

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

A bard has the following game statistics.

__Alignment:__ Any nonlawful.

__Hit Die:__ d6.

__Base Attack Bonus:__ Equal to 3/4 total Hit Dice (as cleric).

__Good Saving Throws:__ Reflex and Will.

#### Class Skills

The bard’s class skills (and the key ability for each skill) are 
Appraise (Int), Balance (Dex), Bluff (Cha), Climb (Str), Concentration 
(Con), Craft (Int), Decipher Script (Int), Diplomacy (Cha), Disguise 
(Cha), Escape Artist (Dex), Gather Information (Cha), Hide (Dex), Jump 
(Str), Knowledge (all skills, taken individually) (Int), Listen (Wis), 
Move Silently (Dex), Perform (Cha), Profession (Wis), Sense Motive 
(Wis), Sleight of Hand (Dex), Speak Language (n/a), Spellcraft (Int), 
Swim (Str), Tumble (Dex), and Use Magic Device (Cha).

__Skill Points at 1st Level:__ (6 + Int modifier) x4.

__Skill Points at Each Additional Level:__ 6 + Int modifier.

#### Class Features

All of the following are class features of the bard.

__Weapon and Armor Proficiency:__ A bard is proficient with all simple 
weapons, plus the longsword, rapier, sap, short sword, shortbow, and 
whip. Bards are proficient with light armor and shields (except tower 
shields). A bard can cast bard spells while wearing light armor without 
incurring the normal arcane spell failure chance. However, like any 
other arcane spellcaster, a bard wearing medium or heavy armor or using 
a shield incurs a chance of arcane spell failure if the spell in 
question has a somatic component (most do). A multiclass bard still 
incurs the normal arcane spell failure chance for arcane spells received 
from other classes.

__Cantrips:__ [Insert class feature description here.]

__Spells:__ A bard casts arcane spells, which are drawn from the bard 
spell list. He can cast any spell he knows without preparing it ahead of 
time. Every bard spell has a verbal component (singing, reciting, or 
music). To learn or cast a spell, a bard must have a Charisma score 
equal to at least 10 + the spell. The Difficulty Class for a saving 
throw against a bard’s spell is 10 + the spell level + the bard’s 
Charisma modifier.

Table: Bard Spells Per Day

Table: Bard Spells Known

Like other spellcasters, a bard can cast only a certain number of spells 
of each spell level per day. His base daily spell allotment is given on 
Table: The Bard. In addition, he receives bonus spells per day if he has 
a high Charisma score. When Table: Bard Spells Known indicates that the 
bard gets 0 spells per day of a given spell level, he gains only the 
bonus spells he would be entitled to based on his Charisma score for 
that spell level.

The bard’s selection of spells is extremely limited. A bard begins play 
knowing four 0-level spells of your choice. At most new bard levels, he 
gains one or more new spells, as indicated on Table: Bard Spells Known. 
(Unlike spells per day, the number of spells a bard knows is not 
affected by his Charisma score; the numbers on Table: Bard Spells Known 
are fixed.)

Upon reaching 5th level, and at every third bard level after that (8th, 
11th, and so on), a bard can choose to learn a new spell in place of one 
he already knows. In effect, the bard “loses” the old spell in exchange 
for the new one. The new spell’s level must be the same as that of the 
spell being exchanged, and it must be at least two levels lower than the 
highest-level bard spell the bard can cast. A bard may swap only a 
single spell at any given level, and must choose whether or not to swap 
the spell at the same time that he gains new spells known for the level.

As noted above, a bard need not prepare his spells in advance. He can 
cast any spell he knows at any time, assuming he has not yet used up his 
allotment of spells per day for the spell’s level.

__Bardic Knowledge:__ This feature grants a bonus to all Knowledge skills equal to half your bard level (round down, minimum +1). This ability also allows you to use any Knowledge skill untrained.

__Bardic Performance:__ [Insert bardic performance description here.]

Bards can use their bardic performance ability for 4 rounds per day + 
their Charisma modifier. This increases by 2 rounds per day for every 
level beyond 1st.

Starting a bardic music effect is a standard action. Some bardic music 
abilities require concentration, which means the bard must take a 
standard action each round to maintain the ability. Even while using 
bardic music that doesn’t require concentration, a bard cannot cast 
spells, activate magic items by spell completion (such as scrolls), or 
activate magic items by magic word (such as wands). Just as for casting 
a spell with a verbal component, a deaf bard has a 20% chance to fail 
when attempting to use bardic music. If he fails, the attempt still 
counts against his daily limit.

In adddition to the following performace types, 1st-level bards receive 
_distraction_, 8th-level bards receive _dirge of doom_, 12th-level bards 
receive _soothing performance_, 14th-level bards receive _frightening 
tune_, and 20th-level bards receive _deadly performance_.

[Insert new performance types.]

_Countersong (Su):_ [Insert countersong description here.]

_Fascinate (Sp):_ A bard with 3 or more ranks in a Perform skill can use his music or poetics to cause one or more creatures to become fascinated with him. Each creature to be fascinated must be within 90 feet, able to see and hear the bard, and able to pay attention to him. The bard must also be able to see the creature. The distraction of a nearby combat or other dangers prevents the ability from working. For every three levels a bard attains beyond 1st, he can target one additional creature with a single use of this ability.

To use the ability, a bard makes a Perform check. His check result is the DC for each affected creature’s Will save against the effect. If a creature’s saving throw succeeds, the bard cannot attempt to fascinate that creature again for 24 hours. If its saving throw fails, the creature sits quietly and listens to the song, taking no other actions, for as long as the bard continues to play and concentrate (up to a maximum of 1 round per bard level). While fascinated, a target takes a –4 penalty on skill checks made as reactions, such as Listen and Spot checks. Any potential threat requires the bard to make another Perform check and allows the creature a new saving throw against a DC equal to the new Perform check result.

Any obvious threat, such as someone drawing a weapon, casting a spell, 
or aiming a ranged weapon at the target, automatically breaks the 
effect. _Fascinate_ is an enchantment (compulsion), mind-affecting 
ability.

_Inspire Courage (Su):_ A bard with 3 or more ranks in a Perform skill can use song or poetics to inspire courage in his allies (including himself ), bolstering them against fear and improving their combat abilities. To be affected, an ally must be able to hear the bard sing. The effect lasts for as long as the ally hears the bard sing and for 5 rounds thereafter. An affected ally receives a +1 morale bonus on saving throws against charm and fear effects and a +1 morale bonus on attack and weapon damage rolls. At 8th level, and every six bard levels thereafter, this bonus increases by 1 (+2 at 8th, +3 at 14th, and +4 at 20th). Inspire courage is a mind-affecting ability.

_Inspire Competence (Su):_ A bard of 3rd level or higher with 6 or more 
ranks in a Perform skill can use his music or poetics to help an ally 
succeed at a task. The ally must be within 30 feet and able to see and 
hear the bard. The bard must also be able to see the ally.

The ally gets a +2 competence bonus on skill checks with a particular skill as long as he or she continues to hear the bard’s music. Certain uses of this ability are infeasible. The effect lasts as long as the bard concentrates, up to a maximum of 2 minutes. A bard can’t inspire competence in himself. Inspire competence is a mind-affecting ability.

_Suggestion (Sp):_ A bard of 6th level or higher with 9 or more ranks in 
a Perform skill can make a _suggestion_ (as the spell) to a creature 
that he has already fascinated (see above). Using this ability does not 
break the bard’s concentration on the _fascinate_ effect, nor does it 
allow a second saving throw against the _fascinate_ effect.

Making a _suggestion_ doesn’t count against a bard’s daily limit on 
bardic music performances. A Will saving throw (DC 10 + 1/2 bard’s level 
+ bard’s Cha modifier) negates the effect. This ability affects only a 
single creature (but see _mass suggestion_, below). _Suggestion_ is an 
enchantment (compulsion), mind-affecting, language dependent ability.

_Inspire Greatness (Su):_ A bard of 9th level or higher with 12 or more ranks in a Perform skill can use music or poetics to inspire greatness in himself or a single willing ally within 30 feet, granting him or her extra fighting capability. For every three levels a bard attains beyond 9th, he can target one additional ally with a single use of this ability (two at 12th level, three at 15th, four at 18th). To inspire greatness, a bard must sing and an ally must hear him sing. The effect lasts for as long as the ally hears the bard sing and for 5 rounds thereafter. A creature inspired with greatness gains 2 bonus Hit Dice (d10s), the commensurate number of temporary hit points (apply the target’s Constitution modifier, if any, to these bonus Hit Dice), a +2 competence bonus on attack rolls, and a +1 competence bonus on Fortitude saves. The bonus Hit Dice count as regular Hit Dice for determining the effect of spells that are Hit Dice dependant. Inspire greatness is a mind-affecting ability.

_Song of Freedom (Sp):_ A bard of 12th level or higher with 15 or more 
ranks in a Perform skill can use music or poetics to create an effect 
equivalent to the _break enchantment_ spell (caster level equals the 
character’s bard level). Using this ability requires 1 minute of 
uninterrupted concentration and music, and it functions on a single 
target within 30 feet. A bard can’t use _song of freedom_ on himself.

_Inspire Heroics (Su):_ A bard of 15th level or higher with 18 or more ranks in a Perform skill can use music or poetics to inspire tremendous heroism in himself or a single willing ally within 30 feet. For every three bard levels the character attains beyond 15th, he can inspire heroics in one additional creature. To inspire heroics, a bard must sing and an ally must hear the bard sing for a full round. A creature so inspired gains a +4 morale bonus on saving throws and a +4 dodge bonus to AC. The effect lasts for as long as the ally hears the bard sing and for up to 5 rounds thereafter. Inspire heroics is a mind-affecting ability.

_Mass Suggestion (Sp):_ This ability functions like _suggestion_, above, 
except that a bard of 18th level or higher with 21 or more ranks in a 
Perform skill can make the _suggestion_ simultaneously to any number of 
creatures that he has already fascinated (see above). _Mass suggestion_ 
is an enchantment (compulsion), mind-affecting, language-dependent 
ability.

__Versatile Performance:__ [Insert class feature description here.]

__Well-versed:__ [Insert class feature description here.]

__Lore Master:__ [Insert class feature description here.]

__Jack-of-all-trades:__ [Insert class feature description here.]
 
#### Ex-Bards

A bard who becomes lawful in alignment cannot progress in levels as a 
bard, though he retains all his bard abilities.


Legal Information
=================

OPEN GAME LICENSE Version 1.0a
 
The following text is the property of Wizards of the Coast, Inc. and is Copyright 2000 Wizards of the Coast, Inc ("Wizards"). All Rights Reserved.
 
1. Definitions: (a)"Contributors" means the copyright and/or trademark owners who have contributed Open Game Content; (b)"Derivative Material" means copyrighted material including derivative works and translations (including into other computer languages), potation, modification, correction, addition, extension, upgrade, improvement, compilation, abridgment or other form in which an existing work may be recast, transformed or adapted; (c) "Distribute" means to reproduce, license, rent, lease, sell, broadcast, publicly display, transmit or otherwise distribute; (d)"Open Game Content" means the game mechanic and includes the methods, procedures, processes and routines to the extent such content does not embody the Product Identity and is an enhancement over the prior art and any additional content clearly identified as Open Game Content by the Contributor, and means any work covered by this License, including translations and derivative works under copyright law, but specifically excludes Product Identity. (e) "Product Identity" means product and product line names, logos and identifying marks including trade dress; artifacts; creatures characters; stories, storylines, plots, thematic elements, dialogue, incidents, language, artwork, symbols, designs, depictions, likenesses, formats, poses, concepts, themes and graphic, photographic and other visual or audio representations; names and descriptions of characters, spells, enchantments, personalities, teams, personas, likenesses and special abilities; places, locations, environments, creatures, equipment, magical or supernatural abilities or effects, logos, symbols, or graphic designs; and any other trademark or registered trademark clearly identified as Product identity by the owner of the Product Identity, and which specifically excludes the Open Game Content; (f) "Trademark" means the logos, names, mark, sign, motto, designs that are used by a Contributor to identify itself or its products or the associated products contributed to the Open Game License by the Contributor (g) "Use", "Used" or "Using" means to use, Distribute, copy, edit, format, modify, translate and otherwise create Derivative Material of Open Game Content. (h) "You" or "Your" means the licensee in terms of this agreement.
 
2. The License: This License applies to any Open Game Content that contains a notice indicating that the Open Game Content may only be Used under and in terms of this License. You must affix such a notice to any Open Game Content that you Use. No terms may be added to or subtracted from this License except as described by the License itself. No other terms or conditions may be applied to any Open Game Content distributed using this License.
 
3. Offer and Acceptance: By Using the Open Game Content You indicate Your acceptance of the terms of this License.
 
4. Grant and Consideration: In consideration for agreeing to use this License, the Contributors grant You a perpetual, worldwide, royalty-free, non-exclusive license with the exact terms of this License to Use, the Open Game Content.
 
5. Representation of Authority to Contribute: If You are contributing original material as Open Game Content, You represent that Your Contributions are Your original creation and/or You have sufficient rights to grant the rights conveyed by this License.
 
6. Notice of License Copyright: You must update the COPYRIGHT NOTICE portion of this License to include the exact text of the COPYRIGHT NOTICE of any Open Game Content You are copying, modifying or distributing, and You must add the title, the copyright date, and the copyright holder's name to the COPYRIGHT NOTICE of any original Open Game Content you Distribute.
 
7. Use of Product Identity: You agree not to Use any Product Identity, including as an indication as to compatibility, except as expressly licensed in another, independent Agreement with the owner of each element of that Product Identity. You agree not to indicate compatibility or co-adaptability with any Trademark or Registered Trademark in conjunction with a work containing Open Game Content except as expressly licensed in another, independent Agreement with the owner of such Trademark or Registered Trademark. The use of any Product Identity in Open Game Content does not constitute a challenge to the ownership of that Product Identity. The owner of any Product Identity used in Open Game Content shall retain all rights, title and interest in and to that Product Identity.
 
8. Identification: If you distribute Open Game Content You must clearly indicate which portions of the work that you are distributing are Open Game Content.
 
9. Updating the License: Wizards or its designated Agents may publish updated versions of this License. You may use any authorized version of this License to copy, modify and distribute any Open Game Content originally distributed under any version of this License.
 
10. Copy of this License: You MUST include a copy of this License with every copy of the Open Game Content You Distribute.
 
11. Use of Contributor Credits: You may not market or advertise the Open Game Content using the name of any Contributor unless You have written permission from the Contributor to do so.
 
12. Inability to Comply: If it is impossible for You to comply with any of the terms of this License with respect to some or all of the Open Game Content due to statute, judicial order, or governmental regulation then You may not Use any Open Game Material so affected.
 
13. Termination: This License will terminate automatically if You fail to comply with all terms herein and fail to cure such breach within 30 days of becoming aware of the breach. All sublicenses shall survive the termination of this License.
 
14. Reformation: If any provision of this License is held to be unenforceable, such provision shall be reformed only to the extent necessary to make it enforceable.
 
15. COPYRIGHT NOTICE
Open Game License v 1.0a Copyright 2000, Wizards of the Coast, Inc.
 
__System Reference Document__ Copyright 2000-2003, Wizards of the Coast, Inc.; Authors Jonathan Tweet, Monte Cook, Skip Williams, Rich Baker, Andy Collins, David Noonan, Rich Redman, Bruce R. Cordell, John D. Rateliff, Thomas Reid, James Wyatt, based on original material by E. Gary Gygax and Dave Arneson.

__Pathfinder Roleplaying Game Conversion Guide__. Copyright 2009, Paizo Publishing, LLC; Author: Jason Bulmahn.

__Pathfinder Roleplaying Game Core Rulebook__. Copyright 2009, Paizo Publishing, LLC; Author: Jason Bulmahn, based on material by Jonathan Tweet, Monte Cook, and Skip Williams.
 
END OF LICENSE
