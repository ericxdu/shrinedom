Cleric
======

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

### Game Rule Information

A cleric has the following game statistics.

__Alignment:__ A cleric’s alignment must be within one step of his deity’s (that is, it may be one step away on either the lawful–chaotic axis or the good–evil axis, but not both). A cleric may not be neutral unless his deity’s alignment is also neutral.

__Hit Die:__ d8.

__Base Attack Bonus:__ Equal to 3/4 total Hit Dice.

__Good Saving Throws:__ Fortitude and Will.

#### Class Skills

The cleric’s class skills (and the key ability for each skill) are 
Concentration (Con), Craft (Int), Diplomacy (Cha), Heal (Wis), Knowledge 
(arcana) (Int), Knowledge (history) (Int), Knowledge (religion) (Int), 
Knowledge (the planes) (Int), Profession (Wis), and Spellcraft (Int).

__Domains and Class Skills:__ A cleric who chooses the Animal or Plant 
domain adds Knowledge (nature) (Int) to the cleric class skills listed 
above. A cleric who chooses the Knowledge domain adds all Knowledge 
(Int) skills to the list. A cleric who chooses the Travel domain adds 
Survival (Wis) to the list. A cleric who chooses the Trickery domain 
adds Bluff (Cha), Disguise (Cha), and Hide (Dex) to the list. See Deity, 
Domains, and Domain Spells, below, for more information.

__Skill Points at Each Level:__ 2 + Int modifier.

#### Class Features

All of the following are class features of the cleric. 

__Weapon and Armor Proficiency:__ Clerics are proficient with all simple 
weapons, with all types of armor except heavy, and with shields (except 
tower shields). A cleric who chooses the War domain receives the Weapon 
Focus feat related to his deity’s weapon as a bonus feat. He also 
receives the appropriate Martial Weapon Proficiency feat as a bonus 
feat, if the weapon falls into that category.

__Aura (Ex):__ [Insert class feature description here.]

__Orisons:__ [Insert class feature description here.]

__Spells:__ [Insert class feature description here.]

A cleric casts divine spells, which are drawn from the 
cleric spell list. However, his alignment may restrict him from casting 
certain spells opposed to his moral or ethical beliefs; see Chaotic, 
Evil, Good, and Lawful Spells, below. A cleric must choose and prepare 
his spells in advance (see below).

To prepare or cast a spell, a cleric must have a Wisdom score equal to 
at least 10 + the spell level. The Difficulty Class for a saving throw 
against a cleric’s spell is 10 + the spell level + the cleric’s Wisdom 
modifier.

Like other spellcasters, a cleric can cast only a certain number of 
spells of each spell level per day. His base daily spell allotment is 
given on Table: The Cleric. In addition, he receives bonus spells per 
day if he has a high Wisdom score. A cleric also gets one domain spell 
of each spell level he can cast, starting at 1st level. When a cleric 
prepares a spell in a domain spell slot, it must come from one of his 
two domains (see Deities, Domains, and Domain Spells, below).

Clerics meditate or pray for their spells. Each cleric must choose a 
time at which he must spend 1 hour each day in quiet contemplation or 
supplication to regain his daily allotment of spells. Time spent resting 
has no effect on whether a cleric can prepare spells. A cleric may 
prepare and cast any spell on the cleric spell list, provided that he 
can cast spells of that level, but he must choose which spells to 
prepare during his daily meditation.

__Deity, Domains, and Domain Spells:__ A cleric’s deity influences his 
alignment, what magic he can perform, his values, and how others see 
him. A cleric chooses two domains from among those belonging to his 
deity. A cleric can select an alignment domain (Chaos, Evil, Good, or 
Law) only if his alignment matches that domain.

If a cleric is not devoted to a particular deity, he still selects two 
domains to represent his spiritual inclinations and abilities. The 
restriction on alignment domains still applies.

Each domain gives the cleric access to a domain spell at each spell 
level he can cast, from 1st on up, as well as a granted power. The 
cleric gets the granted powers of both the domains selected.

With access to two domain spells at a given spell level, a cleric 
prepares one or the other each day in his domain spell slot. If a domain 
spell is not on the cleric spell list, a cleric can prepare it only in 
his domain spell slot.

__Spontaneous Casting:__ A good cleric (or a neutral cleric of a good 
deity) can channel stored spell energy into healing spells that the 
cleric did not prepare ahead of time. The cleric can “lose” any prepared 
spell that is not a domain spell in order to cast any _cure_ spell of 
the same spell level or lower (a _cure_ spell is any spell with “cure” 
in its name).

An evil cleric (or a neutral cleric of an evil deity), can’t convert 
prepared spells to _cure_ spells but can convert them to _inflict_ 
spells (an _inflict_ spell is one with “inflict” in its name).

A cleric who is neither good nor evil and whose deity is neither good 
nor evil can convert spells to either cure spells or inflict spells 
(player’s choice). Once the player makes this choice, it cannot be 
reversed. This choice also determines whether the cleric turns or 
commands undead (see below).

__Chaotic, Evil, Good, and Lawful Spells:__ A cleric can’t cast spells 
of an alignment opposed to his own or his deity’s (if he has one). 
Spells associated with particular alignments are indicated by the chaos, 
evil, good, and law descriptors in their spell descriptions.

__Channel Energy (Su):__ [Insert class feature description.]

A cleric may use this ability a number of times per day equal to 3 + his 
Charisma modifier. This ability also allows a saving throw (DC = 10 + 
1/2 the cleric's level + the cleric's Cha modifier).

__Bonus Languages:__ A cleric’s bonus language options include 
Celestial, Abyssal, and Infernal (the languages of good, chaotic evil, 
and lawful evil outsiders, respectively). These choices are in addition 
to the bonus languages available to the character because of his race.
 
#### Ex-Clerics

A cleric who grossly violates the code of conduct required by his god 
loses all spells and class features, except for armor and shield 
proficiencies and proficiency with simple weapons. He cannot thereafter 
gain levels as a cleric of that god until he atones (see the atonement 
spell description).


Legal Information
=================

OPEN GAME LICENSE Version 1.0a
 
The following text is the property of Wizards of the Coast, Inc. and is Copyright 2000 Wizards of the Coast, Inc ("Wizards"). All Rights Reserved.
 
1. Definitions: (a)"Contributors" means the copyright and/or trademark owners who have contributed Open Game Content; (b)"Derivative Material" means copyrighted material including derivative works and translations (including into other computer languages), potation, modification, correction, addition, extension, upgrade, improvement, compilation, abridgment or other form in which an existing work may be recast, transformed or adapted; (c) "Distribute" means to reproduce, license, rent, lease, sell, broadcast, publicly display, transmit or otherwise distribute; (d)"Open Game Content" means the game mechanic and includes the methods, procedures, processes and routines to the extent such content does not embody the Product Identity and is an enhancement over the prior art and any additional content clearly identified as Open Game Content by the Contributor, and means any work covered by this License, including translations and derivative works under copyright law, but specifically excludes Product Identity. (e) "Product Identity" means product and product line names, logos and identifying marks including trade dress; artifacts; creatures characters; stories, storylines, plots, thematic elements, dialogue, incidents, language, artwork, symbols, designs, depictions, likenesses, formats, poses, concepts, themes and graphic, photographic and other visual or audio representations; names and descriptions of characters, spells, enchantments, personalities, teams, personas, likenesses and special abilities; places, locations, environments, creatures, equipment, magical or supernatural abilities or effects, logos, symbols, or graphic designs; and any other trademark or registered trademark clearly identified as Product identity by the owner of the Product Identity, and which specifically excludes the Open Game Content; (f) "Trademark" means the logos, names, mark, sign, motto, designs that are used by a Contributor to identify itself or its products or the associated products contributed to the Open Game License by the Contributor (g) "Use", "Used" or "Using" means to use, Distribute, copy, edit, format, modify, translate and otherwise create Derivative Material of Open Game Content. (h) "You" or "Your" means the licensee in terms of this agreement.
 
2. The License: This License applies to any Open Game Content that contains a notice indicating that the Open Game Content may only be Used under and in terms of this License. You must affix such a notice to any Open Game Content that you Use. No terms may be added to or subtracted from this License except as described by the License itself. No other terms or conditions may be applied to any Open Game Content distributed using this License.
 
3. Offer and Acceptance: By Using the Open Game Content You indicate Your acceptance of the terms of this License.
 
4. Grant and Consideration: In consideration for agreeing to use this License, the Contributors grant You a perpetual, worldwide, royalty-free, non-exclusive license with the exact terms of this License to Use, the Open Game Content.
 
5. Representation of Authority to Contribute: If You are contributing original material as Open Game Content, You represent that Your Contributions are Your original creation and/or You have sufficient rights to grant the rights conveyed by this License.
 
6. Notice of License Copyright: You must update the COPYRIGHT NOTICE portion of this License to include the exact text of the COPYRIGHT NOTICE of any Open Game Content You are copying, modifying or distributing, and You must add the title, the copyright date, and the copyright holder's name to the COPYRIGHT NOTICE of any original Open Game Content you Distribute.
 
7. Use of Product Identity: You agree not to Use any Product Identity, including as an indication as to compatibility, except as expressly licensed in another, independent Agreement with the owner of each element of that Product Identity. You agree not to indicate compatibility or co-adaptability with any Trademark or Registered Trademark in conjunction with a work containing Open Game Content except as expressly licensed in another, independent Agreement with the owner of such Trademark or Registered Trademark. The use of any Product Identity in Open Game Content does not constitute a challenge to the ownership of that Product Identity. The owner of any Product Identity used in Open Game Content shall retain all rights, title and interest in and to that Product Identity.
 
8. Identification: If you distribute Open Game Content You must clearly indicate which portions of the work that you are distributing are Open Game Content.
 
9. Updating the License: Wizards or its designated Agents may publish updated versions of this License. You may use any authorized version of this License to copy, modify and distribute any Open Game Content originally distributed under any version of this License.
 
10. Copy of this License: You MUST include a copy of this License with every copy of the Open Game Content You Distribute.
 
11. Use of Contributor Credits: You may not market or advertise the Open Game Content using the name of any Contributor unless You have written permission from the Contributor to do so.
 
12. Inability to Comply: If it is impossible for You to comply with any of the terms of this License with respect to some or all of the Open Game Content due to statute, judicial order, or governmental regulation then You may not Use any Open Game Material so affected.
 
13. Termination: This License will terminate automatically if You fail to comply with all terms herein and fail to cure such breach within 30 days of becoming aware of the breach. All sublicenses shall survive the termination of this License.
 
14. Reformation: If any provision of this License is held to be unenforceable, such provision shall be reformed only to the extent necessary to make it enforceable.
 
15. COPYRIGHT NOTICE
Open Game License v 1.0a Copyright 2000, Wizards of the Coast, Inc.
 
__System Reference Document__ Copyright 2000-2003, Wizards of the Coast, Inc.; Authors Jonathan Tweet, Monte Cook, Skip Williams, Rich Baker, Andy Collins, David Noonan, Rich Redman, Bruce R. Cordell, John D. Rateliff, Thomas Reid, James Wyatt, based on original material by E. Gary Gygax and Dave Arneson.

__Pathfinder Roleplaying Game Conversion Guide__. Copyright 2009, Paizo Publishing, LLC; Author: Jason Bulmahn.

__Pathfinder Roleplaying Game Core Rulebook__. Copyright 2009, Paizo Publishing, LLC; Author: Jason Bulmahn, based on material by Jonathan Tweet, Monte Cook, and Skip Williams.
 
END OF LICENSE
