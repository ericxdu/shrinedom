Heroic Skill System
===================

<table style="float:right;">
<caption>Table: Skills Known by Class</caption>
<tbody>
  <tr>
    <th>Class</th>
    <th>Skills Known<sup>1</sup></th>
  </tr>
  <tr>
    <td>Cleric, fighter, paladin, sorceror, or wizard</td>
    <td>2 + Int modifier</td>
  </tr>
  <tr>
    <td>Barbarian, druid, or monk</td>
    <td>4 + Int modifier</td>
  </tr>
  <tr>
    <td>Bard or ranger</td>
    <td>6 + Int modifier</td>
  </tr>
  <tr>
    <td>Rogue</td>
    <td>8 + Int modifier</td>
  </tr>
</tbody>
<tfoot>
  <tr>
    <td colspan="2">
      1 Characters always start with one skill at minimum, regardless of<br/>
      penalty, and humans get one extra skill in addition to the numbers here
    </td>
  </tr>
</tfoot>
</table>

> ...the skills on your character sheet don't actually define everything 
> your character can do....The skills you buy ranks in, however, are 
> those with which you have truly heroic potential. (*CRI*, p. 62)

When you create a character, you choose a number of skills your 
character is heroicially good at. The number of skills you can choose is 
determined by your class and Intelligence modifier. You may only choose 
class skills or cross-class skills for your character. Your character is 
now considered "trained" in the chosen skills. Class skills for your 
class are listed in [Classes I](srd/ClassesI.html) or [Classes 
II](srd/ClassesII.html). Skills are described in [Skills 
I](srd/SkillsI.html) and [Skills II](srd/SkillsII.html). _Note:_ Skills 
I mentions an alternative method for acquiring skills that do not apply 
to the heroic skill system.

### Skill Checks

<table style="clear:both; float:right;">
<caption>Table: Rank Bonus on Skill Checks</caption>
<tbody>
  <tr>
    <th>Untrained</th>
    <td>None</td>
  </tr>
  <tr>
    <th>Trained (class skill)</th>
    <td>3 + character level</td>
  </tr>
    <th>Trained (cross-class skill)</th>
    <td>(3 + character level) &divide; 2</td>
  <tr>
</tbody>
</table>

Whenever you make a skill check with a skill you are trained in, you add 
a rank bonus equal to 3 + your character level if it is a class skill. 
If it is a cross-class skill, the rank bonus is half that number rounded 
down. If you are untrained, you get no rank bonus to the check.

### Multiclass Characters

If you gain a level in a second class, all of the class skills for that 
class become class skill for you. Thus, if you were already trained in a 
cross-class skill that is a class skill for the new class, you now add 
the full rank bonus to skill checks using that skill instead of half the 
rank bonus.

### Learning New Skills

If you increase your Intelligence modifier, you can immediately learn 
one more skill. You can choose a class skill from among any class you 
have levels in, or you can choose any cross-class skill. You now add the 
appropriate bonus to skill checks using that skill.

You can also learn new skills by taking the new skills feat.


New Feat
--------

### New Skills [General]

You become heroically proficient in new skills.

__Prerequisite__: Intelligence 3 or higher.

__Benefit__: Choose two class skills or cross-class skills. You are now 
considered "trained" in these skills and add a rank bonus to skill 
checks involving these skills: 3 + your character level for class skills 
and half that (rounded down) for cross-class skills. You add your 
ability modifier and any miscellaneous modifiers as normal.

__Normal__: When rolling a skill check in which you are not trained, you 
add only your ability score modifier and miscellaneous modifiers

__Special__: You can gain this feat multiple times. Its effects do not 
stack. Each time you take the feat, it applies to two new skills.
