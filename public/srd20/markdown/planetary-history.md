Planetary Romance History and Lore
==================================

Interplanetary culture has a long and storied history. In general, different species and cultures from across the solar system have been intermingling socially, economically, and militarily for centuries; at points cooperating with each other, abusing each other, and becoming isolated from one another. This becomes recorded in cultural history as interactions with mythological beings like gods and sprits.

Old Norse: Aesir, vanir, elves, dwarfs, and giants.
