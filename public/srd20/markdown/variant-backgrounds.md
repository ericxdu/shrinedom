Variant Backgrounds
===================

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

Environmental racial variants were described in <cite>Unearthed 
Arcana</cite>. That portion of the book was identified as Open Game 
Content. The below variant backgrounds are derived from that content. 
The GM may specify that some or all of the following variant backgrounds 
are available for character creation.


Aquatic Backgrounds
-------------------

> None of the aquatic races have level adjustments when your entire 
> campaign is set underwater and all the PCs have the aquatic subtype, 
> or when playing a nonaquatic campaign. The advantages gained by an 
> aquatic character when in an aquatic environment even out with those 
> of other aquatic characters, and their disadvantages in nonaquatic 
> environments make up for any advantages they might enjoy.

> However, when a mix of aquatic and nonaquatic characters occurs in an 
> aquatic or ship-based campaign, aquatic characters enjoy a distinct 
> advantage over their land-based cousins. In this case, consider 
> applying a +1 level adjustment for all aquatic races, due to their 
> swim speed, improved low-light vision or darkvision, and other special 
> abilities.

<table>
<caption>Table: Aquatic Backgrounds</caption>
  <tr>
    <th>Background</th>
    <th>Ability Adjustments</th>
    <th>Favored Class</th>
  </tr>
  <tr>
    <td>Dwarf</td>
    <td>+2 Strength, +2 Constitution, &minus;4 Dexterity, &minus;2 Charisma</td>
    <td>Fighter</td>
  </tr>
  <tr>
    <td>Elf</td>
    <td>+2 Dexterity, &minus;2 Intelligence</td>
    <td>Fighter</td>
  </tr>
  <tr>
    <td>Gnome</td>
    <td>+2 Constitution, &minus;2 Strength</td>
    <td>Bard</td>
  </tr>
  <tr>
    <td>Goblin</td>
    <td>&minus;2 Strength, +2 Constitution, &minus;2 Charisma</td>
    <td>Rogue</td>
  </tr>
  <tr>
    <td>Half-elf</td>
    <td>&mdash;</td>
    <td>Any</td>
  </tr>
  <tr>
    <td>Half-orc</td>
    <td>+2 Strength, &minus;2 Intelligence, &minus;2 Charisma</td>
    <td>Barbarian</td>
  </tr>
  <tr>
    <td>Halfling</td>
    <td>+2 Dexterity, &minus;2 Strength</td>
    <td>Rogue</td>
  </tr>
  <tr>
    <td>Human</td>
    <td>&mdash;</td>
    <td>Any</td>
  </tr>
  <tr>
    <td>Kobold</td>
    <td>&minus;4 Strength, +2 Dexterity, &minus;2 Constitution</td>
    <td>Sorcerer</td>
  </tr>
  <tr>
    <td>Orc</td>
    <td>+4 Strength, &minus;2 Intelligence, &minus;2 Wisdom, &minus;2 Charisma</td>
    <td>Barbarian</td>
  </tr>
</table>

### Aquatic Dwarves

The following traits are in addition to the [dwarf](standard-backgrounds.html#dwarves) traits, except where noted.

* &bull; +2 Strength, +2 Constitution, &minus;4 Dexterity, &minus;2 
  Charisma. Aquatic dwarves are tough enough to survive the brutal 
  environment of the sea floor, but rely on strength and heavy armor 
  rather than agility.
* &bull; Aquatic Subtype: An aquatic creature can breathe underwater. It 
  cannot also breathe air unless it also has the amphibious special 
  quality. An aquatic creature can hold its breath outside the water for 
  2 rounds per point of Constitution. After that, it begins to 
  suffocate.
* &bull; An aquatic dwarf has a swim speed of 20 feet. It has a +8 
  racial bonus on any Swim check to perform some action or avoid a 
  hazard. It can always choose to take 10 on a Swim check, even if 
  distracted or endangered. It can use the run action while swimming, 
  provided it swims in a straight line.
* &bull; Bonus Language: Aquan. Aquatic races are familiar with the 
  language of water-based creatures.

### Aquatic Elves

The following traits are in addition to the [elf](standard-backgrounds.html#elves) traits, except where noted.

* &bull; +2 Dexterity, &minus;2 Intelligence. These adjustments replace 
  the elf&rsquo;s ability score adjustments.
* &bull; Aquatic Subtype: An aquatic creature can breathe underwater. It 
  cannot also breathe air unless it also has the amphibious special 
  quality. An aquatic creature can hold its breath outside the water for 
  2 rounds per point of Constitution. After that, it begins to 
  suffocate.
* &bull; An aquatic elf has a swim speed of 40 feet. It has a +8 racial 
  bonus on any Swim check to perform some action or avoid a hazard. It 
  can always choose to take 10 on a Swim check, even if distracted or 
  endangered. It can use the run action while swimming, provided it 
  swims in a straight line.
* &bull; Superior Low-Light Vision: Aquatic elves can see four times as 
  far as a human in starlight, moonlight, torchlight, and similar 
  conditions of low illumination. This trait replaces the high elf&rsquo;s 
  low-light vision.
* &bull; No gills. Aquatic elves can breathe water, but do not have the 
  ability to survive out of water for a few hours like sea elves can.
* &bull; Bonus Language: Aquan. Aquatic races are familiar with the 
  language of water-based creatures.
* &bull; Favored Class: Fighter. This trait replaces the high elf&rsquo;s 
  favored class.

### Aquatic Gnomes

The following traits are in addition to the [gnome](standard-backgrounds.html#gnomes) traits, except where noted.

* &bull; Aquatic Subtype: An aquatic creature can breathe underwater. It 
  cannot also breathe air unless it also has the amphibious special 
  quality. An aquatic creature can hold its breath outside the water for 
  2 rounds per point of Constitution. After that, it begins to 
  suffocate.
* &bull; An aquatic gnome has a swim speed of 20 feet. It has a +8 
  racial bonus on any Swim check to perform some action or avoid a 
  hazard. It can always choose to take 10 on a Swim check, even if 
  distracted or endangered. It can use the run action while swimming, 
  provided it swims in a straight line.
* &bull; Well-Traveled: Aquatic gnomes receive a +2 racial bonus on 
  Sense Motive and Gather Information checks. They always know when 
  their welcome is wearing out.
* &bull; No +1 racial bonus on attack rolls against kobolds: Aquatic 
  gnomes do not come into combat with kobolds as frequently as their 
  land-bound cousins. They retain their bonus against goblinoids, 
  however.
* &bull; No racial bonus on Craft (alchemy) checks or Listen checks: 
  Aquatic gnomes' visual acuity has improved at the expense of their 
  other senses.
* &bull; Bonus Language: Aquan. Aquatic races are familiar with the 
  language of water-based creatures.

### Aquatic Goblins

The following traits are in addition to the [goblin](standard-backgrounds.html#goblins) traits, except where noted.

* &bull; &minus;2 Strength, +2 Constitution, &minus;2 Charisma. Aquatic 
  goblins are hardy creatures, but weaker than many races.
* &bull; Aquatic Subtype: An aquatic creature can breathe underwater. It 
  cannot also breathe air unless it also has the amphibious special
  quality. An aquatic creature can hold its breath outside the water for 
  2 rounds per point of Constitution. After that, it begins to
  suffocate.
* &bull; An aquatic goblin has a swim speed of 30 feet. It has a +8 
  racial bonus on any Swim check to perform some action or avoid a 
  hazard. It can always choose to take 10 on a Swim check, even if 
  distracted or endangered. It can use the run action while swimming, 
  provided it swims in a straight line.
* &bull; Thievery: Aquatic goblins get a +2 racial bonus on Disable 
  Device and Sleight of Hand checks.
* &bull; An aquatic goblin's racial bonus on Move Silently and Ride 
  checks is only +2. Aquatic goblins are known for riding Medium sharks.
* &bull; Bonus Language: Aquan. Aquatic races are familiar with the 
  language of water-based creatures.

### Aquatic Half-Elves

The following traits are in addition to the [half-elf](standard-backgrounds.html#half-elves) traits, except where noted.

* &bull; Aquatic Subtype: An aquatic creature can breathe underwater. It 
  cannot also breathe air unless it also has the amphibious special
  quality. An aquatic creature can hold its breath outside the water for  
  2 rounds per point of Constitution. After that, it begins to
  suffocate.
* &bull; An aquatic half-elf has a swim speed of 40 feet. It has a +8 
  racial bonus on any Swim check to perform some action or avoid a 
  hazard. It can always choose to take 10 on a Swim check, even if 
  distracted or endangered. It can use the run action while swimming, 
  provided it swims in a straight line.
* &bull; No racial bonus on Gather Information checks.
* &bull; +2 racial bonus on Survival checks: Aquatic half-elves spend a 
  great deal of time traveling between elf and human settlements on 
  their diplomacy work, and so are accustomed to surviving in the 
  undersea wilderness.
* &bull; Bonus Language: Aquan. Aquatic races are familiar with the 
  language of water-based creatures.

### Aquatic Half-Orcs

The following traits are in addition to the [half-orc](standard-backgrounds.html#half-orcs) traits, except where noted.

* &bull; Aquatic Subtype: An aquatic creature can breathe underwater. It 
  cannot also breathe air unless it also has the amphibious special
  quality. An aquatic creature can hold its breath outside the water for  
  2 rounds per point of Constitution. After that, it begins to
  suffocate.
* &bull; An aquatic half-orc has a swim speed of 30 feet. It has a +8 
  racial bonus on any Swim check to perform some action or avoid a 
  hazard. It can always choose to take 10 on a Swim check, even if 
  distracted or endangered. It can use the run action while swimming, 
  provided it swims in a straight line.
* &bull; Emissaries: As emissaries for their tribes, aquatic half-orcs 
  receive a +2 racial bonus on Diplomacy and Gather Information checks. 
  They aren't the best diplomats under the waves, but they're a far 
  sight better than their orc cousins.
* &bull; Bonus Language: Aquan. Aquatic races are familiar with the 
  language of water-based creatures.

### Aquatic Halflings

The following traits are in addition to the [halfling](standard-backgrounds.html#halflings) traits, except where noted.

* &bull; Aquatic Subtype: An aquatic creature can breathe underwater. It 
  cannot also breathe air unless it also has the amphibious special
  quality. An aquatic creature can hold its breath outside the water for 
  2 rounds per point of Constitution. After that, it begins to
  suffocate.
* &bull; An aquatic halfling has a swim speed of 20 feet. It has a +8 
  racial bonus on any Swim check to perform some action or avoid a 
  hazard. It can always choose to take 10 on a Swim check, even if 
  distracted or endangered. It can use the run action while swimming, 
  provided it swims in a straight line.
* &bull; Low-Light Vision: An aquatic halfling can see four times as far 
  as a nonaquatic human in starlight, moonlight, torchlight, and similar 
  conditions of poor illumination. He retains the ability to distinguish 
  color and detail under these conditions.
* &bull; Bonus Language: Aquan. Aquatic races are familiar with the 
  language of water-based creatures.

### Aquatic Humans

The following traits are in addition to the [human](standard-backgrounds.html#humans) traits, except where noted.

* &bull; Aquatic Subtype: An aquatic creature can breathe underwater. It 
  cannot also breathe air unless it also has the amphibious special 
  quality. An aquatic creature can hold its breath outside the water for 
  2 rounds per point of Constitution. After that, it begins to 
  suffocate.
* &bull; An aquatic human has a swim speed of 30 feet. It has a +8 
  racial bonus on any Swim check to perform some action or avoid a 
  hazard. It can always choose to take 10 on a Swim check, even if 
  distracted or endangered. It can use the run action while swimming, 
  provided it swims in a straight line.
* &bull; Low-Light Vision: An aquatic human can see four times as far as 
  a nonaquatic human in starlight, moonlight, torchlight, and similar 
  conditions of poor illumination. He retains the ability to distinguish 
  color and detail under these conditions.
* &bull; Bonus Language: Aquan. Aquatic races are familiar with the 
  language of water-based creatures.

### Aquatic Kobolds

The following traits are in addition to the [kobold](standard-backgrounds.html#kobolds) traits, except where noted.

* &bull; Aquatic Subtype: An aquatic creature can breathe underwater. It 
  cannot also breathe air unless it also has the amphibious special
  quality. An aquatic creature can hold its breath outside the water for 
  2 rounds per point of Constitution. After that, it begins to
  suffocate.
* &bull; An aquatic kobold has a swim speed of 40 feet. It has a +8 
  racial bonus on any Swim check to perform some action or avoid a 
  hazard. It can always choose to take 10 on a Swim check, even if 
  distracted or endangered. It can use the run action while swimming, 
  provided it swims in a straight line.
* &bull; Bonus Language: Aquan. Aquatic races are familiar with the 
  language of water-based creatures.

### Aquatic Orcs

The following traits are in addition to the [orc](standard-backgrounds.html#orcs) traits, except where noted.

* &bull; Aquatic Subtype: An aquatic creature can breathe underwater. It 
  cannot also breathe air unless it also has the amphibious special
  quality. An aquatic creature can hold its breath outside the water for 
  2 rounds per point of Constitution. After that, it begins to
  suffocate.
* &bull; An aquatic orc has a swim speed of 30 feet. It has a +8 racial 
  bonus on any Swim check to perform some action or avoid a hazard. It 
  can always choose to take 10 on a Swim check, even if distracted or 
  endangered. It can use the run action while swimming, provided it 
  swims in a straight line.
* &bull; Bonus Language: Aquan. Aquatic races are familiar with the 
  language of water-based creatures.


Arctic Races
------------

<table>
<caption>Table: Arctic Backgrounds</caption>
  <tr>
    <th>Background</th>
    <th>Ability Adjustments</th>
    <th>Favored Class</th>
  </tr>
  <tr>
    <td>Dwarf</td>
    <td>+2 Strength, +2 Constitution, −4 Dexterity, &minus;2 Charisma</td>
    <td>Fighter</td>
  </tr>
  <tr>
    <td>Elf</td>
    <td>&minus;2 Strength, +2 Dexterity</td>
    <td>Wizard</td>
  </tr>
  <tr>
    <td>Gnome</td>
    <td>+2 Constitution, &minus;2 Strength</td>
    <td>Bard</td>
  </tr>
  <tr>
    <td>Goblin</td>
    <td>&minus;2 Strength, +2 Constitution, &minus;2 Charisma</td>
    <td>Rogue</td>
  </tr>
  <tr>
    <td>Half-elf</td>
    <td>&mdash;</td>
    <td>Any</td>
  </tr>
  <tr>
    <td>Half-orc</td>
    <td>+2 Strength, &minus;2 Intelligence, &minus;2 Charisma</td>
    <td>Barbarian</td>
  </tr>
  <tr>
    <td>Halfling</td>
    <td>+2 Dexterity, &minus;2 Strength</td>
    <td>Ranger</td>
  </tr>
  <tr>
    <td>Kobold</td>
    <td>&minus;2 Strength, +2 Dexterity, &minus;2 Constitution, &minus;2 Wisdom</td>
    <td>Sorcerer</td>
  </tr>
  <tr>
    <td>Orc</td>
    <td>+4 Strength, &minus;2 Intelligence, &minus;2 Wisdom, &minus;2 Charisma</td>
    <td>Barbarian</td>
  </tr>
</table>

### Arctic Dwarves

In the icy wastes, dwarves are the masters of both ice and stone.
They build massive, elaborate defenses of ice and rock around
the mines where they dig for copper, silver, gold, and iron.

The following traits are in addition to the [dwarf](standard-backgrounds.html#dwarves) traits, except where noted.

* &bull; +2 Strength, +2 Constitution, &minus;4 Dexterity, &minus;2 
  Charisma. Arctic dwarves are tough enough to survive the brutal 
  environment of the arctic, but rely on strength and heavy armor rather 
  than agility.
* &bull; Cold Endurance: Members of arctic races have a +4 racial bonus 
  on Fortitude saves made to resist the effects of cold weather or 
  exposure.
* &bull; Icecunning: Arctic dwarves can apply their stonecunning 
  benefits to structures and natural features made of ice, as well as 
  those made of stone.
* &bull; +1 racial bonus on attack rolls against kobolds: Arctic dwarves 
  must defend against constant assaults by kobolds. This trait replaces 
  the dwarf's racial bonus on attack rolls against orcs.
* &bull; +2 racial bonus on Appraise and Craft checks related to items 
  made of ice.

### Arctic Elves

The arctic landscape is dotted here and there with perma-
nent fog banks caused by subterranean heat interacting with
snow and ice or warm springs steaming in the freezing air.
Hidden in these oases of warmth are the ancient settlements
of the elves.

The following traits are in addition to the [elf](standard-backgrounds.html#elves) traits, except where noted.

* &bull; &minus;2 Strength, +2 Dexterity: Arctic elves look down on 
  physical exertion, preferring quick action when necessary.
* &bull; Cold Endurance: Members of arctic races have a +4 racial bonus 
  on Fortitude saves made to resist the effects of cold weather or 
  exposure.
* &bull; +2 racial bonus on any one Craft skill (chosen by the character 
  at the time of creation).
* &bull; +2 racial bonus on Survival checks made in arctic environments: 
  Arctic elves are naturally adept at staying alive in the harsh 
  conditions of their homeland.
* &bull; No racial bonus on Search checks.
* &bull; No special ability to notice a secret or concealed door within 
  5 feet without actively looking for it.

### Arctic Gnomes

Arctic gnomes travel the ice and snow on cleverly constructed
sleds pulled by immense woolly mammoths. The animals serve
as transportation, a source of fibers for weaving, and walls
against the elements. The sleds are enclosed, often transporting
entire families.

The following traits are in addition to the [gnome](standard-backgrounds.html#gnomes) traits, except where noted.

* &bull; Cold Endurance: Members of arctic races have a +4 racial bonus 
  on Fortitude saves made to resist the effects of cold weather or 
  exposure.
* &bull; +2 racial bonus on Sense Motive checks: Arctic gnomes always 
  seem to know instinctively when their welcome is wearing out.
* &bull; No racial bonus on Craft (alchemy) checks.

### Arctic Goblins

The following traits are in addition to the [goblin](standard-backgrounds.html#goblins) traits, except where noted.

* &bull; &minus;2 Strength, +2 Constitution, &minus;2 Charisma. Arctic 
  goblins are hardy creatures, but weaker than many races.
* &bull; Cold Endurance: Members of arctic races have a +4 racial bonus 
  on Fortitude saves made to resist the effects of cold weather or 
  exposure.
* &bull; Low-Light Vision: An arctic goblin can see twice as far as a 
  human in starlight, moonlight, torchlight, and similar conditions of 
  poor illumination. She retains the ability to distinguish color and 
  detail under these conditions.
* &bull; No darkvision.
* &bull; +2 racial bonus on Bluff and Sleight of Hand checks: Arctic 
  goblins are natural thieves, snatching up anything that isn't nailed 
  down.
* &bull; No racial bonus on Move Silently checks: Arctic goblins aren't 
  as stealthy as their nonarctic cousins.

### Arctic Half-Elves

The following traits are in addition to the [half-elf](standard-backgrounds.html#half-elves) traits, except where noted.

* &bull; Cold Endurance: Members of arctic races have a +4 racial bonus 
  on Fortitude saves made to resist the effects of cold weather or 
  exposure.
* &bull; No racial bonus on Diplomacy checks: Their transient nature 
  prevents arctic half-elves from being as well trusted as their normal 
  counterparts.
* &bull; +2 racial bonus on Survival checks: Arctic half-elves spend a 
  great deal of time traveling between elf and human settlements, and 
  are accustomed to surviving in the wilderness.

### Arctic Half-Orcs

Many tribes of arctic orcs eke out a living on the warm slopes of volcanic mountains, herding elk and caribou. often their mutual dependence on sources of fresh water and meat brings them into contact with arctic-dwelling human barbarians.

The following traits are in addition to the [half-orc](standard-backgrounds.html#half-orcs) traits, except where noted.

* &bull; Cold Endurance: Members of arctic races have a +4 racial bonus 
  on Fortitude saves made to resist the effects of cold weather or 
  exposure.
* &bull; Low-Light Vision: An arctic half-orc can see twice as far as a 
  human in starlight, moonlight, torchlight, and similar conditions of 
  poor illumination. He retains the ability to distinguish color and 
  detail under these conditions.
* &bull; No darkvision.
* &bull; +2 racial bonus on Diplomacy checks: Arctic half-orcs often 
  serve as emissaries for the arctic orc tribes that spawned them.

### Arctic Halflings

Arctic halflings are hunters and fishermen, stalking walrus
and seal and putting to sea to fish, usually in the company of
larger races such as humans. They are a peaceful, spiritual people
whose rituals include preexpedition prayers for forgiveness
from the animals they hope to kill.

The following traits are in addition to the [halfling](standard-backgrounds.html#halflings) traits, except where noted.

* &bull; Cold Endurance: Members of arctic races have a +4 racial bonus 
  on Fortitude saves made to resist the effects of cold weather or 
  exposure.
* &bull; +2 racial bonus on Swim checks: Arctic halflings rarely swim by 
  choice, but are reasonably good at saving themselves from drowning.
* &bull; No racial bonus on attack rolls with slings. Arctic Halflings 
  do not particularly favor slings for hunting.
* &bull; Favored Class: Ranger. A multiclass arctic halfling's ranger 
  class does not count when determining whether she takes an experience 
  point penalty for multiclassing. This trait replaces the base 
  halfling's favored class.

### Arctic Kobolds

Arctic kobolds are seldom seen outside their mines. They dig
constantly, always expanding their subterranean territory
(which is often carefully constructed to tap into geothermal
vents for heat), and as a result are in constant conflict with arc-
tic dwarves. Arctic kobolds often hire mercenaries to fight for
them, hiding behind their carefully constructed traps and other
defenses and paying their defenders with the mineral wealth
they uncover while expanding their warrens.

The following traits are in addition to the [kobold](standard-backgrounds.html#kobolds) traits, except where noted.

* &bull; &minus;2 Strength, +2 Dexterity, &minus;2 Constitution, 
  &minus;2 Wisdom: Arctic kobolds are stronger than other kobolds, but 
  are weaker of will.
* &bull; Cold Endurance: Members of arctic races have a +4 racial bonus 
  on Fortitude saves made to resist the effects of cold weather or 
  exposure.

### Arctic Orcs

Arctic orcs are primitive herders, eking out a living on the
slopes of volcanic mountains and building their crude huts
near hot springs or steam vents. They herd woolly cattle,
caribou, and elk. As a result, they control large sources of
meat, water, and heat—all things coveted by others living in
or traveling through the arctic.

The following traits are in addition to the [orc](standard-backgrounds.html#orcs) traits, except where noted.

* &bull; Cold Endurance: Members of arctic races have a +4 racial bonus 
  on Fortitude saves made to resist the effects of cold weather or 
  exposure.
* &bull; Low-Light Vision: An arctic orc can see twice as far as a human 
  in starlight, moonlight, torchlight, and similar conditions of poor 
  illumination. He retains the ability to distinguish color and detail 
  under these conditions.
* &bull; No darkvision.
* &bull; No light sensitivity.
* &bull; +2 racial bonus on Handle Animal checks: Arctic orcs are 
  herders, and they rely on their skills with animals to keep their 
  tribes fed.


Desert Races
------------

<table>
<caption>Table: Desert Backgrounds</caption>
  <tr>
    <th>Background</th>
    <th>Ability Adjustments</th>
    <th>Favored Class</th>
  </tr>
  <tr>
    <td>Dwarf</td>
    <td>&minus;2 Dexterity, +2 Constitution</td>
    <td>Fighter</td>
  </tr>
  <tr>
    <td>Elf</td>
    <td>&minus;2 Strength, +2 Dexterity</td>
    <td>Wizard</td>
  </tr>
  <tr>
    <td>Gnome</td>
    <td>+2 Constitution, &minus;2 Strength</td>
    <td>Bard</td>
  </tr>
  <tr>
    <td>Goblin</td>
    <td>&minus;2 Strength, +2 Dexterity, &minus;2 Charisma</td>
    <td>Rogue</td>
  </tr>
  <tr>
    <td>Half-elf</td>
    <td>&mdash;</td>
    <td>Any</td>
  </tr>
  <tr>
    <td>Half-orc</td>
    <td>+2 Constitution, &minus;2 Intelligence</td>
    <td>Barbarian</td>
  </tr>
  <tr>
    <td>Halfling</td>
    <td>+2 Dexterity, &minus;2 Strength</td>
    <td>Rogue</td>
  </tr>
  <tr>
    <td>Kobold</td>
    <td>−4 Strength, +2 Dexterity, &minus;2 Wisdom</td>
    <td>Rogue</td>
  </tr>
  <tr>
    <td>Orc</td>
    <td>+4 Strength, &minus;2 Intelligence, &minus;2 Wisdom, &minus;2 Charisma</td>
    <td>Barbarian</td>
  </tr>
</table>

### Desert Dwarves

Because of their skill with stone and engineering, dwarves are acknowledged masters of locating water and digging wells. The importance of water to desert peoples gives the dwarves both great wealth and great political power.

The following traits are in addition to the [dwarf](standard-backgrounds.html#dwarves) traits, except where noted.

* &bull; &minus;2 Dexterity, +2 Constitution: Desert dwarves are tough 
  enough to survive the brutal environment of the desert, but they rely 
  more on cunning and fast talk than agility.
* &bull; Heat Endurance: Members of desert races gain a +4 racial bonus 
  on Fortitude saves made to resist the effects of hot weather.
* &bull; No stonecunning.
* &bull; +1 racial bonus on attack rolls against humanoids of the 
  reptilian subtype (including kobolds and lizardfolk) and dragons 
  (including half-dragons): Desert dwarves are trained in special combat 
  techniques that allow them to fight common enemies more effectively. 
  This bonus replaces the normal dwarf's bonus against orcs and 
  goblinoids.
* &bull; +4 dodge bonus to Armor Class against dragons: This replaces 
  the dwarf's dodge bonus to AC against giants. Dragons are a more 
  common threat than giants in desert climes, and desert dwarves have 
  adapted to meet this challenge.
* &bull; +2 racial bonus on Knowledge (architecture and engineering), 
  Knowledge (dungeoneering), and Profession (miner) checks: Desert 
  dwarves protect a great deal of information about what lives beneath 
  the desert and how to reach it.
* &bull; No racial bonus on Craft checks.

### Desert Elves

In the desert, elves follow nomadic lifestyles. They herd horses, cattle, and goats across the sands, sleeping during the day and working or traveling at night. Their wandering takes them many places and puts them in contact with many cultures. As a result, they are welcome everywhere for the news and exotic trade goods they carry. 

The following traits are in addition to the [elf](standard-backgrounds.html#elves) traits, except where noted.

* &bull; &minus;2 Strength, +2 Dexterity: Desert elves are physically 
  weaker than their normal cousins, but are also hardier, toughened by 
  the harsh reality of desert life.
* &bull; Heat Endurance: Members of desert races gain a +4 racial bonus 
  on Fortitude saves made to resist the effects of hot weather.
* &bull; Weapon Proficiency: Desert elves receive the Martial Weapon 
  Proficiency feats for the scimitar, rapier, and shortbow (including 
  composite shortbow) as bonus feats. Desert elves prefer the scimitar 
  to the longsword and the shortbow to the longbow, because they can use 
  shortbows while mounted.
* &bull; +2 racial bonus on Handle Animal and Ride checks: Desert elves 
  spend most of their lives riding and working with animals.
* &bull; No racial bonus on Listen checks.

### Desert Gnomes

Desert dwarves may dig and maintain wells, but desert gnomes pay for them. Desert gnomes establish and operate fortified trading posts around wells and oases, cultivating crops wherever possible and scouring the surrounding desert for the wealth of buried civilizations. Desert gnome outposts are rarely quiet, doing business both day and night.

The following traits are in addition to the [gnome](standard-backgrounds.html#gnomes) traits, except where noted.

* &bull; Heat Endurance: Members of desert races gain a +4 racial bonus 
  on Fortitude saves made to resist the effects of hot weather.
* &bull; +2 racial bonus on Bluff, Diplomacy, and Sense Motive checks: 
  Desert gnomes are skilled at haggling and negotiating.
* &bull; No racial bonus on Listen checks or Craft (alchemy) checks.

### Desert Goblins

The following traits are in addition to the [goblin](standard-backgrounds.html#goblins) traits, except where noted.

* &bull; Heat Endurance: Members of desert races gain a +4 racial bonus 
  on Fortitude saves made to resist the effects of hot weather.
* &bull; Low-Light Vision: A desert goblin can see twice as far as a 
  human in starlight, moonlight, torchlight, and similar conditions of 
  poor illumination. He retains the ability to distinguish color and 
  detail under these conditions.
* &bull; No darkvision.
* &bull; +2 racial bonus on Gather information checks: Desert goblins 
  soak up gossip like sponges when they visit towns or settlements.

### Desert Half-Elves

The following traits are in addition to the [half-elf](standard-backgrounds.html#half-elves) traits, except where noted.

* &bull; Heat Endurance: Members of desert races gain a +4 racial bonus 
  on Fortitude saves made to resist the effects of hot weather.
* &bull; +2 racial bonus on Sense Motive checks: A desert half-elfs 
  survival often relies on his ability to gauge the social situation.
* &bull; No racial bonus on Listen checks.

### Desert Half-Orcs

The following traits are in addition to the [half-orc](standard-backgrounds.html#half-orcs) traits, except where noted.

* &bull; +2 Constitution, &minus;2 Intelligence: The desert half-orc is 
  lean and hardy rather than muscular, but has adapted to be more 
  socially adept than a typical half-orc.
* &bull; Heat Endurance: Members of desert races gain a +4 racial bonus 
  on Fortitude saves made to resist the effects of hot weather.
* &bull; Low-Light Vision: A desert half-orc can see twice as far as a 
  human in starlight, moonlight, torchlight, and similar conditions of 
  poor illumination. He retains the ability to distinguish color and 
  detail under these conditions.
* &bull; No darkvision.
* &bull; Run: Desert half-orcs receive Run as a racial bonus feat. 
  Typically too poor to afford mounts, half-orcs learn from an early age 
  to get from place to place by running.

### Desert Halflings

Like gnomes, halflings are most likely to be active during the day. Most other races regard them somewhat like cats, in that they appear whether invited or not, work at what they want when they want, and disappear without prior notice. A desert gnome proverb observes that “Halflings in your town are a source of trouble, but at least they don't tolerate competition.”

The following traits are in addition to the [halfling](standard-backgrounds.html#halflings) traits, except where noted.

* &bull; Heat Endurance: Members of desert races gain a +4 racial bonus 
  on Fortitude saves made to resist the effects of hot weather.
* &bull; +2 racial bonus on Hide and Sleight of Hand checks: Desert 
  halflings are stealthy and nimble-fingered.
* &bull; No racial bonus on Climb and Jump checks.

### Desert Kobolds

The following traits are in addition to the [kobold](standard-backgrounds.html#kobolds) traits, except where noted.

* &bull; &minus;4 Strength, +2 Dexterity, &minus;2 Wisdom: Desert 
  kobolds are hardier than other kobolds, but are weak-willed.
* &bull; Heat Endurance: Members of desert races gain a +4 racial bonus 
  on Fortitude saves made to resist the effects of hot weather.
* &bull; No light sensitivity.
* &bull; +2 racial bonus on Survival checks.
* &bull; No racial bonus on Profession (mining) checks: Desert kobolds 
  don't mine.
* &bull; Favored Class: Rogue. A multiclass desert kobold's rogue class 
  does not count when determining whether he takes an experience point 
  penalty for multiclassing. This trait replaces the normal kobold's 
  favored class.

### Desert Orcs

The following traits are in addition to the [orc](standard-backgrounds.html#orcs) traits, except where noted.

* &bull; Heat Endurance: Members of desert races gain a +4 racial bonus 
  on Fortitude saves made to resist the effects of hot weather.
* &bull; Low-Light Vision: A desert orc can see twice as far as a human 
  in starlight, moonlight, torchlight, and similar conditions of poor 
  illumination. She retains the ability to distinguish color and detail 
  under these conditions.
* &bull; No darkvision.
* &bull; Endurance: Desert orcs gain Endurance as a racial bonus feat.


Jungle Races
------------

<table>
<caption>Table: Jungle Backgrounds</caption>
  <tr>
    <th>Background</th>
    <th>Ability Adjustments</th>
    <th>Favored Class</th>
  </tr>
  <tr>
    <td>Dwarf</td>
    <td>+2 Constitution, &minus;2 Charisma</td>
    <td>Ranger</td>
  </tr>
  <tr>
    <td>Elf</td>
    <td>+2 Dexterity, &minus;2 Constitution</td>
    <td>Wizard</td>
  </tr>
  <tr>
    <td>Gnome</td>
    <td>+2 Constitution, &minus;2 Strength</td>
    <td>Bard</td>
  </tr>
  <tr>
    <td>Goblin</td>
    <td>&minus;2 Strength, +2 Dexterity, &minus;2 Charisma</td>
    <td>Barbarian</td>
  </tr>
  <tr>
    <td>Half-elf</td>
    <td>&mdash;</td>
    <td>Any</td>
  </tr>
  <tr>
    <td>Half-orc</td>
    <td>+2 Strength, &minus;2 Intelligence, &minus;2 Charisma</td>
    <td>Barbarian</td>
  </tr>
  <tr>
    <td>Halfling</td>
    <td>+2 Dexterity, &minus;2 Strength</td>
    <td>Barbarian</td>
  </tr>
  <tr>
    <td>Kobold</td>
    <td>−4 Strength, +2 Dexterity, &minus;2 Intelligence</td>
    <td>Sorcerer</td>
  </tr>
  <tr>
    <td>Orc</td>
    <td>+4 Strength, &minus;2 Intelligence, &minus;2 Wisdom, &minus;2 Charisma</td>
    <td>Barbarian</td>
  </tr>
</table>

### Jungle Dwarves

Because of their height, dwarves can duck under vegetation that impedes humans—yet they are stockier than gnomes or halflings, and able to bull through what they can't avoid. Hardy to a fault, jungle dwarves are explorers, pathfinders, and traveling merchants. 

The following traits are in addition to the [dwarf](standard-backgrounds.html#dwarves) traits, except where noted.

* &bull; Low-Light Vision: A jungle dwarf can see twice as far as a 
  human in starlight, moonlight, torchlight, and similar conditions of 
  poor illumination. She retains the ability to distinguish color and 
  detail under these conditions.
* &bull; No darkvision.
* &bull; +2 bonus on Heal, Knowledge (nature), and Survival checks: 
  Jungle dwarves know a great deal about what lives in the jungle, and 
  are adept at finding it (or avoiding it, as the case may be).
* &bull; +2 bonus on Spot checks: Jungle dwarves have keen eyesight.
* &bull; No stonecunning: Jungle dwarves live aboveground.
* &bull; No racial bonus on Craft checks related to stone or metal 
  items.
* &bull; Favored Class: Ranger. A multiclass jungle dwarf's ranger class 
  does not count when determining whether she takes an XP penalty for 
  multiclassing. Jungle dwarves need both outdoor skills and combat 
  proficiency to survive. This trait replaces the normal dwarf's favored 
  class.

### Jungle Elves

Jungle elves are somewhat more primal than their baseline counterparts. In many jungles, ancient elven cities have been swallowed up by the voracious encroachment of trees, vines, and animals, leaving just a shadowy remnant of the population to remember the former glories of their lost civilization. 

The following traits are in addition to the [elf](standard-backgrounds.html#elves) traits, except where noted.

* &bull; Weapon Proficiency: Jungle elves receive the Martial Weapon 
  Proficiency feats for the handaxe, rapier, shortsword, and shortbow 
  (including composite shortbow) as bonus feats. Proficiency with a 
  rapier is a vestige of past glories and an honored tradition among 
  jungle elves. The other weapons are useful tools for survival in their 
  environment.
* &bull; +2 racial bonus on Knowledge (history) checks: Jungle elves 
  pride themselves on being the keepers of much lore that has been 
  forgotten to others.
* &bull; No ability to notice secret or concealed doors by passing 
  within 5 feet.

### Jungle Gnomes

Jungle gnomes live in and along the mighty rivers that wind through the jungles. A jungle gnome town is often a collection of boats tied together across a river, with channels to allow passing vessels through (after they pay a toll, of course). Jungle gnomes are also seen traveling the rivers, trading what they can find to those they meet along the way. 

The following traits are in addition to the [gnome](standard-backgrounds.html#gnomes) traits, except where noted.

* &bull; No racial bonus on attack rolls against kobolds.
* &bull; No dodge bonus to Armor Class against giants.
* &bull; +2 racial bonus on Climb and Swim checks: Jungle gnomes live 
  their lives clambering in and out of boats, and in and out of the 
  water.
* &bull; +2 racial bonus on Craft (shipbuilding) checks: A little lumber 
  and a few simple tools make for a happy and industrious jungle gnome.
* &bull; +2 racial bonus on Use Rope checks: Jungle gnomes consider rope 
  the most useful tool on their boats.
* &bull; No racial bonus on Listen or Craft (alchemy) checks.

### Jungle Goblins

The following traits are in addition to the [goblin](standard-backgrounds.html#goblins) traits, except where noted.

* &bull; A jungle goblin has a climb speed of 20 feet. Jungle goblins 
  are natural climbers, able to scramble up trees with ease. A jungle 
  goblin has a +8 racial bonus on all Climb checks. It must make a Climb 
  check to climb any wall or slope with a DC of more than 0, but it 
  always can choose to take 10, even if rushed or threatened while 
  climbing. If it chooses an accelerated climb, it moves at double its 
  climb speed and makes a single Climb check at a -5 penalty. It cannot 
  run while climbing. It retains its Dexterity bonus to Armor Class (if 
  any) while climbing, and opponents get no special bonus on their 
  attacks against a climbing jungle goblin. Also, jungle goblins add 
  their Dexterity modifier to Climb checks instead of their Strength 
  modifier.
* &bull; Low-Light Vision: A jungle goblin can see twice as far as a 
  human in starlight, moonlight, torchlight, and similar conditions of 
  poor illumination. He retains the ability to distinguish color and 
  detail under these conditions.
* &bull; No darkvision.
* &bull; +4 racial bonus on Jump checks: Jungle goblins are accomplished 
  leapers.
* &bull; No racial bonus on Hide and Move Silently checks.
* &bull; Favored Class: Barbarian. A multiclass jungle goblin's 
  barbarian class does not count when determining whether he takes an 
  experience point penalty for multiclassing. This trait replaces the 
  normal goblins favored class.

### Jungle Half-Elves

The following traits are in addition to the [half-elf](standard-backgrounds.html#half-elves) traits, except where noted.

* &bull; +2 racial bonus on Bluff and Sense Motive checks: Jungle 
  half-elves must maintain constant awareness of their place within 
  society.
* &bull; No racial bonus on Diplomacy and Gather Information checks.

### Jungle Half-Orcs

The following traits are in addition to the [half-orc](standard-backgrounds.html#half-orcs) traits, except where noted.

* &bull; Low-Light Vision: A jungle half-orc can see twice as far as a 
  human in starlight, moonlight, torchlight, and similar conditions of 
  poor illumination. He retains the ability to distinguish color and 
  detail under these conditions.
* &bull; No darkvision.
* &bull; +2 racial bonus on Climb and Jump checks: Jungle half-orcs move 
  almost as easily through the trees oftheir jungle home as they do on the 
  ground.

### Jungle Halflings

Jungle halflings live a much more settled life than their normal counterparts do. They build villages along rivers and burn clearings in the jungle to plant their crops. In addition, they gather the most abundant food source in their environment, fish, and hunt with poison-tipped weapons.

The following traits are in addition to the [halfling](standard-backgrounds.html#halflings) traits, except where noted.

* &bull; Weapon Proficiency: Jungle Halflings receive the Martial Weapon 
  Proficiency feats for the throwing axe, handaxe, and shortbow 
  (including composite shortbow) as bonus feats. For jungle halflings, 
  the axe is both a practical tool and a ceremonial item. They grow up 
  using the other weapons for hunting and fishing.
* &bull; Poison Use: jungle halflings use poison extensively when 
  hunting, and as a result never poison themselves when applying poison 
  to a weapon or when using a poisoned weapon in combat. Jungle 
  halflings make extensive use of poisons harvested from vermin, 
  particularly those that damage Dexterity scores.
* &bull; No racial bonus on saving throws. Jungle Halflings have no 
  unusual ability to avoid mishaps.
* &bull; +2 racial bonus on Fortitude saying throws against poison: 
  Jungle Halflings have adapted a resistance to their own favorite 
  method of bringing down prey.
* &bull; No morale bonus on saving throws against fear: Jungle halflings 
  may be cunning, but they are no braver than other races.
* &bull; No racial bonus on attack rolls with slings.
* &bull; Favored Class: Barbarian. A multiclass halfling's barbarian 
  class does not count when determining whether she takes an experience 
  point penalty for multiclassing. Jungle halflings live much closer to 
  nature, and must rely on cunning, resourcefulness, persistence, 
  courage, and mercilessness to survive. This trait replaces the normal 
  halfling's favored class.

### Jungle Kobolds

Clouded mountain peaks, many of which have religious or ceremonial importance to those who live below, tower over jungle canopies. Few know of the ancient ruined cities near the rocky summits, concealed amid the clouds. Dwelling in such ancient ruins are the jungle kobolds.

Those few explorers who have reached the cities report that their jungle kobold residents claim to have built them, pointing to reptilian and draconic images on the walls and in the architecture as evidence. The explorers report, however, that the cities were obviously built by and for people larger than the little reptilian humanoids. Jungle kobolds lack the cowardice common to other kobolds, thanks to the isolation of their mountain peak homes. By all reports they show a calmer, more generous side than other kobolds do. 

The following traits are in addition to the [kobold](standard-backgrounds.html#kobolds) traits, except where noted.

* &bull; &minus;4 Strength, +2 Dexterity, &minus;2 Intelligence: Life at 
  high altitude has toughened jungle kobolds, but their isolation has 
  hindered their intellectual growth.
* &bull; Altitude Adaptation: Jungle kobolds are adapted to life at high 
  altitudes, and thus don't suffer altitude sickness. They are always 
  considered acclimated to high altitudes.
* &bull; +2 racial bonus on Climb checks: Jungle kobolds add their 
  Dexterity modifier to Climb checks instead of their Strength modifier.
* &bull; No racial bonus on Craft (trapmaking) or Profession (miner) 
  checks.

### Jungle Orcs

The following traits are in addition to the [orc](standard-backgrounds.html#orcs) traits, except where noted.

* &bull; Low-Light Vision: A jungle orc can see twice as far as a human 
  in starlight, moonlight, torchlight, and similar conditions of poor 
  illumination. He retains the ability to distinguish color and detail 
  under these conditions.
* &bull; No darkvision.
* &bull; +2 racial bonus on Heal and Survival checks: Jungle orcs must 
  be self-sufficient in order to survive their environment.


Legal Information
=================

OPEN GAME LICENSE Version 1.0a
 
The following text is the property of Wizards of the Coast, Inc. and is Copyright 2000 Wizards of the Coast, Inc ("Wizards"). All Rights Reserved.
 
1. Definitions: (a)"Contributors" means the copyright and/or trademark owners who have contributed Open Game Content; (b)"Derivative Material" means copyrighted material including derivative works and translations (including into other computer languages), potation, modification, correction, addition, extension, upgrade, improvement, compilation, abridgment or other form in which an existing work may be recast, transformed or adapted; (c) "Distribute" means to reproduce, license, rent, lease, sell, broadcast, publicly display, transmit or otherwise distribute; (d)"Open Game Content" means the game mechanic and includes the methods, procedures, processes and routines to the extent such content does not embody the Product Identity and is an enhancement over the prior art and any additional content clearly identified as Open Game Content by the Contributor, and means any work covered by this License, including translations and derivative works under copyright law, but specifically excludes Product Identity. (e) "Product Identity" means product and product line names, logos and identifying marks including trade dress; artifacts; creatures characters; stories, storylines, plots, thematic elements, dialogue, incidents, language, artwork, symbols, designs, depictions, likenesses, formats, poses, concepts, themes and graphic, photographic and other visual or audio representations; names and descriptions of characters, spells, enchantments, personalities, teams, personas, likenesses and special abilities; places, locations, environments, creatures, equipment, magical or supernatural abilities or effects, logos, symbols, or graphic designs; and any other trademark or registered trademark clearly identified as Product identity by the owner of the Product Identity, and which specifically excludes the Open Game Content; (f) "Trademark" means the logos, names, mark, sign, motto, designs that are used by a Contributor to identify itself or its products or the associated products contributed to the Open Game License by the Contributor (g) "Use", "Used" or "Using" means to use, Distribute, copy, edit, format, modify, translate and otherwise create Derivative Material of Open Game Content. (h) "You" or "Your" means the licensee in terms of this agreement.
 
2. The License: This License applies to any Open Game Content that contains a notice indicating that the Open Game Content may only be Used under and in terms of this License. You must affix such a notice to any Open Game Content that you Use. No terms may be added to or subtracted from this License except as described by the License itself. No other terms or conditions may be applied to any Open Game Content distributed using this License.
 
3. Offer and Acceptance: By Using the Open Game Content You indicate Your acceptance of the terms of this License.
 
4. Grant and Consideration: In consideration for agreeing to use this License, the Contributors grant You a perpetual, worldwide, royalty-free, non-exclusive license with the exact terms of this License to Use, the Open Game Content.
 
5. Representation of Authority to Contribute: If You are contributing original material as Open Game Content, You represent that Your Contributions are Your original creation and/or You have sufficient rights to grant the rights conveyed by this License.
 
6. Notice of License Copyright: You must update the COPYRIGHT NOTICE portion of this License to include the exact text of the COPYRIGHT NOTICE of any Open Game Content You are copying, modifying or distributing, and You must add the title, the copyright date, and the copyright holder's name to the COPYRIGHT NOTICE of any original Open Game Content you Distribute.
 
7. Use of Product Identity: You agree not to Use any Product Identity, including as an indication as to compatibility, except as expressly licensed in another, independent Agreement with the owner of each element of that Product Identity. You agree not to indicate compatibility or co-adaptability with any Trademark or Registered Trademark in conjunction with a work containing Open Game Content except as expressly licensed in another, independent Agreement with the owner of such Trademark or Registered Trademark. The use of any Product Identity in Open Game Content does not constitute a challenge to the ownership of that Product Identity. The owner of any Product Identity used in Open Game Content shall retain all rights, title and interest in and to that Product Identity.
 
8. Identification: If you distribute Open Game Content You must clearly indicate which portions of the work that you are distributing are Open Game Content.
 
9. Updating the License: Wizards or its designated Agents may publish updated versions of this License. You may use any authorized version of this License to copy, modify and distribute any Open Game Content originally distributed under any version of this License.
 
10. Copy of this License: You MUST include a copy of this License with every copy of the Open Game Content You Distribute.
 
11. Use of Contributor Credits: You may not market or advertise the Open Game Content using the name of any Contributor unless You have written permission from the Contributor to do so.
 
12. Inability to Comply: If it is impossible for You to comply with any of the terms of this License with respect to some or all of the Open Game Content due to statute, judicial order, or governmental regulation then You may not Use any Open Game Material so affected.
 
13. Termination: This License will terminate automatically if You fail to comply with all terms herein and fail to cure such breach within 30 days of becoming aware of the breach. All sublicenses shall survive the termination of this License.
 
14. Reformation: If any provision of this License is held to be unenforceable, such provision shall be reformed only to the extent necessary to make it enforceable.
 
15. COPYRIGHT NOTICE
Open Game License v 1.0a Copyright 2000, Wizards of the Coast, Inc.
 
System Reference Document Copyright 2000&minus;2003, Wizards of the Coast, Inc.; Authors Jonathan Tweet, Monte Cook, Skip Williams, Rich Baker, Andy Collins, David Noonan, Rich Redman, Bruce R. Cordell, John D. Rateliff, Thomas Reid, James Wyatt, based on original material by E. Gary Gygax and Dave Arneson.

Modern System Reference Document Copyright 2002, Wizards of the Coast, Inc; Authors Bill Slavicsek, Jeff Grubb, Rich Redman, Charles Ryan, based on material by Johnathan Tweet, Monte Cook, Skip Williams, Richard Baker, Peter Adkison, Bruce R. Cordell, John Tynes, Andy Collins, and JD Wiker.

_Swords of Our Fathers_ Copyright 2003, The Game Mechanics.

_Mutants & Masterminds_ Copyright 2002, Green Ronin Publishing.

_Unearthed Arcana_ Copyright 2004, Wizards of the Coast, Inc; Andy Collins, Jesse Decker, David Noonan, Rich Redman.
 
END OF LICENSE
