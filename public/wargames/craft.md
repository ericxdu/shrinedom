Fighter Craft
-------------

League fighter craft all have designations starting with the "S-" prefix, while all Navy fighter craft have designations beginning with the "T-" prefix. Faction craft have the same designations as League craft, but with a "F-" prefix replacing the "S-" prefix. Every fighter craft has a colored blinking strobe light which makes it easy to spot and identify from a distance.

### Scout

- F-5 Vampire
- T-9 Tornado

Scount craft have a cyan FOF strobe.

### Interceptor

- F-4 Dark Angel
- T-45 Thunderchild

Interceptor craft have a red FOF strobe.

### Strike

- F-9 Hydra
- T-5 Avalanche

Strike craft have a blue FOF strobe.

### Bomber

- F-7 Chimera
- T-29 Stormlord

Bomber craft have a violet FOF strobe.

### Heavy Support

- F-11 Demon
- T-57 Blizzard

Heavy support craft have a yellow FOF strobe.
