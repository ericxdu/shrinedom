Tabletop Vehicle Mechanics
==========================

Vehicle Stats and Systems
-------------------------

### Installation

Capabilities of each vehicle are associated with it usually by a record sheet like a Ship Systems Display.

### Operation and Destruction



Range and Movement
------------------

### Measurement Units

Distance is measured by ruler or measuring tape and expressed in _measurement units_ or MU, often indicated by the double-prime glyph &Prime;.

### Hexagonal Tiles

A more precise measurement system utilizes a grid of hexagons across the playing surface in a pattern called tessellation. This system is more user-friendly and resembles a board game, but accommodation must be made for vectors that don't align with the grid (for instance by expressing a vector in two directions).

Space Wargames
--------------

Some of the most fascinating tabletop games are war simulators like Full 
Thrust and Dirtside. I'm developing my own space simulation rulesets and 
worlds.

- [Star War](starwar.html)
- [Full Thrust Link Archive](fullthrust.html)

External Links
--------------

- [Attack Vector](http://web.archive.org/web/20010613071537/www.io.com/~desantom/AttVec/ATTVEC.HTM)
- [Battlestar Galactica Technical Manual](http://www.tecr.com/galactica/)
- [Star Trek Designs](http://homegame.org/siefert/uftwwwp/files/st-ft.htm)
- [The Unofficial Full Thrust Blog](https://uftb.blogspot.com/)
- [Weapons & Defences Archive](http://nift.firedrake.org/Weap-Def_Archive.htm)
- [WarpWar Rules](http://www.contrib.andrew.cmu.edu/usr/gc00/reviews/warpwar.html)
