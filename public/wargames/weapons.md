Ship-to-Ship
------------

Fleet craft are equipped with several laser weapons for anti-fighter defense, and usually at least one large weapon for anti-fleet assault. The larger weapon sounds like an electrical discharge and looks like a very large beam of light.

League fleet lasers tend to be colored green, while Navy lasers are blue and nearly white.


Fleet Weapons
-------------

### Defensive Lasers

Orange flashes mark where these heavy laser weapons are projected from. They are more powerful than the lasers mounted on fighters and are effective against shield and hull. They begin to activate whenever an enemy fighter craft is in range of a fleet craft. It is difficult to pinpoint where these weapons are mounted and how many are equipped on a ship, but there are enough to tear through a fighter craft that doesn't take evasive action.

### Offensive Lasers

This large weapon is mounted on most military capital ships, usually as a single offensive weapon. They are designed to take down enemy fleet craft, and when two fleet craft engage you'll hear them exchanging fire every few seconds with these weapons. It sounds like an electrical discharge and looks like a continuous beam of light. As a fighter pilot, do not come to a stop anywhere near an enemy fleet ship or they will be able to target you with this powerful weapon. Keep moving and they won't be able to fire it on you. It is difficult to tell where this weapon is mounted.
