Full Thrust Official and Fan Works
==================================

Also known as "Full Thrust 2.5", referring to the original 2nd edition rulebook "Full Thrust" and additions and revisions to the rules in "More Thrust" and Fleet Books 1 and 2.

- [Weapons and Defences Archive](https://nift.firedrake.org/Weap-Def_Archive.htm)

Tuffleyverse
------------

A future history detailing the factionalization of Earth and space forces starting in 1992, presented as an optional background for Full Thrust, Dirtside, and other GZG products.

- [1: Full Thrust 2nd Ed](http://downloads.groundzerogames.co.uk/FullThrust.pdf)[archived](https://web.archive.org/web/20150317055714/http://downloads.groundzerogames.co.uk/FullThrust.pdf)
  + Contains the first set of refined rules for the system. Establishes 
turn order, movement rules, "beam" damage, screen and point defense, 
optional systems, fighter rules, and a comprehensive set of optional 
rules to cover various scenarios. Also introduced a couple of one-off 
scenarios and an entire campaign system, as well as a detailed future 
history.
- [2: More Thrust](http://downloads.groundzerogames.co.uk/mt.pdf)[archived](https://web.archive.org/web/20200925120950/http://downloads.groundzerogames.co.uk/mt.pdf)
  + Improved game balance with better turn order, better costing, more 
nuance for fighters and defensive systems, morale and surrender rules, 
and more new optional systems. Some new scenarios and the introduction 
of alien fleets.
- [3: Fleet Book 1](http://downloads.groundzerogames.co.uk/FB1Full.pdf)[archived](https://web.archive.org/web/20150317100855/http://downloads.groundzerogames.co.uk/FB1Full.pdf)
  + Completely overhauled shipbuilding system, keeping all of the 
previously introduced weapons and ship systems but with refined rules 
and more flexible and balanced costing. Refinements to the turn order 
and mandating previously optional rules. Prefabricated ships from 
various factions.
- [4: Fleet Book 2](http://downloads.groundzerogames.co.uk/FB2Full.pdf)[archived](https://web.archive.org/web/20150317033929/http://downloads.groundzerogames.co.uk/FB2Full.pdf)
  + Continuation of the previous book focusing on alien factions and 
  ships.
