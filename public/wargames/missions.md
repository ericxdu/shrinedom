Colony Wars takes place in the 45th(?) century.



Gallonigher System
==================

Previously content to spread its forces thin across the star systems, the Navy has begun to target Gallonigher, the home system of the League of Free Worlds.

Act 1: A New Threat
-------------------

In this act, the Navy engages in covert military operations against civilian targets in advance of a full military invasion of Gallonigher.

### Initial Duty

"High speed capabilities are imperative, therefore the S-5 Vampire has been assigned to this mission."

__Location:__ Launch from Midas Starport and dock upon success. Open jump gate on failure. A dim planet with bright rings is visible beside a small yellow star.

__Objectives:__ Protect Midas Starport. Destroy enemy fighter craft.

__Fleet Craft:__ Cargo Trans 89. Civil Trans 143. Cargo Trans 314. Civil Trans 476. Midas Starport x 1.

__Fighter Craft:__ T-9 Tornado x 3.

__Success or Failure:__ Defending the civilian convoy.

### Defending the Civilian Convoy

"This mission carries a heavy flak potential. T.I.D.S. is commissioning the S-4 Dark Angel due to its defensive capabilities."

__Location:__ Travel to Acheron and open jump gate on conclusion. Acheron is visible nearby with its dark surface and bright green phosphorescent lakes. Evanstar is visible in the middle distance.

There is a convoy from from Midas en route to a classified destination in Acheron zone.

__Objectives:__ Protect civilian transporters. Destroy enemy fighter craft.

__Fleet Craft:__ Civilian Transporter 1. Civilian Transporter 2. Civilian Transporter 3.

__Fighter Craft:__ S-4 Dark Angel x 1 (friendly). T-5 Avalanche x 3.

__Additional Craft:__ T-29 Stormlord x 3 (opening jumpgate).

__Failure:__ Training Missions.

__Success:__ The Navy threatens Gallonigher.

### The Navy Threatens Gallonigher

"This mission carries a heavy flak potential. T.I.D.S. is commissioning the S-4 Dark Angel due to its defensive capabilities."

Loading Screen: Travel to Proteus and open jump gate upon completion. A very dark planet is visible in front of a red dust cloud near a medium yellow sun.

__Objectives:__ Protect prison ship. Destroy all enemy craft.

__Fleet Craft:__ Prison Ship 21.

__Fighter Craft:__ S-4 Dark Angel x 1. T-9 Tornado x 2.

__Additional Craft:__ Navy Frigate x 1 (uncloaking).

__Failure:__ Training Missions.

__Success:__ Battling for the platform.


Act 2: The Terrified Planets
----------------------------

In this act the Navy attempts a full military invasion of Gallonigher while the League attempts to control an enemy battle platform to prevent the assault. This entire act takes place in near Galatea, a large orange gas planet with orange rings.

### Battling for the Platform

"Ship assignment analysis indicates that an S-9 Hydra will be required for this mission."

__Location:__ 

__Objectives:__ Disable Navy battle platform sensor within time limit.

__Fleet Craft:__ League Frigate x 1. Navy Battle Platform x 1.

__Fighter Craft:__ S-9 Hydra x 1. T-45 Thunderchild x 2.

__Additional Craft:__ T-45 Thunderchild x 2 (launching).

__Failure:__ Regrouping for the platform assault.

__Success:__ Attempting to secure Gallonigher.

### Regrouping for the Platform Assault

> Ship assignment analysis indicates that an S-9 Hydra will be required for this mission.

__Location:__ Travel to Galatea warp hole and open jump gate on completion.

__Objectives:__ Disable Navy Battle Platform. Protect at least one League APC craft.

__Fleet Craft:__ Frigate x 1. Navy Battle Platform x 1.

__Fighter Craft:__ S-9 Hydra x 1.

__Additional Craft:__ T-9 Tornado x 1 (opening jumpgate). T-45 Thunderchild x 4 (opening jumpgate). League APC x 3 (launching).

__Respawning Craft:__ T-9 Tornado x 1 (opening jumpgate).

__Success:__ Closing the warp hole.

__Failure:__ Saving the battle platform.

### Attempting to Secure Gallonigher

"Ship assignment analysis indicates that an S-9 Hydra will be required for this mission."

__Location:__ Travel to Galatea warp hole and open jump gate on conclusion.

__Objectives:__ Protect at least one League APC craft.

__Fighter Craft:__ S-9 Hydra x 1 (friendly). T-45 Thunderchild x 2.

__Fleet Craft:__ Navy Battle Platform x 1.

__Additional Craft:__ League Frigate x 1 (uncloaking). League APC x 3 (launching). T-45 Thunderchild x 4 (opening jumpgate).

__Respawning Craft:__ T-9 Tornado x 1 (opening jumpgate).

__Success:__ Closing the warp hole.

__Failure:__ Saving the battle platform.

### Saving the battle platform

"This mission carries a heavy flak potential. T.I.D.S. is commissioning the S-4 Dark Angel due to its defensive capabilities."

__Location:__ Launch  and dock with battle platform on success.

__Objectives:__ Protect War Sphere. Destroy Navy Frigate. Destroy all enemy fighter craft.

__Fighter Craft:__ S-4 Dark Angel x 1.

__Fleet Craft:__ War Sphere x 1. League Frigate x 1.

__Additional Craft:__ T-29 Stormlord x 2 (opening jumpgate). Navy Frigate x 1 (uncloaking). T-5 Avalanche x 2 (launching).

__Failure:__ The Navy turns on Diomedes.

__Success:__ Closing the warp hole.

### Closing the Warp Hole

"Extensive heavy support capabilities are required; the S-11 Demon has been assigned."

__Objectives:__ Destroy all enemy craft.

__Fighter Craft:__ S-11 Demon x 1.

__Fleet Craft:__ League Battle Platform x 1.

__Additional Craft:__ T-29 Stormlord x 3 (entering sector via warphole). Navy Cruiser x 1 (entering sector via warphole). T-45 Thunderchild x 2 (launching). Navy Titan x 1 (entering sector via warphole). T-5 Avalanche x 2 (opening jumpgate).

__Success:__ The league enters Draco.

__Failure:__ The Navy turns on Diomedes.


Diomedes System
===============

Prolonged defense against invasion in Gallonigher has left a neighboring star system open to attack, and the Navy redirects its war operations to Diomedes.


Act 3: Wasted And Drained
-------------------------

In this act the Navy intends to destroy the League's allies in Diomedes, forcing the League to stretch their battered resources and defend the system.

### The Navy turns on Diomedes

"High speed capabilities are imperative, therefore the S-5 Vampire has been assigned to this mission."

__Fleet Craft:__ League Battle Platform x 1. Navy Siren x 4.

__Objectives:__ Destroy all Navy Siren weapons. Protect League Frigate. Destroy all enemy fighter craft.

__Additional Craft:__ League Frigate. T-9 Tornado x 2. T-5 Avalanche x 2. T-9 Tornado x 2.

__Success:__ A bomb in the system.

__Failure:__ The threatened frigate.

### The Threatened Frigate

"This mission carries a heavy flak potential. T.I.D.S. is commissioning the S-4 Dark Angel due to its defensive capabilities."

__Objectives:__ Destroy Navy Siren weapons. Destroy Navy fighter craft. Protect League Frigate.

__Fleet Craft:__ League Frigate x 1. Navy Siren x 2.

__Additional Craft:__ Navy Frigate x 2. T-29 Stormlord x 2. T-29 Stormlord x 2.

__Failure:__ The Judgement.

__Success:__ A bomb in the system.

### A Bomb in the System

"Ship assignment analysis indicates that an S-9 Hydra will be required for this mission."

__Objectives:__ Destroy all Navy APC craft.

__Fleet Craft:__ Navy Destroyer x 1. Navy Frigate x 1.

__Additional Craft:__ Navy APC x 3. T-9 Tornado x 2. T-9 Tornado x 2. T-9 Tornado x 2.

__Failure:__ The Judgement.

__Success:__ Raiding Diomedes.


Act 4: All Or Nothing
---------------------

### Raiding Diomedes

"High speed capabilities are imperative, therefore the S-5 Vampire has been assigned to this mission."



### Diomedes under Threat of Darkness

### Holding Off the Enemy


Draco System
============

After successfully repelling the Navy invasion of Gallonigher, the League consolidates resources and prepares to strike back at a Navy-aligned star system.

Draco is a binary star system with the red sun Helios at its center and its violet counterpart Hecate orbiting outside of all the planets. Both of these stars are visible in every mission(?), but sometimes one is much larger than the other, suggesting some missions take place near the outer orbit.

Act 5: A Baptism By Fire
------------------------

### The League Enters Draco

"Ship assignment analysis indicates that an S-9 Hydra will be required for this mission."

__Location:__ Travel to Hecate and open jumpgate on completion.

__Objectives:__ Destroy all Navy tanker craft.

__Fighter Craft:__ S-9 Hydra x 1 (friendly). T-9 Tornado x 1. T-45 Thunderchild x 2.

__Fleet Craft:__ Navy Space Port x 1. N Tanker1. N Tanker2. N Tanker3.

__Additional Craft:__ T-9 Tornado x 4 (opening jumpgate).

__Success:__ Solitary Duty.

__Failure:__ Targeting Navy Supplies.

### Targeting Navy Supplies

"The heavy payload necessary for this mission requires the use of the S-7 Chimera."

__Location:__ Travel to ???? and open jumpgate on completion. A large violet star and a small red star shine nearby.

__Objectives:__ Destroy Navy starport.

__Fighter Craft:__ S-7 Chimera x 1 (friendly). T-9 Tornado x 2.

__Fleet Craft:__ Navy Space Port x 1.

__Additional Craft:__ Navy Cruiser x 1 (uncloaking). T-45 Thunderchild x 1 (launching).

__Failure:__ The Navy Retaliates.

__Success:__ Navy intelligence threatens the League.

### Solitary Duty

"The heavy payload necessary for this mission requires the use of the S-7 Chimera."

__Location:__ Near a dark rocky planet. Red and violet twin suns visible.

__Objectives:__ Destroy Navy Mining Station.

__Fighter Craft:__ T-45 Thunderchild x 2.

__Fleet Craft:__ Navy Mining Station x 1.

__Success:__ Navy intelligence threatens the League.

__Failure:__ The Navy Retailiates.

### The Navy Retaliates

"This mission carries a heavy flak potential. T.I.D.S. is commissioning the S-4 Dark Angel due to its defensive capabilities."

__Location:__ Travel to a location between Selene and Eurydice. A dark, rocky planet is visible as well as a small red star and a small violet star.

__Objectives:__ Destroy Navy Siren weapons. Protect League Frigate.

__Fleet Craft:__ League Frigate x 1. Navy Frigate x 1. Navy Siren x 6.

__Additional Craft:__ T-29 Stormlord x 2 (launching). Navy Frigate x 1 (uncloaking). T-29 Stormlord x 2 (launching).

__Failure:__ A damaged cruiser attempts to dock.

__Success:__ Navy intelligence threatens the League?

### Navy Intelligence Threatens the League

"Extensive heavy support capabilities are required; the S-11 Demon has been assigned."

__Location:__ Travel to Selene and open jump gate on success. Dock with League cruiser on failure. A white planet with a violet tint, dark markings, and bright green rings is visible nearby.

__Objectives:__ Disable and grapple Navy comms craft to League Cruiser before time limit.

__Fighter Craft:__ S-9 Hydra (friendly). T-45 Thunderchild x 3.

__Fleet Craft:__ N Comms x 1.

__Additional Craft:__ L Cruiser x 1 (uncloaking). Navy Frigate x 1 (uncloaking).

__Respawning Craft:__ T-45 Thunderchild x 1 (launching).

__Success:__ The League targets enemy intelligence.

__Failure:__ A damaged cruiser attempts to dock.


Act 6: Into The Enemies Heart
-----------------------------

### A Damaged Cruiser Attempts to Dock

"Ship assignment analysis indicates that an S-9 Hydra will be required for this mission."

__Location:__ Dock with platform on failure. A white planet with dark spots and green rings is visible, as well as a small red star and a small violet star.

__Objectives:__ Protect League cruiser.

__Fighter Craft:__ S-9 Hydra x 1 (friendly).

__Fleet Craft:__ League Battle Platform x 1. L Cruiser x 1.

__Additional Craft:__ T-9 Tornado x 1 (opening jumpgate). Navy Frigate x 1 (uncloaking). T-29 Stormlord x 2 (launching). T-45 Thunderchild x ?.

__Success:__ Medical units under attack.

__Failure:__ Attacking a Navy frigate.

### Attacking a Navy Frigate

"The heavy payload necessary for this mission requires the use of the S-7 Chimera."

__Location:__ Launch from League frigate in Eos zone and ???? on completion. A white planet with dark spots and green rings is visible, as well as a small red star and a small violet star.

__Objectives:__ Destroy Navy frigate.

__Fighter Craft:__ S-7 Hydra x 1 (friendly).

__Fleet Craft:__ League Frigate x 1. Navy Frigate x 1.

__Additional Craft:__ Navy Cruiser x 2 (uncloaking). T-45 Thunderchild x 1 (launching, respawning).

__Success:__ Tracking down survivors.

__Failure:__ The Navy begins its counter-attack.

### Medical Units Under Attack

"This mission carries a heavy flak potential. T.I.D.S. is commissioning the S-4 Dark Angel due to its defensive capabilities."

__Location:__ Eos zone. Asteroid field.

__Objectives:__ Protect League medical craft. Destroy threatening asteroids.

__Fleet Craft:__ L Medi Craft x 1.

__Respawning Craft:__ T-9 Tornado x 1 (opening jumpgate).

__Success:__ Tracking down survivors.

__Failure:__ The Navy begins its counter-attack?

### The Navy Begins Its Counter-Attack

"Extensive heavy support capabilities are required; the S-11 Demon has been assigned."

__Location:__ Launch from League cruiser in Janus zone. Open jump gate on failure.

__Objectives:__ Protect League cruiser.

__Fighter Craft:__ S-4 Dark Angel x 1 (friendly).

__Fleet Craft:__ ???? Battle Platform? x 1. League Cruiser x 1.

__Additional Craft:__ Navy Cruiser x 2 (uncloaking). T-9 Tornado x 2 (launching, respawning).

__Failure:__ Entering Alpha-Centauri.

### Tracking Down Survivors

"This mission carries a heavy flak potential. T.I.D.S. is commissioning the S-4 Dark Angel due to its defensive capabilities."

__Location:__ Travel to Janus and open jump gate on completion. A large red star dominates the sky opposite a small violet star and a dark rocky planet is nearby.

__Objectives:__ Protect League medical craft.

__Fighter Craft:__ S-4 Dark Angel x 1 (friendly).

__Fleet Craft:__ L Medi x 1.

__Respawning Craft:__ T-9 Tornado x 2 (opening jumpgate).

__Success:__ Destroyer versus destroyer.

__Failure:__ Entering Alpha-Centauri.


Act 7: Time To Strike
---------------------

### The League Targets Enemy Intelligence

"Ship assignment analysis indicates that an S-9 Hydra will be required for this mission."

__Location:__ Shalina.

__Objectives:__ Disable Navy cruiser. Protect ???? League APC craft.

### The Defense of League Supply Routes

### The Funeral Raid

### Moving the Strike Cannons

### Civilians in Danger


Act 8: Pulling In Two Directions
--------------------------------

### Destroyer Versus Destroyer

"Ship assignment analysis indicates that an S-9 Hydra will be required for this mission."

__Location:__ Travel to Harmonia and open jump gate on completion. A large red star dominates the sky near a small violet star.

__Objectives:__ Protect League destroyer. Destroy Navy destroyer.

__Fleet Craft:__ League Destroyer x 1. Navy Destroyer x 1.

__Additional Craft:__ T-57 Blizzard x 2 (launching). S-4 Dark Angel x 2 (launching).

__Respawning Craft:__ T-45 Thunderchild x 1 (launching).

__Success:__ Entering the Sol system.

__Failure:__ Entering Alpha-Centauri.


Alpha System
============

Alpha-Centauri is a system beleaguered by civil war between freedom fighters and empire loyalists.

Act 9: The Rage of Civil War
----------------------------

The League must begin operations in Alpha-Centauri to combat anti-League military activities.

### Entering Alpha-Centauri

"Ship assignment analysis indicates that an S-9 Hydra will be required for this mission."

__Location:__ Travel to Virgia and dock with League destroyer on success. Open jump gate on failure.

__Objectives:__ Protect League destroyer.

__Fleet Craft:__ Navy Cruiser x 1.

__Additional Craft:__ League Destroyer x 1 (entering sector via warphole). T-29 Stormlord x 4 (launching). S-4 Dark Angel x 1 (friendly, launching). Faction Frigate x 1 (opening jumpgate). F-4 Dark Angel x 1 (enemy, launching).

__Success:__ Unkown forces in the system.

__Failure:__ An force unknown attacks.

### An Unknown Force Attacks

"Ship assignment analysis indicates that an S-9 Hydra will be required for this mission."

__Location:__ Launch from League space station at warp hole zone and dock on success. A green, rocky planet is visible nearby.

__Objectives:__ Protect League space station.

__Fleet Craft:__ L Space Station x 1. League Civil Trans x 4.

__Additional Craft:__ F-11 Demon x 2 (opening jumpgate). F-5 Vampire x 1 (opening jumpgate). F-7 Chimera x 1 (opening jumpgate). F-9 Hydra x 1 (opening jumpgate). F-7 Chimera x 1 (opening jumpgate).

__Success:__ Maintaining the League's supplies.

__Failure:__

### Unknown Forces in the System

### League Communications Under Threat

### Maintaining the League Supplies

"This mission carries a heavy flak potential. T.I.D.S. is commissioning the S-4 Dark Angel due to its defensive capabilities."

__Location:__ Launch from League space port at warp hole zone. A light brown planet is visible as well as a small red star near the warp hole.

__Objectives:__ Protect League comms craft.

__Fleet Craft:__ League Spaceport x 1. League Comms x 1.

__Additional Craft:__ Navy Siren x 4 (decloaking). F-5 Vampire (opening jumpgate, respawning?). Faction Destroyer x 1 (opening jumpgate).

__Success:__

__Failure:__ Under attack in Alpha-Centauri.


Act 10: Glory Tomorrow, Treachery Today
---------------------------------------

In this act, a traitor offshoot attempts to betray the League by negotiating a surrender to the Navy.

### Under Attack in Alpha-Centauri

"This mission carries a heavy flak potential. T.I.D.S. is commissioning the S-4 Dark Angel due to its defensive capabilities."

__Location:__ ???? Calliopia and open jump gate on success.

__Objectives:__ Protect League cruiser. Destroy Faction destroyer.

__Fleet Craft:__ League Cruiser x 1. F Destroyer x 1.

__Additional Craft:__ S-7 Chimera x 2 (friendly, launching). F-7 Chimera x 2 (launching). F-5 Vampire x 1 (launching).

__Success:__ Stranded in Alpha-Centauri.

__Failure:__ Targeting a Faction Destroyer.

### Targeting a Faction Destroyer

"The heavy payload necessary for this mission requires the use of the S-7 Chimera."

__Location:__ Travel to ? and ?.

__Objectives:__ Destroy Faction destroyer.

__Fighter Craft:__ S-11 Demon x 1 (friendly).

__Fleet Craft:__ Faction Destroyer x 1. League Dreadnought? x 1. ???? Siren x ?.

__Respawning Craft:__ F-7 Chimera x 1? F-9 Hydra x 1?

__Success:__ Destroying an Enemy Starport.

__Failure:__ Sabotaging the peace talks.

### Stranded in Alpha-Centauri

"High speed capabilities are imperative, therefore the S-5 Vampire has been assigned to this mission."

__Location:__ Dock with League dreadnought on success. A large planet with bright rings and a dark slash across its surface is visible as well as a medium red star.

__Objectives:__

__Fighter Craft:__ T-45 Thunderchild x 1.

__Additional Craft:__ T-9 Tornado x 2 (opening jumpgate). T-29 Stormlord x 2 (opening jumpgate). T-45 Thunderchild x 1 (opening jumpgate). T-5 Avalanche x 2 (opening jumpgate). T-29 Stormlord x 3 (opening jumpgate).

__Success:__ Destroying an enemy starport.

__Failure:__ Sabotaging the peace talks.

### Sabotaging the Peace Talks

"Ship assignment analysis indicates that an S-9 Hydra will be required for this mission."

__Objectives:__ Destroy Faction command craft.

__Fighter Craft:__ S-9 Hydra x 1 (friendly).

__Fleet Craft:__ Navy Starport x 1.

__Respawning Craft:__ T-5 Avalanche x 2 (launching). T-9 Tornado x 1 (launching).

Additional Craft. Navy Dreadnought x 1 (Uncloaking). Faction Command Craft x 1 (opening jumpgate?).

__Failure:__ The Old Life Returns.

__Success:__ Destroying an enemy starport.

### Destroying an Enemy Starport

"The heavy payload necessary for this mission requires the use of the S-7 Chimera."

__Location:__ Travel to Hesta zone. A light-colored greenish planet is visible near a large red star.

__Objectives:__ Destroy Navy starport.

__Fighter Craft:__ S-5 Vampire x 1 (friendly). S-11 Demon x 1 (friendly).

__Fleet Craft:__ Navy Frigate x 2. Navy Starport x 1.

__Additional Craft:__ T-9 Tornado x 2 (launching). T-45 Thunderchild x 1 (launching). T-9 Tornado x 2 (launching). T-45 Thunderchild x 1 (launching). T-9 Tornado x 2 (launching). T-45 Thunderchild x 1 (launching).

__Success:__ Dreadnought versus dreadnought.

__Failure:__ The Old Life Returns.


Act 11: The League In Agony
---------------------------

Virus Alert

League VIPs Under Threat

Protecting the League's Commanders

Talks Disrupted by Traitors

Looking Forward to Peace


12: One Last Push
-----------------

### Dreadnought Versus Dreadnought

"Ship assignment analysis indicates that an S-9 Hydra will be required for this mission."

__Location:__ Travel to Erato zone and dock with dreadnought on success. Open jump gate on failure.

__Objectives:__ Destroy Navy dreadnought.

__Fleet Craft:__ League Dreadnought x 1. Navy Dreadnought x 1.

__Additional Craft:__ T-9 Tornado x 2 (launching). T-29 Stormlord x 1 (launching).

__Success:__ The Caucus for Peace.

__Failure:__ 


Act 13: Closing In On Sol
-------------------------

### Entering the Sol System

__Objectives:__ Destroy all Navy Siren weapons. Protect League Dreadnought.

"High speed capabilities are imperative, therefore the S-5 Vampire has been assigned to this mission."

__Fighter Craft:__ S-5 Vampire x 1 (friendly). T-45 Thunderchild x 1.

__Fleet Craft:__ Navy Battle Platform x 1. Navy Siren x 8.

__Additional Craft:__ League Dreadnought x 1. T-45 Thunderchild x 1 (respawning). T-29 Stormlord x 2.

__Success:__ Assault on the Sol platform.

__Failure:__ Urgent assistance required.

### Urgent Assistance Required

__Location:__ Pluto zone.

__Objectives:__ Protect League Dreadnought and support craft.

"Extensive heavy support capabilities are required; the S-11 Demon has been assigned."

__Fleet Craft:__ League Destroyer x 1. S-11 Demon (friendly).

__Additional Craft:__ S-9 Tornado x 3. T-29 Stormlord x 2. T-57 Blizzard x ?.

__Failure:__ Asteroid alert.


### Assault on the Sol Platform

__Objectives:__ Disable Navy Battle Platform.

"Ship assignment analysis indicates that an S-9 Hydra will be required for this mission."

__Fighter Craft:__ S-4 Dark Angel (friendly). T-9 Tornado x 2.

__Fleet Craft:__ League Destroyer x 1. Navy Battle Platform x 1.

__Additional Craft:__ League APC x 3. T-45 Thunderchild x 2 (respawning).

__Success:__ Civilians at risk.

### Asteroid Alert

__Location:__ Pluto zone.

Objective: Destroy threatening asteroid.

"Extensive heavy support capabilities are required; the S-11 Demon has been assigned."

__Fleet Craft:__ L Tanker1. L Tanker2. L Tanker3.

__Additional Craft:__ T-9 Tornado x 2 (respawning).

__Failure:__

__Success:__ Civilians at risk.

### Civilians at Risk

__Objectives:__ Protect Neptune space station. Destroy all Navy fighter craft.

"Extensive heavy support capabilities are required; the S-11 Demon has been assigned."

__Fighter Craft:__ S-9 Hydra x 1 (friendly).

__Fleet Craft:__ Neptune Space Station x 1.

__Additional Craft:__ T-9 Tornado x 2. Strike Cannon 1. Strike Cannon 2. Strike Cannon 3. Strike Cannon 4. T-57 Blizzard x 2. T-9 Tornado x 2.

__Failure:__ Securing the League's positions.



Act 14: A Vast And Lonely Place
-------------------------------

### Securing the League's Positions

__Location:__ Neptune zone, League Space Station 4.

"High speed capabilities are imperative, therefore the S-5 Vampire has been assigned to this mission."

__Fighter Craft:__ S-5 Vampire x 1 (friendly).

S-4 Dark Angel x 2 (enemy).

__Fleet Craft:__ League Space Station x 1. Navy Comms x 1.

__Failure:__ In defense of the battle platform.

### In Defense of the Battle Platform

__Objectives:__ Protect League Battle Platform. Destroy Navy Destroyer.

"Ship assignment analysis indicates that an S-9 Hydra will be required for this mission."

__Fighter Craft:__ S-9 Hydra x 1 (friendly).

__Fleet Craft:__ League Battle Platform x 1. Navy Destroyer x 1. Something that blew up?

__Additional Craft:__ T-29 Stormlord x 2. T-29 Stormlord x 2.

### Capturing a Clone Craft

### Sabotaging the Sol System

### Strike Cannons Turned on Saturn


Act 15: Inside The Enemy's Home
-------------------------------

Destruction of an Arms Orbitol

Enemy Commander Required Alive

Raid on the Jupiter Starport

Protecting a League Commander Craft

In Search of a Mission Transporter


Act 16: Descent Into Hell
-------------------------

A Titan Hits Back


Act 17: A Collision Course With Evil
------------------------------------

The League Reaches Out

Battling a Cloned Dreadnought

Attack on Mars Starport

A Stranded Dreadnought

Destruction of the Clone Fighters


Act 18: Facing The Heat Of Sol
------------------------------

Preparing for the Last Assault

Encountering Fierce Resistance

Insight of Earth


Finale 19: The Judgement
------------------------


Finale 20: The Old Life Returns
-------------------------------

The Faction negotiates with the Earth Empire to 


Finale 21: The Caucus For Peace
-------------------------------

The war is fought to a stalemate, and the League of Free Worlds and the Earth Empire negotiate a peaceful resolution. The colonies are granted independence and enter into a relationship of free trade and mutual benefit with the empire.


Finale 22: A Slow Lingering Death
---------------------------------


Finale 23: Time To Move On
--------------------------


Finale 24: A New Threat?
------------------------
