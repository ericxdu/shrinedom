  - [Breach](https://www.doomworld.com/idgames/levels/doom2/Ports/a-c/breach)
    + Gameplay: singleplayer sprawling scripted infiltration
    + Engines: PrBoom, Odamex, GZDoom
    + Hideous Destructor: compatible
    + Copyright/Permissions: MAY base for modification/reuse
    + Distribution: MAY distribute with invariant txt
  - [Bury My Heart Knee-Deep](https://www.doomworld.com/idgames/levels/doom2/j-l/kneedeep)
    + Gameplay: singleplayer, cooperative, sprawling scripted infiltration
    + Engines: PrBoom, Odamex, GZDoom
    + Hideous Destructor: breaks weapons (fixable), lag spike (known cause)
    + Copyright/Permissions: may NOT base for modification/reuse
    + Distribution: MAY distribute with invariant txt
  - [Cyberdreams](https://www.doomworld.com/idgames/levels/doom2/megawads/cydreams)
    + Gameplay: singleplayer pacifist puzzles
    + Engines: PrBoom, ?
    + Hideous Destructor: ?
    + Copyright/Permissions: MAY NOT base for additional levels
    + Distribution: MAY distribute WAD with invariant txt
  - [Doom II: The Way Id Did](https://www.doomworld.com/idgames/levels/doom2/megawads/d2twid)
    + Gameplay: singleplayer homage megawad
    + Engines: PrBoom, ?
    + Hideous Destructor: ?
    + Copyright/Permissions: may NOT use contents for modification/reuse
    + Distribution: MAY distribute with invariant txt
  - [Fortress](https://www.doomworld.com/idgames/levels/doom2/deathmatch/d-f/fortress)
    + Gameplay: deathmatch with bases, area defense, and win conditions
    + Engines: PrBoom, Odamex, GZDoom
    + Hideous Destructor: compatible
    + Copyright/Permissions: MAY use as base for additional levels
    + Distribution: MAY distribute WAD with invariant txt
  - FreeDM
    + Gameplay: deathmatch megawad
    + Copyright/Permissions: BSD-like
  - Freedoom Phase 1 / Phase 2
    + Gameplay: singleplayer original campaigns
    + Copyright/Permissions: BSD-like
  - [Going Down](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/gd)
    + Gameplay: singleplayer, ?
    + Engines: PrBoom, ?
    + Hideous Destructor: ?
    + Copyright/Permissions: may NOT base for modification/reuse
    + Distribution: MAY distribute with invariant txt
  - [Sawdust](https://www.doomworld.com/idgames/levels/doom2/Ports/s-u/sawdust)
    + Gameplay: singleplayer several maps
    + Engines: PrBoom, GZDoom, ?
    + Hideous Destructor: ?
    + Copyright/Permissions: may NOT use contents as a base for modification/reuse
    + Distribution: MAY distribute with invariant txt
  - [Solar Flare](https://www.doomworld.com/idgames/levels/doom2/deathmatch/d-f/flare-2)
    + Gameplay: deathmatch
    + Engines: PrBoom, ?
    + Hideous Destructor: ?
    + Copyright/Permissions: may NOT base for modification/reuse
    + Distribution: MAY distribute with invariant txt
  - [Suspended in Dusk](https://www.doomworld.com/idgames/levels/doom2/s-u/sid)
    + Gameplay: singleplayer infiltration four levels
    + Engines: PrBoom, Odamex, GZDoom
    + Hideous Destructor: compatible
    + Copyright/Permissions: MAY use contents as a base for modification/reuse with credits
    + Distribution: MAY distribute with invariant txt
  - [Valiant](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/valiant)
    + Gameplay: singleplayer infiltration megawad modded monsters/weapons
    + Engines: PrBoom, Odamex, GZDoom
    + Hideous Destructor: breaks weapons (fixable)
    + Copyright/Permissions: may NOT use contents as a base for modification/reuse
    + Distribution: May distribute with invariant txt
