Hideous Destructor may have altogether too many binds to be effectively 
played with a gamepad. But gamepads are made for gaming so here goes.

There are several binds meant to be held down as a mode shift, which 
would be best bound to a grip trigger or maybe a bumper.

Crouch
Sprint
Zoom

            ____________________________              __
           / [_Jump_]          [__F___] \               | Front Triggers
          / [_Sprint_]        [____AF__] \              | Bumpers
       __/________________________________\__         __|
      /                                  _   \          |
     /      Ip           __             ( )   \         |
    /       ||      __  |  |  __     _       _ \        | Main Pad
   |    Wp==##==Wn |  |      |  |   ( ) -|- ( ) |       |
    \       ||    ___          ___       _     /        |
    /\      In   /   \        /   \     ( )   /\      __|
   /  \____ ___ |Move | ____ | Look| ________/  \       | Analog Controls
  |         /  \ \___/ /    \ \___/ /  \         |    __|
  |        /    \__C__/      \_____/    \        |    __| Grip Triggers
  |       /                              \       |
   \_____/                                \_____/

     |__________|______|    |______|___________|
      Direct Pad  Left       Right   Action Pad
                 Analog      Analog

                   |_____________|
                      Menu Pad


With GZDoom defaults

            ____________________________              __
           / [__AF__]          [__F___] \               |
          / [___Wp___]        [___Wn___] \              | Front Triggers
       __/________________________________\__         __|
      /                                  _   \          |
     /     Map           __            (Jmp)  \         |
    /       ||      __  |  |  __     _       _ \        | Main Pad
   |    Ip==##==In |  |      |  |   (R) -|- ( ) |       |
    \       ||    ___          ___       _     /        |
    /\      Act  /   \        /   \     (E)   /\      __|
   /  \____ ___ |Move | ____ | Look| ________/  \       | Analog Controls
  |         /  \ \___/ /    \ \___/ /  \         |    __|
  |        /    \__X__/      \_____/    \        |    __| Grip Triggers
  |       /                              \       |
   \_____/                                \_____/

       |________|______|    |______|___________|
         Di Pad   Left       Right   Action Pad
                 Analog      Analog

                   |_____________|
                      Menu Pad
