# Generalizing the Engine

## Legacy

As DeHacked, BEX, and Boom-compatible are essentially frozen
standards, new generalizations ought to be named something different
to reflect the updated standards.

## Terminology

Boom mods suffer from a sort of rigid association with the original
intent and branding of Doom. In order to soften this its useful to
use more generic names and terms sometimes referring to the source
code itself rather than *id*'s finished product. For instance, refer
to the sixth Thing as "Undead" after its source code MT_UNDEAD name.

- change the priority order of weapons
- override the two-rounds behavior of "weapon 7"
- override the random-frame behavior of "weapon 5"

## Projectile Spawn Height

All projectiles spawn at a standard hard-coded height. MT_UNDEAD has a
hard-coded exception that spawns its projectiles higher than standard.
The following are some examples of how this might be configured.

    Thing 7 (Tracer)
    spawn height = 16
    
    Thing 6 (Undead)
    missile height = 16
