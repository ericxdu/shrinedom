Generalized Doom
================

Legacy
------

The original Doom engine has been modified extensively over the years to implement new games like Heretic and Chex Quest or generalized engines like ZDoom and Eternity. Furthermore, efforts have been made to generalize source port behavior like BEX (Boom EXtensions for DeHackEd), UDMF, and MBF 21.

### Monsters

Generic dummy monsters to be overridden by DeHackEd and BEX files. Hardcoded behavior will need to be hacked out of the engine to implement the generic behavior, possibly to be added back in via BEX, UDMF, or MBF 21 files.

### Pickups

Collectable objects have hard-coded effects. More functions could be written and new pickup types added for a more generic gamut of effects.

### Intermission Text

Find and display intermission text, if present, before entering any or all levels.

### Demos

Autodetect and play all demos, in order, from lumps within the WAD.


Implementations
---------------

### All Hell

All Hell is an experimental implementation of generalized doom based on LibRetro PrBoom. Implemented as a LibRetro core, All Hell will be cross-platform and highly compatible with general and embedded computer systems.
### Chocolate Doom

Chocolate Doom is a faithful and easy-to-compile source port intended to replicate the look and feel of the original Doom engine. The project also releases engines capable of playing Heretic, Hexen, Chex Quest, Strife, and many Doom mods and total conversions.
