`raisestate`
============

*iD* implemented a new state for map objects in *Doom II*,
implemented it for some and not for others. This is the
`raisestate`. Monsters enter this state if they are healed by
a new monster in *Doom II* known as the Arch-vile.
