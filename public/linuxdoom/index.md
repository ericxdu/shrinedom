Linux Doom Legacy
=================

<cite>Linux Doom</cite> began development in 1992 and was released in 
1997 under the terms of the [GNU General Public License 
v2+](https://en.wikipedia.org/wiki/GPL). It is a [game 
engine](https://en.wikipedia.org/wiki/Game_engine) that powers many 
commercial games and fan creations. The free software, Linux release was 
prepared by Dave Taylor and Bernd Kreimeier.
<br><br>
Although designed to be modified, the engine was nontheless intended to 
play one game and is thus limited in many ways. Many of these limits can 
be removed as bugs. Others can be abstracted away with generalizations. 
Efforts have been made to radically rebuild the engine for a specific 
project. Others have gone to great lengths to conceptualize and 
implement extensions to the engine's capabilities.

### Essential References for Modding

- [Using DeHackEd](dehacked.html)
- [Frame Definition](frame.html)
- [Thing Definition](thing.html)
- [Action Table](codeptr.txt)
- [Sound Table](sounds.html)
- [Sprite Table](sprites.html)


### Boom

+ PrBoom+
  + UMAPINFO development
+ MBF
+ [MBF21](https://www.doomworld.com/forum/topic/122323-mbf21-specification-v12-release/)

### Chocolate Doom

+ *The Ultimate Doom*
+ *Doom II: Hell on Earth*
+ *Heretic: Shadow of the Serpent Riders*
+ *Hexen: Beyond Heretic*
+ *Strife: Quest for the Sigil*
+ *Freedoom: Phase 1*
+ *Freedoom: Phase 2*

### Eternity Engine

+ SMMU

### GZDoom

Highly advanced engine with scripting technologies that have been 
incorporated into the base.

+ Doom: The Golden Souls
+ Hideous Destructor


Local Pages
-----------

- [Bestiary](mthing.html)
- [Boom Scripting](boomscr.html)
- [Distributable WADs](wadsmay.html)
- [Non-distributable WADs](wadsnot.html)


External Links
--------------

- [Doom Wiki](https://doomwiki.org/wiki/Entryway)
- [Freedoom](https://freedoom.github.io/)

