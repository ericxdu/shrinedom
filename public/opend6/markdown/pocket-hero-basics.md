Combat
======

Damage Roll
-----------

To inflict damage after hitting requires a successful Strength damage roll. The roll must meet or exceed the defender's Endurance roll. If it does, the defender suffers 1 wound.

The defender suffers an additional wound

A melee weapon may add a bonus to the Strength roll. Ranged weapons have their own damage rolls that are not added to Strength.
