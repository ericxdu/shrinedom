Galaxy System Aliens
====================

Thanoperium - *Fallen Necromancer Empire*
-----------------------------------------

Former members and servants of a now-defunct ancient empire, who ruled a 
significant portion of the galaxy with strange technology and 
metaphysical powers.


Brutallion - *Military Warlord Confederacy*
-------------------------------------------

The Brutallion are a warrior species united in a cause of expanding and 
holding galactic territory. They operate on a "might makes right" 
ideology and a strict military heirarchy. Bio-technology augments their 
already formidable bodies for combat.


Cyberborg - *Sentient Robot Collective*
---------------------------------------

The Cyberborg are an inscrutable species of androids. They rarely 
communicate with bio-sentients and typically go about their business in 
a society of collective production and construction. They are 
technologically advanced, but not warlike and mostly isolationist in 
behavior.


Espemorpha - *Elder Psionic Bioengineers*
-----------------------------------------

This is an ancient species of enigmatic sentiences. You'll usually not 
encounter them as they are immobile and reclusive, but they send out 
biologically grown *contructs* to do their bidding. The most prominent 
of these are their *bioships* which they use to aggressively defend 
their territory and interests in the galaxy.
