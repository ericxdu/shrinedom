Gravity Drives
==============

> Grav-drives and graviton generators serve as forward propulsion for
> starships and artificial gravity for crew.

Starships in Stardust Empire employ a propulsion technology known as 
"gravitic propulsion" or "Grav-drives". These engines use a fusion 
reactor to pump energy into a gravitic distortion generator that
accelerates the ship along a limited axis. This propulsion system can 
also turn the ship, causing it to streak through space in the general 
direction of the forward bow. Surplus energy from the fusion drive is 
used to power the rest of the ship.

Similar gravitic technology is used to generate artificial gravity for 
the ship, reducing inertial effects and providing a comfortable 1-G for 
walking and exercise. Grav tech also plays a role in the standard beam 
weapons employed by military ships.
