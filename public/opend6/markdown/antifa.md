<img alt="OpenD6 Logo" src="images/d6logo150.png" width="100" 
height="auto" style="float:right;"/>

Anti Fantasy
============

A dungeon crawl based on [Charles Gabriel's 16x18 character sprites][1] 
and the [OpenD6][2] combat system.

[1]: https://opengameart.org/content/twelve-16x18-rpg-sprites-plus-base
[2]: https://opend6.fandom.com/wiki/D6_System

Characters
----------

In *Anti Fantasy*, characters have a collection of attributes
representing their skill and ability in combat. Each attribute is
expressed in an amount of dice. The dice are rolled and compared to
a difficulty number to determine if the character succeeds in using
their skill. Here is a sample of characters.

<table style="float:left; width:auto;">
<tr><td>Warrior</td><th>Trait</th><th>Dice</th></tr>
<tr><td rowspan="5"><img style="float:left;" src="images/f_war.png" />
    <td>Aim</td><td>&#127922;&#127922;</td></tr>
<tr><td>Stamina</td><td>&#127922;&#127922;&#127922;</td></tr>
<tr><td>Balance</td><td>&#127922;&#127922;</td></tr>
<tr><td>Strength</td><td>&#127922;&#127922;&#127922;&#127922;&#127922;</td></tr>
<tr><td>Magic</td><td>&#127922;</td></tr>
</table>

<table style="float:right; width:auto;">
<tr><td>Magician</td><th>Trait</th><th>Dice</th></tr>
<tr><td rowspan="5"><img style="float:left;" src="images/f_mag.png" />
    <td>Aim</td><td>&#127922;&#127922;&#127922;</td></tr>
<tr><td>Stamina</td><td>&#127922;</td></tr>
<tr><td>Balance</td><td>&#127922;&#127922;&#127922;</td></tr>
<tr><td>Strength</td><td>&#127922;</td></tr>
<tr><td>Magic</td><td>&#127922;&#127922;&#127922;&#127922;&#127922;</td></tr>
</table>

<table style="float:left; width:auto;">
<tr><td>Healer</td><th>Trait</th><th>Dice</th></tr>
<tr><td rowspan="5"><img style="float:left;" src="images/f_hea.png" />
    <td>Aim</td><td>&#127922;&#127922;</td></tr>
<tr><td>Stamina</td><td>&#127922;&#127922;&#127922;</td></tr>
<tr><td>Balance</td><td>&#127922;&#127922;</td></tr>
<tr><td>Strength</td><td>&#127922;&#127922;</td></tr>
<tr><td>Magic</td><td>&#127922;&#127922;&#127922;&#127922;</td></tr>
</table>

<table style="float:right; width:auto;">
<tr><td>Ninja</td><th>Trait</th><th>Dice</th></tr>
<tr><td rowspan="5"><img style="float:left;" src="images/f_nin.png" />
    <td>Aim</td><td>&#127922;&#127922;&#127922;&#127922;</td></tr>
<tr><td>Stamina</td><td>&#127922;&#127922;</td></tr>
<tr><td>Balance</td><td>&#127922;&#127922;&#127922;</td></tr>
<tr><td>Strength</td><td>&#127922;&#127922;</td></tr>
<tr><td>Magic</td><td>&#127922;&#127922;</td></tr>
</table>

<table style="float:left; width:auto;">
<tr><td>Ranger</td><th>Trait</th><th>Dice</th></tr>
<tr><td rowspan="5"><img style="float:left;" src="images/f_ran.png" />
    <td>Aim</td><td>&#127922;&#127922;</td></tr>
<tr><td>Stamina</td><td>&#127922;&#127922;&#127922;</td></tr>
<tr><td>Balance</td><td>&#127922;&#127922;</td></tr>
<tr><td>Strength</td><td>&#127922;&#127922;</td></tr>
<tr><td>Magic</td><td>&#127922;&#127922;&#127922;&#127922;</td></tr>
</table>

<table style="float:right; width:auto;">
<tr><td>Monk</td><th>Trait</th><th>Dice</th></tr>
<tr><td rowspan="5"><img style="float:left;" src="images/f_mon.png" />
    <td>Aim</td><td>&#127922;&#127922;</td></tr>
<tr><td>Stamina</td><td>&#127922;&#127922;&#127922;&#127922;&#127922;</td></tr>
<tr><td>Balance</td><td>&#127922;&#127922;</td></tr>
<tr><td>Strength</td><td>&#127922;&#127922;</td></tr>
<tr><td>Magic</td><td>&#127922;&#127922;</td></tr>
</table>

---

## Attributes

Each character has a number of "dice" in each attribute. These dice are
rolled to determine success or failure after attempting an action.

### Aim

Roll these dice to hit with a ranged weapon. Difficulty 10.

### Balance

Roll these dice to hit with a melee weapon. Difficulty 10.

*Roll these dice to dodge all damage from an attack?

*Difficulty determined by weapon?

*When to dodge/parry?

### Strength

Roll these dice to inflict damage after hitting with a melee attack.

The attack inflicts 1 wound if the Strength roll is greater than or
equal to the defender's damage resistance (see Endurance). It inflicts
2 wounds if the roll is greater than or equal to two times the damage
resistance, and so on. There are 0 wounds inflicted if the Strength
roll is lower than the damage resistance.

### Stamina

Roll these dice to resist damage from an attack.

If the result is higher than the damage roll, there are no wounds
inflicted. Otherwise, the defender is wounded (see Strength). A
character who takes 5 wounds can no longer fight.

*wounds cause an action roll penalty?

### Magic

Roll these dice to successfully cast a magic spell.

Some characters possess magical powers, depending on their class.

---

Weapons
-------

- Knuckles
  + accuracy: Balance
  + difficulty: 10
  + damage: Str+1
- Knife
  + accuracy: Balance
  + difficulty: 10
  + damage: Str+2
- Sword
  + accuracy: Balance
  + difficulty: 10
  + damage: Str+4
- Warhammer
  + accuracy: Balance
  + difficulty: 10
  + damage: Str+5
- Longbow
  + accuracy: Aim
  + difficulty: 10
  + damage:

## Armor

- Padded
  + damage resistance: Stamina+1
- Chain
  + damage resistance: Stamina+6
- Plate
  + damage resistance: Stamina+11
- Buckler
  + damage resistance: Balance+1
- Shield
  + damage resistance: Balance+3

## Notes

Use flanking and backstabbing?

## Links

  - Art by [Charles Gabriel](https://opengameart.org/users/charlesgabriel). Commissioned by OpenGameArt.org (http://opengameart.org)
    + [10 Basic Message Boxes](https://opengameart.org/content/10-basic-message-boxes)
    + [48x48 Faces 1st Sheet](https://opengameart.org/content/48x48-faces-1st-sheet)
  - Art by [surt](https://opengameart.org/users/surt)
    + [Town Tiles](https://opengameart.org/content/town-tiles)
  - [Die Code Simplification](http://opend6project.org/?page_id=359)
  - [Mathematical Mean](http://opend6project.org/?page_id=53)
  - [Style Guide](http://opend6project.org/?page_id=65)
