<img style="float:right;" src="images/d6logo150.png" />

Character Creation Template
===========================

Game World Name: Solar Frontier

Game Designer: Nox Banners Game Design

Character Information
---------------------

Species: human with a minimum of 2D and a maximum of 4D in each attribute.

Age Requirements: to be determined.

Groups: to be determined.

Starting Attribute Dice: 15

Starting Skill Dice: 7


Attributes and Skills
---------------------

- Fitness
  + Manual Dexterity
  + Reflexes
  + Stamina
  + Strength
  + Unarmed Combat

- Reason
  + Command
  + Persuasion

- Science
  + Computers
  + Languages
  + Engineering
  + Medicine
  + Robotics

- Observation
  + Investigation
  + Navigation

- Technology
  + Atmospheric Flight Systems
  + Communications
  + Defensive Systems
  + Personal Equipment
  + Security Systems
  + Sensors
  + Space Flight Systems


Game System Template
====================

Genre: hard science fiction

World Overview: Solar Frontier spans centuries starting in the mid-21st century
after humans from Earth begin to explore the solar system and expand to nearby
stars. People live in settlements on moons, asteroids, and at lagrange points
that are generally autonomous and self-governing. Settlements produce materials
from renewable resources and redistribute them to where they are needed via
mutually beneficial trade.

Technology Level: rocketships, space habitats

Powers Section
--------------

Combat Section
--------------

### Damage System

Wounds #: 5

### Round Structure

Initiative rounds

### Options


Miscellaneous Notes
-------------------

Solar Frontier has been in-development since the mid 1990s by a young, 
naive *Star Trek* fan who has since read a little Arthur C. Clarke, 
Larry Niven, and political theory. Its basically my brainchild. I hope 
to see my vision brought to life. &mdash; Ed.
