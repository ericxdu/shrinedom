<img style="float:right;" src="images/d6logo150.png" />

<a href="index.html">goto index</a>

OpenD6 Genre Book Skills
========================

In this document, we will collate a large collection of "skills" shared by the
D6 "genre books" *D6 Adventure*, *D6 Fantasy*, and *D6 Space*. This is meant to
assist in the creation of new genre books and D6 "systems" as described in
*System Book*.


Skill Table of Contents
-----------------------

There are 88 uniquely named non-extranormal skills across the three genre
books. Only 71 of these skills have unique descriptions, simply being listed
under different names. Furthermore, in each genre some skills are absent or
embedded in the description of another skill, like Contortion, Disguise, and
Lockpicking, resulting in a condensed list of 37 root skills to work with.

1. [Acrobatics](#acrobatics) <cite>(D6 Adventure, D6 Space, D6 Fantasy)</cite>
  + Contortion <cite>(D6 Adventure, D6 Fantasy)</cite>: See [Acrobatics](#acrobatics)
2. [Artist](#artist) <cite>(D6 Adventure, D6 Space, D6 Fantasy)</cite>
  + Forgery <cite>(D6 Adventure, D6 Space)</cite>: See [Artist](#artist)
  + Reading/Writing <cite>(D6 Fantasy)</cite>: See [Artist](#artist)
3. [Brawling](#brawling) <cite>(D6 Adventure, D6 Space)</cite> or Fighting <cite>(D6 Fantasy)</cite>
4. [Business](#business) <cite>(D6 Adventure, D6 Space)</cite> or Trading <cite>(D6 Fantasy)</cite>
5. [Climb/Jump](#climbjump) <cite>(D6 Space)</cite> or Jumping <cite>(D6 Adventure, D6 Fantasy)</cite>
  + Climbing <cite>(D6 Adventure, D6 Fantasy)</cite>: See [Climb/Jump](#climbjump)
6. [Command](#command) <cite>(D6 Adventure, D6 Space, D6 Fantasy)</cite>
7. [Con](#con) <cite>(D6 Adventure, D6 Space)</cite> or Bluff <cite>(D6 Fantasy)</cite>
  + Disguise <cite>(D6 Adventure, D6 Fantasy)</cite>: See [Con](#con)
8. [Dodge](#dodge) <cite>(D6 Adventure, D6 Space, D6 Fantasy)</cite>
9. [Flying](#flying) <cite>(D6 Adventure, D6 Fantasy)</cite> or Flying/0-G <cite>(D6 Space)</cite>
10. [Gambling](#gambling) <cite>(D6 Adventure, D6 Space, D6 Fantasy)</cite>
11. [Hide](#hide) <cite>(D6 Adventure, D6 Space, D6 Fantasy)</cite>
12. [Intimidation](#intimidation) <cite>(D6 Adventure, D6 Space, D6 Fantasy)</cite>
13. [Investigation](#investigation) <cite>(D6 Adventure, D6 Space, D6 Fantasy)</cite>
14. [Know-how](#knowhow) <cite>(D6 Adventure, D6 Space, D6 Fantasy)</cite>
15. [Languages](#languages) <cite>(D6 Adventure, D6 Space)</cite> or Speaking <cite>(D6 Fantasy)</cite>
16. [Lifting](#lifting) <cite>(D6 Adventure, D6 Fantasy)</cite> or Lift <cite>(D6 Space)</cite>
17. [Marksmanship](#marksmanship) <cite>(D6 Adventure, D6 Fantasy)</cite> or Firearms <cite>(D6 Space)</cite>
  + Gunnery <cite>(D6 Space)</cite>: See [Marksmanship](#marksmanship)
  + Missile Weapons <cite>(D6 Adventure, D6 Space)</cite>: See [Marksmanship](#marksmanship)
18. [Medicine](#medicine) <cite>(D6 Adventure, D6 Space)</cite> or Healing <cite>(D6 Fantasy)</cite>
19. [Melee Combat](#meleecombat) <cite>(D6 Adventure, D6 Space, D6 Fantasy)</cite>
20. [Navigation](#navigation) <cite>(D6 Adventure, D6 Space, D6 Fantasy)</cite>
21. [Persuasion](#persuasion) <cite>(D6 Adventure, D6 Space, D6 Fantasy)</cite>
  + Animal Handling <cite>(D6 Adventure, D6 Fantasy)</cite>: See [Persuasion](#persuasion)
  + Bargain <cite>(D6 Space)</cite>: See [Persuasion](#persuasion)
  + Charm <cite>(D6 Adventure, D6 Fantasy)</cite>: See [Persuasion](#persuasion)
22. [Piloting](#piloting) <cite>(D6 Adventure, D6 Space)</cite> or Pilotry <cite>(D6 Fantasy)</cite>
  + Charioteering <cite>(D6 Fantasy)</cite>: See [Piloting](#piloting)
  + Exoskeleton Operation <cite>(D6 Space)</cite>: See [Piloting](#piloting)
  + Vehicle Operation <cite>(D6 Space)</cite>: See [Piloting](#piloting)
23. [Repair](#repair) <cite>(D6 Adventure)</cite> or Crafting <cite>(D6 Fantasy)</cite>
  + Armor Repair <cite>(D6 Space)</cite>: See [Repair](#repair)
  + Exoskeleton Repair <cite>(D6 Space)</cite>: See [Repair](#repair)
  + Firearms Repair <cite>(D6 Space)</cite>: See [Repair](#repair)
  + Flight Systems Repair <cite>(D6 Space)</cite>: See [Repair](#repair)
  + Gunnery Repair <cite>(D6 Space)</cite>: See [Repair](#repair)
  + Personal Equipment Repair <cite>(D6 Space)</cite>: See [Repair](#repair)
  + Vehicle Repair <cite>(D6 Space)</cite>: See [Repair](#repair)
24. [Riding](#riding) <cite>(D6 Adventure, D6 Space, D6 Fantasy)</cite>
25. [Running](#running) <cite>(D6 Adventure, D6 Space, D6 Fantasy)</cite>
26. [Scholar](#scholar) <cite>(D6 Adventure, D6 Space, D6 Fantasy)</cite>
  + Aliens <cite>(D6 Space)</cite>: See [Scholar](#scholar)
  + Astrography <cite>(D6 Space)</cite>: See [Scholar](#scholar)
  + Bureaucracy <cite>(D6 Space)</cite>: See [Scholar](#scholar)
  + Cultures <cite>(D6 Space, D6 Fantasy)</cite>: See [Scholar](#scholar)
  + Security Regulations <cite>(D6 Space)</cite>: See [Scholar](#scholar)
  + Tactics <cite>(D6 Space)</cite>: See [Scholar](#scholar)
27. [Search](#search) <cite>(D6 Adventure, D6 Fantasy)</cite>
  + Tracking <cite>(D6 Adventure, D6 Fantasy)</cite>: See [Search](#search)
28. [Security](#security) <cite>(D6 Adventure, D6 Space)</cite> or Traps <cite>(D6 Fantasy)</cite>
  + Demolition <cite>(D6 Space)</cite> or Demolitions <cite>(D6 Adventure)</cite>: See [Security](#security)
29. [Sleight of Hand](#sleightofhand) <cite>(D6 Adventure, D6 Space, D6 Fantasy)</cite>
  + Lockpicking <cite>(D6 Adventure, D6 Fantasy)</cite>: See [Sleight of Hand](#sleightofhand)
30. [Stamina](#stamina) <cite>(D6 Adventure, D6 Space, D6 Fantasy)</cite>
31. [Stealth](#sneak) <cite>(D6 Fantasy)</cite> or Sneak <cite>(D6 Adventure, D6 Space)</cite>
32. [Streetwise](#streetwise) <cite>(D6 Adventure, D6 Space, D6 Fantasy)</cite>
33. [Survival](#survival) <cite>(D6 Adventure, D6 Space, D6 Fantasy)</cite>
34. [Swimming](#swimming) <cite>(D6 Adventure, D6 Fantasy)</cite> or Swim <cite>(D6 Space)</cite>
35. [Tech](#tech) <cite>(D6 Adventure)</cite> or Devices <cite>(D6 Fantasy)</cite>
  + Comm <cite>(D6 Space)</cite>: See [Tech](#tech)
  + Computer Interface/Repair <cite>(D6 Space)</cite>: See [Tech](#tech)
  + Robot Interface/Repair <cite>(D6 Space)</cite>: See [Tech](#tech)
  + Sensors <cite>(D6 Space)</cite>: See [Tech](#tech)
  + Shields <cite>(D6 Space)</cite>: See [Tech](#tech)
36. [Throwing](#throwing) <cite>(D6 Adventure, D6 Space, D6 Fantasy)</cite>
37. [Willpower](#willpower) <cite>(D6 Adventure, D6 Space)</cite> or Mettle <cite>(D6 Fantasy)</cite>

Common Skills
-------------

In determining which skills are common among the genre books and which skills
are spin-offs, a close reading of each skill description was employed, as well
as a [Genre Conversion][1] chart and careful cross-checking of common phrases
in each skill description.

[1]: (http://opend6project.org/section-4-game-basics/genre-conversion/)

<h3 id="acrobatics">Acrobatics</h3>

> Performing feats of gymnastics, extraordinary balance, and dance (and
> related performance arts), as well as break falls. Useful for running
> obstacle courses or doing water ballet. (*D6 Adventure*,
> **acrobatics**)

> Performing feats of gymnastics, extraordinary balance, and dance (and
> related performance arts), as well as breaking falls and escaping
> from bonds. (*Space*, **acrobatics**)

> Performing feats of gymnastics, extraordinary balance, and dance (and
> related performance arts), as well as breaking falls. Useful for
> running obstacle courses or doing courtly promenade. (*Fantasy*,
> **acrobatics**)

> Escaping from otherwise secure physical bonds by twisting, writhing,
> and contorting the body. (*Adventure*, *Fantasy*, **contortion**)

<table>
<caption>genre conversion for Acrobatics</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>acrobatics (Agility)</td><td>acrobatics, contortion (Reflexes)</td><td>acrobatics, contortion (Agility)</td></tr>
</table>

---

<h3 id="artist">Artist</h3>

> Making works of art, like paintings, photographs, and music or
> literary compositions. (*Adventure*, *Space*, **artist**)

> Making works of art, like paintings, music compositions, and dance
> choreographies. (*Fantasy*, **artist**)

> Creating and noticing false or altered documentation in various
> media (paper, electronic, plastic card, etc.), including
> counterfeiting, though tasks may require other skills to help detect
> or make the forgery. (*Adventure*, *Space*, **forgery**)

> Familiarity with and ability to understand various forms of written
> communication, as well as the ability to create literary
> compositions, including forging papers and identifying such
> forgeries. Characters do not begin with the ability to read or write.
> (*Fantasy*, **reading/writing**)

In *D6 Fantasy*, Artist noticeably omits "literary compositions" from its
possible applications. A close reading of the rest of the skill list reveals
Reading/Writing took over that responsibility. Therefore, Reading/Writing is
considered in general a part of the Artist skill.

*D6 Fantasy* is missing the Forgery skill because expertise in written language
is part of the Reading/Writing skill. For this reason as a general rule Forgery
is considered an Artist talent that is spun off into its own skill for certain
genres.

Of note is the fact that Forgery is an ancillary skill that explicitly states
in its description that other skills are needed to detect or make a forgery.
Artist seems like a natural fit for this "other skills", as does
Reading/Writing. Overall there is a strong case for merging both skills into
Artist.

<table>
<caption>genre conversion for Artist</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>artist (Perception)</td><td>artist (Perception)</td><td>artist (Acumen)</td></tr>
<tr><td>forgery (Perception)</td><td>forgery (Knowledge)</td><td>reading/writing (Intellect), artist (Acumen)</td></tr>
</table>

---

<h3 id="brawling">Brawling</h3>

> Competence in unarmed combat. (*Adventure*, *Space*, **brawling**,
> *Fantasy*, **fighting**)

Brawling is one of the three forms of unarmed combat described in
*System Book*.

<table>
<caption>genre conversion for Brawling</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>brawling (Agility)</td><td>brawling (Reflexes)</td><td>fighting (Agility)</td></tr>
</table>

---

<h3 id="business">Business</h3>

> Comprehension of business practices and the monetary value of goods
> and opportunities, including the ability to determine how to make
> money with another skill the character has. Business can complement
> charm, con, and persuasion when haggling over prices for goods and
> services being bought or sold. (*Adventure*, **business**)

> Comprehension of business practices and the monetary value of goods
> and opportunities, including the ability to determine how to make
> money with another skill the character has. Business can complement
> bargain when haggling over prices for goods and services being bought
> or sold. (*Space*, **business**)

> Knowledge of business practices, exchange rates, the monetary value
> of goods and opportunities, and other information regarding barter
> and sales, including the ability to determine how to make money with
> another skill the character has. Trading can complement bluff, charm,
> and persuasion when haggling over prices for goods and services being
> bought or sold. (*Fantasy*, **trading**)

Business and Trading are essentially the same skill in each genre book with
marginally different flavor text. *D6 Fantasy* mentions exchange rates,
bartering, and sales in addition to the common "business practices", "monetary
value", and "how to make money".

Each description refers to a different set of other skills it can complement.
In *D6 Space*, Business only complements Bargain. In *D6 Adventure*,
*D6 Fantasy*, Business complements Bluff/Con, Charm, and Persuasion.

Business is one of the skills who's description included "Knowledge of",
"Comprehension of", "Understanding of", et cetera.

<table>
<caption>genre conversion for Business</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>business (Knowledge)</td><td>business (Knowledge)</td><td>trading (Intellect)</td></tr>
</table>

---

<h3 id="climbjump">Climb/Jump</h3>

> Scaling various surfaces. (*Adventure*, *Fantasy*, **climbing**)

> Leaping over obstacles. (*Adventure*, *Fantasy*, **jumping**)

> Climbing or jumping over obstacles. (*Space*, **climb/jump**)

Climbing and Jumping are skills of Reflexes or Agility in *D6 Adventure* and
*D6 Fantasy*. *D6 Space* combines them into a Strength skill called Climb/Jump.
Climb/Jump is worded more like Jumping ("over obstacles").

<table>
<caption>genre conversion for Climb/Jump</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>climb/jump (Strength)</td><td>climbing, jumping (Reflexes)</td><td>climbing, jumping (Agility)</td></tr>
</table>

---

<h3 id="command">Command</h3>

> Effectively ordering and coordinating others in team situations.
> (*Adventure*, *Fantasy*, **command**)

> Effectively ordering and coordinating others in team situations (such
> as commanding a battleship crew). (*Space*, **command**)

<table>
<caption>genre conversion for Command</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>command (Perception)</td><td>command (Presence)</td><td>command (Charisma)</td></tr>
</table>

---

<h3 id="con">Con</h3>

> Lying, tricking, or deceiving others, as well as verbal evasion,
> misdirection, and blustering. Disguise can complement uses of this
> skill. Also useful in putting on acting performances. (*Fantasy*,
> **bluff**)

> Bluffing, lying, tricking, or deceiving others, as well as verbal
> evasiveness, misdirection, and blustering. Also useful in putting on
> acting performances. (*Adventure*, **con**)

> Bluffing, lying, tricking, or deceiving others, as well as verbal
> evasiveness, misdirection, blustering, and altering features or
> clothing to be unrecognizable or to look like someone else. Also
> useful in putting on acting performances. (*Space*, **con**)

> Altering features or clothing to be unrecognizable or to look like
> someone else. Also useful in acting performances. (*Adventure*,
> *Fantasy*, **disguise**)

Con is the same as a skill called Bluff in *D6 Fantasy*, with minor differences
in wording. Disguise is covered by Con in *D6 Space*.

The Confidence attribute is known by alternate names, Presence and Charisma, in
<cite>D6 Adventure</cite>, <cite>D6 Fantasy</cite>. <cite>D6 Space</cite>
combines Confidence and Perception into one attribute. A close reading of the
optional attributes in *System Book Chapter 3* suggests Con could be a skill
governed by either Perception or Confidence.

Bluff and Disguise are uncannily similar to the eponymous "Charisma" skills in
*Dungeons & Dragons*.

<table>
<caption>genre conversion for Con</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>con (Perception)</td><td>con, disguise (Presence)</td><td>bluff, disguise (Charisma)</td></tr>
</table>

---

<h3 id="dodge">Dodge</h3>

> Slipping out of danger's way, whether avoiding an attack or a sprung
> booby trap. (*Adventure*, *Space*, *Fantasy*, **dodge**)

Dodging an attack is an essential combat function of the Reflexes attribute,
making Dodge a core skill.

<table>
<caption>genre conversion for Dodge</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>dodge (Agility)</td><td>dodge (Reflexes)</td><td>dodge (Agility)</td></tr>
</table>

---

<h3 id="flying">Flying</h3>

> Maneuvering under one's own power (such as with wings) or in null
> gravity. (*Adventure*, **flying**)

> Maneuvering under on one's own power (such as with wings) or in
> zero-gravity environments (such as drifting through space in an
> environmental suit). (*Space*, **flying/0-G**)

> Maneuvering under one's own power (such as with wings). (*Fantasy*,
> **flying**)

<table>
<caption>genre conversion for Flying</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>flying/0G (Agility)</td><td>flying (Reflexes)</td><td>flying (Agility)</td></tr>
</table>

---

<h3 id="gambling">Gambling</h3>

> Playing and cheating at games of strategy and luck. (*Adventure*,
> *Space*, *Fantasy*, **gambling**)

<table>
<caption>genre conversion for Gambling</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>gambling (Perception)</td><td>gambling (Perception)</td><td>gambling (Acumen)</td></tr>
</table>

---

<h3 id="hide">Hide</h3>

> Concealing objects, both on oneself and using camouflage.
> (*Adventure*, *Space*, *Fantasy*, **hide**)

<table>
<caption>genre conversion for Hide</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>hide (Perception)</td><td>hide (Perception)</td><td>hide (Acumen)</td></tr>
</table>

---

<h3 id="intimidation">Intimidation</h3>

> Using physical presence, verbal threats, taunts, torture, or fear to
> influence others or get information out of them. (*Adventure*,
> *Fantasy*, **intimidation**)

> Using physical presence, verbal threats, taunts, or fear to influence
> others or get information out of them. (*Space*, **intimidation**)

<table>
<caption>genre conversion for Intimidation</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>intimidation (Knowledge)</td><td>intimidation (Presence)</td><td>intimidation (Charisma)</td></tr>
</table>

---

<h3 id="investigation">Investigation</h3>

> Gathering information, researching topics, analyzing data, and
> piecing together clues. (*Adventure*, *Space*, *Fantasy*,
> **investigation**)

<table>
<caption>genre conversion for Investigation</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>investigation (Perception)</td><td>investigation (Perception)</td><td>investigation (Acumen)</td></tr>
</table>

---

<h3 id="knowhow">Know-how</h3>

> Ability to figure out how to perform an action in which the
> character does not have experience, as well as a catch-all skill
> encompassing areas not covered by other skills (such as basic, not
> fancy, sewing or cooking). (*Adventure*, **know-how**)

> Figuring out how to perform an action in which the character does
> not have experience, as well as a catch-all skill encompassing areas
> not covered by other skills (such as utilitarian sewing or cooking).
> (*Space*, *Fantasy*, **know-how**)

Know-how is reserved for performing basic tasks with no particular
specialization or perfection, hence "basic", "not fancy" and "utilitarian". It
is described as a "catch-all" skill which could mean its meant to generally
cover a character's mundane abilities, or is meant to govern specific everyday
tasks as specializations.

The Perception attribute that governs this skill is known as the alternate name
Acumen in *D6 Fantasy*.

<table>
<caption>genre conversion for Know-how</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>know-how (Perception)</td><td>know-how (Perception)</td><td>know-how (Acumen)</td></tr>
</table>

---

<h3 id="languages">Languages</h3>

> Familiarity with and ability to use various forms of communication,
> including written, spoken, and nonverbal. Characters may choose one
> “native” language in which they have written and spoken fluency.
> Additional languages in which a character has proficiency can be
> represented by specializations of this skill. (*Adventure*, *Space*,
> **languages**)

> Familiarity with and ability to understand various forms of verbal
> communication. Characters know the Trade Speech (assuming the
> setting has one) and one “native” language in which they have spoken
> fluency. Additional languages in which a character has proficiency
> can be represented by specializations of this skill. (*Fantasy*,
> **speaking**)

The Languages skill is identical in *D6 Adventure*, *D6 Space*. *D6 Fantasy*
splits the skill into verbal and written variants because in this setting,
literacy is not as common as in other genres. Speaking is the skill most
similar to Languages because it is common to learn to speak and the description
is analogous.

<table>
<caption>genre conversion for Languages</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>languages (Knowledge)</td><td>languages (Knowledge)</td><td>speaking, reading/writing (Intellect)</td></tr>
</table>

---

<h3 id="lifting">Lifting</h3>

> Moving or lifting heavy objects, as well as representing the ability
> to inflict additional damage with strength-powered weapons.
> (*Adventure*, *Fantasy*, **lifting**)

> Moving or lifting heavy objects, as well as the ability to inflict
> additional damage with strength-powered weapons. (*Space*, **lift**)

Lifting objects and inflicting damage are essential functions of the Strength
attribute, making Lifting a core skill and a core combat skill.

<table>
<caption>genre conversion for Lifting</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>lift (Strength)</td><td>lifting (Physique)</td><td>lifting (Physique)</td></tr>
</table>

---

<h3 id="marksmanship">Marksmanship</h3>

> Shooting any gun a person can carry, even if it requires a tripod
> setup to fire. Covers everything from small slug throwers to
> shoulder-launched rockets. (*Space*, **firearms**)

> Accurately firing weapons mounted on powered armor, vehicles,
> and spaceships, or within fortresses. (*Space*, **gunnery**)

> Shooting any kind of mechanical device - such as a bow or sling -
> that projects missiles across a distance. (*Fantasy*,
> **marksmanship**)

> Shooting guns of any type. Covers everything from small slugthrowers
> to vehicle-mounted rockets. (*Adventure*, **marksmanship**)

> Firing unpowered ranged weapons. (*Adventure*, *Space*,
> **missile weapons**)

*Fantasy* is technically the most comprehensive form of this skill, because it
has no other skills for handling weapons, so Marksmanship covers everything.

*Adventure* splits this into modern weapons with guns and rocket launchers
being covered by Marksmanship, while primitive weapons like bows and slings
are covered by Missile Weapons.

*Space* specifies further by categorizing mounted weapons under Gunnery.
However, Gunnery is governed by the Mechanical attribute rather than Agility
because it's closely related to machine and vehicle operation.

Firing a ranged weapon is an essential combat function of the Coordination
attribute, making Marksmanship a core combat skill.

<table>
<caption>genre conversion for Marksmanship</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>firearms (Agility)</td><td>marksmanship (Coordination)</td><td>marksmanship (Coordination)</td></tr>
<tr><td>gunnery (Mechanical)</td><td>marksmanship (Coordination)</td><td>marksmanship (Coordination)</td></tr>
<tr><td>missile weapons (Agility)</td><td>missile weapons (Coordination)</td><td>marksmanship (Coordination)</td></tr>
</table>

---

<h3 id="medicine">Medicine</h3>

> Dressing wounds, applying splints, and disinfecting injuries, plus an
> understanding and application of medical procedures, such as
> diagnosing illnesses and performing surgery. (*Fantasy*, **healing**)

> Using first aid techniques to treat injuries, as well as an
> understanding and application of medical procedures, including
> diagnosing illnesses and performing surgery. (*Adventure*,
> **medicine**)

> Using basic field medicine to treat injuries, as well as detailed
> understanding and applying medical procedures, such as diagnosing
> illnesses, performing surgery, and implanting cybernetics.
> (*Space*, **medicine**)

This skill is essentially the same across genres with marginally different
flavor text. All of the descriptions include treating "injuries",
understanding/applying "medical procedures", "diagnosing illnesses", and
"performing surgery". *D6 Space* adds cybernetic implants, suggesting Technical
refers to "technology" as well as "technique".

*D6 Space* does reserve repair of cybernetic implants for the Personal
Equipment Repair skill. Medicine can only install the cybernetic implants, not
repair them.

<table>
<caption>genre conversion for Medicine</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>medicine (Technical)</td><td>medicine (Knowledge)</td><td>healing (Intellect)</td></tr>
</table>

---

<h3 id="meleecombat">Melee Combat</h3>

> Wielding hand-to-hand weapons. (*Adventure*, *Space*, *Fantasy*,
> **melee combat**)

Fighting with a melee weapon is an essential combat function of the Reflexes
attribute, making Melee Combat a core combat skill.

<table>
<caption>genre conversion for Melee Combat</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>melee combat (Agility)</td><td>melee combat (Reflexes)</td><td>melee combat (Agility)</td></tr>
</table>

---

<h3 id="navigation">Navigation</h3>

> Determining the correct course using external reference points, such
> as stars, maps, or landmarks, as well as creating maps.
> (*Adventure*, *Fantasy*, **navigation**)

> Plotting courses, such as through space using a vessel's navigational
> computer interface, or on land using maps or landmarks, as well as
> creating maps. (*Space*, **navigation**)

Navigation serves nearly the same purpose across genres, but *D6 Space* adds in
the ability to use a ship's navigation computer. The skill retains the broader
description, suggesting another skill has been combined into Navigation for
*D6 Space*.

<table>
<caption>genre conversion for Navigation</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>astrography (Knowledge)</td><td>scholar, navigation (Knowledge)</td><td>scholar, navigation (Intellect)</td></tr>
<tr><td>navigation (Mechanical)</td><td>navigation (Knowledge)</td><td>navigation (Intellect)</td></tr>
</table>

---

<h3 id="persuasion">Persuasion</h3>

> Controlling animals and making them perform tricks and follow
> commands. (*Adventure*, *Fantasy*, **animal handling**)

Animal Handling is identical in <cite>D6 Adventure</cite>,
<cite>D6 Fantasy</cite>. The skill is covered by Survival in
<cite>D6 Space</cite>.

> Haggling over prices for goods and services being bought or sold, as
> well as using bribery. (*Space*, **bargain**)

> Using friendliness, flattery, or seduction to influence someone
> else. Also useful in business transactions, putting on performances
> (such as singing, acting, or storytelling), and situations involving
> etiquette. (*Adventure*, **charm**)

> Using friendliness, flattery, or seduction to influence someone
> else. Also useful in sales and bartering transactions, putting on
> performances (such as singing, acting, or storytelling), and
> situations involving etiquette. (*Fantasy*, **charm**)

> Influencing others or getting information out of them through
> bribery, honest discussion, debate, diplomacy, or speeches. Also
> useful in negotiations, business transactions, and putting on
> performances (such as singing, acting, or storytelling).
> (*Adventure*, **persuasion**)

> Influencing others or getting information out of them through
> bribery, honest discussion, debate, diplomacy, speeches,
> friendliness, flattery, or seduction. Also useful in negotiations,
> business transactions, and putting on performances (such as singing,
> acting, or storytelling). (*Space*, **persuasion**)

> Influencing others or getting information out of them through
> bribery, honest discussion, debate, diplomacy, or speeches. Also
> useful in negotiations, business transactions, storytelling, and
> oration. (*Fantasy*, **persuasion**)

*D6 Adventure*, *D6 Fantasy* both have a skill named Charm and a skill called
Persuasion with minor wording differences. *D6 Space* covers both skills with
Persuasion alone, but describes a separate skill called Bargain.

<table>
<caption>genre conversion for Persuasion</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>bargain (Perception)</td><td>persuasion (Presence)</td><td>persuasion (Charisma)</td></tr>
<tr>
<td>persuasion (Perception)</td><td>persuasion, charm, animal handling (Presence)</td><td>persuasion, charm, animal handling (Charisma)</td>
</tr>
</table>

---

<h3 id="piloting">Piloting</h3>

> Using single-person conveyances with skills and abilities that
> substitute for (not augment) the character's own skills and
> abilities. This skill substitutes for the character's Agility and
> Strength skills when using the "suit." (*Space*,
> **exoskeleton operation**)

> Accelerating, steering, and decelerating chariots (in particular) or
> any kind of cart-and-animal vehicle. (*Fantasy*, **charioteering**)

> Operating any water-faring vehicle, including steering, applying the
> oars, or managing the sails. (*Fantasy*, **pilotry**)

> Operating any kind of vehicle or powered armor traveling on or
> through the ground, a liquid medium, the air, or space.
> (*Adventure*, **piloting**)

> Flying air- or space-borne craft, from hovercraft and in-atmosphere
> fighters to transports and battleships. (*Space*, **piloting**)

> Operating non-flying vehicles traveling on or through the ground or
> a liquid medium. (*Space*, **vehicle operation**)

The *D6 Adventure* skill Piloting has the broadest definition, covering every
possible vehicle be it air-, space-, land-, or water-craft. *D6 Space* narrows
this skill to flying only air and space craft.

*D6 Space* lumps into one type both ground and water vehicles with the skill
Vehicle Operation, while *D6 Fantasy* splits them into exactly two catergories
with Charioteering and Pilotry respectively.

Exoskeleton Operation and Charioteering are similar to each other in that they
are both specific to their genre and time period. They are also both ground
vehicles (a bipedal suit is made for walking).

The governing attribute is always Coordination or Mechanical.

<table>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>exoskeleton operation (Mechanical)</td><td>piloting (Coordination)</td><td>charioteering (Mechanical)</td></tr>
<tr><td>piloting (Mechanical)</td><td>piloting (Coordination)</td><td>charioteering (Coordination)</td></tr>
<tr><td>vehicle operation (Mechanical)</td><td>piloting (Coordination)</td><td>charioteering, pilotry (Coordination)</td></tr>
</table>

---

<h3 id="repair">Repair</h3>

> Fixing damaged armor. (*Space*, **armor repair**)

> Creating, fixing, or modifying equipment, weapons, armor, and
> vehicles, as well as woodworking, metalworking, constructing
> buildings, and the like. (*Fantasy*, **crafting**)

> Repairing and modifying exoskeletons, powered armor, environmental
> suits, and similar suits. (*Space*, **exoskeleton repair**)

> Repairing and modifying any gun a person can carry, from small slug
> throwers to shoulder-launched rockets. (*Space*, **firearms repair**)

> Fixing damaged systems aboard flying vehicles and spaceships.
> (*Space*, **flight systems repair**)

> Fixing weapons mounted on powered armor, vehicles, or spaceships, or
> within fortresses. (*Space*, **gunnery repair**)

> Fixing small electronic equipment, including damaged cybernetics.
> (*Space*, **personal equipment repair**)

> Fixing ground- and ocean-based vehicles that do not fly.
> (*Space*, **vehicle repair**)

> Creating, fixing, or modifying gadgets, weapons, armor, and
> vehicles. (*Adventure*, **repair**)

In *Space*, Repair is split into seven specialized skills representing
distinct technologies.

Repairing machinery is an essential function of the Mechanical attribute,
making Repair a core skill.

<table>
<caption>genre conversion for Repair</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>repair only skills (Technical)</td><td>repair (Perception)</td><td>crafting (Acumen)</td></tr>
</table>

---

<h3 id="riding">Riding</h3>

> Controlling and riding domesticated mounts. (*Adventure, *Space*,
> *Fantasy*, **riding**)

<table>
<caption>genre conversion for Riding</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>riding (Agility)</td><td>riding (Reflexes)</td><td>riding (Agility)</td></tr>
</table>

---

<h3 id="running">Running</h3>

> Moving quickly on the ground while avoiding obstacles and keeping
> from stumbling. (*Adventure*, *Fantasy*, **running**)

> Moving quickly while avoiding obstacles and keeping from stumbling.
> (*Space*, **running**)

<table>
<caption>genre conversion for Running</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>running (Agility)</td><td>running (Physique)</td><td>running (Physique)</td></tr>
</table>

---

<h3 id="scholar">Scholar</h3>

> Understanding of aliens not of the character's own species and their
> physiology, customs, and history. (*Space*, **aliens**)

> Familiarity with astrographic features (planets, star systems,
> nebulae), and general knowledge of any civilized elements present
> (settlements, industry, government, orbital installations).
> (*Space*, **astrography**)

> Knowledge of and ability to use a bureaucracy's intricate procedures
> to gain information, and favors, or attain other goals.
> (*Space*, **bureaucracy**)

> Understanding of the manners, customs, and social expectations of
> different cultures, including one's own. (*Space*, **cultures**)

> This skill represents knowledge and/or education in areas not
> covered under any other skill[...]. This may be restricted to a
> specific field (represented by specializations) or a general
> knowledge of a wide range of subjects. It is used to remember
> details, rumors, tales, legends, theories, important people, and the
> like, as appropriate for the subject in question. However, the
> broader the category, the fewer the details that can be recalled. It
> covers what the character himself can recall. Having another skill
> as a specialization of the scholar skill means that the character
> knows the theories and history behind the skill but can't actually
> use it. Scholar can be useful with investigation to narrow a search
> for information. (*Adventure*, *Space*, *Fantasy*, **scholar**)

> Understanding of how law enforcement organizations, regulations, and
> personnel operate. (*Space*, **security regulations**)

> Familiarity with deploying military forces and maneuvering them to
> the best advantage. (*Space*, **tactics**)

My theory is that Scholar is an essential catch-all skill that should
encompass a lot of knowledge-centric optional skills as specializations, and
each genre book has "spun-off" several into individual skills of their own for
flavor; the most obvious example being the Knowledge skills of D6 Space.

In *D6 Adventure*, *D6 Space*, scholar falls under the Knowledge attribute. In
*D6 Fantasy* it falls under the Intellect attribute.

Scholar might accumulate the following skills under its domain. Some of them
are separate skills in certain genre books, while others are suggested in the
description(s) of Scholar itself.

- *aliens* (Knowledge (*Space*))
- *alchemy* (Scholar (*Fantasy*))
- *arcane lore* (Scholar (*Fantasy*))
- *archeology*  (Scholar (*Adventure*, *Space*))
- *art* (Scholar (*Space*))
- *astrography* (Knowledge (*Space*))
- *bureaucracy* (Knowledge (*Space*))
- *chemistry* (Scholar (*Adventure*, *Space*))
- *cooking* (Scholar (*Space*, *Fantasy*))
- *cultures* (Knowledge (*Space*)), (Intellect (*Fantasy*))
- *interior design* (Scholar (*Adventure*))
- *mathematics* (Scholar (*Adventure*, *Space*))
- *security regulations* (Knowledge (*Space*))
- *tactics* (Knowledge (*Space*))

The defining characteristic of Scholar is specializing in academic knowledge
of a subject, but needing other skills in order to properly apply the
knowledge.

Education in various fields is an essential function of the Knowledge
attribute, making Scholar a core skill.

<table>
<caption>genre conversion for Scholar</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>aliens (Knowledge)</td><td>scholar (Knowledge)</td><td>scholar or cultures (Intellect)</td></tr>
<tr><td>astrography (Knowledge)</td><td>scholar, navigation (Knowledge)</td><td>scholar, navigation (Intellect)</td></tr>
<tr><td>bureaucracy (Knowledge)</td><td>scholar (Knowledge)</td><td>scholar or cultures (Intellect)</td></tr>
<tr><td>cultures (Knowledge)</td><td>scholar (Knowledge)</td><td>cultures (Intellect)</td></tr>
<tr><td>scholar (Knowledge)</td><td>scholar (Knowledge)</td><td>scholar (Intellect)</td></tr>
<tr><td>security regulations (Knowledge)</td><td>security (Knowledge)</td><td>scholar, cultures (Intellect)</td></tr>
<tr><td>tactics (Knowledge)</td><td>scholar (Knowledge), command (Presence)</td><td>scholar (Intellect), command (Charisma)</td></tr>
</table>

---

<h3 id="search">Search</h3>

> Spotting hidden objects or people, reconnoitering, lipreading, or
> eavesdropping on or watching another person. (*Adventure*, *Fantasy*,
> **search**)

> Spotting hidden objects or people, reconnoitering, lipreading,
> eavesdropping on or watching other people, or tracking the trails
> they've left behind. (*Space*, **search**)

> Following the trail of another person, animal, or creature, or
> keeping after a moving target without being noticed. (*Adventure*,
> *Fantasy*, **tracking**)

Noticing something is an essential function of the Perception attribute, making
Search a core skill.

<table>
<caption>genre conversion for Search</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>search (Perception)</td><td>search, tracking (Perception)</td><td>search, tracking (Acumen)</td></tr>
</table>

---

<h3 id="security">Security</h3>

> Setting explosives to achieve particular destructive effects.
> (*Space*, **demolition**)

> Using corrosives and explosives to achieve particular destructive
> effects. (*Adventure*, **demolitions**)

> Installing, altering, and bypassing electronic security and
> surveillance systems. (*Adventure*, *Space*, **security**)

> Installing, altering, and bypassing security devices, as well as
> identifying various kinds of traps (gas, pit, wire-triggered, etc.).
> (*Fantasy*, **traps**)

Across all three genre books, Security and Traps are clearly related. The
description for Security in *D6 Adventure*, *D6 Space* are identical.
*D6 Fantasy* has the similar skill Traps, but omits advanced electronics.
Demolition from *D6 Adventure*, *D6 Space* falls under the description of Traps
in *D6 Fantasy*.

Security is unfortunately named, as the word fails to encompass both Traps and
Demolition. The overarching theme seems to be *automated devices*, as security
and surveillence systems, traps, and explosive charges are all meant to be
operated remotely or automatically.

<table>
<caption>genre conversion for Security</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>demolitions (Technical)</td><td>demolition (Knowledge)</td><td>traps (Intellect)</td></tr>
<tr><td>security (Technical)</td><td>security (Knowledge)</td><td>traps (Intellect)</td></tr>
</table>

---

<h3 id="sleightofhand">Sleight of Hand</h3>

> Opening a mechanical lock without possessing the key or combination.
> (*Fantasy*, **lockpicking**)

> Opening a mechanical (not electronic) lock or safe without
> possessing the key or combination, as well as disarming small
> mechanical traps. (*Adventure*, **lockpicking**)

> Nimbleness with the fingers and misdirection, including picking
> pockets, palming items, and stage magic. (*Adventure*, *Fantasy*,
> **sleight of hand**)

> Nimbleness with the fingers, including picking pockets, palming
> items, and opening mechanical locks. (*Space*, **sleight of hand**)

Lockpicking is a peculiar skill across genres and seems to reflect relative
technology level. In *D6 Fantasy* lockpicking is a simple and specific skill
for opening two kinds of locks. In *D6 Adventure* the craft has expanded into
disarming simpler non-electronic traps. B *D6 Space* lockpicking has been
subsumed into simple dexterity and Sleight of Hand.

This progression, I suppose, suggests in a fantasy setting where technology is
new and primitive, people specialize in picking locks. While in a modern
adventure technology is more familiar and a general knowledge of how to
disable mechanical devices allows one to disable locks and traps. Finally, in
a sci-fi setting lockpicking is such a general knowledge that one only need
a bit of deftness with the fingers to open a lock. In the latter two cases, a
knowledge of Security systems is required to handle anything more complicated
than a simple mechanical lock or trap.

Lockpicking or Sleight of Hand is always governed by the attribute
Coordination or Agility.

Feats of manual dexterity are an essential function of the Coordination
attribute, making Sleight of Hand a core skill.

<table>
<caption>genre conversion for Sleight of Hand</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr>
<td>sleight of hand (Agility)</td><td>sleight of hand, lockpicking (Coordination)</td><td>sleight of hand, lockpicking (Coordination)</td>
</tr>
</table>

---

<h3 id="stamina">Stamina</h3>

> Physical endurance and resistance to pain, disease, and poison.
> (*Adventure*, *Space*, *Fantasy*, **stamina**)

Bodily endurance and resisting damage are essential functions of the Endurance
attribute, making Stamina a core skill and a core combat skill.

<table>
<caption>genre conversion for Stamina</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>stamina (Strength)</td><td>stamina (Physique)</td><td>stamina (Physique)</td></tr>
</table>

---

<h3 id="sneak">Stealth</h3>

> Moving silently and avoiding detection, whether through shadows or
> crowds. (*Adventure*, **sneak**, *Fantasy*, **stealth**)

> Moving silently, avoiding detection and hiding one-self. (*Space*,
> **sneak**)

Sneak and Stealth are nearly identical. <cite>D6 Space</cite> specifies "hiding one-self",
suggesting Hide is specifically about hiding and disguising objects.

<table>
<caption>genre conversion for Stealth</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>sneak (Perception)</td><td>sneak (Reflexes)</td><td>stealth (Agility)</td></tr>
</table>

---

<h3 id="streetwise">Streetwise</h3>

> Finding information, goods, and contacts in an urban environment,
> particularly through criminal organizations, black markets, and other
> illicit operations. Also useful for determining possible motives and
> methods of criminals. (*Adventure*, *Space*, **streetwise**)

> Finding information, goods, and contacts in an urban environment,
> particularly through thieves' guilds and similar criminal
> organizations, black markets, and other illicit operations. Also
> useful for determining possible motives and methods of criminals.
> (*Fantasy*, **streetwise**)

<table>
<caption>genre conversion for Streetwise</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>streetwise (Knowledge)</td><td>streetwise (Perception)</td><td>streetwise (Acumen)</td></tr>
</table>

---

<h3 id="survival">Survival</h3>

> Surviving in wilderness environments. (*Adventure*, **survival**)

> Knowledge of techniques for surviving in hostile, wilderness
> environments, as well as the ability to handle animals.
> (*Space*, **survival**)

> Surviving in wilderness environments, including the ability to
> identify plants, animals, and their nutritional and medicinal uses.
> (*Fantasy*, **survival**)

<table>
<caption>genre conversion for Survival</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>survival (Knowledge)</td><td>survival (Perception)</td><td>survival (Acumen)</td></tr>
</table>

---

<h3 id="swimming">Swimming</h3>

> Moving and surviving in a liquid medium. (*Adventure*, *Space*,
> *Fantasy*, **swimming**)

<table>
<caption>genre conversion for Swimming</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>swim (Strength)</td><td>swimming (Physique)</td><td>swimming (Physique)</td></tr>
</table>

---

<h3 id="tech">Tech</h3>

> Effectively using communication devices and arrays. (*Space*,
> **comm**)

> Using and designing complex mechanical equipment. Actually building
> items requires crafting. (*Fantasy*, **devices**)

> Programming, interfacing with, and fixing computer systems.
> (*Space*, **computer interface/repair**)

> Programming, interfacing with and fixing robots and their systems.
> (*Space*, **robot interface/repair**)

> Operating scanner arrays to gather information about one's
> surroundings. (*Space*, **sensors**)

> Deploying and redirecting shields aboard vehicles. (*Space*,
> **shields**)

> Using and designing (not making) complex mechanical or electronic
> equipment, such as programming and operating computers and
> manipulating communication devices. (*Adventure*, **tech**)

In *Space*, Tech is split into a few different skills like Comm,
Sensors, and Shields.

Both Tech and Devices clearly forbid using the skill to *construct* anything.
This is a skill for using a technology for its intended purpose. Building and
modifying anything falls under Repair or Crafting.

Aptitude with technological equipment is an essential function of the Technical
attribute, making Tech a core skill.

<table>
<caption>genre conversion for Tech</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>comm (Mechanical)</td><td>tech (Knowledge)</td><td>devices (Intellect)</td></tr>
<tr><td>sensors (Mechanical)</td><td>tech (Knowledge)</td><td>devices (Intellect)</td></tr>
<tr><td>shields (Mechanical)</td><td>tech (Knowledge)</td><td>devices (Intellect)</td></tr>
<tr><td>interface/repair skills (Technical)</td><td>tech (Knowledge)</td><td>devices (Intellect)</td></tr>
</table>

---

<h3 id="throwing">Throwing</h3>

> Hitting a target accurately with a thrown item, including grenades,
> stones, and knives. Also used for catching thrown items. (Using or
> modifying grenades as explosives for special destructive effects
> requires the demolitions skill.) (*Adventure*, *Space*, **throwing**)

> Hitting a target accurately with a thrown item, including stones,
> javelins, bottles, and knives. Also used for catching thrown items.
> (*Fantasy*, **throwing**)

<table>
<caption>genre conversion for Throwing</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>throwing (Agility)</td><td>throwing (Coordination)</td><td>throwing (Coordination)</td></tr>
</table>

---

<h3 id="willpower">Willpower</h3>

> Ability to withstand stress, temptation, other people's interaction
> attempts, mental attacks, and pain. The Game Master may allow a
> specialization in a specific faith tradition or belief system to
> enhance many, though not all, applications of willpower.
> (*Adventure*, *Space*, **willpower**)

> Ability to withstand stress, temptation, other people's interaction
> attempts, mental attacks, and pain. The Game Master may allow a
> specialization in a specific faith tradition or belief system to
> enhance many, though not all, applications of mettle. (*Fantasy*,
> **mettle**)

Withstanding mental attacks is an essential function of the Willpower
attribute, making Willpower a core skill.

<table>
<caption>genre conversion for Willpower</caption>
<tr><td><strong>Space</strong></td><td><strong>Adventure</strong></td><td><strong>Fantasy</strong></td></tr>
<tr><td>willpower (Knowledge)</td><td>willpower (Presence)</td><td>mettle (Charisma)</td></tr>
</table>

---

All Common Skills Sorted by Attribute
-------------------------------------

All 37 *common skills* can be sorted by a governing attribute taken from
*System Book*. There are skills that can functionally replace an attribute if
it is absent in a particular genre. For example: Endurance is absent from every
genre book and is functionally replaced by Stamina. These are called
*core skills*.

The D6 system employs a combat system that normally requires 4 core attributes.
The genre books instead provide 5 skills to cover combat situations, so the
combat system can work if any of the core attributes are absent. These are
called *core combat skills*.

Making a new genre book or system does *not* require including all 37 common
skills. This table is a guide to skills that *may* be appropriate to include in
a new genre or game.

<table>
<tr><th>Coordination</th>		<th>Endurance</th>		<th>Reflexes</th>		<th>Strength</th></tr>
<tr><td>Marksmanship&dagger;</td>	<td>Stamina&ast;&dagger;</td>	<td>Acrobatics&ast;</td>	<td>Climb/Jump</td></tr>
<tr><td>Sleight of Hand&ast;</td>	<td>Swimming</td>		<td>Brawling</td>		<td>Lifting&ast;&dagger;</td></tr>
<tr><td>Throwing</td>			<td></td>			<td>Dodge&dagger;</td>		<td></td></tr>
<tr><td></td>				<td></td>			<td>Flying</td>			<td></td></tr>
<tr><td></td>				<td></td>			<td>Melee Combat&dagger;</td>	<td></td></tr>
<tr><td></td>				<td></td>			<td>Running</td>		<td></td></tr>
<tr><td></td>				<td></td>			<td>Sneak</td>			<td></td></tr>
<tr><td></td>				<td></td>			<td></td>			<td></td></tr>

<tr><th>Intellect</th>			<th>Knowledge</th>		<th>Mechanical</th>		<th>Perception</th></tr>
<tr><td>Investigation</td>		<td>Business</td>		<td>Piloting</td>		<td>Gambling</td></tr>
<tr><td>Know-how&ast;</td>		<td>Languages</td>		<td>Repair&ast;</td>		<td>Hide</td></tr>
<tr><td>Navigation</td>			<td>Scholar&ast;</td>		<td>Riding</td>			<td>Search&ast;</td></tr>
<tr><td></td>				<td>Survival</td>		<td></td>			<td>Streetwise</td></tr>
<tr><td></td>				<td></td>			<td></td>			<td></td></tr>

<tr><th>Confidence</th>			<th>Technical</th>		<th>Willpower</th>		<th></th></tr>
<tr><td>Command</td>			<td>Artist</td>			<td>Willpower&ast;</td>		<td></td></tr>
<tr><td>Con</td>			<td>Medicine</td>		<td></td>			<td></td></tr>
<tr><td>Intimidation</td>		<td>Security</td>		<td></td>			<td></td></tr>
<tr><td>Persuasion&ast;</td>		<td>Tech&ast;</td>		<td></td>			<td></td></tr>
<tr><td></td>				<td></td>			<td></td>			<td></td></tr>
<tfoot>
<tr><td>&ast; core skill</td></tr>
<tr><td>&dagger; core combat skill</td></tr>
</tfoot>
</table>


References
----------

- [*D6 System Book (WEG51005)*][sb]
- *D6 Adventure (WEG51011)* [attribute and skill list][a]
- *D6 Space (WEG51012)* [attribute and skill list][s]
- *D6 Fantasy (WEG51013)* [attribute and skill list][f]

[sb]: https://ogc.rpglibrary.org/images/b/bf/D6_System_Book_weg51005eOGL.pdf "D6 System Book"
[a]: http://opend6project.org/chapter-1-character-basics/adventure-attribute-and-skill-list/ "D6 Adventure"
[s]: http://opend6project.org/chapter-1-character-basics/space-attribute-and-skill-list/ "D6 Space"
[f]: http://opend6project.org/chapter-1-character-basics/fantasy-attribute-and-skill-list/ "D6 Fantasy"


Links
-----

- [Cognition - Wikipedia](https://en.wikipedia.org/wiki/Cognition)
- [Genre Conversion - The OpenD6 Project](http://opend6project.org/section-4-game-basics/genre-conversion/)
- [Gymnastics - Wikipedia](https://en.wikipedia.org/wiki/Gymnastics)
- [Land navigation - Wikipedia](https://en.wikipedia.org/wiki/Land_navigation)
- [Markdown - Daring Fireball](https://daringfireball.net/projects/markdown/)
- [Mechanics - Wikipedia](https://en.wikipedia.org/wiki/Mechanics)
- [MLA Formatting and Style Guide](https://owl.purdue.edu/owl/research_and_citation/mla_style/mla_formatting_and_style_guide/mla_formatting_and_style_guide.html)
- [Motor skill - Wikipedia](https://en.wikipedia.org/wiki/Motor_skill)
- [Piloting - Wikipedia](https://en.wikipedia.org/wiki/Piloting)
- [OpenD6 Products](https://ogc.rpglibrary.org/index.php?title=OpenD6)
- [OpenD6 Project](http://opend6project.org/)
- [Style Guide - The OpenD6 Project](http://opend6project.org/style-guide/)
- [Technology - Wikipedia](https://en.wikipedia.org/wiki/Technology)
