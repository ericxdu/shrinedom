Introductory Scenario for Pocket Hero
=====================================

Before beginning, you should create your character if you haven't already. Upon starting Pocket Hero (see the link at the bottom of this page if you don't have it yet) you'll be shown the "Attributes" screen where you can increase and decrease your score in each of the seven attributes. Each attribute starts at 2D (two dice). You have enough points to increase every attribute to 3D, but you may want to have higher scores in some attributes in exchange for lower scores in attributes you consider less important.

Use the D-Pad to move the cursor and the A button to select an element on the screen. You can push A over an attribute name to learn more about what that attribute is used for. Push A over the actual score to open a prompt to increase or decrease the score. When you are done assigning scores, push A over the icon in the upper right-hand corner to save your character and again to begin rolling dice.

Start this introductory scenario by reading prompt 1 below and following the instructions.

<center><h2 id="1">1</h2></center>

As you enter the saloon, you notice how quiet and empty it is. Only two men playing cards at one table and a solitary drunk at another. Suddenly, upon looking up from his cups, the drunk rises to his feet and staggers toward you.

- Before you decide what to do, let's find out who goes first: <a href="#12">go to 12</a>.

<center><h2 id="2">2</h2></center>

You rolled a critical error on the Wild Die. A scenario may describe something unlucky that happens as a result of a critical error, or it may just represent a low total result. For this scenario, a critical error means an automatic failure.

- <a href="#5">Go to 5</a>.

<center><h2 id="3">3</h2></center>

"I don't want to have to hurt you" you threaten menacingly through a fierce glower. The drunk is intimidated by your display, and it shakes him out of his unprovoked stupor. He slowly backs away a few steps and then stumbles back to his table, sitting roughly and staring at his half-empty whiskey.

The bartenter appears at your side. "I'm glad he didn't try anything. We had some worse trouble with him yesterday. Let me offer you a drink on the house."

- <a href="#13">Go to 13</a>.

<center><h2 id="4">4</h2></center>

You decide to knock some sense into the drunk, but your reflexes must be off tonight, because you miss with your right hook as he lurches clumsily out of the way. He takes this chance to give you a shove squarely on the shoulders, which puts you off balance.

It is then that the bartender, having come swiftly around the bar at the first sign of trouble, reaches the two of you and grabs your assailant by the shirt collars. "I told you not to make trouble in here again. Now git!" he says as he shoves the drunk toward the swinging doors and plants a boot on his rear end for good measure. Turning to you he apoligizes, noting that his bouncer isn't due around for another fifteen minutes, and offers you a drink on the house.

- <a href="#13">Go to 13</a>.

<center><h2 id="5">5</h2></center>

Before you can decide whether he is a threat or not, the stranger makes a wild swing that connects with your stomach. At this point, you must make an Endurance roll to determine whether you take wounds. If your result is 5 or higher, you take no wounds. If your result is 3 or 4, you take one wound. If your result is 2, you take two wounds.

Now that he has acted, its your turn.

- To persuade him to back off, <a href="#7">go to 7</a>.
- To teach him a lesson, <a href="#9">go to 9</a>.

<center><h2 id="6">6</h2></center>

You rolled a critical succcess on the Wild Die. This always means you get to add the 6 to your total result and reroll the Wild Die. Pocket Hero takes care of adding the results and rerolls for you. In addition, a scenario may describe something lucky or exceptional that happens as the result of a critical success. In this scenario, it simply results in an automatic success.

- Go to 10</a>.

<center><h2 id="7">7</h2></center>

You can make a Perception roll to convince your attacker to give up the fight and walk away, but it will take a silver tongue or an imposing demeanor to succeed at this Moderate difficulty action.

- If the Perception result is greater than or equal to 15, <a href="#3">go to 3</a>.
- If the Perception result is less than 15, <a href="#14">go to 14</a>.

<center><h2 id="8">8</h2></center>

You manage to punch your assailant squarely in the jaw, which rattles 
him. Already staggering and out-of-sorts, he trips on his boots and falls to the floorboards with a pained groan.

It is then that the bartender, having come swiftly around the bar at the first sign of trouble, gives the drunk a kick in the rear and exclaims "I told you not to make trouble in here again. When you can stand again you're gonna gather your things and get out." He then apologizes to you and offers you a drink on the house.

- <a href="#13">Go to 13</a>.

<center><h2 id="9">9</h2></center>

If you decide to attack your opponent, you must make another Reflexes 
roll for hand-to-hand combat. The difficulty of hitting an attack is 
usually 10, but this man leaves an opening in his defenses and the 
number to beat is lower. However, if you took wounds at prompt 5, you must subtract 1 or 2 from your Reflexes result.

- If the Reflexes result is greater than or equal to 5, <a href="#8">go to 8</a>.
- If the Reflexes result is less than 5, <a href="#4">go to 4</a>.

<center><h2 id="10">10</h2></center>

You're quicker on your feet than your opponent, so you get to make the first move.

- To talk some sense into him, <a href="#7">go to 7</a>.
- To make a show of force, <a href="#9">go to 9</a>.

<center><h2 id="11">11</h2></center>

Since the Wild Die didn't show a result of 1 or 6, nothing special 
happens for this Reflexes roll. Its a simple success or failure.

- If the Reflexes total is greater than or equal to 6, <a href="#10">go to 10</a>.
- If the Reflexes total is less than 6, <a href="#5">go to 5</a>.

<center><h2 id="12">12</h2></center>

To find out who acts first you must make a Reflexes roll. Pay attention to the first result on the Wild Die in case the scenario asks for it.

- If you rolled a 1 on the Wild Die, <a href="#2">go to 2</a>.
- If you rolled a 6 on the Wild Die (before rerolling), <a href="#6">go to 6</a>.
- If you rolled any other number on the Wild Die, read your total result and <a href="#11">go to 11</a>.

<center><h2 id="13">13</h2></center>

With the encounter over and nothing more to do about it, you decide to retire to your rooms upstairs.

- <a href="#15">Go to 15</a>.

<center><h2 id="14">14</h2></center>

The man is not convinced, and this time he attempts to give you a shove squarely on the shoulders. However, he misjudges his momentum and ends up tripping on his own boots, landing hard on his side and groaning in pain on the floorboards.

It is then that the bartender, having come swiftly around the bar at the first sign of trouble, gives the drunk a kick in the rear and exclaims "I told you not to make trouble in here again. When you can stand again you're gonna gather your things and get out." He then apologizes to you and offers you a drink on the house.

- <a href="#13">Go to 13</a>.

<center><h2 id="15">15</h2></center>

This is a short adventure designed to show how OpenD6 and Pocket Hero 
can work in a typical scene. Try it again a few times to familiarize 
yourself with the system and experience the results of different choices and attribute rolls. You may like to write your own text adventures using these rules, and that's easy with a text editor that can publish to HTML or PDF. For further information about the OpenD6 system, you can find the official rulebook at the following links and stay tuned for detailed rules explanations on this website.

- [D6 System Book at D6 Holocron](http://www.d6holocron.com/downloads/books/weg51005OGL.pdf) - high quality scan
- [D6 System Book at RPG Library](https://ogc.rpglibrary.org/index.php?title=File:D6_System_Book_weg51005eOGL.pdf) - smaller file
- [Pocket Hero -to be released-](https://radicalgnu.itch.io/pocket-hero)


Legal
-----

This scenario in its entirety is Open Game Content used and provided under the terms of the Open Game License. It is based on Open Game Content published in <cite>D6 Adventure</cite> by Purgatory Publishing.


OPEN GAME LICENSE Version 1.0a 
<br>
The following text is the property of Wizards of the Coast, Inc. and is
Copyright 2000 Wizards of the Coast, Inc ("Wizards"). All Rights Reserved.
<br>
1. Definitions: (a)"Contributors" means the copyright and/or trademark owners
who have contributed Open Game Content; (b)"Derivative Material" means
copyrighted material including derivative works and translations (including into
other computer languages), potation, modification, correction, addition,
extension, upgrade, improvement, compilation, abridgment or other form in which
an existing work may be recast, transformed or adapted; (c) "Distribute" means
to reproduce, license, rent, lease, sell, broadcast, publicly display, transmit
or otherwise distribute; (d)"Open Game Content" means the game mechanic and
includes the methods, procedures, processes and routines to the extent such
content does not embody the Product Identity and is an enhancement over the
prior art and any additional content clearly identified as Open Game Content by
the Contributor, and means any work covered by this License, including
translations and derivative works under copyright law, but specifically excludes
Product Identity. (e) "Product Identity" means product and product line names,
logos and identifying marks including trade dress; artifacts; creatures
characters; stories, storylines, plots, thematic elements, dialogue, incidents,
language, artwork, symbols, designs, depictions, likenesses, formats, poses,
concepts, themes and graphic, photographic and other visual or audio
representations; names and descriptions of characters, spells, enchantments,
personalities, teams, personas, likenesses and special abilities; places,
locations, environments, creatures, equipment, magical or supernatural abilities
or effects, logos, symbols, or graphic designs; and any other trademark or
registered trademark clearly identified as Product identity by the owner of the
Product Identity, and which specifically excludes the Open Game Content; (f)
"Trademark" means the logos, names, mark, sign, motto, designs that are used by
a Contributor to identify itself or its products or the associated products
contributed to the Open Game License by the Contributor (g) "Use", "Used" or
"Using" means to use, Distribute, copy, edit, format, modify, translate and
otherwise create Derivative Material of Open Game Content. (h) "You" or "Your"
means the licensee in terms of this agreement.
<br>
2. The License: This License applies to any Open Game Content that contains a
notice indicating that the Open Game Content may only be Used under and
in terms of this License. You must affix such a notice to any Open Game Content
that you Use. No terms may be added to or subtracted from this License
except as described by the License itself. No other terms or conditions may be
applied to any Open Game Content distributed using this License.
<br>
3. Offer and Acceptance: By Using the Open Game Content You indicate Your
acceptance of the terms of this License.
<br>
4. Grant and Consideration: In consideration for agreeing to use this License,
the Contributors grant You a perpetual, worldwide, royalty-free, non-exclusive
license with the exact terms of this License to Use, the Open Game Content.
<br>
5.Representation of Authority to Contribute: If You are contributing original
material as Open Game Content, You represent that Your Contributions are Your
original creation and/or You have sufficient rights to grant the rights conveyed
by this License.
<br>
6.Notice of License Copyright: You must update the COPYRIGHT NOTICE portion of
this License to include the exact text of the COPYRIGHT NOTICE of any Open Game
Content You are copying, modifying or distributing, and You must add the title,
the copyright date, and the copyright holder's name to the COPYRIGHT NOTICE of
any original Open Game Content you Distribute.
<br>
7. Use of Product Identity: You agree not to Use any Product Identity, including
as an indication as to compatibility, except as expressly licensed in another,
independent Agreement with the owner of each element of that Product Identity.
You agree not to indicate compatibility or co-adaptability with any Trademark or
Registered Trademark in conjunction with a work containing Open Game Content
except as expressly licensed in another, independent Agreement with the owner of
such Trademark or Registered Trademark. The use of any Product Identity in Open
Game Content does not constitute a challenge to the ownership of that Product
Identity. The owner of any Product Identity used in Open Game Content shall
retain all rights, title and interest in and to that Product Identity.
<br>
8. Identification: If you distribute Open Game Content You must clearly
indicate which portions of the work that you are distributing are Open Game
Content.
<br>
9. Updating the License: Wizards or its designated Agents may publish updated
versions of this License. You may use any authorized version of this License to
copy, modify and distribute any Open Game Content originally distributed under
any version of this License.
<br>
10. Copy of this License: You MUST include a copy of this License with every
copy of the Open Game Content You Distribute.
<br>
11. Use of Contributor Credits: You may not market or advertise the Open Game
Content using the name of any Contributor unless You have written permission
from the Contributor to do so.
<br>
12. Inability to Comply: If it is impossible for You to comply with any of the
terms of this License with respect to some or all of the Open Game Content due
to statute, judicial order, or governmental regulation then You may not Use any
Open Game Material so affected.
<br>
13. Termination: This License will terminate automatically if You fail to
comply with all terms herein and fail to cure such breach within 30 days of
becoming aware of the breach. All sublicenses shall survive the termination of
this License.
<br>
14. Reformation: If any provision of this License is held to be unenforceable,
such provision shall be reformed only to the extent necessary to make it
enforceable.
<br>
15. COPYRIGHT NOTICE 
Open Game License v 1.0a Copyright 2000, Wizards of the Coast, Inc. 
<br>
D6 Adventure (WEG51011), Copyright 2004, Purgatory Publishing Inc
<br>
Introductory Scenario for Pocket Hero, Copyright 2023, Eric Abides.
<br>
West End Games, WEG, and D6 System are trademarks and properties of Purgatory Publishing Inc.
<br>
PRODUCT IDENTIFICATION:
<br>
Product Identity: The D6 System; the D6 trademarks, the D6 and related logos and any derivative trademarks not specified as Open Game Content;
and all cover and interior art and trade dress are designated as Product Identity (PI) and are properties of Purgatory Publishing Inc. All rights reserved.
<br>
Open Game Content: All game mechanics and material not covered under Product Identity (PI) above; OpenD6 trademark and OpenD6 logo (as
displayed on this document cover page).
<br>
END OF LICENSE
