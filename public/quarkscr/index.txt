# Getting Started

Quake maps are compiled, meaning human-readable source data is 
interpreted and translated into a quake-readable binary file. The 
geometry, lighting, visibility, and textures are baked into the 
binary. Enemies, pickups, sounds, and other elements are only 
referenced and must exist elsewhere in the game data.

Therefore, each map file in a Quake mod exist on their own but must 
be accompanied by a library of files if there are to be any objects 
in them.

The start menu and pause menu appear to be a part of the game data as
well.
