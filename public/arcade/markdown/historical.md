Historical Games
================

As the free software movement strives to make more and more useful 
software for people to use in freedom, its useful to look to the past 
and find classic games that have yet to inspire a satisfying open 
source clone (a new version that satisfies the ideals of portability, 
accessibility, and reproducability). In looking through the past, you 
may even find an interesting historical perspective, like the various 
"firsts" of each video game genre.

- Artillery
- Blockade
- Bosconian
- Nebulus
- Clean Sweep
- Pipe Mania
- Scramble
- Thrust

