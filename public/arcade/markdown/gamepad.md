The Gamepad
===========


Intuitive Expectations
----------------------

Broadly speaking, if you plug a gamepad into the USB port you expect it to do something. At the very least you expect it to control the next video game you start up, or the one you are currently playing. You might even expect it to interact with the desktop in some way. Although its very complicated to make these expectations a reality, these expectations are reasonable.

Difference Between Input Devices
--------------------------------

On a basic level, connected devices have no difference from each other. Each keyboard, pointer, and joystick is a generic and discreet event device that sends standardized events for applications to react to. In practice however, devices are necessarily treated differently.

Keyboards and Pointers
----------------------

Keyboard and mouse devices are important to the operation of the computer and are given special treatment and consideration by the kernel and downstream applications. The computer is tuned to detect and connect nearly any keyboard or pointing device on the market without fail. The desktop environment then detects all devices reported by the kernel and funnels their input into a single pointer on the screen and a single keyboard layout to send keys to applications that have focus.

This is the most reliable form of input for applications to receive; a computer game can *always* expect the operating system to provide a pointer and keyboard device and the user can rely on any such device to work if plugged in. This results in the majority of games supporting at the very least keyboard and/or pointer control.

This would point to an obvious solution: standardize around controllers acting as keyboard/mouse input devices sending keydown/keyup and relative motion events from the kernel. The main problem is multiplayer support. The desktop environment doesn't differentiate between which device is sending the keyboard and pointer events, and it doesn't make sense to send e.g. `KEY_A` and `KEY_Z` from on controller's buttons and `KEY_N` and `KEY_M` from another so they can be told apart by the game. Games must detect individual gamepad devices and sort identical buttons into different player inputs.

Joysticks
---------

Gamepads are somewhat less reliably supported by the kernel but are much more appropriate for games. Supporting them takes more effort because instead of just detecting keys sent to the game, it must often detect and register joystick devices and to start receiving events from them. Interruptions in the connection can result in temporary or permanent inability to continue playing.


Application Support
-------------------

Individual games supporting each gamepad is probably most difficult goal of this expectation. Due to a lack of standards for several decades, games have been developed with widely varying joystick support. Here is a list of issues gathered from testing during a 12-year period (2012-2024).

- games with incomplete controller support

  Some games have no way to quit using a controller. Worse games have no way to navigate their menu or start the game. They expect you to control primarily with the keyboard and use the joystick only to control the game objects. Examples: Atari800.

- games with poor calibration

  Some games do not support joystick control very well. They will detect motion when the stick is in the neutral position. Moving the stick in the menu will rapidly shuffle through options instead of selecting one at a time. Games that would benefit from snappy, accurate control from the D-Pad will ignore it and only detect the first stick. Examples: Kobo Deluxe.

- game with unstandardized axes

  Some games use more than two axes (the first stick) and that can cause problems. SDL detects the left trigger of the Xbox controller as the third axis (right stick left-right) and squeezes it into a range of -32768 to 32768 that causes the game to think the neutral position is the player deflecting it to the left. This can make for instance the camera constantly move with no reliable way to stop it.

### Linux Gamepad API

The new Linux gamepad API offers a standardization of controls that computer games can benefit from. Following the API a game can standardize on a Dualshock-like controller scheme similar to that produced by Xboxdrv. It doesn't even need to detect the controller name or event names since the standard ensures there is no button before `BTN_SOUTH` and no tricky `ABS_Z` axis.

### Source Modification

Open source games can be updated to properly support the new controller standards in the gamepad API. Examples: Kobo Deluxe and Minetest.

### Curation

Games can be curated for the controller experience, leaving aside anything that has incomplete or no support and sorting the rest into a category like "Games | Gamepad Support".

### SDL

As mentioned in the document, historically SDL flattened gamepads into numbered inputs and standardized ranges. This makes it impossible for an SDL game to tell if you are pressing `BTN_LB` or `BTN_C`, and indeed makes a second stick nearly impossible to use since the game cannot tell if you are deflecting `ABS_RX` or `ABS_Z`.

### Wine

For Windows games that cannot be modified to support the gamepad API, Wine can be modified to remap controllers to properly support them.

### Windows, MacOS, and Android

This document describes computers and stardards from a GNU/Linux perspective. Other operating systems can be expected to support controllers in their own way, out of scope with this document. However, user-space tools like SC Controller can be considered a part of gamepad standards described here.


Hypervisor
----------

A major quality-of-life improvement for users would be to implement an overlay accessible by the controller at all levels. Only controllers fully supporting the gamepad API would probably support this overlay by pressing the system button `BTN_MODE`. This should be conceptualized as a "gamepad helper" that provides absolute overview control of the system to anyone with access to such a controller; including pausing/quitting the current game at the very least, but also providing access to a foolproof remapper and calibrator in addition to a games launcher.


References
----------

- [Linux Gamepad API](files/gamepad.txt)


Links
-----

- [Linux Gamepad Specification](https://www.kernel.org/doc/html/latest/input/gamepad.html)
- [Molten Gamepad](https://github.com/jgeumlek/MoltenGamepad)
- [SC Controller](https://github.com/kozec/sc-controller)
- [Xboxdrv](https://github.com/xboxdrv/xboxdrv)
- [Joystick input driver for the Xorg X server](https://gitlab.freedesktop.org/xorg/driver/xf86-input-joystick)
