Renegade Arcade
===============


Principles
----------

Good games should be [portable](portable.html), [accessible](accessible.html), and [reproducible](reproducible.html).


Controls
--------

It is necessary to develop a [loosely-unified theory](controller.html) of how video games should be controlled depending on the complexity of the game and available input devices.


Historical
----------

Many games have an interesting [historical](historical.html) context with obscure original titles that spawned many imitators.


Aesthetics
----------

Keyboard inputs: <span class="keyboard-key">A</span>, <span class="keyboard-key">B</span>, <button class="keyboard-key">C</button>, <span class="keyboard-key">Esc</span>, <span class="keyboard-key">↹ Tab</span>, <span class="keyboard-key">⟵ Backspace</span>, <span class="keyboard-key">↵ Return</span>, <span class="keyboard-key">↑</span>, <span class="keyboard-key">←</span>, <span class="keyboard-key">→</span>, <span class="keyboard-key">↓</span>.

Joystick inputs: <span class="joystick-key">S</span>, <span class="joystick-key">E</span>, <span class="joystick-key">TR</span>, <span class="joystick-key">ZR</span>.

### Some Stylized Buttons

<button class="arcade red"></button>
<button class="arcade yellow"></button>
<button class="arcade blue"></button>
<button class="arcade white">&#128100;</button>
<button class="arcade white">&#128101;</button>

<span class="keyboard-key" style="writing-mode:vertical-rl;text-orientation:upright;">⌅ Enter</span>

- ▶️◀️🔼🔽
- 🔘

[Open Source](freelibre.html)
-----------------------------


External Links
--------------

- [2DEngine Archived](https://web.archive.org/web/20220426204622/https://2dengine.com/?p=tutorials)
- [Arcade Carpet Textures](https://opengameart.org/content/arcade-carpet-textures) by [Luckius](https://opengameart.org/users/luckius)
- [LibRetro](https://www.libretro.com/)
- [List of video game console launch titles](https://gamicus.fandom.com/wiki/List_of_video_game_console_launch_games)
