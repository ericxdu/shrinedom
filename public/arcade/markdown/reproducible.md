Reproducible Games
==================

Good games should be designed for reproducability. Sometimes achieving 
this is as simple as giving the user an open source license to your 
code, but it goes further than that. Game code should use 
comprehensible functions and objects to promote innovation on top of 
the original ideas. The code should also be easy to port to different 
platforms if need be.

Its even better if a game has mechanics and concepts that are easy to 
understand and reproduce. Of course, there's plenty of room for games 
to fill every esoteric niche, but think of the lasting popularity of 
timeless concepts like Asteroids and Sokoban.

- Asteroids
- Flappy Bird
- Snake
- Sokoban
- Tetris

Additional Considerations
-------------------------

- Pseudocode
- Reprodcible Builds
