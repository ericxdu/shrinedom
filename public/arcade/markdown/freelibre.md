Free/Libre and Open-Source Games
================================

Also known as "open-source", libre games are legally allowed to be shared among players and developers. Users are provided with a copy of the source code used to build the game, giving everyone the opportunity to study and change the code and build a new and improved version. Contrary to the word "free", users are even allowed to publish and/or sell the game for their own benefit or that of their organization.


Master List
-----------

* [Anarch](https://drummyfish.gitlab.io/anarch/)
* Barbie the Seahorse
* Battle for Wesnoth
* Chocolate Freedoom
* Cupid
* Freedroid
* HASE
* Kobo Deluxe
* Maze of Galious
* Mindustry
* Minetest
* OpenArena
* OpenTTD
* Puzzletube
* Red Eclipse
* ReTux
* RogueBox Adventures
* Ryzom
* rRootage
* Solarwolf
* Veloran
* XMage
* Xonotic
* XTux Arena
* Zero-K
