Desktop Games
=============


Input Grabbing
--------------

Desktops designed around a "window" paradigm use the concept of "focus" to determine which application to send mouse and keyboard events to. Many computer games break this paradigm by "grabbing" an input device for exclusive use. This breaks normal desktop operation.


Full Screen
-----------

Most computer games offer an immersive experience and operate in a full-screen mode, or allow the option to operate full-screen. This has created a strange environment where, when playing a game, there are essentially two full-screen applications running that the user can sometimes switch between.


Before the Desktop
------------------

Computer games invented before the prominence of desktop environments ran in a single-task mode that took over the user interface in full-screen.
