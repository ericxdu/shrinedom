This is a guide to contemporary fashion trends in Eorzea, the westernmost of The Three Great Continents of Hydaelyn. The continent of Aldenard and the neighboring island of Vylbrand make up the major landmasses in this geopolitical region.


Terms
=====

As fashion trends change over time, so does the language used to describe it. This guide uses terms extant to Eorzea in the Seventh Umbral Era, which may be unfamiliar or confusing to readers from a different culture or time period.

__Culottes:__ This term typically refers to a pair of "shorts"; that is, an article of clothing covering the body from the waist down, split to fit each leg, and extending to about knee-level. The most popular style of culottes are loose-fitting and made of leather.


La Noscea
=========

Yellowjackets:

- Spiked Labrys
- Sallet
- Shirt (variant)
- Sarouel
- Thighboots

Lower La Noscea
---------------

Located on the east coast of Vylbrand, Lower La Noscea appears to have a spectacular view of the Strait of Merlthor; the body of water connecting the Rhotano Sea to the south and Indigo Deep to the north.

Upper La Noscea
---------------

### Camp Overlook

A detachment of the Maelstrom is headquartered here to keep watch on a large community of Kobolds near the border between La Noscea and O'Ghomoro. They call themselves the Red Swallows, and their standard uniform appears to have two variants. These Maelstrom soldiers are former privateers, and remember their time upon the seas fondly while they protect La Noscea from threats on land.

- Mythril Cavalry Bow/Steel Bhuj
- Gryphonskin Bandana
- Linen Shirt/Linen Bliaud
- Sarouel
- Leather Leggings


### Cedarwood

Many of the people who live and work in this agricultural area are farmers. Their work clothes tend toward the standard Disciple of the Land outfit.

- Jerkin
- Culottes
- Moccasins

They wear a variety of work gloves, and many of them sport a bandana as headwear.

### The Gods' Grip

Storm ?????:

- Spiked Labrys
- Sallet
- Bliaud
- Sarouel
- Leggings

Dockworkers:

- Doublet
- Work Gloves
- Slops
- Thighboots


The Black Shroud
================

Gridania
--------

### Old Gridania

#### Stillglade Fane

The conjurers of Gridania wear a uniform of sorts in shades of dark blue and violet with accents in brown leather.

- Budding Rosewood Wand/Sprouting Rosewood Radical
- Felt Hat/Hempen Hat
- Woolen Robe
- Boarskin Ringbands
- Woolen Tights
- Leather Crakows


Mor Dhona
=========

Revenant's Toll
---------------

### North Silvertear

#### Saint Coinach's Find


Legendarium
===========

### Warriors of Light
