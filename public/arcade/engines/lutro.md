LibRetro Lutro
==============

Lutro is a Lua API for LibRetro based on a limited subset of the LÖVE API.

[Website](https://lutro.libretro.com/)

[API Status](https://github.com/libretro/lutro-status)
