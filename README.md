Web Shrines (Webs)
==================

Webs is a [fansite](https://en.wikipedia.org/wiki/Fansite) dedicated to a variety of subjects. The documents are provided in HTML format with source documents written in Markdown, intended for display in a web browser with support for HTML5, CSS3, and JavaScript.

This is also a learning experiment in the use of information exchange standards; primarily those of the World Wide Web Consortium, but also practices like typography and document style guides. Composing documents on various subjects provides ample opportunity to explore standard uses of HTML like [sections](https://html.spec.whatwg.org/#sections) and [text-level semantics](https://html.spec.whatwg.org/#text-level-semantics) as well as accessibility, embedded content, and cross-platform compatibility.


Installation (Markdown)
-----------------------

### Requirements

- [GNU Make](https://www.gnu.org/software/make/)
- [Perl](https://www.perl.org/)

Each subfolder in `public` contains its own Makefile which builds the HTML document from a source Markdown document in a specific way. In general, to build a document you must invoke GNU Make and specify one or more documents to build, for example `make index.html`. Perl runs the actual script which converts the document text into HTML tags.


Installation (Other)
--------------------

Future versions of the build system may experiment with other static web-page build systems like [Discount](http://www.pell.portland.or.us/~orc/Code/discount/) and [Pandoc](https://pandoc.org/) as long as the Markdown source files are not required to change.


Distribution
------------

Webs used to depend on Markdown via Debian's APT package manager. It was decided to instead include `Markdown.pl` directly, ensuring the publication scripts can behave identically instead of depending on your operating system's version of Markdown. For instance, Debian's `markdown` command invokes `Markdown.pl` with Perl as a system-wide dependency, MacOS doesn't provide a `markdown` command at all, and Fedora provides the `markdown` command via the Discount program. It can be argued that Webs, by using a standard such as Markdown, should not necessarily provide the `markdown` command in it build process (except for practicality and consistency purposes). The following are some ways the build process may be changed.

- continue providing Markdown.pl officially including source and licensing information in a `Markdown_1.0.1` subfolder
- provide an alternative in the form of the source code to the Discount version of Markdown
- provide instructions on installing Markdown by pointing to a suitable cross-platform implementation on the web
- provide instructions on setting up a Python virtual environment and installing a Markdown implementation from PyPI
- provide one or more packages in file formats such as `deb` and `.rpm`


Links
-----

- [Internet Archive](https://archive.org/)
- [HTML Semantic Elements](https://www.w3schools.com/html/html5_semantic_elements.asp)
- [What is the Web Revival?](https://thoughts.melonking.net/guides/introduction-to-the-web-revival-1-what-is-the-web-revival)
